/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

public class Materia {
    private int IDMateria;
    private String nombre;
    private int costoCreditos;
    private int totalMinutos;

    public Materia(int IDMateria,String nombre, int costoCreditos, int totalMinutos) {
        this.IDMateria = IDMateria;
        this.nombre = nombre;
        this.costoCreditos = costoCreditos;
        this.totalMinutos = totalMinutos;
    }

    public Materia() {
       
    }
    @Override
    public String toString(){
        return getIDMateria() + " : " + nombre; 
    }
    /**
     * @return the nombre
     */
    public String getNombre() {
        return nombre;
    }

    /**
     * @return the costoCreditos
     */
    public int getCostoCreditos() {
        return costoCreditos;
    }

    /**
     * @return the nivel
     */
    public int getTotalMinutos() {
        return totalMinutos;
    }

    /**
     * @return the IDMateria
     */
    public int getIDMateria() {
        return IDMateria;
    }

    /**
     * @param IDMateria the IDMateria to set
     */
    public void setIDMateria(int IDMateria) {
        this.IDMateria = IDMateria;
    }
    
}
