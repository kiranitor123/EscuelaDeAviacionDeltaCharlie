/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

/**
 *
 * @author Ritha Carolina
 */
public class TelefonoReferencia {
    private int numTelefono;
    private String localizacion;

    public TelefonoReferencia(int numTelefono, String localizacion) {
        this.numTelefono = numTelefono;
        this.localizacion = localizacion;
    }

    /**
     * @return the numTelefono
     */
    public int getNumTelefono() {
        return numTelefono;
    }

    /**
     * @return the localizacion
     */
    public String getLocalizacion() {
        return localizacion;
    }
    
    
    
}
