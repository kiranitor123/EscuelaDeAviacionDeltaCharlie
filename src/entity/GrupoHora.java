/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

/**
 *
 * @author Andres
 */
public class GrupoHora {
    private String nombreGrupo;
    private String horario;
    
    public GrupoHora()
    {
        
    }
    
    public GrupoHora(String _nombreGrupo,String _horario)
    {
        this.nombreGrupo = _nombreGrupo;
        this.horario = _horario;
    }

    public String getNombreGrupo() {
        return nombreGrupo;
    }

    public void setNombreGrupo(String nombreDocente) {
        this.nombreGrupo = nombreDocente;
    }

    public String getHorario() {
        return horario;
    }

    public void setHorario(String horario) {
        this.horario = horario;
    }   
}
