
package entity;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import javax.swing.ImageIcon;




public class Persona {
    private int id;
    private int iDMalla;
    private String nombre;
    private String apellidoPaterno;
    private String apellidoMaterno;
    private int tipoDocumento;
    private String ciRucPas;
    private int numCredicialCoorporativo;
    private int numCredencialSABSA;
    private int numFolio;
    private int numLicencia;
    private int isPiloto;
    private int isDocente;
    private int usaLentes;
    private int salario;
    private String direccion;
    private String tipoSangra;
    private String fechaNacimiento;

    private int telefono;
    private ImageIcon foto;
    private String updateAt;
    private String createAt;
    private int isFinalizado;
    private int creditoFalso;
    private String fechaVigenciaSABSA;
    private String fechaVigenciaCoorporativo;
    private String fechaVencimientoAptoMedico;
    private String fechaVencimientoProficienceCheck;
    private String nombreMalla;

    //Alumno
    /**
     * @param id
     * @param iDMalla
     * @param nombre
     * @param apellidoPaterno
     * @param apellidoMaterno
     * @param fechaVigenciaSABSA
     * @param fechaVigenciaCoorporativo
     * @param fechaVencimientoAptoMedico
     * @param fechaNacimiento
     * @param ciRucPas
     * @param numCredicialCoorporativo
     * @param numCredencialSABSA
     * @param numFolio
     * @param creditoFalso
     * @param foto

     * @param telefono
     * @param numLicencia
     * @param usaLentes
     * @param direccion
     * @param tipoSangra
     * @see Crea_un_Alumno
     */
    
    public Persona()
    {
        
    }
    
    public Persona(int id,int iDMalla, String nombre, String apellidoPaterno, String apellidoMaterno,int tipoDocumento,
            String ciRucPas, int numCredicialCoorporativo, int numCredencialSABSA, int numFolio, 
            int numLicencia, int usaLentes, String direccion, String tipoSangra,
                 String fechaNacimiento,int telefono, ImageIcon foto,int creditoFalso, 
                    String fechaVigenciaSABSA, String fechaVigenciaCoorporativo, String fechaVencimientoAptoMedico, String nombreMalla) {
        this.id = id;
        this.iDMalla = iDMalla;
        this.nombre = nombre;
        this.apellidoPaterno = apellidoPaterno;
        this.apellidoMaterno = apellidoMaterno;
        this.ciRucPas = ciRucPas;
        this.numCredicialCoorporativo = numCredicialCoorporativo;
        this.numCredencialSABSA = numCredencialSABSA;
        this.numFolio = numFolio;
        this.numLicencia = numLicencia;
        this.isPiloto = 0;
        this.isDocente = 0;
        this.usaLentes = usaLentes;
        this.salario = -1;
        this.tipoDocumento = tipoDocumento;
        this.direccion = direccion;
        this.tipoSangra = tipoSangra;
   
        this.fechaNacimiento = fechaNacimiento;
 
        this.telefono = telefono;
        this.foto = foto;
        Calendar d = Calendar.getInstance();
        SimpleDateFormat  sd = new SimpleDateFormat("yyyy-MM-dd");
        String fechaActual = sd.format(d.getTime());
        this.updateAt = fechaActual;
        this.createAt = fechaActual;
        this.isFinalizado = 0;
        this.creditoFalso = creditoFalso;
        this.fechaVigenciaSABSA = fechaVigenciaSABSA;
        this.fechaVigenciaCoorporativo = fechaVigenciaCoorporativo;
        this.fechaVencimientoAptoMedico = fechaVencimientoAptoMedico;
        this.nombreMalla = nombreMalla;
        fechaVencimientoProficienceCheck = "";
    }
     //Docente-Piloto
    /**
     * @see  Crea_un_DocentePiloto
     */
      public Persona(int id, String nombre, String apellidoPaterno, String apellidoMaterno, int tipoDocumento,
              String ciRucPas, int numCredicialCoorporativo, int numCredencialSABSA,  
              int numLicencia, int isPiloto, int usaLentes, String direccion, 
              String tipoSangra,String fechaNacimiento,int telefono, 
              String fechaVigenciaSABSA, String fechaVigenciaCoorporativo, String fechaVencimientoAptoMedico, 
              String fechaVencimientoProficienceCheck) {
        this.id = id;
        this.iDMalla = -1;
        this.nombre = nombre;
        this.apellidoPaterno = apellidoPaterno;
        this.apellidoMaterno = apellidoMaterno;
        this.tipoDocumento = tipoDocumento;
        this.ciRucPas = ciRucPas;
        this.numCredicialCoorporativo = numCredicialCoorporativo;
        this.numCredencialSABSA = numCredencialSABSA;
        this.numFolio = -1;
        this.numLicencia = numLicencia;
        this.isPiloto = isPiloto;
        this.isDocente = 1;
        this.usaLentes = usaLentes;
        this.salario = salario;
        this.direccion = direccion;
        this.tipoSangra = tipoSangra;
        this.fechaNacimiento = fechaNacimiento;

        this.telefono = telefono;
        this.foto = null;
          Calendar d = Calendar.getInstance();
           SimpleDateFormat  sd = new SimpleDateFormat("yyyy-MM-dd");
        String fechaActual = sd.format(d.getTime());
        this.updateAt = fechaActual;
        this.createAt = fechaActual;
        this.isFinalizado = 1;
        this.creditoFalso = 0;
        this.fechaVigenciaSABSA = fechaVigenciaSABSA;
        this.fechaVigenciaCoorporativo = fechaVigenciaCoorporativo;
        this.fechaVencimientoAptoMedico = fechaVencimientoAptoMedico;
        this.fechaVencimientoProficienceCheck = fechaVencimientoProficienceCheck;
    }
      public Calendar getFechaCalendar(String fecha) {
    
        SimpleDateFormat formatoDelTexto = new SimpleDateFormat("yyyy-MM-dd");
        System.out.println(fecha+"++");
        Calendar cal = Calendar.getInstance();
        Date date = null;
        try {

        date = formatoDelTexto.parse(fecha);

        } catch (ParseException ex) {

        ex.printStackTrace();

        }
        cal.setTime(date);
          System.out.println(date.toInstant().toString()+" :Date");
          System.out.println(cal.toInstant().toString()+" :cal");
          
        return cal;
        
    }
    
    public void mostrarPersona()
    {
        System.out.println("ID Persona :" +id );
        System.out.println("iDMalla : "+iDMalla);
        System.out.println("nombre : "+nombre);
        System.out.println("apellidoPaterno : "+apellidoPaterno);
        System.out.println("apellidoMaterno : "+apellidoMaterno);
        System.out.println("ciRucPas : "+ciRucPas);
        System.out.println("numCredicialCoorporativo : "+numCredicialCoorporativo);
        System.out.println("numCredencialSABSA : "+numCredencialSABSA);
        System.out.println("numLicencia : "+numLicencia);
        System.out.println("numFolio : "+numFolio);
        System.out.println("isPiloto : "+isPiloto);
        System.out.println("isDocente : "+isDocente);
        System.out.println("usaLentes : "+usaLentes);
        System.out.println("salario : "+salario);
        System.out.println("fechaNacimiento : "+fechaNacimiento);

        System.out.println("telefono : "+telefono);
        System.out.println("foto : "+foto);
        System.out.println("updateAt : "+updateAt);
        System.out.println("createAt : "+createAt);
        System.out.println("isFinalizado: "+isFinalizado);
        System.out.println("creditoFalso : "+creditoFalso);
        System.out.println("fechaVigenciaSABSA : "+fechaVigenciaSABSA);
        System.out.println("fechaVigenciaCoorporativo : "+fechaVigenciaCoorporativo);
        System.out.println("fechaVencimientoAptoMedico : "+fechaVencimientoAptoMedico);
        System.out.println("fechaVencimientoProficienceCheck : "+getFechaVencimientoProficienceCheck());
    }

    /**
     * @return the iD
     */
    public int getiD() {
        return id;
    }
      /**
     * @return the iDMalla
     */
    public int getiDMalla() {
        return iDMalla;
    }

    /**
     * @return the nombre
     */
    public String getNombre() {
        return nombre;
    }

    /**
     * @return the apellidoPaterno
     */
    public String getApellidoPaterno() {
        return apellidoPaterno;
    }

    /**
     * @return the apellidoMaterno
     */
    public String getApellidoMaterno() {
        return apellidoMaterno;
    }

    /**
     * @return the ciRucPas
     */
    public String getCiRucPas() {
        return ciRucPas;
    }

    /**
     * @return the numCredicialCoorporativo
     */
    public int getNumCredicialCoorporativo() {
        return numCredicialCoorporativo;
    }

    /**
     * @return the numCredencialSABSA
     */
    public int getNumCredencialSABSA() {
        return numCredencialSABSA;
    }

    /**
     * @return the numFolio
     */
    public int getNumFolio() {
        return numFolio;
    }

    /**
     * @return the numLicencia
     */
    public int getNumLicencia() {
        return numLicencia;
    }

    /**
     * @return the isPiloto
     */
    public int getIsPiloto() {
        return isPiloto;
    }

    /**
     * @return the isDocente
     */
    public int getIsDocente() {
        return isDocente;
    }

    /**
     * @return the usaLentes
     */
    public int getUsaLentes() {
        return usaLentes;
    }

    /**
     * @return the salario
     */
    public int getSalario() {
        return salario;
    }

    /**
     * @return the direccion
     */
    public String getDireccion() {
        return direccion;
    }

    /**
     * @return the tipoSangra
     */
    public String getTipoSangra() {
        return tipoSangra;
    }
    /**
     * @return the fechaNacimiento
     */
    public String getFechaNacimiento() {
        return fechaNacimiento;
    }



    /**
     * @return the telefono
     */
    public int getTelefono() {
        return telefono;
    }

    /**
     * @return the foto
     */
    public ImageIcon getFoto() {
        return foto;
    }

    /**
     * @return the updateAt
     */
    public String getUpdateAt() {
        return updateAt;
    }

    /**
     * @return the createAt
     */
    public String getCreateAt() {
        return createAt;
    }

    /**
     * @return the isFinalizado
     */
    public int getIsFinalizado() {
        return isFinalizado;
    }

    /**
     * @return the creditoFalso
     */
    public int getCreditoFalso() {
        return creditoFalso;
    }

    /**
     * @return the fechaVigenciaSABSA
     */
    public String getFechaVigenciaSABSA() {
        return fechaVigenciaSABSA;
    }

    /**
     * @return the fechaVigenciaCoorporativo
     */
    public String getFechaVigenciaCoorporativo() {
        return fechaVigenciaCoorporativo;
    }

    /**
     * @return the fechaVencimientoAptoMedico
     */
    public String getFechaVencimientoAptoMedico() {
        return fechaVencimientoAptoMedico;
    }

    /**
     * @return the fechaVencimientoProficienceCheck
     */
    public String getFechaVencimientoProficienceCheck() {
        return fechaVencimientoProficienceCheck;
    }
    
    public String getNombreMalla() {
        return nombreMalla;
    }
    
    @Override
    public String toString()
    {
        String piloto;
        if(isPiloto==1)
        {
            piloto = "Docente-Piloto";
        }
        else{
            piloto = "Docente";
        }
        return nombre+" "+apellidoPaterno+" "+apellidoMaterno+":"+piloto;
    }

    /**
     * @return the tipoDocumento
     */
    public int getTipoDocumento() {
        return tipoDocumento;
    }

    /**
     * @param tipoDocumento the tipoDocumento to set
     */
    public void setTipoDocumento(int tipoDocumento) {
        this.tipoDocumento = tipoDocumento;
    }
    
}
