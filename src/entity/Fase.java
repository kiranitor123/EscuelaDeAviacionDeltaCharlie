/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;


public class Fase {
    private int IDFase;
    private String descrip;

    public Fase(int IDFase, String descrip) {
        this.IDFase = IDFase;
        this.descrip = descrip;
    }

    /**
     * @return the IDFase
     */
    public int getIDFase() {
        return IDFase;
    }

    /**
     * @return the descrip
     */
    public String getDescrip() {
        return descrip;
    }
    public String toString()
    {
        return IDFase+":"+descrip;
    }
    
    
}
