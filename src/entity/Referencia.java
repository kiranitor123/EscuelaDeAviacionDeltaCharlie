/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import java.util.LinkedList;

/**
 *
 * @author Ritha Carolina
 */
public class Referencia {
    private int idReferencia;
    private String nombre;
    private LinkedList<TelefonoReferencia> telefono;
    private String parentesco;

    public Referencia(int idReferencia, String nombre, LinkedList<TelefonoReferencia> telefono) {
        this.idReferencia = idReferencia;
        this.nombre = nombre;
        this.telefono = telefono;
        parentesco = "";
    }
    public Referencia(String nombre, LinkedList<TelefonoReferencia> telefono, String parentesco)
    {
        this.nombre = nombre;
        this.telefono = telefono;
        this.parentesco = parentesco;
        idReferencia = -1;
    }

    /**
     * @return the nombre
     */
    public String getNombre() {
        return nombre;
    }

    /**
     * @return the telefono
     */
    public LinkedList<TelefonoReferencia> getTelefono() {
        return telefono;
    }

    /**
     * @return the idReferencia
     */
    public int getIdReferencia() {
        return idReferencia;
    }

    /**
     * @return the parentesco
     */
    public String getParentesco() {
        return parentesco;
    }

    /**
     * @param parentesco the parentesco to set
     */
    public void setParentesco(String parentesco) {
        this.parentesco = parentesco;
    }

    /**
     * @param idReferencia the idReferencia to set
     */
    public void setIdReferencia(int idReferencia) {
        this.idReferencia = idReferencia;
    }
    
    
    
    
}
