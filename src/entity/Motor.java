/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

/**
 *
 * @author Rithita Bebita
 */
public class Motor { 
    private int IDMotor;
    private int IDHelice;
    private String marca;
    private String modelo;
    private String numeroDeSerie;
    private int TT;
    private int TTO;
    private int TBO;

    public Motor(int IDMotor, int IDHelice, String marca, String modelo, String numeroDeSerie, int TT, int TTO, int TBO) {
        this.IDMotor = IDMotor;
        this.IDHelice = IDHelice;
        this.marca = marca;
        this.modelo = modelo;
        this.numeroDeSerie = numeroDeSerie;
        this.TT = TT;
        this.TTO = TTO;
        this.TBO = TBO;
    }

    /**
     * @return the IDMotor
     */
    public int getIDMotor() {
        return IDMotor;
    }

    /**
     * @return the IDHelice
     */
    public int getIDHelice() {
        return IDHelice;
    }

    /**
     * @return the marca
     */
    public String getMarca() {
        return marca;
    }

    /**
     * @return the modelo
     */
    public String getModelo() {
        return modelo;
    }

    /**
     * @return the numeroDeSerie
     */
    public String getNumeroDeSerie() {
        return numeroDeSerie;
    }

    /**
     * @return the TT
     */
    public int getTT() {
        return TT;
    }

    /**
     * @return the TTO
     */
    public int getTTO() {
        return TTO;
    }

    /**
     * @return the TBO
     */
    public int getTBO() {
        return TBO;
    }
     @Override
    public String toString()
    {
        return marca+"-"+modelo+"-"+numeroDeSerie;
    }
    
}
