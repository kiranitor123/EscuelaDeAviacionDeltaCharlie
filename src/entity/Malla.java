/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

/**
 *
 * @author Jesus
 */
public class Malla {
    private String nombreMalla;
    private int RequiereLicencia;
    private int costo;
    private int HorasVuelo;
    private int HorasSimulador;
    private int isBorrado;
    
    public Malla(String nombreMalla, int RequiereLicencia, int costo,int HorasVuelo,int HorasSimulador,int isBorrado)
    {
        this.nombreMalla=nombreMalla;
        this.RequiereLicencia=RequiereLicencia;
        this.costo=costo;
        this.HorasVuelo=HorasVuelo;
        this.HorasSimulador=HorasSimulador;
        this.isBorrado=isBorrado;
    }

    public Malla() {
    }


    /**
     * @return the nombreMalla
     */
    public String getNombreMalla() {
        return nombreMalla;
    }

    /**
     * @param nombreMalla the nombreMalla to set
     */
    public void setNombreMalla(String nombreMalla) {
        this.nombreMalla = nombreMalla;
    }

    /**
     * @return the RequiereLicencia
     */
    public int getRequiereLicencia() {
        return RequiereLicencia;
    }

    /**
     * @param RequiereLicencia the RequiereLicencia to set
     */
    public void setRequiereLicencia(int RequiereLicencia) {
        this.RequiereLicencia = RequiereLicencia;
    }

    /**
     * @return the costo
     */
    public int getCosto() {
        return costo;
    }

    /**
     * @param costo the costo to set
     */
    public void setCosto(int costo) {
        this.costo = costo;
    }

    /**
     * @return the isBorrado
     */
    public int getIsBorrado() {
        return isBorrado;
    }

    /**
     * @param isBorrado the isBorrado to set
     */
    public void setIsBorrado(int isBorrado) {
        this.isBorrado = isBorrado;
    }

    /**
     * @return the HorasVuelo
     */
    public int getHorasVuelo() {
        return HorasVuelo;
    }

    /**
     * @param HorasVuelo the HorasVuelo to set
     */
    public void setHorasVuelo(int HorasVuelo) {
        this.HorasVuelo = HorasVuelo;
    }

    /**
     * @return the HorasSimulador
     */
    public int getHorasSimulador() {
        return HorasSimulador;
    }

    /**
     * @param HorasSimulador the HorasSimulador to set
     */
    public void setHorasSimulador(int HorasSimulador) {
        this.HorasSimulador = HorasSimulador;
    }
}
