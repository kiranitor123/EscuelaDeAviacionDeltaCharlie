/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

/**
 *
 * @author Andres
 */
public class MateriasATomar {
    private String Materia;
    private String Grupo;
    
    public MateriasATomar()
    {
        
    }
    
    public MateriasATomar(String _Materia,String _Grupo)
    {
        this.Materia = _Materia;
        this.Grupo = _Grupo;
    }

    public String getMateria() {
        return Materia;
    }

    public void setMateria(String Materia) {
        this.Materia = Materia;
    }

    public String getGrupo() {
        return Grupo;
    }

    public void setGrupo(String Grupo) {
        this.Grupo = Grupo;
    }
}
