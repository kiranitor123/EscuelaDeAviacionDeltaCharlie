/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

/**
 *
 * @author LENOVO
 */
public class Pagos {
    private int id;
    private int iDAlumno;
    private String fechaPago;
    private String horaPago;
    private String descripcion;
    private int cantidadPagada;
    private int descuento;
    private int createdBy;
    private int tipoDePago;
    private int iDMalla;
    
    public Pagos(int id, int iDAlumno, String fechaPago, String horaPago, String descripcion, int cantidadPagada, int createdBy, int tipoDePago, int iDMalla, int descuento){
        this.id = id;
        this.iDAlumno = iDAlumno;
        this.fechaPago = fechaPago;
        this.horaPago = horaPago;
        this.descripcion = descripcion;
        this.cantidadPagada = cantidadPagada;
        this.createdBy = createdBy;
        this.tipoDePago = tipoDePago;
        this.iDMalla = iDMalla;
        this.descuento = descuento;
    }  
     /**
     * @return the id
     */
    public int getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * @return the iDAlumno
     */
    public int getiDAlumno() {
        return iDAlumno;
    }

    /**
     * @param iDAlumno the iDAlumno to set
     */
    public void setiDAlumno(int iDAlumno) {
        this.iDAlumno = iDAlumno;
    }

    /**
     * @return the fechaPago
     */
    public String getFechaPago() {
        return fechaPago;
    }

    /**
     * @param fechaPago the fechaPago to set
     */
    public void setFechaPago(String fechaPago) {
        this.fechaPago = fechaPago;
    }

    /**
     * @return the horaPago
     */
    public String getHoraPago() {
        return horaPago;
    }

    /**
     * @param horaPago the horaPago to set
     */
    public void setHoraPago(String horaPago) {
        this.horaPago = horaPago;
    }

    /**
     * @return the descripcion
     */
    public String getDescripcion() {
        return descripcion;
    }

    /**
     * @param descripcion the descripcion to set
     */
    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    /**
     * @return the cantidadPagada
     */
    public int getCantidadPagada() {
        return cantidadPagada;
    }

    /**
     * @param cantidadPagada the cantidadPagada to set
     */
    public void setCantidadPagada(int cantidadPagada) {
        this.cantidadPagada = cantidadPagada;
    }

    /**
     * @return the createdBy
     */
    public int getCreatedBy() {
        return createdBy;
    }

    /**
     * @param createdBy the createdBy to set
     */
    public void setCreatedBy(int createdBy) {
        this.createdBy = createdBy;
    }

    /**
     * @return the tipoDePago
     */
    public int getTipoDePago() {
        return tipoDePago;
    }

    /**
     * @param tipoDePago the tipoDePago to set
     */
    public void setTipoDePago(int tipoDePago) {
        this.tipoDePago = tipoDePago;
    }

    /**
     * @return the iDMalla
     */
    public int getiDMalla() {
        return iDMalla;
    }

    /**
     * @param iDMalla the iDMalla to set
     */
    public void setiDMalla(int iDMalla) {
        this.iDMalla = iDMalla;
    }
    
     /**
     * @return the descuento
     */
    public int getDescuento() {
        return descuento;
    }

    /**
     * @param descuento the descuento to set
     */
    public void setDescuento(int descuento) {
        this.descuento = descuento;
    }
}
