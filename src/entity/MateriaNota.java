/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

/*
 *
 * @author Deluxe
 */
public class MateriaNota {
    private String materia;
    private String notaMayor;
    private String notaMenor;
    private String nota;
    
    
    public MateriaNota(String materia, String notaMayor, String notaMenor, String nota)
    {
        this.materia = materia;
        this.notaMayor = notaMayor;
        this.notaMenor = notaMenor;
        this.nota = nota;
    }

    /**
     * @return the materia
     */
    public String getMateria() {
        return materia;
    }

    /**
     * @param materia the materia to set
     */
    public void setMateria(String materia) {
        this.materia = materia;
    }

    /**
     * @return the notaMayor
     */
    public String getNotaMayor() {
        return notaMayor;
    }

    /**
     * @param notaMayor the notaMayor to set
     */
    public void setNotaMayor(String notaMayor) {
        this.notaMayor = notaMayor;
    }

    /**
     * @return the notaMenor
     */
    public String getNotaMenor() {
        return notaMenor;
    }

    /**
     * @param notaMenor the notaMenor to set
     */
    public void setNotaMenor(String notaMenor) {
        this.notaMenor = notaMenor;
    }

    /**
     * @return the nota
     */
    public String isNota() {
        return nota;
    }

    /**
     * @param nota the nota to set
     */
    public void setNota(String nota) {
        this.nota = nota;
    }
}
