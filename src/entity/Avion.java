/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

/**
 *
 * @author Rithita Bebita
 */
public class Avion {
    private int IDAvion;
    private String marca;
    private int minutosVuelo;
    private String modelo;
    private String numSerie;
    private String fechaVencimientoExtintor;
    private String fechaVencimientoSeguro;
    private String fechaVencimientoRegistroMatricula;
    private String fechaVencimientoAeronavegabilidad;
    private String fechaVencimientoEquipoEmergencia;
    
    public Avion(){
        
    }

    public Avion(int IDAvion,String marca,int minutosVuelo, String modelo, String numSerie, String fechaVencimientoExtintor, String fechaVencimientoSeguro, String fechaVencimientoRegistroMatricula, String fechaVencimientoAeronavegabilidad, String fechaVencimientoEquipoEmergencia) {
        this.IDAvion = IDAvion;
        this.marca = marca;
        this.minutosVuelo = minutosVuelo;
        this.modelo = modelo;
        this.numSerie = numSerie;
        this.fechaVencimientoExtintor = fechaVencimientoExtintor;
        this.fechaVencimientoSeguro = fechaVencimientoSeguro;
        this.fechaVencimientoRegistroMatricula = fechaVencimientoRegistroMatricula;
        this.fechaVencimientoAeronavegabilidad = fechaVencimientoAeronavegabilidad;
        this.fechaVencimientoEquipoEmergencia = fechaVencimientoEquipoEmergencia;
    }

    /**
     * @return the marca
     */
    public String getMarca() {
        return marca;
    }

    public int getMinutos(){
        return minutosVuelo;
    }
    /**
     * @return the modelo
     */
    public String getModelo() {
        return modelo;
    }

    /**
     * @return the numSerie
     */
    public String getNumSerie() {
        return numSerie;
    }

    /**
     * @return the fechaVencimientoExtintor
     */
    public String getFechaVencimientoExtintor() {
        return fechaVencimientoExtintor;
    }

    /**
     * @return the fechaVencimientoSeguro
     */
    public String getFechaVencimientoSeguro() {
        return fechaVencimientoSeguro;
    }

    /**
     * @return the fechaVencimientoRegistroMatricula
     */
    public String getFechaVencimientoRegistroMatricula() {
        return fechaVencimientoRegistroMatricula;
    }

    /**
     * @return the fechaVencimientoAeronavegabilidad
     */
    public String getFechaVencimientoAeronavegabilidad() {
        return fechaVencimientoAeronavegabilidad;
    }

    /**
     * @return the fechaVencimientoEquipoEmergencia
     */
    public String getFechaVencimientoEquipoEmergencia() {
        return fechaVencimientoEquipoEmergencia;
    }

    /**
     * @return the IDAvion
     */
    public int getIDAvion() {
        return IDAvion;
    }

    /**
     * @param IDAvion the IDAvion to set
     */
    public void setIDAvion(int IDAvion) {
        this.IDAvion = IDAvion;
    }
    @Override
    public String toString()
    {
        return marca+"-"+modelo+"-"+numSerie;
    }
    
    
   
    
    
    
}
