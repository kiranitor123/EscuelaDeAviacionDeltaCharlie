/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

/**
 *
 * @author Acer Aspire E1
 */
public class CRTVuelo {
    private int ID;
    private String nombre;
    private int minutos;
    public CRTVuelo(int ID, String nombre)
    {
        this.ID = ID;
        this.nombre = nombre;
        minutos = 0;
    }
    public CRTVuelo(int ID, String nombre, int minutos) {
        this.ID = ID;
        this.nombre = nombre;
        this.minutos = minutos;
    }
    @Override
    public String toString()
    {
        return ID+":"+nombre+":"+minutos;
    }
    /**
     * @return the ID
     */
    public int getID() {
        return ID;
    }

    /**
     * @return the nombre
     */
    public String getNombre() {
        return nombre;
    }

    /**
     * @return the minutos
     */
    public int getMinutos() {
        return minutos;
    }

    /**
     * @param minutos the minutos to set
     */
    public void setMinutos(int minutos) {
        this.minutos = minutos;
    }
    
    
}
