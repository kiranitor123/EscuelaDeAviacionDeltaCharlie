/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 *
 * @author Jesus
 */
public class Grupo {
    private int IDGrupo;
    private String NombreMateria;
    private String NombreGrupo;
    private String NombreDocente;
    private String FechaInicio;
    private String FechaFin;
    private String Hora;
    
    public Grupo(int IDGrupo,String NombreMateria,String NombreGrupo,String NombreDocente,String FechaInicio,String FechaFin,String Hora){
        this.IDGrupo=IDGrupo;
        this.NombreMateria=NombreMateria;
        this.NombreGrupo=NombreGrupo;
        this.NombreDocente=NombreDocente;
        this.FechaInicio=FechaInicio;
        this.FechaFin=FechaFin;
        this.Hora=Hora;
    }

    public Grupo() {
    }
    
    public Calendar getFechaCalendar(String fecha) {

        SimpleDateFormat formatoDelTexto = new SimpleDateFormat("yyyy-MM-dd");
        System.out.println(fecha + "++");
        Calendar cal = Calendar.getInstance();
        Date date = null;
        try {

            date = formatoDelTexto.parse(fecha);

        } catch (ParseException ex) {

            ex.printStackTrace();

        }
        cal.setTime(date);
        System.out.println(date.toInstant().toString() + " :Date");
        System.out.println(cal.toInstant().toString() + " :cal");

        return cal;

    }

    /**
     * @return the IDGrupo
     */
    public int getIDGrupo() {
        return IDGrupo;
    }

    /**
     * @param IDGrupo the IDGrupo to set
     */
    public void setIDGrupo(int IDGrupo) {
        this.IDGrupo = IDGrupo;
    }

    /**
     * @return the NombreMateria
     */
    public String getNombreMateria() {
        return NombreMateria;
    }

    /**
     * @param NombreMateria the NombreMateria to set
     */
    public void setNombreMateria(String NombreMateria) {
        this.NombreMateria = NombreMateria;
    }

    /**
     * @return the NombreGrupo
     */
    public String getNombreGrupo() {
        return NombreGrupo;
    }

    /**
     * @param NombreGrupo the NombreGrupo to set
     */
    public void setNombreGrupo(String NombreGrupo) {
        this.NombreGrupo = NombreGrupo;
    }

    /**
     * @return the NombreDocente
     */
    public String getNombreDocente() {
        return NombreDocente;
    }

    /**
     * @param NombreDocente the NombreDocente to set
     */
    public void setNombreDocente(String NombreDocente) {
        this.NombreDocente = NombreDocente;
    }

    /**
     * @return the FechaInicio
     */
    public String getFechaInicio() {
        return FechaInicio;
    }

    /**
     * @param FechaInicio the FechaInicio to set
     */
    public void setFechaInicio(String FechaInicio) {
        this.FechaInicio = FechaInicio;
    }

    /**
     * @return the FechaFin
     */
    public String getFechaFin() {
        return FechaFin;
    }

    /**
     * @param FechaFin the FechaFin to set
     */
    public void setFechaFin(String FechaFin) {
        this.FechaFin = FechaFin;
    }

    /**
     * @return the Hora
     */
    public String getHora() {
        return Hora;
    }

    /**
     * @param Hora the Hora to set
     */
    public void setHora(String Hora) {
        this.Hora = Hora;
    }
}
