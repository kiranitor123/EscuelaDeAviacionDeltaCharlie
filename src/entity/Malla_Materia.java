/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

/**
 *
 * @author Jesus
 */
public class Malla_Materia {
    private int IDMalla;
    private int IDMateria;
    
    public Malla_Materia(int IDMalla,int IDMateria)
    {
        this.IDMalla=IDMalla;
        this.IDMateria=IDMateria;
    }

    /**
     * @return the IDMalla
     */
    public int getIDMalla() {
        return IDMalla;
    }

    /**
     * @param IDMalla the IDMalla to set
     */
    public void setIDMalla(int IDMalla) {
        this.IDMalla = IDMalla;
    }

    /**
     * @return the IDMateria
     */
    public int getIDMateria() {
        return IDMateria;
    }

    /**
     * @param IDMateria the IDMateria to set
     */
    public void setIDMateria(int IDMateria) {
        this.IDMateria = IDMateria;
    }
}
