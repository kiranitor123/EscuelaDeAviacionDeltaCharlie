package modelo;

import controller.controlador;
import deltacharlie.FrameLogin;
import entity.*;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.Blob;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import java.sql.SQLException;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.LinkedList;
import java.util.Vector;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.ComboBoxModel;
import javax.swing.DefaultComboBoxModel;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.table.DefaultTableModel;

public class conexion {

    /*
    Inicio y cerrado de conexion
     */
    private static Connection conection = null;
    public int nivel=0;
    public int intaux = 0;


    private static Connection conectar(Connection cone) throws SQLException {
        if (cone == null) {
            try {
                conexion con = new conexion();
                Class.forName("com.mysql.jdbc.Driver");
                cone = DriverManager.getConnection("jdbc:mysql://"+FrameLogin.jTextFieldIP.getText()+"/deltachar", "root2", "");
            } catch (ClassNotFoundException ex) {
                Logger.getLogger(conexion.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return cone;
    }
    
    public static Connection getMySQLConnection()
            throws ClassNotFoundException, SQLException {
        conexion con = new conexion();
        String hostName = "";
        hostName = FrameLogin.jTextFieldIP.getText();
        String dbName = "deltachar";
        String userName = "root2";
        String password = "";
        return getMySQLConnection(hostName, dbName, userName, password);
    }

    public static Connection getMySQLConnection(String hostName, String dbName,
            String userName, String password) {
        Connection conn = null;
        try {
            String connectionURL = "jdbc:mysql://" + hostName + ":3306/" + dbName;

            conn = DriverManager.getConnection(connectionURL, userName,
                    password);

        } catch (Exception e) {
        }

        return conn;
    }
    
    public int isBorrado(int id){
        int isBorrado = 0;
        ResultSet variablex = null;
        String sql = ("SELECT \n"
                + "  isDeleted\n"
                + "  FROM usuarios\n"
                + "  WHERE usuarios.IDUser = " + id);
        Connection con;
        try {
            con = conexion.conectar(conection);
            Statement st = con.createStatement();
            variablex = st.executeQuery(sql);
            while (variablex.next()) 
            {
                isBorrado = variablex.getInt(1);
            }
        } catch (SQLException ex) {
            Logger.getLogger(conexion.class.getName()).log(Level.SEVERE, null, ex);
        }
        return isBorrado;
    }
    
    public void anadirMalla(Malla v,String HV,String HS)
    {
        try {
        String sql = "INSERT INTO Malla(nombreMalla,RequiereLicencia,Costo,HorasRequeridasVuelo,HorasRequeridasSimulador,isBorrado)\n"
                + "VALUES ( ?, ?, ?, ?, ?, ?)";
                Connection con = conexion.conectar(conection);
                PreparedStatement pstm = con.prepareStatement(sql);
                pstm.setString(1, v.getNombreMalla());
                pstm.setString(2, Integer.toString(v.getRequiereLicencia()));
                pstm.setString(3, Integer.toString(v.getCosto()));
                pstm.setString(4, HV);
                pstm.setString(5, HS);
                pstm.setString(6, Integer.toString(v.getIsBorrado()));
                pstm.executeUpdate();
                pstm.close();
                
                con.close();
        } catch (SQLException ex) {
            System.err.println(ex);
        }
    }
    
    public void ModificarMalla(Malla v,int idmalla,String HV,String HS)
    {
        try {
        String sql = "UPDATE Malla\n"
                + "SET nombreMalla = ?, RequiereLicencia = ?, Costo = ?,HorasRequeridasVuelo = ?,HorasRequeridasSimulador = ?, isBorrado = ?\n"
                + "WHERE IDMalla = "
                + idmalla;
                Connection con = conexion.conectar(conection);
                PreparedStatement pstm = con.prepareStatement(sql);
                pstm.setString(1, v.getNombreMalla());
                pstm.setString(2, Integer.toString(v.getRequiereLicencia()));
                pstm.setString(3, Integer.toString(v.getCosto()));
                pstm.setString(4, HV);
                pstm.setString(5, HS);
                pstm.setString(6, Integer.toString(v.getIsBorrado()));
                pstm.executeUpdate();
                pstm.close();
                
                con.close();
        } catch (SQLException ex) {
            System.err.println(ex);
        }
    }
    
    public void anadirMalla_Materia(Malla_Materia v)
    {
        try {
        String sql = "INSERT INTO Malla_Materia(IDMalla,IDMateria)\n"
                + "VALUES ( ?, ?)";
                Connection con = conexion.conectar(conection);
                PreparedStatement pstm = con.prepareStatement(sql);
                pstm.setString(1, Integer.toString(v.getIDMalla()));
                pstm.setString(2, Integer.toString(v.getIDMateria()));
                pstm.executeUpdate();
                pstm.close();
                
                con.close();
        } catch (SQLException ex) {
            System.err.println(ex);
        }
    }
    
    public int ConseguirHorasUsadas(int IDPersona) throws SQLException {
        int horasUsadas = 0;
        String sql = "SELECT HorasDeVuelo\n"
                + "FROM vuelosregistrados,alumno_docente_piloto\n"
                + "WHERE alumno_docente_piloto.isPiloto = 0 AND alumno_docente_piloto.isBorrado = 0 AND alumno_docente_piloto.IDPersona = "+IDPersona+" AND alumno_docente_piloto.IDPersona = vuelosregistrados.IDEstudiante AND vuelosregistrados.TipoDeVuelo = 'Vuelo acompañado'";
        Connection con = conexion.conectar(conection);
        Statement st = con.createStatement();
        ResultSet rs = st.executeQuery(sql);
        while (rs.next()) {
            horasUsadas = horasUsadas + rs.getInt(1);
        }
        close(con);
        return horasUsadas;
    }
    
    public int ConseguirHorasUsadasSimulacion(int IDPersona) throws SQLException {
        int horasUsadas = 0;
        String sql = "SELECT HorasDeVuelo\n"
                + "FROM vuelosregistrados,alumno_docente_piloto\n"
                + "WHERE alumno_docente_piloto.isPiloto = 0 AND alumno_docente_piloto.IDPersona = "+IDPersona+" AND alumno_docente_piloto.IDPersona = vuelosregistrados.IDEstudiante AND vuelosregistrados.TipoDeVuelo = 'Simulacion'";
        Connection con = conexion.conectar(conection);
        Statement st = con.createStatement();
        ResultSet rs = st.executeQuery(sql);
        while (rs.next()) {
            horasUsadas = horasUsadas + rs.getInt(1);
        }
        close(con);
        return horasUsadas;
    }
    
    public int ConseguirHorasPagadas(int IDPersona) throws SQLException {
        int horaspagadas = 0;
        String sql = "SELECT CantidadPagada\n"
                + "FROM pago,alumno_docente_piloto\n"
                +"WHERE alumno_docente_piloto.isPiloto = 0 AND alumno_docente_piloto.isBorrado = 0 AND alumno_docente_piloto.IDPersona = "+IDPersona+" AND alumno_docente_piloto.IDPersona = pago.IDAlumno AND pago.Descripcion = 4";
        Connection con = conexion.conectar(conection);
        Statement st = con.createStatement();
        ResultSet rs = st.executeQuery(sql);
        while (rs.next()) {
            horaspagadas = horaspagadas + rs.getInt(1);
        }
        close(con);
        return horaspagadas/100;
    }
    
    public int ConseguirHorasVuelo(int IDPersona,int IDMalla) throws SQLException{
        int horas=0;
        String sql = "SELECT HorasRequeridasVuelo\n"
                + "FROM malla,alumno_docente_piloto\n"
                + "WHERE alumno_docente_piloto.isPiloto = 0 AND alumno_docente_piloto.isBorrado = 0 AND alumno_docente_piloto.IDPersona ="+IDPersona+" AND alumno_docente_piloto.IDMalla = malla.IDMalla AND malla.IDMalla = "+IDMalla;
        Connection con = conexion.conectar(conection);
        Statement st = con.createStatement();
        ResultSet rs = st.executeQuery(sql);
        while (rs.next()) {
            horas = rs.getInt(1);
        }
        close(con);   
        return horas;
    }
    
    public int ConseguirHorasSimulador(int IDPersona,int IDMalla) throws SQLException{
        int horas=0;
        String sql = "SELECT HorasRequeridasSimulador\n"
                + "FROM malla,alumno_docente_piloto\n"
                + "WHERE alumno_docente_piloto.isPiloto = 0 AND alumno_docente_piloto.isBorrado = 0 AND alumno_docente_piloto.IDPersona ="+IDPersona+" AND alumno_docente_piloto.IDMalla = malla.IDMalla AND malla.IDMalla = "+IDMalla;
        Connection con = conexion.conectar(conection);
        Statement st = con.createStatement();
        ResultSet rs = st.executeQuery(sql);
        while (rs.next()) {
            horas = rs.getInt(1);
        }
        close(con);   
        return horas;
    }
    
    public void EliminarMalla_Materia(int idmalla) {
        try {
            String sql = "DELETE FROM Malla_Materia\n"
                    + "WHERE IDMalla = "
                    + idmalla;
            Connection con = conexion.conectar(conection);
            PreparedStatement pstm = con.prepareStatement(sql);
            pstm.executeUpdate();
            pstm.close();

            con.close();
        } catch (SQLException ex) {
            System.err.println(ex);
        }
    }
    
    public void EliminarMalla(int idmalla) {
        try {
            String sql = "UPDATE malla\n"
                    + "SET isBorrado = ?\n"
                    + "WHERE IDMalla = "
                    + idmalla;
            Connection con = conexion.conectar(conection);
            PreparedStatement pstm = con.prepareStatement(sql);
            pstm.setInt(1, 1);
            pstm.executeUpdate();
            close(con);
        } catch (SQLException ex) {
            System.err.println(ex);
        }
    }
    
    public int sacaridmalla(String nombremalla) throws SQLException
    {
        int auxIDMalla = 0;
        String sql = "SELECT\n"
                + "IDMalla\n"
                + "FROM malla\n"
                + "WHERE nombreMalla = '" + nombremalla + "'";
        Connection con = conexion.conectar(conection);
        Statement st = con.createStatement();
        ResultSet rs = st.executeQuery(sql);
        while (rs.next()) {
            auxIDMalla=Integer.parseInt(rs.getString(1));
        }
        return auxIDMalla;
    }

    public DefaultComboBoxModel ObtenerLista(String cadena, LinkedList<Materia> materiasBuscadas) throws SQLException {
        materiasBuscadas.clear();
        DefaultComboBoxModel modelo = new DefaultComboBoxModel();
        String sql = "SELECT \n"
                + "         IDMateria\n"
                + "         ,Materia\n"
                + "         ,CostoCreditos\n"
                + "         ,TotalMinutos\n"
                + "         FROM materia\n"
                + "         Where\n"
                + "         Materia\n"
                + "         like '%" + cadena + "%' AND isBorrado = 0";
        Connection con = conexion.conectar(conection);
        Statement st = con.createStatement();
        ResultSet rs = st.executeQuery(sql);
        modelo.removeAllElements();
        while (rs.next()) {
            int IdMateria = rs.getInt(1);
            modelo.addElement(rs.getString(2));
            String nombre = rs.getString(2);
            int costoCreditos = rs.getInt(3);
            int totalMinutos = rs.getInt(4);
            Materia aux = new Materia(IdMateria,nombre, costoCreditos, totalMinutos);
            materiasBuscadas.addLast(aux);
        }
        close(con);
        return modelo;
    }
    
    public DefaultComboBoxModel ObtenerListaPilotos(String cadena, LinkedList<String> pilotosBuscados) throws SQLException {
        pilotosBuscados.clear();
        DefaultComboBoxModel modelo = new DefaultComboBoxModel();
        String sql = "SELECT\n"
                + "CONCAT(Nombre,\" \",ApellidoPaterno,\" \",ApellidoMaterno)\n"
                + "FROM alumno_docente_piloto\n"
                + "Where\n"
                + "CONCAT(Nombre,\" \",ApellidoPaterno,\" \",ApellidoMaterno) like '%"+ cadena +"%' AND isBorrado = 0 AND isPiloto = 1";
        Connection con = conexion.conectar(conection);
        Statement st = con.createStatement();
        ResultSet rs = st.executeQuery(sql);
        modelo.removeAllElements();
        while (rs.next()) {
            String NombreCompleto = rs.getString(1);
            pilotosBuscados.addLast(NombreCompleto);
            modelo.addElement(NombreCompleto);
        }
        close(con);
        return modelo;
    }
    
    public void compareDateVuelos(LinkedList<String> AlumnosIns,Calendar date1,Calendar datel2, Date aux,Date aux2, DefaultComboBoxModel modelo,String Nombre) {
        Date date = new Date();
        Date date2 = new Date();
        date1.setTime(aux);
        datel2.setTime(aux2);
        date1.add(Calendar.WEEK_OF_YEAR, -3);
        datel2.add(Calendar.WEEK_OF_YEAR, -3);
        if (date.after(date1.getTime()) || date2.after(datel2.getTime())) {
            System.out.println("ERROR DEBE RENOVAR SUS CREDENCIALES");
        }
        else{
            AlumnosIns.add(Nombre);
            modelo.addElement(Nombre);
        }
    }
    
    public DefaultComboBoxModel ObtenerListaAlumnosIns(String cadena, LinkedList<String> AlumnosIns) throws SQLException {
        AlumnosIns.clear();
        Calendar weeks = Calendar.getInstance();
        DefaultComboBoxModel modelo = new DefaultComboBoxModel();
        String sql = "SELECT\n"
                + "CONCAT(Nombre,\" \",ApellidoPaterno,\" \",ApellidoMaterno),alumno_docente_piloto.FechaVencimientoAptoMedico,alumno_docente_piloto.FechaVigenciaSabsa\n"
                + "FROM alumno_docente_piloto,alumno_grupoinscrito\n"
                + "Where\n"
                + "CONCAT(Nombre,\" \",ApellidoPaterno,\" \",ApellidoMaterno) like '%"+cadena+"%' AND isBorrado = 0 AND isPiloto = 0 AND IsDocente = 0";
        Connection con = conexion.conectar(conection);
        Statement st = con.createStatement();
        ResultSet rs = st.executeQuery(sql);
        modelo.removeAllElements();
        while (rs.next()) {
            String NombreCompleto = rs.getString(1);
            Date Fecha1 = rs.getDate(2);
            Date Fecha2 = rs.getDate(3);
            compareDateVuelos(AlumnosIns,weeks, weeks, Fecha1, Fecha2, modelo, NombreCompleto);
        }
        close(con);
        return modelo;
    }
    
    public DefaultComboBoxModel ObtenerListaAlumnosInsSimulacion(String cadena, LinkedList<String> AlumnosIns) throws SQLException {
        AlumnosIns.clear();
        DefaultComboBoxModel modelo = new DefaultComboBoxModel();
        String sql = "SELECT\n"
                + "CONCAT(Nombre,\" \",ApellidoPaterno,\" \",ApellidoMaterno)\n"
                + "FROM alumno_docente_piloto\n"
                + "Where\n"
                + "CONCAT(Nombre,\" \",ApellidoPaterno,\" \",ApellidoMaterno) like '%" + cadena + "%' AND isBorrado = 0 AND isPiloto = 0 AND IsDocente = 0";
        Connection con = conexion.conectar(conection);
        Statement st = con.createStatement();
        ResultSet rs = st.executeQuery(sql);
        modelo.removeAllElements();
        while (rs.next()) {
            String NombreCompleto = rs.getString(1);
            AlumnosIns.add(NombreCompleto);
            modelo.addElement(NombreCompleto);
        }
        close(con);
        return modelo;
    }
    
    public DefaultComboBoxModel ObtenerListaAlumnosN(String cadena, LinkedList<String> AlumnosIns) throws SQLException {
        AlumnosIns.clear();
        DefaultComboBoxModel modelo = new DefaultComboBoxModel();
        String sql = "SELECT\n"
                + "CONCAT(Nombre,\" \",ApellidoPaterno,\" \",ApellidoMaterno)\n"
                + "FROM alumno_docente_piloto\n"
                + "Where\n"
                + "CONCAT(Nombre,\" \",ApellidoPaterno,\" \",ApellidoMaterno) like '%" + cadena + "%' AND isBorrado = 0 AND isPiloto = 0 AND IsDocente = 0";
        Connection con = conexion.conectar(conection);
        Statement st = con.createStatement();
        ResultSet rs = st.executeQuery(sql);
        modelo.removeAllElements();
        while (rs.next()) {
            String NombreCompleto = rs.getString(1);
            AlumnosIns.addLast(NombreCompleto);
            modelo.addElement(NombreCompleto);
        }
        close(con);
        return modelo;
    }
    
    public DefaultComboBoxModel ObtenerListaAviones(String cadena, LinkedList<String> Aviones) throws SQLException {
        Aviones.clear();
        DefaultComboBoxModel modelo = new DefaultComboBoxModel();
        String sql = "SELECT\n"
                + "CONCAT(Marca,\"-\",Modelo,\"-\",NumeroDeSerie)\n"
                + "FROM avion\n"
                + "Where\n"
                + "CONCAT(Marca,\"-\",Modelo,\"-\",NumeroDeSerie) like '%"+cadena+"%' AND isBorrado = 0";
        Connection con = conexion.conectar(conection);
        Statement st = con.createStatement();
        ResultSet rs = st.executeQuery(sql);
        modelo.removeAllElements();
        while (rs.next()) {
            String NombreCompleto = rs.getString(1);
            Aviones.addLast(NombreCompleto);
            modelo.addElement(NombreCompleto);
        }
        close(con);
        return modelo;
    }
    
    public int ObtenerMinutosAvion(String cadena) throws SQLException
    {
        int minutos=0;
        String sql = "SELECT\n"
                + "minutosVuelo\n"
                + "FROM avion\n"
                + "Where\n"
                + "CONCAT(Marca,\"-\",Modelo,\"-\",NumeroDeSerie) = '"+cadena+"' AND isBorrado = 0";
        Connection con = conexion.conectar(conection);
        Statement st = con.createStatement();
        ResultSet rs = st.executeQuery(sql);
        while (rs.next()) {
            minutos = rs.getInt(1);
        }
        close(con);
        return minutos;
    }
    
    public void IngresarReserva(String TipoVuelo,int IDPiloto,int IDEstudiante,int IDAvion,int HorasDeVuelo) throws SQLException{
        int nulo = 0;
        String sql;
        if(TipoVuelo == "Simulacion")
        {
            sql = "INSERT INTO vuelosregistrados(TipoDeVuelo,IDPiloto,IDEstudiante,IDAvion,HorasDeVuelo,isBorrado)\n"
                + "VALUES ('"+TipoVuelo+"',"+IDPiloto+","+IDEstudiante+",null,"+HorasDeVuelo+",0)";
        }
        else{
            sql = "INSERT INTO vuelosregistrados(TipoDeVuelo,IDPiloto,IDEstudiante,IDAvion,HorasDeVuelo,isBorrado)\n"
                + "VALUES ('"+TipoVuelo+"',"+IDPiloto+","+IDEstudiante+","+IDAvion+","+HorasDeVuelo+",0)";
        }
        Connection con = conexion.conectar(conection);
        PreparedStatement pstm = con.prepareStatement(sql);
        pstm.executeUpdate();
        pstm.close();
    }
    
    public DefaultComboBoxModel ObtenerListaDocente(String cadena, LinkedList<String> docentes) throws SQLException {
        docentes.clear();
        DefaultComboBoxModel modelo = new DefaultComboBoxModel();
        String sql = ("SELECT \n"
                + "CONCAT(doc.Nombre,\" \",doc.ApellidoPaterno,\" \",doc.ApellidoMaterno)\n"
                + "FROM alumno_docente_piloto doc\n"
                + "WHERE doc.IsDocente = 1 AND CONCAT(doc.Nombre,\" \",doc.ApellidoPaterno,\" \",doc.ApellidoMaterno) like '%" + cadena +"%' AND isBorrado = 0");
        /*String sql = "SELECT \n"
                + "       Materia as IDMAt , \n"
                + "	   IDMateria\n"
                + "      ,Materia\n"
                + "      ,CostoCreditos\n"
                + "      ,TotalMinutos\n"
                + "  FROM Materia\n"
                + "  Where CONCAT( \n"
                + "  IDMateria\n"
                + "	  ,Materia)\n"
                + "	   like '%" + cadena + "%' AND isBorrado = 0";*/
        Connection con = conexion.conectar(conection);
        Statement st = con.createStatement();
        ResultSet rs = st.executeQuery(sql);
        modelo.removeAllElements();
        while (rs.next()) {
            modelo.addElement(rs.getString(1));
            docentes.addLast(rs.getString(1));
        }
        close(con);
        return modelo;
    }

    public DefaultComboBoxModel ObtenerLista2(String cadena, LinkedList<Referencia> referenciasbuscadas) throws SQLException {
        referenciasbuscadas.clear();
        DefaultComboBoxModel modelo = new DefaultComboBoxModel();
        String sql = "SELECT ref.IDReferencia, ref.NombreCompletoReferencia\n"
                + "FROM referencia_alumno_docente_piloto ref\n"
                + "WHERE CONCAT(ref.NombreCompletoReferencia,'') LIKE '%" + cadena + "%'";
        Connection con = conexion.conectar(conection);
        Statement st = con.createStatement();
        ResultSet rs = st.executeQuery(sql);
        while (rs.next()) {
            LinkedList<TelefonoReferencia> telRef = new LinkedList<>();
            int idRef = rs.getInt(1);
            String nombreRef = rs.getString(2);
            llenarListaReferencia(telRef, idRef);
            Referencia aux = new Referencia(idRef, nombreRef, telRef);
            referenciasbuscadas.addLast(aux);
            modelo.addElement(nombreRef);
        }
        con.close();
        close(con);
        return modelo;
    }

    public DefaultComboBoxModel ObtenerListaPersonas(String cadena, LinkedList<Persona> personasBuscadas, int isDocente) throws SQLException, IOException {
        if(isDocente==0)
        {
            personasBuscadas.clear();
            DefaultComboBoxModel modelo = new DefaultComboBoxModel();
            String where2 = "";

            String sql = "SELECT CONCAT(Nombre,' ',ApellidoPaterno,' ',ApellidoMaterno) AS nombrecompleto, alumno_docente_piloto.*, malla.nombreMalla\n"
                    + "FROM alumno_docente_piloto, malla\n"
                    + "WHERE CONCAT(Nombre,' ',ApellidoPaterno,' ',ApellidoMaterno) like '%" + cadena + "%' AND IsDocente= " + isDocente + " AND alumno_docente_piloto.isBorrado = 0 AND alumno_docente_piloto.IDMalla = malla.IDMalla";
            Connection con = conexion.conectar(conection);
            Statement st = con.createStatement();
            ResultSet rs = st.executeQuery(sql);
            modelo.removeAllElements();
            while (rs.next()) {
                modelo.addElement(rs.getString(1));
                int iDPersona = rs.getInt(2);
                int iDMalla = rs.getInt(3);
                String nombre = rs.getString(4);
                String apellidoPaterno = rs.getString(5);
                if (rs.wasNull()) {
                    apellidoPaterno = "";
                }
                String apellidoMaterno = rs.getString(6);
                if (rs.wasNull()) {
                    apellidoMaterno = "";
                }
                int tipoDocumento = rs.getInt(7);
                String ciRucPas = rs.getString(8);
                int nroCredencialCorporativo = rs.getInt(9);
                if (rs.wasNull()) {
                    nroCredencialCorporativo = 0;
                }
                int nroCredencialSabsa = rs.getInt(10);
                if (rs.wasNull()) {
                    nroCredencialSabsa = 0;
                }
                int numFolio = rs.getInt(11);
                if (rs.wasNull()) {
                    numFolio = 0;
                }
                int numLicencia = rs.getInt(12);
                if (rs.wasNull()) {
                    numLicencia = 0;
                }
                int isPiloto = rs.getInt(13);

                int usaLentes = rs.getInt(15);

                String direccion = rs.getString(16);
                String tipoDeSangre = rs.getString(17);
                String fechaDeNacimiento = rs.getString(18);
                int celular = rs.getInt(19);//idReferencia

                ImageIcon image;
                Blob blob = rs.getBlob(20);
                if (rs.getBlob(20) != null)
                {
                    int blobLength = (int) blob.length();  
                    byte[] img = blob.getBytes(1, blobLength);
                    image = new ImageIcon(img);
                }
                else{
                    image = null;
                }





                int creditoFalso = rs.getInt(24);

                String fechaVigenciaSabsa = rs.getString(25);
                if (rs.wasNull()) {
                    fechaVigenciaSabsa = "";
                }
                String fechaVigenciaCorporativo = rs.getString(26);
                if (rs.wasNull()) {
                    fechaVigenciaCorporativo = "";
                }
                String fechaVencimientoAptoMedico = rs.getString(27);
                if (rs.wasNull()) {
                    fechaVencimientoAptoMedico = "";
                }
                String fechaVencimientoProficciencyCheck = rs.getString(28);
                if (rs.wasNull()) {
                    fechaVencimientoProficciencyCheck = "";
                }
                String nombreMalla = rs.getString(30);
                if (rs.wasNull()) {
                    nombreMalla = "";
                }
                Persona aux;
                if (isDocente == 1) {
                    aux = new Persona(iDPersona, nombre, apellidoPaterno, apellidoMaterno, tipoDocumento, ciRucPas, nroCredencialCorporativo, nroCredencialSabsa, numLicencia, isPiloto, usaLentes, direccion, tipoDeSangre, fechaDeNacimiento, celular, fechaVigenciaSabsa, fechaVigenciaCorporativo, fechaVencimientoAptoMedico, fechaVencimientoProficciencyCheck);
                } else {
                    aux = new Persona(iDPersona, iDMalla, nombre, apellidoPaterno, apellidoMaterno, tipoDocumento,
                            ciRucPas, nroCredencialCorporativo, nroCredencialSabsa, numFolio, numLicencia,
                            usaLentes, direccion, tipoDeSangre, fechaDeNacimiento, celular, image,
                            creditoFalso, fechaVigenciaSabsa, fechaVigenciaCorporativo, fechaVencimientoAptoMedico, nombreMalla);
                }
                personasBuscadas.addLast(aux);
            }
            close(con);
            return modelo;
        }
        else{
            personasBuscadas.clear();
            DefaultComboBoxModel modelo = new DefaultComboBoxModel();
            String where2 = "";

            String sql = "SELECT CONCAT(Nombre,' ',ApellidoPaterno,' ',ApellidoMaterno) AS nombrecompleto, alumno_docente_piloto.*\n" +
"                FROM alumno_docente_piloto\n" +
"                WHERE CONCAT(Nombre,' ',ApellidoPaterno,' ',ApellidoMaterno) like '%"+cadena+"%' AND IsDocente = "+isDocente+" AND alumno_docente_piloto.isBorrado = 0 ";
            Connection con = conexion.conectar(conection);
            Statement st = con.createStatement();
            ResultSet rs = st.executeQuery(sql);
            modelo.removeAllElements();
            while (rs.next()) {
                modelo.addElement(rs.getString(1));
                int iDPersona = rs.getInt(2);
                int iDMalla = rs.getInt(3);
                String nombre = rs.getString(4);
                String apellidoPaterno = rs.getString(5);
                if (rs.wasNull()) {
                    apellidoPaterno = "";
                }
                String apellidoMaterno = rs.getString(6);
                if (rs.wasNull()) {
                    apellidoMaterno = "";
                }
                int tipoDocumento = rs.getInt(7);
                String ciRucPas = rs.getString(8);
                int nroCredencialCorporativo = rs.getInt(9);
                if (rs.wasNull()) {
                    nroCredencialCorporativo = 0;
                }
                int nroCredencialSabsa = rs.getInt(10);
                if (rs.wasNull()) {
                    nroCredencialSabsa = 0;
                }
                int numFolio = rs.getInt(11);
                if (rs.wasNull()) {
                    numFolio = 0;
                }
                int numLicencia = rs.getInt(12);
                if (rs.wasNull()) {
                    numLicencia = 0;
                }
                int isPiloto = rs.getInt(13);

                int usaLentes = rs.getInt(15);

                String direccion = rs.getString(16);
                String tipoDeSangre = rs.getString(17);
                String fechaDeNacimiento = rs.getString(18);
                int celular = rs.getInt(19);//idReferencia

                ImageIcon image;
                Blob blob = rs.getBlob(20);
                if (rs.getBlob(20) != null)
                {
                    int blobLength = (int) blob.length();  
                    byte[] img = blob.getBytes(1, blobLength);
                    image = new ImageIcon(img);
                }
                else{
                    image = null;
                }





                int creditoFalso = rs.getInt(24);

                String fechaVigenciaSabsa = rs.getString(25);
                if (rs.wasNull()) {
                    fechaVigenciaSabsa = "";
                }
                String fechaVigenciaCorporativo = rs.getString(26);
                if (rs.wasNull()) {
                    fechaVigenciaCorporativo = "";
                }
                String fechaVencimientoAptoMedico = rs.getString(27);
                if (rs.wasNull()) {
                    fechaVencimientoAptoMedico = "";
                }
                String fechaVencimientoProficciencyCheck = rs.getString(28);
                if (rs.wasNull()) {
                    fechaVencimientoProficciencyCheck = "";
                }
                Persona aux = null;
                if (isDocente == 1) {
                    aux = new Persona(iDPersona, nombre, apellidoPaterno, apellidoMaterno, tipoDocumento, ciRucPas, nroCredencialCorporativo, nroCredencialSabsa, numLicencia, isPiloto, usaLentes, direccion, tipoDeSangre, fechaDeNacimiento, celular, fechaVigenciaSabsa, fechaVigenciaCorporativo, fechaVencimientoAptoMedico, fechaVencimientoProficciencyCheck);
                }
                personasBuscadas.addLast(aux);
            }
            close(con);
            return modelo;
        }
    }

    public DefaultComboBoxModel ObtenerListaMaterias(LinkedList<Materia> materias) throws SQLException {
        DefaultComboBoxModel modelo = new DefaultComboBoxModel();
        String sql = "SELECT IDMateria\n"
                + "      ,Materia\n"
                + "      ,CostoCreditos\n"
                + "      ,TotalMinutos\n"
                + "  FROM materia\n";
        Connection con = conexion.conectar(conection);
        Statement st = con.createStatement();
        ResultSet rs = st.executeQuery(sql);
        modelo.removeAllElements();
        modelo.addElement("----------------");
        while (rs.next()) {

            int IdMateria = rs.getInt(1);
            String nombre = rs.getString(2);
            int costoCreditos = rs.getInt(3);
            int totalMinutos = rs.getInt(4);
            modelo.addElement(nombre);
            Materia aux = new Materia(IdMateria,nombre, costoCreditos, totalMinutos);
            materias.addLast(aux);
        }

        close(con);
        return modelo;
    }

    public DefaultComboBoxModel ObtenerListaUsuarios(String cadena, LinkedList<Usuario> usuariosBuscados) throws SQLException {
        usuariosBuscados.clear();
        DefaultComboBoxModel modelo = new DefaultComboBoxModel();
        String sql = "SELECT CONCAT(usuarios.NombreOwner,'') AS nomUs, usuarios.*\n"
                + "FROM usuarios\n"
                + "WHERE CONCAT(usuarios.NombreOwner,'') like '%" + cadena + "%' AND isDeleted = 0";
        Connection con = conexion.conectar(conection);
        Statement st = con.createStatement();
        ResultSet rs = st.executeQuery(sql);
        modelo.removeAllElements();
        while (rs.next()) {
            String usuarios = rs.getString(1);

            int idUser = rs.getInt(2);
            String userName = rs.getString(3);
            String nombreUsuario = rs.getString(5);
            int rol = rs.getInt(6);
            if (deltacharlie.DeltaCharlie.IDUser != 1) {
                if (rol == 1) {
                    continue;
                }
            }

            Usuario aux = new Usuario(idUser, nombreUsuario, userName, "", rol);
            usuariosBuscados.add(aux);
            modelo.addElement(usuarios);
        }
        close(con);
        return modelo;
    }

    public int buscarID(String nombre, String apellidoPaterno, String apellidoMaterno) {
        try {
            int id = 0;

            String query = "SELECT IDPersona\n"
                    + "FROM alumno_docente_piloto\n"
                    + "WHERE Nombre='" + nombre + "' AND ApellidoPaterno='" + apellidoPaterno + "' AND ApellidoMaterno='" + apellidoMaterno + "'";
            Statement st = conection.createStatement();
            ResultSet rs = st.executeQuery(query);
            while (rs.next()) {
                id = rs.getInt(1);
            }
            return id;

        } catch (Exception ex) {
            return 0;
        }
    }

    public void close(Connection con) throws SQLException {
        con.close();

    }

    public boolean existeReferencia(String referencia) throws SQLException {
        String sql = "SELECT NombreCompletoReferencia\n" +
"FROM referencia_alumno_docente_piloto ,alumno_docente_piloto_referencia\n" +
"WHERE referencia_alumno_docente_piloto.NombreCompletoReferencia LIKE '"+referencia+"'";
        Connection con = conexion.conectar(conection);
        Statement st = con.createStatement();
        ResultSet rs = st.executeQuery(sql);
        return rs.next();
    }

    /*public void getDependencias(LinkedList<Integer> depend, LinkedList<Materia> dep) throws SQLException {

        for (Materia materia : dep) {
            Connection con = conexion.conectar(conection);
            Statement st = con.createStatement();
            String query = ("SELECT IDMateriaAnt\n"
                    + "FROM dependenciasmateria\n"
                    + "WHERE IDMateria = " + materia.getIDMateria());
            ResultSet rs = st.executeQuery(query);
            while (rs.next()) {
                int id = rs.getInt(1);
                if (!isInserted(depend, id)) {
                    depend.add(id);
                }
            }
            close(con);
        }
    }*/

    public DefaultComboBoxModel getDocentesPilotos(LinkedList<Persona> docentePilotos) throws SQLException {
        docentePilotos.clear();
        String where2 = "";
        DefaultComboBoxModel modelo = new DefaultComboBoxModel();
        String sql = "SELECT CONCAT(Nombre,' ',ApellidoPaterno,' ',ApellidoMaterno) AS nombrecompleto, alumno_docente_piloto.*\n"
                + "FROM alumno_docente_piloto\n"
                + "WHERE  IsDocente=1  AND isBorrado = 0 " + where2;
        Connection con = conexion.conectar(conection);
        Statement st = con.createStatement();
        ResultSet rs = st.executeQuery(sql);
        modelo.removeAllElements();
        while (rs.next()) {
            modelo.addElement(rs.getString(1));
            int iDPersona = rs.getInt(2);
            int iDMalla = rs.getInt(3);
            String nombre = rs.getString(4);
            String apellidoPaterno = rs.getString(5);
            if (rs.wasNull()) {
                apellidoPaterno = "";
            }
            String apellidoMaterno = rs.getString(6);
            if (rs.wasNull()) {
                apellidoMaterno = "";
            }
            int tipoDocumento = rs.getInt(7);
            String ciRucPas = rs.getString(8);
            int nroCredencialCorporativo = rs.getInt(9);
            if (rs.wasNull()) {
                nroCredencialCorporativo = 0;
            }
            int nroCredencialSabsa = rs.getInt(10);
            if (rs.wasNull()) {
                nroCredencialSabsa = 0;
            }
            int numFolio = rs.getInt(11);
            if (rs.wasNull()) {
                numFolio = 0;
            }
            int numLicencia = rs.getInt(12);
            if (rs.wasNull()) {
                numLicencia = 0;
            }
            int isPiloto = rs.getInt(13);

            int usaLentes = rs.getInt(15);

            String direccion = rs.getString(16);
            String tipoDeSangre = rs.getString(17);
            String fechaDeNacimiento = rs.getString(18);
            int celular = rs.getInt(19);//idReferencia

            String foto = rs.getString(20);

            int creditoFalso = rs.getInt(23);

            String fechaVigenciaSabsa = rs.getString(24);
            if (rs.wasNull()) {
                fechaVigenciaSabsa = "";
            }
            String fechaVigenciaCorporativo = rs.getString(25);
            if (rs.wasNull()) {
                fechaVigenciaCorporativo = "";
            }
            String fechaVencimientoAptoMedico = rs.getString(26);
            if (rs.wasNull()) {
                fechaVencimientoAptoMedico = "";
            }
            String fechaVencimientoProficciencyCheck = rs.getString(27);
            if (rs.wasNull()) {
                fechaVencimientoProficciencyCheck = "";
            }
            Persona aux = new Persona(iDPersona, nombre, apellidoPaterno, apellidoMaterno, tipoDocumento, ciRucPas, nroCredencialCorporativo, nroCredencialSabsa, numLicencia, isPiloto, usaLentes, direccion, tipoDeSangre, fechaDeNacimiento, celular, fechaVigenciaSabsa, fechaVigenciaCorporativo, fechaVencimientoAptoMedico, fechaVencimientoProficciencyCheck);

            docentePilotos.addLast(aux);
            modelo.addElement(aux.toString());

        }
        return modelo;
    }

    public int getIDGenerated(String tabla, String Idtabla) throws SQLException {
        int id = 0;
        String query = ("SELECT MAX(" + Idtabla + ") as ID\n"
                + "   \n"
                + "  FROM " + tabla);
        Connection con = conexion.conectar(conection);
        Statement st = con.createStatement();
        ResultSet rs = st.executeQuery(query);
        if (rs.next()) {
            id = rs.getInt(1);

        }
        close(con);
        return id;
    }

    public int getIDUSer(String user, String pass) throws Exception {
        int id = 0;
        String query = ("SELECT\n" +
                        " IDUser\n" +
                        "FROM usuarios\n" +
                        "WHERE usuarios.isDeleted = 0 AND usuarios.UserName  = \""+user+"\"\n" +
                        "AND usuarios.UserPassword = \""+pass+"\"");
        Connection con = conexion.conectar(conection);
        Statement st = con.createStatement();
        ResultSet rs = st.executeQuery(query);
        if (rs.next()) {
            id = rs.getInt(1);
            close(con);

        } else {
            close(con);
            throw new Exception("Usuario o password Incorrecto");
        }

        return id;

    }
    
    public int getIDRol(int user) throws SQLException {
        int rol = 0;
        String query = ("SELECT\n" +
                        "IDRol\n" +
                        "FROM usuarios\n" +
                        "WHERE usuarios.IDUser  = " + user);
        Connection con = conexion.conectar(conection);
        Statement st = con.createStatement();
        ResultSet rs = st.executeQuery(query);
        if (rs.next()) {
            rol = rs.getInt(1);
            close(con);

        } else {
            close(con);
            throw new SQLException("Usuario o password Incorrecto");
        }

        return rol;

    }

    public int getIDUsuario(String usuario) {
        int nombre = -1;
        try {
            String query = "SELECT IDUser\n"
            + "FROM usuarios\n"
            + "WHERE usuarios.UserName = '" + usuario + "'";
            Connection con = conexion.conectar(conection);
            Statement st = con.createStatement();
            ResultSet rs = st.executeQuery(query);
            while (rs.next()) {
                nombre = rs.getInt(1);
            }
        } catch (SQLException ex) {
            Logger.getLogger(conexion.class.getName()).log(Level.SEVERE, null, ex);
        }

        return nombre;
    }
    
    public int getIDMalla(String malla) {
        System.out.println(malla);
        String query = "SELECT IDMalla\n"
                + "FROM malla\n"
                + "WHERE malla.nombreMalla = '" + malla + "'";
        int idMalla = -1;
        try {
            Connection con = conexion.conectar(conection);
            Statement st = con.createStatement();
            ResultSet rs = st.executeQuery(query);
            while (rs.next()) {
                idMalla = rs.getInt(1);
            }
        } catch (SQLException ex) {
            Logger.getLogger(conexion.class.getName()).log(Level.SEVERE, null, ex);
        }
        return idMalla;
    }
    
    public int getIDMotor(String motor){
        String query = "SELECT IDMotor\n"
                + "FROM motor\n"
                + "WHERE CONCAT(motor.Marca,\"-\",motor.Modelo,\"-\",motor.NumeroSerie) = '" + motor + "'";
        int nombre = -1;
        try {
            Connection con = conexion.conectar(conection);
            Statement st = con.createStatement();
            ResultSet rs = st.executeQuery(query);
            while (rs.next()) {
                nombre = rs.getInt(1);
            }
        } catch (SQLException ex) {
            Logger.getLogger(conexion.class.getName()).log(Level.SEVERE, null, ex);
        }
        return nombre;
    }
    
    public int getIDHelice(String helice){
        String query = "SELECT IDHelice\n"
                + "FROM helice\n"
                + "WHERE CONCAT(helice.Marca,\"-\",helice.Modelo,\"-\",helice.NroSerie) = '" + helice + "'";
        int nombre = -1;
        try {
            Connection con = conexion.conectar(conection);
            Statement st = con.createStatement();
            ResultSet rs = st.executeQuery(query);
            while (rs.next()) {
                nombre = rs.getInt(1);
            }
        } catch (SQLException ex) {
            Logger.getLogger(conexion.class.getName()).log(Level.SEVERE, null, ex);
        }
        return nombre;
    }
    
    public int getIDAvion(String avion){
        String query = "SELECT IDAvion\n"
                + "FROM avion\n"
                + "WHERE CONCAT(avion.Marca,\"-\",avion.Modelo,\"-\",avion.NumeroDeSerie) = '" + avion + "'";
        int nombre = -1;
        try {
            Connection con = conexion.conectar(conection);
            Statement st = con.createStatement();
            ResultSet rs = st.executeQuery(query);
            while (rs.next()) {
                nombre = rs.getInt(1);
            }
        } catch (SQLException ex) {
            Logger.getLogger(conexion.class.getName()).log(Level.SEVERE, null, ex);
        }
        return nombre;
    }
    
    public int getIDAlumno_Docente_Piloto(String alumno) {
        String query = "SELECT IDPersona\n"
                + "FROM alumno_docente_piloto\n"
                + "WHERE CONCAT(alumno_docente_piloto.Nombre,'" + " " + "', alumno_docente_piloto.ApellidoPaterno,'" + " " + "', alumno_docente_piloto.ApellidoMaterno) = '" + alumno + "'";
        int nombre = -1;
        try {
            Connection con = conexion.conectar(conection);
            Statement st = con.createStatement();
            ResultSet rs = st.executeQuery(query);
            while (rs.next()) {
                nombre = rs.getInt(1);
            }
        } catch (SQLException ex) {
            Logger.getLogger(conexion.class.getName()).log(Level.SEVERE, null, ex);
        }
        return nombre;
    }
    
    public int getTipoDePago(String tipopago) {
        String query = "SELECT IDTipoPago\n"
                + "FROM tipopago\n"
                + "WHERE tipopago.Descripcion = '" + tipopago + "'";
        int nombre = -1;
        try {
            Connection con = conexion.conectar(conection);
            Statement st = con.createStatement();
            ResultSet rs = st.executeQuery(query);
            while (rs.next()) {
                nombre = rs.getInt(1);
            }
        } catch (SQLException ex) {
            Logger.getLogger(conexion.class.getName()).log(Level.SEVERE, null, ex);
        }
        return nombre;
    }
    
    
    public DefaultTableModel crearTablaCreditos(){
        ResultSet variablex = null;
        DefaultTableModel modelo = new DefaultTableModel(){
            public boolean isCellEditable(int row, int column){
                return false;
            }
        };
        try {  
            String sql = (" SELECT\n" +
                        "	CONCAT(\n" +
                        "		alumnos.Nombre,\n" +
                        "		\" \",\n" +
                        "		alumnos.ApellidoPaterno,\n" +
                        "		\" \",\n" +
                        "		alumnos.ApellidoMaterno\n" +
                        "	) AS nombreAlumno,\n" +
                        "	malla.nombreMalla,\n" +
                        "	malla.costo,\n" +
                        "	SUM(pago.CantidadPagada) AS \"Total Pagado\",\n" +
                        "	SUM(pago.Descuento) AS \"Total Descontado\",\n" +
                        "	(\n" +
                        "		malla.costo - SUM(pago.CantidadPagada) - SUM(pago.Descuento)\n" +
                        "	) AS \"Deuda\"\n" +
                        "FROM\n" +
                        "	alumnos,\n" +
                        "	malla,\n" +
                        "	pago\n" +
                        "WHERE\n" +
                        "	alumnos.IDMalla = malla.IDMalla\n" +
                        "AND alumnos.IDPersona = pago.IDAlumno\n" +
                        "AND malla.IDMalla = pago.IDMalla\n" +
                        "AND pago.Descripcion != \"Multa\"\n" +
                        "GROUP BY\n" +
                        "	alumnos.IDPersona\n"+
                        "HAVING SUM(pago.CantidadPagada) < malla.costo");
            Connection con = conexion.conectar(conection);
            Statement st = con.createStatement();
            variablex = st.executeQuery(sql);
            modelo.setColumnIdentifiers(new Object[]{"Nombre de Alumno","Malla inscrita","Costo de Malla","Monto Pagado","Monto Descontado ","Deuda"});
            while(variablex.next()){
                modelo.addRow(new Object[]{variablex.getString(1),variablex.getString(2),variablex.getString(3),variablex.getString(4),variablex.getString(5),variablex.getString(6)});
            }
            con.close();     
        } catch (SQLException ex) {
            Logger.getLogger(conexion.class.getName()).log(Level.SEVERE, null, ex);
        }
        return modelo;
    }
    
    public DefaultTableModel crearTablaCreditosMulta(){
        ResultSet variablex = null;
        DefaultTableModel modelo = new DefaultTableModel(){
            public boolean isCellEditable(int row, int column){
                return false;
            }
        };
        try {  
            String sql = ("SELECT\n" +
                            "	CONCAT(\n" +
                            "		alumnos.Nombre,\n" +
                            "		\" \",\n" +
                            "		alumnos.ApellidoPaterno,\n" +
                            "		\" \",\n" +
                            "		alumnos.ApellidoMaterno\n" +
                            "	) AS nombreAlumno,\n" +
                            "	malla.nombreMalla,\n" +
                            "	SUM(pago.CantidadPagada) AS totalPagado\n" +
                            "FROM\n" +
                            "	alumnos,\n" +
                            "	malla,\n" +
                            "	pago\n" +
                            "WHERE\n" +
                            "	alumnos.IDMalla = malla.IDMalla\n" +
                            "AND alumnos.IDPersona = pago.IDAlumno\n" +
                            "AND malla.IDMalla = pago.IDMalla\n" +
                            "AND pago.Descripcion != \"Mensualidad\"\n" +
                            "GROUP BY\n" +
                            "	alumnos.IDPersona");
            Connection con = conexion.conectar(conection);
            Statement st = con.createStatement();
            variablex = st.executeQuery(sql);
            modelo.setColumnIdentifiers(new Object[]{"Nombre de Alumno","Malla inscrita","Monto Pagado"});
            while(variablex.next()){
                modelo.addRow(new Object[]{variablex.getString(1),variablex.getString(2),variablex.getString(3)});
            }
            con.close();     
        } catch (SQLException ex) {
            Logger.getLogger(conexion.class.getName()).log(Level.SEVERE, null, ex);
        }
        return modelo;
    }
    
    public DefaultTableModel crearTablaCreditosGeneral(){
        ResultSet variablex = null;
        DefaultTableModel modelo = new DefaultTableModel(){
            public boolean isCellEditable(int row, int column){
                return false;
            }
        };
        try {  
            String sql = ("SELECT\n" +
                            "	CONCAT(\n" +
                            "		alumnos.Nombre,\n" +
                            "		\" \",\n" +
                            "		alumnos.ApellidoPaterno,\n" +
                            "		\" \",\n" +
                            "		alumnos.ApellidoMaterno\n" +
                            "	) AS NombreC,\n" +
                            "	malla.nombreMalla,\n" +
                            "	pago.CantidadPagada,\n" +
                            "	pago.Descuento,\n" +
                            "	pago.Descripcion,\n" +
                            "	pago.FechaPago,\n" +
                            "	pago.HoraPago\n" +
                            "FROM\n" +
                            "	alumnos,\n" +
                            "	malla,\n" +
                            "	pago\n" +
                            "WHERE\n" +
                            "	alumnos.IDPersona = pago.IDAlumno\n" +
                            "AND malla.IDMalla = pago.IDMalla\n" +
                            "ORDER BY\n" +
                            "	alumnos.IDPersona");
            Connection con = conexion.conectar(conection);
            Statement st = con.createStatement();
            variablex = st.executeQuery(sql);
            modelo.setColumnIdentifiers(new Object[]{"Nombre de Alumno","Malla inscrita","Monto Pagado","Monto Descontado","Descripcion","Fecha de Pago","Hora de Pago"});
            while(variablex.next()){
                modelo.addRow(new Object[]{variablex.getString(1),variablex.getString(2),variablex.getString(3),variablex.getInt(4),variablex.getString(5),variablex.getString(6),variablex.getString(7)});
            }
            con.close();     
        } catch (SQLException ex) {
            Logger.getLogger(conexion.class.getName()).log(Level.SEVERE, null, ex);
        }
        return modelo;
    }
    
    public Vector<Integer> ConseguirIDMaterias(int idmalla) throws SQLException
    {
        Vector<Integer> ids = new Vector<>();
        ResultSet variablex = null;
        String sql = ("SELECT \n"
                + "  IDMateria\n"
                + "  FROM malla_materia\n"
                + "  WHERE malla_materia.IDMalla = " + idmalla);
        Connection con = conexion.conectar(conection);
        Statement st = con.createStatement();
        variablex = st.executeQuery(sql);
        while (variablex.next()) 
        {
            ids.add(variablex.getInt(1));
        }
        return ids;
    }
    
    public void ConseguirNombreMalla(int idmalla,JTextField jTextFieldNombreMalla,JCheckBox jCheckBoxRequiereLicencia) throws SQLException
    {
        ResultSet variablex = null;
        String sql = ("SELECT \n"
                + "  nombreMalla,RequiereLicencia\n"
                + "  FROM malla\n"
                + "  WHERE malla.IDMalla = " + idmalla);
        Connection con = conexion.conectar(conection);
        Statement st = con.createStatement();
        variablex = st.executeQuery(sql);
        while (variablex.next()) 
        {
            jTextFieldNombreMalla.setText(variablex.getString(1));
            if(variablex.getInt(2)== 1)
            {
                jCheckBoxRequiereLicencia.setSelected(true);
            }
            else{
                jCheckBoxRequiereLicencia.setSelected(false);
            }
        }
    }
    
    public void CrearTablaMateriasMallaModificar( Vector<Integer> ids,Hashtable<String,String> contenedormateriaid,Hashtable<String,String> contenedormateria,Hashtable<String,String> contenedorCosto,Hashtable<String,String> contenedorMinutos)
    {
        ResultSet variablex = null;
        String letras = "IDMateria\tMateria\tCosto Creditos\tTotal Minutos\n";
        try {
            
            String sql = ("SELECT \n"
                + "       IDMateria\n"
                + "      ,materia\n"
                + "      ,CostoCreditos\n"
                + "      ,TotalMinutos\n"
                + "  FROM materia\n"
                + "  WHERE ");
            for (int i = 0; i < ids.size(); i++) {
                String aux = Integer.toString(ids.get(i));
                if(i<ids.size()-1)
                {
                    sql = sql + " IDMateria = " + aux + " OR ";
                }
                else
                {
                    sql = sql + " IDMateria = " + aux;
                }
            }
                Connection con = conexion.conectar(conection);
                Statement st = con.createStatement();
                variablex = st.executeQuery(sql);
                while (variablex.next()) {
                    contenedormateriaid.put(variablex.getString(1), variablex.getString(1));
                    contenedormateria.put(variablex.getString(1), variablex.getString(2));
                    contenedorCosto.put(variablex.getString(1), variablex.getString(3));
                    contenedorMinutos.put(variablex.getString(1), variablex.getString(4));
                }
        } catch (SQLException ex) {
            Logger.getLogger(conexion.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public void CrearTablaMateriasMalla(String NombreM,Hashtable<String,String> contenedormateriaid,Hashtable<String,String> contenedormateria,Hashtable<String,String> contenedorCosto,Hashtable<String,String> contenedorMinutos)
    {
        ResultSet variablex = null;
        String letras = "IDMateria\tMateria\tCosto Creditos\tTotal Minutos\n";
        try {
            
            String sql = ("SELECT \n"
                + "       IDMateria\n"
                + "      ,materia\n"
                + "      ,CostoCreditos\n"
                + "      ,TotalMinutos\n"
                + "  FROM materia\n"
                + "  WHERE materia = '"
                + NombreM + "'");
                Connection con = conexion.conectar(conection);
                Statement st = con.createStatement();
                variablex = st.executeQuery(sql);
                while (variablex.next()) {
                    contenedormateriaid.put(variablex.getString(1), variablex.getString(1));
                    contenedormateria.put(variablex.getString(1), variablex.getString(2));
                    contenedorCosto.put(variablex.getString(1), variablex.getString(3));
                    contenedorMinutos.put(variablex.getString(1), variablex.getString(4));
                }
        } catch (SQLException ex) {
            Logger.getLogger(conexion.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public void llenarFaltantes(int nivel,Hashtable<String,String> contenedormateriaid,Hashtable<String,String> contenedormateria,Hashtable<String,String> contenedorCosto,Hashtable<String,String> contenedorNivel,Hashtable<String,String> contenedorMinutos,Hashtable<String,String> contenedorEsVuelo)
    {
        ResultSet variablex=null;
        try {
            String sql2 = ("SELECT \n"
                    + "       IDMateria\n"
                    + "      ,materia\n"
                    + "      ,CostoCreditos\n"
                    + "      ,TotalMinutos\n"
                    + "FROM materia\n"
                    + "WHERE materia.Nivel <" + nivel + " AND materia.Nivel > 0");
            Connection con = conexion.conectar(conection);
            Statement st = con.createStatement();
            variablex = st.executeQuery(sql2);
            while (variablex.next()) {
                contenedormateriaid.put(variablex.getString(1), variablex.getString(1));
                contenedormateria.put(variablex.getString(1), variablex.getString(2));
                contenedorCosto.put(variablex.getString(1), variablex.getString(3));
                contenedorMinutos.put(variablex.getString(1), variablex.getString(4));
            }
            con.close();
        } catch (SQLException ex) {
            Logger.getLogger(conexion.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public DefaultTableModel LlenarTabla(Hashtable<String,String> contenedormateriaid,Hashtable<String,String> contenedormateria,Hashtable<String,String> contenedorCosto,Hashtable<String,String> contenedorMinutos)
    {
        DefaultTableModel modelo = new DefaultTableModel();
        Enumeration<String> llaves1 = contenedormateriaid.keys();
        Enumeration<String> llaves2 = contenedormateria.keys();
        Enumeration<String> llaves3 = contenedorCosto.keys();
        Enumeration<String> llaves5 = contenedorMinutos.keys();
        modelo.setColumnIdentifiers(new Object[]{"IDMateria","Materia","Costo Creditos","Total Minutos"});
        while (llaves1.hasMoreElements() && llaves2.hasMoreElements()&& llaves3.hasMoreElements()&& llaves5.hasMoreElements()) {
            modelo.addRow(new Object[]{contenedormateriaid.get(llaves1.nextElement()),contenedormateria.get(llaves2.nextElement()),contenedorCosto.get(llaves3.nextElement()),contenedorMinutos.get(llaves5.nextElement())});
        }
        return modelo;
    }
    
    public void DatosIngresados(JLabel jLabelCostoTotalMallaV,JLabel jLabelTotalMinutosV,JLabel jLabelNumeroTotalMateriasV,Hashtable<String,String> contenedormateriaid,Hashtable<String,String> contenedormateria,Hashtable<String,String> contenedorCosto,Hashtable<String,String> contenedorMinutos)
    {
        int CostoMaterias = 0;
        int NumeroNormales = 0;
        int TotalMinutos = 0;
        int MateriasVuelo = 0;
        int TotalMaterias = 0;
        Enumeration<String> llaves1 = contenedormateriaid.keys();
        Enumeration<String> llaves2 = contenedormateria.keys();
        Enumeration<String> llaves3 = contenedorCosto.keys();
        Enumeration<String> llaves5 = contenedorMinutos.keys();
        while (llaves1.hasMoreElements() && llaves2.hasMoreElements() && llaves3.hasMoreElements() && llaves5.hasMoreElements()) 
        {
            TotalMaterias++;
            CostoMaterias = CostoMaterias + Integer.parseInt(contenedorCosto.get(llaves3.nextElement()));
            TotalMinutos = TotalMinutos + Integer.parseInt(contenedorMinutos.get(llaves5.nextElement()));
            
            jLabelCostoTotalMallaV.setText(Integer.toString(CostoMaterias));
            jLabelTotalMinutosV.setText(Integer.toString(TotalMinutos));
            jLabelNumeroTotalMateriasV.setText(Integer.toString(TotalMaterias));
        }
    }
    
    public void EliminarFila(JTable tabla,Hashtable<String,String> contenedormateriaid,Hashtable<String,String> contenedormateria,Hashtable<String,String> contenedorCosto,Hashtable<String,String> contenedorMinutos)
    {
        DefaultTableModel model = (DefaultTableModel) tabla.getModel();
        String dato=String.valueOf(model.getValueAt(tabla.getSelectedRow(),0));
        int[] rows = tabla.getSelectedRows();
        for(int i=0;i<rows.length;i++)
        {
            model.removeRow(rows[i]-i);
        }
        contenedormateriaid.remove(dato);
        contenedormateria.remove(dato);
        contenedorCosto.remove(dato);
        contenedorMinutos.remove(dato);
    }
    
    public void getListMaterias(ArrayList<Materia> materias) throws SQLException {
        String query = ("SELECT \n"
                + "       IDMateria\n"
                + "      ,Materia\n"
                + "      ,CostoCreditos\n"
                + "      ,TotalMinutos\n"
                + "  FROM materia");

        Connection con = conexion.conectar(conection);
        Statement st = con.createStatement();
        ResultSet rs = st.executeQuery(query);
        while (rs.next()) {
            int id = rs.getInt(1);
            String nombre = rs.getString(2);
            int costoCredito = rs.getInt(3);
            int totalMinutos = rs.getInt(4);

            Materia aux = new Materia(id, nombre, costoCredito, totalMinutos);
            materias.add(aux);

        }
        close(con);
    }
    
    public void getListMateriasDependecias(int idMateria,Vector<Integer> materias) throws SQLException {
        String query = ("SELECT IDMateriaAnt\n"
                + "FROM dependenciasmateria\n"
                + "WHERE IDMateria = "
                + idMateria);

        Connection con = conexion.conectar(conection);
        Statement st = con.createStatement();
        ResultSet rs = st.executeQuery(query);
        while (rs.next()) {
            materias.add(rs.getInt(1));
        }
        close(con);
    }

    public DefaultComboBoxModel getModelo(String tabla, LinkedList<CRTVuelo> mandar) throws SQLException {

        DefaultComboBoxModel modelo = new DefaultComboBoxModel();
        String sql = "SELECT *\n"
                + "  FROM " + tabla 
                + "  WHERE ";
        Connection con = conexion.conectar(conection);
        Statement st = con.createStatement();
        ResultSet rs = st.executeQuery(sql);

        modelo.removeAllElements();
        modelo.addElement("--------");
        boolean isMalla = (tabla.equals("malla"));
        while (rs.next()) {
            int ID = rs.getInt(1);
            String desc = rs.getString(2);
            int minutos = 0;
            if (isMalla) {
                minutos = rs.getInt(3);
            }

            CRTVuelo aux = new CRTVuelo(ID, desc, minutos);
            mandar.add(aux);

            modelo.addElement(desc);

        }
        close(con);
        return modelo;
    }

    public DefaultComboBoxModel getModelo2(String tabla, LinkedList<Fase> mandar) throws SQLException {

        DefaultComboBoxModel modelo = new DefaultComboBoxModel();
        String sql = "SELECT *\n"
                + "  FROM " + tabla + "";
        Connection con = conexion.conectar(conection);
        Statement st = con.createStatement();
        ResultSet rs = st.executeQuery(sql);

        modelo.removeAllElements();
        modelo.addElement("--------");
        boolean flag = true;
        if (tabla.equalsIgnoreCase("rol")) {
            while (rs.next()) {
                if (flag) {
                    flag = false;
                    continue;
                }
                int ID = rs.getInt(1);
                String desc = rs.getString(2);
                Fase aux = new Fase(ID, desc);
                mandar.add(aux);
                modelo.addElement(desc);

            }
            close(con);
            return modelo;
        } else {
            while (rs.next()) {
                int ID = rs.getInt(1);
                String desc = rs.getString(2);
                Fase aux = new Fase(ID, desc);
                mandar.add(aux);
                modelo.addElement(desc);

            }
            close(con);
            return modelo;
        }
    }

    public DefaultComboBoxModel getModeloHelice(LinkedList<Helices> heli) throws SQLException {

        DefaultComboBoxModel modelo = new DefaultComboBoxModel();
        String sql = "SELECT *\n"
                + "  FROM helice";
        Connection con = conexion.conectar(conection);

        Statement st = con.createStatement();
        ResultSet rs = st.executeQuery(sql);

        modelo.removeAllElements();
        modelo.addElement("--------");
        while (rs.next()) {
            int ID = rs.getInt(1);
            String marca = rs.getString(2);
            String mod = rs.getString(3);
            String numSerie = rs.getString(4);
            int tt = rs.getInt(5);
            int tto = rs.getInt(6);
            if (rs.wasNull()) {
                tto = 0;
            }
            int tbo = rs.getInt(7);
            if (rs.wasNull()) {
                tbo = 0;
            }

            Helices aux = new Helices(ID, marca, mod, numSerie, tt, tto, tbo);
            heli.add(aux);
            modelo.addElement(marca + "-" + mod + "-" + numSerie);

        }
        close(con);
        return modelo;
    }

    public DefaultComboBoxModel getModeloReportes(String tabla) throws SQLException {
        DefaultComboBoxModel modelo = new DefaultComboBoxModel();
        Connection con = conexion.conectar(conection);
        Statement st = con.createStatement();
        ResultSet rs;
        if (tabla.equals("materia")) {
            String sql = "SELECT Materia"
                    + "  FROM materia";
            rs = st.executeQuery(sql);

            modelo.removeAllElements();
            modelo.addElement("--------");
            while (rs.next()) {

                String desc = rs.getString(1);

                modelo.addElement(desc);
            }
        } else if (tabla.equals("serie")) {
            String sql = "SELECT NumeroDeSerie"
                    + "  FROM avion";
            rs = st.executeQuery(sql);

            modelo.removeAllElements();
            modelo.addElement("--------");
            while (rs.next()) {

                String desc = rs.getString(1);

                modelo.addElement(desc);

            }
        } else if (tabla.equals("docente")) {
            String sql = "SELECT Nombre, ApellidoPaterno\n"
                    + "FROM  alumno_docente_piloto\n"
                    + "WHERE isPiloto=1 AND IsDocente=1 ";
            rs = st.executeQuery(sql);

            modelo.removeAllElements();
            modelo.addElement("--------");
            while (rs.next()) {

                String desc = rs.getString(1);
                String desc1 = rs.getString(2);

                String ans = desc + " " + desc1;
                modelo.addElement(ans);

            }

        }
        close(con);
        return modelo;
    }

    public Usuario getUsuario(int iDUsuario) {
        String query = ("SELECT *\n"
                + "FROM usuarios\n"
                + "WHERE usuarios.IDUser=" + iDUsuario);
        int idUsuario = 0;
        String nombreUsuario = "";
        String nombre = "";
        String password = "";
        int rol = 0;
        try {
            Connection con = conexion.conectar(conection);
            Statement st = con.createStatement();
            ResultSet rs = st.executeQuery(query);

            if (rs.next()) {
                idUsuario = rs.getInt(1);
                nombreUsuario = rs.getString(2);
                nombre = rs.getString(3);
                password = rs.getString(4);
                rol = rs.getInt(5);

            }
            Usuario usuario = new Usuario(idUsuario, nombreUsuario, nombre, password, rol);
            return usuario;
        } catch (SQLException ex) {
            Logger.getLogger(conexion.class.getName()).log(Level.SEVERE, null, ex);
        }
        return new Usuario(idUsuario, nombreUsuario, nombre, password, rol);
    }
    
    public void insertarparciales(int notasparciales,int id) throws SQLException{
        String query = ("INSERT INTO parcialesnotas(parcialesnotas.IDNotaFinal,parcialesnotas.NotaParcial)\n"
                + "VALUES("+id+","+notasparciales+")");
        Connection con = conexion.conectar(conection);
        Statement statement = con.createStatement();
        statement.executeUpdate(query);
        close(con);
    }

    public void insertMalla(String nombreMalla, int costo, int requiereLicencia, LinkedList<Materia> materiasMalla) throws SQLException {
        int resultado;
        String query = ("INSERT INTO malla\n"
                + "(\n"
                + "  IDMalla\n"
                + " ,nombreMalla\n"
                + " ,RequiereLicencia\n"
                + " ,costo\n"
                + " ,isBorrado\n"
                + ")\n"
                + "VALUES\n"
                + "(?,?,?,?,?)");
        Connection con = conexion.conectar(conection);
        PreparedStatement pst;
        pst = con.prepareStatement(query);
        pst.setObject(1, null);
        pst.setString(2, nombreMalla);
        pst.setInt(3, requiereLicencia);
        pst.setInt(4, costo);
        pst.setInt(5, 0);
        resultado = pst.executeUpdate();
        pst.close();
        close(con);

        int idMalla = getIDGenerated("malla", "IDMalla");
        try {
            for (Materia materia : materiasMalla) {
                insertMateriasMalla(idMalla, materia.getIDMateria());
            }
        } catch (SQLException sq) {
            throw new SQLException("Error al inserar las materias en malla");
        }
    }

    /*
    Referenta a Malla
     */
    private void insertMateriasMalla(int idMalla, int idMateria) throws SQLException {
        String query = ("INSERT INTO malla_materia\n"
                + "(\n"
                + "  IDMalla\n"
                + " ,IDMateria\n"
                + ")\n"
                + "VALUES\n"
                + "(\n"
                + " " + idMalla + " -- IDMalla - int NOT NULL\n"
                + " ," + idMateria + " -- IDMateria - int NOT NULL\n"
                + ")");
        Connection con = conexion.conectar(conection);
        Statement statement = con.createStatement();
        statement.executeUpdate(query);
        close(con);

    }

    public void insertarAlumno_docente_piloto(Persona p, LinkedList<Referencia> referencias,
            int createdBy, String FotoPath) throws SQLException, UnsupportedEncodingException, FileNotFoundException {
        String query = "INSERT INTO alumno_docente_piloto\n"
                + "(  IDPersona\n" //1
                + " ,IDMalla\n"//2
                + " ,Nombre\n"//3
                + " ,ApellidoPaterno\n"//4
                + " ,ApellidoMaterno\n"//5
                + " ,IDDocumento\n"//6
                + " ,NumeroDocumento\n"//7
                + " ,NroCredencialCorporativo\n"//8
                + " ,NroCredencialSabsa\n"//9
                + " ,NumFolio\n"//10
                + " ,NumLicencia\n"//11
                + " ,isPiloto\n"//12
                + " ,IsDocente\n"//13
                + " ,UsaLentes\n"//14
                + " ,Direccion\n"//15
                + " ,TipoSangre\n"//16
                + " ,FechaNacimiento\n"//17
                + " ,Telefono\n"//18
                + " ,Foto\n"//19
                + " ,isFinalizado\n"//20
                + " ,isBorrado\n"//21
                + " ,isBloqueado\n"//22

                + " ,CreditoFalso\n"//23
                + " ,FechaVigenciaSabsa\n"//24
                + " ,FechaVigenciaCorporativo\n"//25
                + " ,FechaVencimientoAptoMedico\n"//26
                + " ,FechaVencimientoProficciencyCheck\n"//27
                + " ,CreatedBy"//28
                + ")\n"
                + "VALUES\n"
                + "(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
        Connection con = conexion.conectar(conection);
        PreparedStatement pst;
        pst = con.prepareStatement(query);
        String nombreCompleto = "";
        pst.setObject(1, null); //IDPersona1
        if (p.getiDMalla() == -1) //idMalla2
        {
            pst.setObject(2, null);
        } else {
            pst.setInt(2, p.getiDMalla());
        }
        pst.setString(3, p.getNombre()); //nombre3
        nombreCompleto += p.getNombre();
        if (p.getApellidoPaterno().isEmpty()) //apellidoPaterno4
        {
            pst.setObject(4, null);
        } else {
            pst.setString(4, p.getApellidoPaterno());
            nombreCompleto += " " + p.getApellidoPaterno();
        }
        if (p.getApellidoMaterno().isEmpty()) //apellidoMaterno5
        {
            pst.setObject(5, null);
        } else {
            pst.setString(5, p.getApellidoMaterno());
            nombreCompleto += " " + p.getApellidoMaterno();
        }
        pst.setInt(6, p.getTipoDocumento());//6
        pst.setString(7, p.getCiRucPas());//cirucpas7
        if (p.getNumCredicialCoorporativo() == 0) //numcoorp8
        {
            pst.setObject(8, null);
        } else {
            pst.setInt(8, p.getNumCredicialCoorporativo());
        }
        if (p.getNumCredencialSABSA() == 0)//numSABSA
        {
            pst.setObject(9, null);
        } else {
            pst.setInt(9, p.getNumCredencialSABSA());
        }
        if (p.getNumFolio() == 0)//numfolio
        {
            pst.setObject(10, null);
        } else {
            pst.setInt(10, p.getNumFolio());
        }
        if (p.getNumLicencia() == 0)//numLicencia
        {
            pst.setObject(11, null);
        } else {
            pst.setInt(11, p.getNumLicencia());
        }
        pst.setInt(12, p.getIsPiloto());//ispiloto
        pst.setInt(13, p.getIsDocente());//isdocente
        pst.setInt(14, p.getUsaLentes());//usalentes

        pst.setString(15, p.getDireccion());//direccion
        pst.setString(16, p.getTipoSangra());//tiposangre
        pst.setString(17, p.getFechaNacimiento());//fechanacimiento
        if (p.getTelefono() == -1) {
            pst.setObject(18, null);
        } else {
            pst.setInt(18, p.getTelefono());
        }
        
        InputStream is = null;
        if(FotoPath!=null)
        {
            is = new FileInputStream(new File(FotoPath));
            
        }
        pst.setBlob(19,is);//foto
        
        pst.setInt(20, p.getIsFinalizado());//isfinalizado
        pst.setInt(21, 0);//isBorrado
        pst.setInt(22, 0);//isBloqueado
        pst.setInt(23, p.getCreditoFalso());//creditofalso
        if (p.getFechaVigenciaSABSA().isEmpty())//SABSA
        {
            pst.setObject(24, null);
        } else {
            pst.setString(24, p.getFechaVigenciaSABSA());
        }
        if (p.getFechaVigenciaCoorporativo().isEmpty())//corporativo
        {
            pst.setObject(25, null);
        } else {
            pst.setString(25, p.getFechaVigenciaCoorporativo());
        }
        if (p.getFechaVencimientoAptoMedico().isEmpty())//aptoMedico
        {
            pst.setObject(26, null);
        } else {
            pst.setString(26, p.getFechaVencimientoAptoMedico());
        }
        if (p.getFechaVencimientoProficienceCheck().isEmpty())//profiencycheck
        {
            pst.setObject(27, null);
        } else {
            pst.setString(27, p.getFechaVencimientoProficienceCheck());
        }
        pst.setInt(28, createdBy);
        System.out.println(pst);
        pst.executeUpdate();

        pst.close();

        close(con);
        int idAlumno = getIDGenerated("alumno_docente_piloto", "IDPersona");
        System.out.println(idAlumno);
        try {
            for (Referencia referencia : referencias) {
                if(referencia.getIdReferencia() == 0)
                {
                    System.out.println(referencia.getTelefono().get(0));
                    insertarReferencia(referencia.getNombre(), referencia.getTelefono());
                    int idReferenciaCreada = getIDGenerated("referencia_alumno_docente_piloto", "IDReferencia");
                    referencia.setIdReferencia(idReferenciaCreada);
                    insertarAlumno_docente_piloto_referencia(idAlumno, referencia);
                }
                else
                {
                    insertarAlumno_docente_piloto_referencia(idAlumno, referencia);
                }
                
            }
        } catch (SQLException sq) {
            throw new SQLException("Error al agregar referencias:" + sq.getMessage());
        }
        try {
            loger("logalumnopilotodocente", "IDAlumnoPilotoDocente", idAlumno, createdBy, "created");
        } catch (SQLException sq) {
            throw new SQLException("Error al insertar el log:" + sq.getMessage());
        }
    }
    
    public int getIDTipoPago (String tipopago){
        String query = "SELECT IDTipoPago\n"
                + "FROM tipopago\n"
                + "WHERE Descripcion = '" + tipopago + "'";
        int nombre = -1;
        try {
            Connection con = conexion.conectar(conection);
            Statement st = con.createStatement();
            ResultSet rs = st.executeQuery(query);
            while (rs.next()) {
                nombre = rs.getInt(1);
            }
        } catch (SQLException ex) {
            Logger.getLogger(conexion.class.getName()).log(Level.SEVERE, null, ex);
        }
        return nombre;
    }
    
    public void insertarPago(Pagos p, int createdBy) throws SQLException {
        String query = "INSERT INTO pago\n"
                + "(  IDPago\n" //1
                + " ,IDAlumno\n"//2
                + " ,FechaPago\n"//3
                + " ,HoraPago\n"//4
                + " ,Descripcion\n"//5
                + " ,CantidadPagada\n"//6
                + " ,CreatedBy\n"//7
                + " ,TipoDePago\n"//8
                + " ,Descuento\n"//8
                + " ,IDMalla\n"//9
                + ")\n"
                + "VALUES\n"
                + "(?,?,?,?,?,?,?,?,?,?)";
        Connection con = conexion.conectar(conection);
        PreparedStatement pst;
        pst = con.prepareStatement(query);
        pst.setObject(1, null); //IDPago1
        int aux = getIDTipoPago(p.getDescripcion());
        if (p.getiDAlumno()== -1) //idAlumno2
        {
            pst.setObject(2, null);
        } else {
            pst.setInt(2, p.getiDAlumno());
        }
        
        if (p.getFechaPago().isEmpty()) //Fecha3
        {
            pst.setObject(3, null);
        } else {
            pst.setString(3, p.getFechaPago());
        }
        
        if (p.getHoraPago().isEmpty()) //Hora4
        {
            pst.setObject(4, null);
        } else {
            pst.setString(4, p.getHoraPago());
        }
        
        if (p.getDescripcion().isEmpty()) //descripcion5
        {
            pst.setObject(5, null);
        } else {
            pst.setInt(5, aux);
        }
        
        if (p.getCantidadPagada()== -1) //cantidad6
        {
            pst.setObject(6, null);
        } else {
            pst.setDouble(6, p.getCantidadPagada());
        }
        
        if (p.getCreatedBy()== -1) //createdBy7
        {
            pst.setObject(7, null);
        } else {
            pst.setInt(7, p.getCreatedBy());
        }
        
        if (p.getTipoDePago() == -1) //tipoDePago8
        {
            pst.setObject(8, null);
        } else {
            pst.setInt(8, p.getTipoDePago());
        }
     
        if (p.getDescuento()== -1) //descuento9
        {
            pst.setObject(9, null);
        } else {
            pst.setInt(9, p.getDescuento());
        }
        
        if (p.getiDMalla()== -1) //idMalla10
        {
            pst.setObject(10, null);
        } else {
            pst.setInt(10, p.getiDMalla());
        }
        pst.executeUpdate();

        pst.close();

        close(con);
    }

    public void insertarAlumno_docente_piloto_referencia(int idAlumno, Referencia referencia) throws SQLException {
        String query = "INSERT INTO alumno_docente_piloto_referencia\n"
                + "(\n"
                + "  IDAlumno_docente_piloto\n"
                + " ,IDReferencia\n"
                + " ,parentescoreferencia\n"
                + ")\n"
                + "VALUES\n"
                + "(?,?,?)";
        Connection con = conexion.conectar(conection);
        PreparedStatement pst;
        pst = con.prepareStatement(query);
        pst.setInt(1, idAlumno);
        pst.setInt(2, referencia.getIdReferencia());
        pst.setString(3, referencia.getParentesco());
        System.out.println(pst);
        pst.executeUpdate();
        pst.close();

        close(con);
    }
    
    public int ConseguirIDNota() throws SQLException {
        int id=0;
        ResultSet variablex = null;
        String query = ("SELECT notas.IDNotas FROM notas WHERE IDNotas = (SELECT MAX(notas.IDNotas) FROM notas)");
        Connection con = conexion.conectar(conection);
        Statement st = con.createStatement();
        variablex = st.executeQuery(query);
        while (variablex.next()) 
        {
            id = variablex.getInt(1);
        }
        return id;
    }
    
    public int ConseguirIDMateria(String nombre) throws SQLException
    {
        int id=-1;
        ResultSet variablex = null;
        String sql = ("SELECT \n"
                + "  IDMateria\n"
                + "  FROM materia\n"
                + "  WHERE materia.Materia = '" + nombre + "'");
        Connection con = conexion.conectar(conection);
        Statement st = con.createStatement();
        variablex = st.executeQuery(sql);
        while (variablex.next()) 
        {
            id = variablex.getInt(1);
        }
        return id;
    }
    
    public int ConseguirIDMalla(String nombre) throws SQLException
    {
        int id=-1;
        ResultSet variablex = null;
        String sql = ("SELECT \n"
                + "  IDMalla\n"
                + "  FROM malla\n"
                + "  WHERE malla.nombreMalla = '" + nombre + "'");
        Connection con = conexion.conectar(conection);
        Statement st = con.createStatement();
        variablex = st.executeQuery(sql);
        while (variablex.next()) 
        {
            id = variablex.getInt(1);
        }
        return id;
    }
    
    public void ConseguirMateria(JComboBox combo) throws SQLException{
        ResultSet variablex = null;
        String sql = ("SELECT \n"
                + "materia.Materia\n"
                + "FROM materia\n"
                + "WHERE materia.isBorrado = 0");
        Connection con = conexion.conectar(conection);
        Statement st = con.createStatement();
        variablex = st.executeQuery(sql);
        while (variablex.next()) 
        {
            combo.addItem(variablex.getString(1));
        }
    }
    
    public void ConseguirDocentes(JComboBox combo) throws SQLException{
        ResultSet variablex = null;
        String sql = ("SELECT \n"
                + "CONCAT(doc.Nombre,\" \",doc.ApellidoPaterno,\" \",doc.ApellidoMaterno)\n"
                + "FROM alumno_docente_piloto doc\n"
                + "WHERE doc.IsDocente = 1");
        Connection con = conexion.conectar(conection);
        Statement st = con.createStatement();
        variablex = st.executeQuery(sql);
        while (variablex.next()) 
        {
            combo.addItem(variablex.getString(1));
        }
    }
    
    public String ConseguirNombreMateria(int id) throws SQLException
    {
        String nombre = "";
        ResultSet variablex = null;
        String sql = ("SELECT \n"
                + "  materia.Materia\n"
                + "  FROM materia\n"
                + "  WHERE materia.IDMateria = " + id);
        Connection con = conexion.conectar(conection);
        Statement st = con.createStatement();
        variablex = st.executeQuery(sql);
        while (variablex.next()) 
        {
            nombre = variablex.getString(1);
        }
        return nombre;
    }
    
    public String ConseguirDocente(int id) throws SQLException
    {
        String nombre = "";
        ResultSet variablex = null;
        String sql = ("SELECT \n"
                + "  CONCAT(alumno_docente_piloto.Nombre,\" \",alumno_docente_piloto.ApellidoPaterno,\" \",alumno_docente_piloto.ApellidoMaterno)\n"
                + "  FROM alumno_docente_piloto\n"
                + "  WHERE alumno_docente_piloto.IDPersona = " + id);
        Connection con = conexion.conectar(conection);
        Statement st = con.createStatement();
        variablex = st.executeQuery(sql);
        while (variablex.next()) 
        {
            nombre = variablex.getString(1);
        }
        return nombre;
    }
    
    public String ConseguirAvion(int id) throws SQLException
    {
        String nombre = "";
        ResultSet variablex = null;
        String sql = ("SELECT \n"
                + "  CONCAT(avion.Marca, \"-\", avion.Modelo, \"-\", avion.NumeroDeSerie )\n"
                + "  FROM avion\n"
                + "  WHERE avion.IDAvion = " + id);
        Connection con = conexion.conectar(conection);
        Statement st = con.createStatement();
        variablex = st.executeQuery(sql);
        while (variablex.next()) 
        {
            nombre = variablex.getString(1);
        }
        return nombre;
    }
    
    public void ActualizarNotasIngresadas(int idp,int idg){
        try {
        String sql = "UPDATE alumno_grupoinscrito\n"
                + "SET NotasIngresadas = ?\n"
                + "WHERE IDGrupoDisponible = "+idg+ " AND IDAlumno = "+idp;
                Connection con = conexion.conectar(conection);
                PreparedStatement pstm = con.prepareStatement(sql);
                pstm.setInt(1,1);               
                pstm.executeUpdate();
                pstm.close();
                
                con.close();
        } catch (SQLException ex) {
            System.err.println(ex);
        }
    }
    
    public void ConseguirDatosNotas(int idP,LinkedList<String> NombreG,DefaultComboBoxModel modelo1) throws SQLException
    {
        NombreG = new LinkedList<>();
        ResultSet variablex = null;
        String sql = (  "SELECT grupodisponibles.NombreGrupo\n" +
                        "FROM alumno_grupoinscrito,grupodisponibles\n" +
                        "WHERE alumno_grupoinscrito.IDAlumno="+idP+" AND grupodisponibles.IDGrupo=alumno_grupoinscrito.IDGrupoDisponible AND alumno_grupoinscrito.NotasIngresadas=0");
        Connection con = conexion.conectar(conection);
        Statement st = con.createStatement();
        variablex = st.executeQuery(sql);
        while (variablex.next()) {
            NombreG.add(variablex.getString(1));
            modelo1.addElement(variablex.getString(1));
        }
    }
    
    public String ConseguirDatosGrupo(String nombre) throws SQLException
    {
        int idm=0;
        ResultSet variablex = null;
        String sql = (  "SELECT grupodisponibles.IDMateria\n" +
                        "FROM grupodisponibles\n" +
                        "WHERE grupodisponibles.NombreGrupo ='"+nombre+"'");
        Connection con = conexion.conectar(conection);
        Statement st = con.createStatement();
        variablex = st.executeQuery(sql);
        while (variablex.next()) {
            idm=variablex.getInt(1);
        }
        String nombreM = ConseguirNombreMateria(idm);
        return nombreM;
    }

    public int ConseguirIDDocente(String nombre) throws SQLException
    {
        int id=-1;
        ResultSet variablex = null;
        String sql = ("SELECT\n" +
                      "IDPersona\n" +
                      "FROM alumno_docente_piloto\n" +
                      "WHERE CONCAT(alumno_docente_piloto.Nombre,\" \",alumno_docente_piloto.ApellidoPaterno,\" \",alumno_docente_piloto.ApellidoMaterno) = '" + nombre + "'");
        Connection con = conexion.conectar(conection);
        Statement st = con.createStatement();
        variablex = st.executeQuery(sql);
        while (variablex.next()) 
        {
            id = variablex.getInt(1);
        }
        return id;
    }
    
    private void insertarDEpendencias(Materia principal, Materia materiasDependiente) throws SQLException {
        String query = ("INSERT INTO dependenciasmateria\n"
                + "(\n"
                + "  IDMateria\n"
                + " ,IDMateriaAnt\n"
                + ")\n"
                + "VALUES\n"
                + "(\n"
                + "  " + principal.getIDMateria() + " -- IDMateria - int NOT NULL\n"
                + " ," + materiasDependiente.getIDMateria() + " -- IDMateriaAnt - int NOT NULL\n"
                + ")");

        Connection con = conexion.conectar(conection);
        Statement statement = con.createStatement();
        statement.executeUpdate(query);
        close(con);
    }
    
    public int getNotaMinima(int Idgrupo) throws SQLException{
        int notaP=0;
        ResultSet variablex = null;
        String query ="SELECT grupodisponibles.NotaPase\n"
                + "FROM grupodisponibles\n"
                + "WHERE grupodisponibles.IDGrupo="+Integer.toString(Idgrupo);
        Connection con = conexion.conectar(conection);
        Statement st = con.createStatement();
        variablex = st.executeQuery(query);
        while (variablex.next()) 
        {
            notaP=variablex.getInt(1);
        }
        
        return notaP;
    }
    
    public int getNotaMaxima(int Idgrupo) throws SQLException {
        int notaA = 0;
        ResultSet variablex = null;
        String query = "SELECT grupodisponibles.NotaAprobacion\n"
                + "FROM grupodisponibles\n"
                + "WHERE grupodisponibles.IDGrupo="+Integer.toString(Idgrupo);
        Connection con = conexion.conectar(conection);
        Statement st = con.createStatement();
        variablex = st.executeQuery(query);
        while (variablex.next()) 
        {
            notaA=variablex.getInt(1);
        }
        return notaA;
    }
    
    
    public void insertarNota(int IDGrupo, int IDAlumno,int nota) throws SQLException {
        
        int notap=getNotaMinima(IDGrupo);
        int notaa=getNotaMaxima(IDGrupo);
        String query = ("UPDATE notas\n"
                + "SET Nota = ?  ,  "
                + "Fecha = ? , "
                + "Estado = ? \n"
                + "WHERE IDGrupo = ? AND IDAlumno = ?");
        Connection con = conexion.conectar(conection);
        PreparedStatement pst;
        pst = con.prepareStatement(query);
        pst.setInt(1, nota);
        pst.setString(2, FechaActual2());
        if(nota<notap){
            pst.setString(3,"APLAZADO");
        }else if(nota>notap && nota<notaa){
            pst.setString(3,"PASADO");
        }else{
            pst.setString(3,"APROBADO");
        }
         pst.setInt(4, IDGrupo);
          pst.setInt(5, IDAlumno);
        
        pst.executeUpdate();
        close(con);
    }
    
    

    public void insertarGrupo(String nombreGrupo, int IdDocente, int idMateria, String fechaIni, String fechaFin,String horario,int notapase,int notaaprobacion) throws SQLException {
        String query = ("INSERT INTO grupodisponibles\n"
                + "(\n"
                + "  IDDocente\n"
                + " ,IDMateria\n"
                + " ,FechaIni\n"
                + " ,FechaFin\n"
                + " ,Horario\n"
                + " ,NombreGrupo\n"
                + " ,NotaPase\n"
                + " ,NotaAprobacion"
                + " ,isBorrado\n"
                + " ,isTerminado\n"
                + ")\n"
                + "VALUES\n"
                + "(?,?,?,?,?,?,?,?,0,0)");
        Connection con = conexion.conectar(conection);
        PreparedStatement pst;
        pst = con.prepareStatement(query);
        pst.setInt(1, IdDocente);
        pst.setInt(2, idMateria);
        pst.setString(3, fechaIni);
        pst.setString(4, fechaFin);
        pst.setString(5, horario);
        pst.setString(6, nombreGrupo);
        pst.setInt(7,notapase);
        pst.setInt(8,notaaprobacion);

        pst.executeUpdate();
        pst.close();

        close(con);

    }
    
    public void ModificarGrupo(Grupo v,int notap,int notaa,int idGrupo)
    {
        try {
        String sql = "UPDATE grupodisponibles\n"
                + "SET IDDocente = ?, IDMateria = ?, FechaIni = ?, FechaFin = ?, Horario = ?,NombreGrupo = ?,NotaPase = ?,NotaAprobacion = ?\n"
                + "WHERE IDGrupo = "
                + idGrupo;
                Connection con = conexion.conectar(conection);
                PreparedStatement pstm = con.prepareStatement(sql);
                pstm.setInt(1, ConseguirIDDocente(v.getNombreDocente()));
                pstm.setInt(2, ConseguirIDMateria(v.getNombreMateria()));
                pstm.setString(3, v.getFechaInicio());
                pstm.setString(4, v.getFechaFin());
                pstm.setString(5, v.getHora());
                pstm.setString(6, v.getNombreGrupo());
                pstm.setInt(7, notap);
                pstm.setInt(8, notaa);                
                pstm.executeUpdate();
                pstm.close();
                
                con.close();
        } catch (SQLException ex) {
            System.err.println(ex);
        }
    }
    
    public void eliminarGrupo(int idGrupo) throws SQLException {
        String query = ("UPDATE  grupodisponibles\n"
                + " SET isBorrado = ? \n"
                + " WHERE IDGrupo = ?\n");
        Connection con = conexion.conectar(conection);
        PreparedStatement pst;

        pst = con.prepareStatement(query);
        pst.setInt(1, 1);
        pst.setInt(2, idGrupo);
        pst.executeUpdate();
        pst.close();
        close(con);
    }

    public void insertarHelice(Helices principal, int idUser) throws SQLException {
        String query = ("INSERT INTO helice (\n" +
                        "	IDHelice,\n" +
                        "	Marca,\n" +
                        "	Modelo,\n" +
                        "	NroSerie,\n" +
                        "	TT,\n" +
                        "	TTO,\n" +
                        "	TBO,\n" +
                        "	CreatedBy,\n" +
                        "	isBorrado\n" +
                        ")\n" +
                        "VALUES\n" +
                        "	(?,?,?,?,?,?,?,?,?)");

        Connection con = conexion.conectar(conection);
        PreparedStatement pst;
        pst = con.prepareStatement(query);
        
        System.out.println(principal.getMarca());
        System.out.println(principal.getModelo());
        System.out.println(principal.getNumeroDeSerie());
        System.out.println(principal.getTT());
        System.out.println(principal.getTTO());
        System.out.println(principal.getTBO());

        pst.setObject(1, null);
        pst.setString(2, principal.getMarca());
        pst.setString(3, principal.getModelo());
        pst.setString(4, principal.getNumeroDeSerie());
        if (principal.getTT() == -1) {
            pst.setObject(5, null);
        } else {
            pst.setInt(5, principal.getTT());
        }
        if (principal.getTTO() == -1) {
            pst.setObject(6, null);
        } else {
            pst.setInt(6, principal.getTTO());
        }
        if (principal.getTBO() == -1) {
            pst.setObject(7, null);
        } else {
            pst.setInt(7, principal.getTBO());
        }
        
        pst.setInt(8, idUser);
        pst.setInt(9, 0);

        pst.executeUpdate();

        pst.close();

        close(con);

        int idHelice = getIDGenerated("helice", "IDHelice");
        try {

            loger("loghelice", "IDHelice", idHelice, idUser, "created");
        } catch (SQLException sq) {
            throw new SQLException("Error al insertar el Log");
        }
    }

    public void insertarHeliceMotor(int createdby, String marca, String modelo, String serie, int tt, int tto, int tbo,
            String fechaCreado) throws SQLException {
        String query = ("");
    }

    public void insertarMateria(Materia materia,Hashtable<Integer,Materia> materiasDependencia,int IDUser) throws SQLException {
        int resultado = 0;
        String query = ("INSERT INTO materia\n"
                + "(\n"
                + " Materia\n"
                + " ,CostoCreditos\n"
                + " ,TotalMinutos\n"
                + " ,isBorrado\n"
                + " ,CreatedBy\n"
                + ")\n"
                + "VALUES\n"
                + "(?,?,?,?,?)");
        Connection con = conexion.conectar(conection);
        PreparedStatement pst;
        pst = con.prepareStatement(query);

        pst.setString(1, materia.getNombre());
        pst.setDouble(2, materia.getCostoCreditos());
        pst.setInt(3, materia.getTotalMinutos());
        pst.setInt(4, 0);
        pst.setInt(5, IDUser);

        resultado = pst.executeUpdate();
        pst.close();

        close(con);
        
        int idMateria = getIDGenerated("materia", "IDMateria");
        materia.setIDMateria(idMateria);
        try {
            if (!materiasDependencia.isEmpty()) {
                for (Materia materiaDep : materiasDependencia.values()) {
                    insertarDEpendencias(materia, materiaDep);
                }
            }
        } catch (SQLException sq) {
            throw new SQLException("Error al insertar Dependencias");
        }
        
        try {
            loger("logmateria", "IDMateria", ConseguirIDMateria(materia.getNombre()), IDUser, "created");
        } catch (SQLException sq) {
            throw new SQLException("Error al insertar el Log");
        }

    }
    
    public void datosMateria(Materia materiaBuscada, Hashtable<Integer,Materia> dependencias,int[] minutos) throws SQLException {
        buscarMateriaDependencia(materiaBuscada.getIDMateria(), dependencias);
    }

    private void insertarMotorHelice_Avion(Avion av, LinkedList<Motor> motores) {

    }
    
    public int getIDReferencia(String NombreCompleto) {
        String query = "SELECT referencia_alumno_docente_piloto.IDReferencia\n" +
                        "FROM referencia_alumno_docente_piloto\n" +
                        "WHERE referencia_alumno_docente_piloto.NombreCompletoReferencia = \'"+NombreCompleto+"\'";
        int ID = -1;
        try {
            Connection con = conexion.conectar(conection);
            Statement st = con.createStatement();
            ResultSet rs = st.executeQuery(query);
            while (rs.next()) {
                ID = rs.getInt(1);
            }
        } catch (SQLException ex) {
            Logger.getLogger(conexion.class.getName()).log(Level.SEVERE, null, ex);
        }
        return ID;
    }
    
    public int insertarReferencia(String referencia, LinkedList<TelefonoReferencia> telefonos) throws SQLException {
        String sql = "INSERT INTO referencia_alumno_docente_piloto\n"
                + "(\n"
                + "  IDReferencia\n"
                + " ,NombreCompletoReferencia\n"
                + ")\n"
                + "VALUES\n"
                + "(?,?)";
        Connection con = conexion.conectar(conection);
        PreparedStatement pst;
        pst = con.prepareStatement(sql);

        pst.setObject(1, null);
        pst.setString(2, referencia);
        pst.executeUpdate();

        pst.close();

        close(con);
        int idReferencia = getIDReferencia(referencia);
        try {
            for (TelefonoReferencia telefono : telefonos) {
                insertarTelefonoReferencia(idReferencia, telefono);
            }
        } catch (SQLException sq) {
            throw new SQLException("Error al insertar telefonos");
        }
        return idReferencia;

    }

    private void insertarTelefonoReferencia(int idReferencia, TelefonoReferencia telRef) throws SQLException {
        String sql = "INSERT INTO telefonoreferencia\n"
                + "(\n"
                + "  telefonoReferencia\n"
                + " ,idReferencia\n"
                + " ,lugarReferencia\n"
                + ")\n"
                + "VALUES\n"
                + "(?,?,?)";
        Connection con = conexion.conectar(conection);
        PreparedStatement pst;
        pst = con.prepareStatement(sql);

        pst.setInt(1, telRef.getNumTelefono());
        pst.setInt(2, idReferencia);
        pst.setString(3, telRef.getLocalizacion());
        pst.executeUpdate();
        System.out.print("hola2222");

        pst.close();

        close(con);
    }

    public void insertarUsuario(String username, String password, String owner,
            int rol, int createdby) throws SQLException {
        String query = ("INSERT INTO usuarios\n"
                + "(\n"
                + "  IDUser\n"
                + " ,UserName\n"
                + " ,UserPassword\n"
                + " ,NombreOwner\n"
                + " ,IDRol\n"
                + " ,CreatedBy\n"
                + " ,isDeleted\n"
                + ")\n"
                + "VALUES\n"
                + "(?,?,?,?,?,?,?)");
        Connection con = conexion.conectar(conection);
        PreparedStatement pst;
        pst = con.prepareStatement(query);
        pst.setObject(1, null);
        pst.setString(2, username);
        pst.setString(3, password);
        pst.setString(4, owner);
        pst.setInt(5, rol);
        pst.setInt(6, createdby);
        pst.setInt(7, 0);

        pst.executeUpdate();
        pst.close();

        close(con);
        int idUsuario = getIDGenerated("usuarios", "IDUser");
        try {
            loger("logusuarios", "IDUsuario", idUsuario, createdby, "created");
        } catch (SQLException sq) {
            throw new SQLException("Error al insertar el Log " + sq.getMessage());
        }

    }

    public void llenarListaReferencia(LinkedList<TelefonoReferencia> telefonos, int IDReferencia) throws SQLException {
        String sql = "SELECT tr.telefonoReferencia, tr.lugarReferencia\n"
                + "FROM telefonoreferencia tr\n"
                + "WHERE tr.idReferencia = " + IDReferencia;
        Connection con = conexion.conectar(conection);
        Statement st = con.createStatement();
        ResultSet rs = st.executeQuery(sql);
        while (rs.next()) {
            int telefono = rs.getInt(1);
            String lugar = rs.getString(2);
            TelefonoReferencia aux = new TelefonoReferencia(telefono, lugar);
            telefonos.addLast(aux);
        }
    }
//"logalumnopilotodocente", "IDAlumnoPilotoDocente", p.getiD(), IDpersona, "modificated"
    private void loger(String tabla, String IDTabla, int Idtabla, int IdUser, String accion) throws SQLException {
        int resultado = 0;
        String query = ("INSERT INTO " + tabla + "\n"
                + "( IDLog\n"
                + " ," + IDTabla + "\n"
                + " ,UpdatedAt\n"
                + " ,UpdatedBy\n"
                + " ,accion"
                + ")\n"
                + "VALUES\n"
                + "(?,?,?,?,?)");
        Connection con = conexion.conectar(conection);
        PreparedStatement pst;
        pst = con.prepareStatement(query);

        pst.setObject(1, null);
        pst.setInt(2, Idtabla);
        pst.setString(3, FechaActual());
        pst.setInt(4, IdUser);
        pst.setString(5, accion);
        pst.executeUpdate();
        pst.close();

        close(con);
    }
    
    
    
    public void eliminarReferencias(int IDPersona) throws SQLException
    {
        String query = ("DELETE FROM alumno_docente_piloto_referencia \n" +
                        "WHERE alumno_docente_piloto_referencia.IDAlumno_docente_piloto = ?");
        Connection con = conexion.conectar(conection);
        PreparedStatement pst;
        pst = con.prepareStatement(query);
        pst.setInt(1, IDPersona);
        pst.executeUpdate();
        pst.close();

        close(con);
    }
        
    
    public void modificarAlumno_docente_piloto(Persona p, LinkedList<Referencia> referencias,
            int IDpersona,String FotoPath) throws SQLException, FileNotFoundException {
        p.mostrarPersona();
        String query = ("UPDATE  alumno_docente_piloto\n"
                + " SET \n" //
                + " IDMalla = ?\n"//1
                + " ,Nombre = ?\n"//2
                + " ,ApellidoPaterno = ?\n"//3
                + " ,ApellidoMaterno= ?\n"//4
                + " ,IDDocumento= ?\n"//5
                + " ,NumeroDocumento= ?\n"//6
                + " ,NroCredencialCorporativo= ?\n"//7
                + " ,NroCredencialSabsa= ?\n"//8
                + " ,NumFolio= ?\n"//9
                + " ,NumLicencia= ?\n"//10
                + " ,isPiloto= ?\n"//11
                + " ,IsDocente= ?\n"//12
                + " ,UsaLentes= ?\n"//13
                + " ,Direccion= ?\n"//14
                + " ,TipoSangre= ?\n"//15
                + " ,FechaNacimiento= ?\n"//16
                + " ,Foto= ?\n"//17

                + " ,FechaVigenciaSabsa= ?\n"//18
                + " ,FechaVigenciaCorporativo= ?\n"//19
                + " ,FechaVencimientoAptoMedico= ?\n"//20
                + " ,FechaVencimientoProficciencyCheck= ?\n"//21"
                + " WHERE IDPersona = ?\n");//22
        
        Connection con = conexion.conectar(conection);
        PreparedStatement pst;
        pst = con.prepareStatement(query);
        String nombreCompleto = "";

        if (p.getiDMalla() == -1) //idMalla
        {
            pst.setObject(1, null);
        } else {
            pst.setInt(1, p.getiDMalla());
        }
        pst.setString(2, p.getNombre()); //nombre
        nombreCompleto += p.getNombre();
        if (p.getApellidoPaterno().isEmpty()) //apellidoPaterno
        {
            pst.setObject(3, null);
        } else {
            pst.setString(3, p.getApellidoPaterno());
            nombreCompleto += " " + p.getApellidoPaterno();
        }
        if (p.getApellidoMaterno().isEmpty()) //apellidoMaterno
        {
            pst.setObject(4, null);
        } else {
            pst.setString(4, p.getApellidoMaterno());
            nombreCompleto += " " + p.getApellidoMaterno();
        }
        pst.setInt(5, p.getTipoDocumento());
        pst.setString(6, p.getCiRucPas());//cirucpas
        if (p.getNumCredicialCoorporativo() == 0) //numcoorp
        {
            pst.setObject(7, null);
        } else {
            pst.setInt(7, p.getNumCredicialCoorporativo());
        }
        if (p.getNumCredencialSABSA() == 0)//numSABSA
        {
            pst.setObject(8, null);
        } else {
            pst.setInt(8, p.getNumCredencialSABSA());
        }
        if (p.getNumFolio() == 0)//numfolio
        {
            pst.setObject(9, null);
        } else {
            pst.setInt(9, p.getNumFolio());
        }
        if (p.getNumLicencia() == 0)//numLicencia
        {
            pst.setObject(10, null);
        } else {
            pst.setInt(10, p.getNumLicencia());
        }
        pst.setInt(11, p.getIsPiloto());//ispiloto
        pst.setInt(12, p.getIsDocente());//isdocente
        pst.setInt(13, p.getUsaLentes());//usalentes

        pst.setString(14, p.getDireccion());//direccion
        pst.setString(15, p.getTipoSangra());//tiposangre
        pst.setString(16, p.getFechaNacimiento());//fechanacimiento

        InputStream is = null;
        if(FotoPath!=null)
        {
            is = new FileInputStream(new File(FotoPath));
            
        }
        pst.setBlob(17,is);
        //foto

        if (p.getFechaVigenciaSABSA().isEmpty())//SABSA
        {
            pst.setObject(18, null);
        } else {
            pst.setString(18, p.getFechaVigenciaSABSA());
        }
        if (p.getFechaVigenciaCoorporativo().isEmpty())//corporativo
        {
            pst.setObject(19, null);
        } else {
            pst.setString(19, p.getFechaVigenciaCoorporativo());
        }
        if (p.getFechaVencimientoAptoMedico().isEmpty())//aptoMedico
        {
            pst.setObject(20, null);
        } else {
            pst.setString(20, p.getFechaVencimientoAptoMedico());
        }
        if (p.getFechaVencimientoProficienceCheck().isEmpty())//profiencycheck
        {
            pst.setObject(21, null);
        } else {
            pst.setString(21, p.getFechaVencimientoProficienceCheck());
        }
        pst.setInt(22,p.getiD());
        pst.executeUpdate();
        pst.close();
        close(con);

        try {
            eliminarReferencias(p.getiD());
            for (Referencia referencia : referencias) {
                if(referencia.getIdReferencia() == 0)
                {
                    System.out.println(referencia.getTelefono().get(0));
                    insertarReferencia(referencia.getNombre(), referencia.getTelefono());
                    int idReferenciaCreada = getIDGenerated("referencia_alumno_docente_piloto", "IDReferencia");
                    referencia.setIdReferencia(idReferenciaCreada);
                    insertarAlumno_docente_piloto_referencia(p.getiD(), referencia);
                }
                else
                {
                    insertarAlumno_docente_piloto_referencia(p.getiD(), referencia);
                }
                
            }
        } catch (SQLException sq) {
            throw new SQLException("Error al actualizar referencias");
        }
        try {
            loger("logalumnopilotodocente", "IDAlumnoPilotoDocente", p.getiD(), IDpersona, "modificated");
        } catch (SQLException sq) {
            throw new SQLException("Error al insertar el log");
        }

        //actualizar Referencias
        //actualizar logs
    }
    
    public void modificarAlumno_docente_piloto(Persona p, LinkedList<Referencia> referencias,
            int IDpersona) throws SQLException, FileNotFoundException {
        String query = ("UPDATE  alumno_docente_piloto\n"
                + " SET \n" //
                + " IDMalla = ?\n"//1
                + " ,Nombre = ?\n"//2
                + " ,ApellidoPaterno = ?\n"//3
                + " ,ApellidoMaterno= ?\n"//4
                + " ,IDDocumento= ?\n"//5
                + " ,NumeroDocumento= ?\n"//6
                + " ,NroCredencialCorporativo= ?\n"//7
                + " ,NroCredencialSabsa= ?\n"//8
                + " ,NumFolio= ?\n"//9
                + " ,NumLicencia= ?\n"//10
                + " ,isPiloto= ?\n"//11
                + " ,IsDocente= ?\n"//12
                + " ,UsaLentes= ?\n"//13
                + " ,Direccion= ?\n"//14
                + " ,TipoSangre= ?\n"//15
                + " ,FechaNacimiento= ?\n"//16

                + " ,FechaVigenciaSabsa= ?\n"//17
                + " ,FechaVigenciaCorporativo= ?\n"//18
                + " ,FechaVencimientoAptoMedico= ?\n"//19
                + " ,FechaVencimientoProficciencyCheck= ?\n"//20"
                + " WHERE IDPersona = ?\n");//21
        Connection con = conexion.conectar(conection);
        PreparedStatement pst;
        pst = con.prepareStatement(query);
        String nombreCompleto = "";

        if (p.getiDMalla() == -1) //idMalla
        {
            pst.setObject(1, null);
        } else {
            pst.setInt(1, p.getiDMalla());
        }
        pst.setString(2, p.getNombre()); //nombre
        nombreCompleto += p.getNombre();
        if (p.getApellidoPaterno().isEmpty()) //apellidoPaterno
        {
            pst.setObject(3, null);
        } else {
            pst.setString(3, p.getApellidoPaterno());
            nombreCompleto += " " + p.getApellidoPaterno();
        }
        if (p.getApellidoMaterno().isEmpty()) //apellidoMaterno
        {
            pst.setObject(4, null);
        } else {
            pst.setString(4, p.getApellidoMaterno());
            nombreCompleto += " " + p.getApellidoMaterno();
        }
        pst.setInt(5, p.getTipoDocumento());
        pst.setString(6, p.getCiRucPas());//cirucpas
        if (p.getNumCredicialCoorporativo() == 0) //numcoorp
        {
            pst.setObject(7, null);
        } else {
            pst.setInt(7, p.getNumCredicialCoorporativo());
        }
        if (p.getNumCredencialSABSA() == 0)//numSABSA
        {
            pst.setObject(8, null);
        } else {
            pst.setInt(8, p.getNumCredencialSABSA());
        }
        if (p.getNumFolio() == 0)//numfolio
        {
            pst.setObject(9, null);
        } else {
            pst.setInt(9, p.getNumFolio());
        }
        if (p.getNumLicencia() == 0)//numLicencia
        {
            pst.setObject(10, null);
        } else {
            pst.setInt(10, p.getNumLicencia());
        }
        pst.setInt(11, p.getIsPiloto());//ispiloto
        pst.setInt(12, p.getIsDocente());//isdocente
        pst.setInt(13, p.getUsaLentes());//usalentes

        pst.setString(14, p.getDireccion());//direccion
        pst.setString(15, p.getTipoSangra());//tiposangre
        pst.setString(16, p.getFechaNacimiento());//fechanacimiento

        
        
       
       

        if (p.getFechaVigenciaSABSA().isEmpty())//SABSA
        {
            pst.setObject(17, null);
        } else {
            pst.setString(17, p.getFechaVigenciaSABSA());
        }
        if (p.getFechaVigenciaCoorporativo().isEmpty())//corporativo
        {
            pst.setObject(18, null);
        } else {
            pst.setString(18, p.getFechaVigenciaCoorporativo());
        }
        if (p.getFechaVencimientoAptoMedico().isEmpty())//aptoMedico
        {
            pst.setObject(19, null);
        } else {
            pst.setString(19, p.getFechaVencimientoAptoMedico());
        }
        if (p.getFechaVencimientoProficienceCheck().isEmpty())//profiencycheck
        {
            pst.setObject(20, null);
        } else {
            pst.setString(20, p.getFechaVencimientoProficienceCheck());
        }
        pst.setInt(21, p.getiD());
        System.out.println(pst);
        pst.executeUpdate();
        pst.close();
        close(con);

        try {
            eliminarReferencias(p.getiD());
            for (Referencia referencia : referencias) {
                insertarAlumno_docente_piloto_referencia(p.getiD(), referencia);
            }
        } catch (SQLException sq) {
            throw new SQLException("Error al actualizar referencias");
        }
        try {
            loger("logalumnopilotodocente", "IDAlumnoPilotoDocente", p.getiD(), IDpersona, "modificated");
        } catch (SQLException sq) {
            throw new SQLException("Error al insertar el log "+ sq.getMessage());
        }

        //actualizar Referencias
        //actualizar logs
    }

    private void actualizarReferenciaTelefonos(Referencia referencia) throws SQLException {
        try {
            String query = ("UPDATE referencia_alumno_docente_piloto\n"
                    + "SET NombreCompletoReferencia = ? \n"
                    + "WHERE IDReferencia = ? ");
            Connection con = conexion.conectar(conection);
            PreparedStatement pst;
            pst = con.prepareStatement(query);

            pst.setString(1, referencia.getNombre());
            pst.setInt(2, referencia.getIdReferencia());
            pst.executeUpdate();
            pst.close();

            close(con);
            removerTelefonos(referencia.getIdReferencia());
            try {
                for (TelefonoReferencia telefono : referencia.getTelefono()) {
                    insertarTelefonoReferencia(referencia.getIdReferencia(), telefono);
                }
            } catch (SQLException sq) {
                throw new SQLException("Error al insertar telefonos");
            }

        } catch (SQLException e) {
            throw new SQLException("Error al borrar Dependencias");
        }
    }

    private void removerTelefonos(int IDReferencia) throws SQLException {
        String query = ("DELETE FROM telefonoreferencia\n"
                + "WHERE idReferencia = ? ");
        Connection con = conexion.conectar(conection);
        PreparedStatement pst;
        pst = con.prepareStatement(query);

        pst.setInt(1, IDReferencia);

        pst.executeUpdate();
        pst.close();

        close(con);
    }

    public void modificarUsuario(Usuario usuario, int modificatedby) throws SQLException {

        Connection con = conexion.conectar(conection);

        PreparedStatement pstm = con.prepareStatement("update usuarios "
                + "set UserName= ?  ,  "
                + "UserPassword= ? , "
                + "NombreOwner= ? , "
                + "IDRol= ? \n"
                + "where IDUser= ? ");
        pstm.setString(1, usuario.getNombreUsuario());
        pstm.setString(2, usuario.getContrasena());
        pstm.setString(3, usuario.getNombre());
        pstm.setInt(4, usuario.getRol());
        pstm.setInt(5, usuario.getIdUsuario());

        pstm.executeUpdate();
        close(con);

        try {
            loger("logusuarios", "IDUsuario", usuario.getIdUsuario(), modificatedby, "modificated");
        } catch (SQLException sq) {
            throw new SQLException("Error al insertar el Log " + sq.getMessage());
        }
    }

    public void eliminarUsuario(Usuario usuario, int borrador) throws SQLException {

        Connection con = conexion.conectar(conection);

        PreparedStatement pstm = con.prepareStatement("UPDATE usuarios\n"
                + "SET isDeleted = ?\n"
                + "WHERE IDUser = ?");
        pstm.setInt(1, 1);
        pstm.setInt(2, usuario.getIdUsuario());
        pstm.executeUpdate();
        close(con);
        System.out.println("bb");
        try {
            loger("logusuarios", "IDUsuario", usuario.getIdUsuario(), borrador, "eliminado");
        } catch (SQLException sq) {
            throw new SQLException("Error al insertar el Log " + sq.getMessage());
        }
    }

    public void recuperarReferencias(int iDPersona, LinkedList<Referencia> referencias) throws SQLException {
        referencias.clear();

        String sql = "SELECT adpr.IDReferencia,adpr.parentescoreferencia, radp.NombreCompletoReferencia\n"
                + "FROM alumno_docente_piloto_referencia adpr, referencia_alumno_docente_piloto radp\n"
                + "WHERE adpr.IDAlumno_docente_piloto = " + iDPersona + " AND adpr.IDReferencia =radp.IDReferencia";
        Connection con = conexion.conectar(conection);
        Statement st = con.createStatement();
        ResultSet rs = st.executeQuery(sql);
        while (rs.next()) {
            LinkedList<TelefonoReferencia> telRef = new LinkedList<>();
            int idRef = rs.getInt(1);
            String parentesco = rs.getString(2);
            String nombreReferencia = rs.getString(3);
            llenarListaReferencia(telRef, idRef);
            Referencia aux = new Referencia(idRef, nombreReferencia, telRef);
            aux.setParentesco(parentesco);
            referencias.addLast(aux);
        }
        close(con);
    }

    private String FechaActual() {
        java.util.Date fechaActual = new java.util.Date();
        SimpleDateFormat formato = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
        return formato.format(fechaActual);
    }   
    private String FechaActual2() {
        java.util.Date fechaActual = new java.util.Date();
        SimpleDateFormat formato = new SimpleDateFormat("yyyy/MM/dd");
        return formato.format(fechaActual);
    }

    public void login(int IDUser) throws SQLException {
        String query = ("INSERT INTO logindresousuarios\n"
                + "( IDlog\n"
                + " ,IDUser\n"
                + " ,fechaIngreso\n"
                + ")\n"
                + "VALUES\n"
                + "(?,?,?)");
        Connection con = conexion.conectar(conection);
        PreparedStatement pst;
        pst = con.prepareStatement(query);

        pst.setObject(1, null);
        pst.setInt(2, IDUser);
        pst.setString(3, FechaActual());

        pst.executeUpdate();
        pst.close();

        close(con);
    }

    public void modificarMateria(Materia aux, int IDUser) throws SQLException {
        //eliminarInternosMateria(aux);
        String query = ("UPDATE materia\n"
                + "set Materia = ?  ,  "
                + "CostoCreditos = ? , "
                + "TotalMinutos = ? \n"
                + "WHERE IDMateria = ? ");
        Connection con = conexion.conectar(conection);
        PreparedStatement pst;
        pst = con.prepareStatement(query);

        pst.setString(1, aux.getNombre());
        pst.setFloat(2, aux.getCostoCreditos());
        pst.setInt(3, aux.getTotalMinutos());
        pst.setInt(4, aux.getIDMateria());

        pst.executeUpdate();
        pst.close();

        close(con);
        try {
            loger("logmateria", "IDMateria", aux.getIDMateria(), IDUser, "modificated");
        } catch (SQLException sq) {
            throw new SQLException("Error al insertar el Log");
        }
    }

    /*private void eliminarInternosMateria(Materia materia) throws SQLException {
        try {
            String query = ("DELETE FROM dependenciasmateria\n"
                    + "WHERE IDMateria = ? ");
            Connection con = conexion.conectar(conection);
            PreparedStatement pst;
            pst = con.prepareStatement(query);

            pst.setInt(1, materia.getIDMateria());
            pst.executeUpdate();
            pst.close();

            close(con);
        } catch (SQLException e) {
            throw new SQLException("Error al borrar Dependencias");
        }
    }*/
    
    public void limpiarAlertas(){
        try {
            String sql = ("DELETE FROM logalertaavion;");
            Connection con = conexion.conectar(conection);
            PreparedStatement pstm = con.prepareStatement(sql);
            pstm.executeUpdate();
            con.close();
            sql = ("DELETE FROM logalertapersona;");
            con = conexion.conectar(conection);
            pstm = con.prepareStatement(sql);
            pstm.executeUpdate();
            con.close();
            sql = ("DELETE FROM alertaavion;");
            con = conexion.conectar(conection);
            pstm = con.prepareStatement(sql);
            pstm.executeUpdate();
            con.close();
            sql = ("DELETE FROM alertapersona;");
            con = conexion.conectar(conection);
            pstm = con.prepareStatement(sql);
            pstm.executeUpdate();
            con.close();
            sql = ("ALTER TABLE logalertaavion AUTO_INCREMENT = 1;");
            con = conexion.conectar(conection);
            pstm = con.prepareStatement(sql);
            pstm.executeUpdate();
            con.close();
            sql = ("ALTER TABLE logalertapersona AUTO_INCREMENT = 1;");
            con = conexion.conectar(conection);
            pstm = con.prepareStatement(sql);
            pstm.executeUpdate();
            con.close();
            sql = ("ALTER TABLE alertaavion AUTO_INCREMENT = 1;");
            con = conexion.conectar(conection);
            pstm = con.prepareStatement(sql);
            pstm.executeUpdate();
            con.close();
            sql = ("ALTER TABLE alertapersona AUTO_INCREMENT = 1;");
            con = conexion.conectar(conection);
            pstm = con.prepareStatement(sql);
            pstm.executeUpdate();
            con.close();
        } catch (SQLException ex) {
            Logger.getLogger(conexion.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public DefaultTableModel buscarAlertas(){
        ResultSet variablex = null;
        DefaultTableModel modelo = new DefaultTableModel(){
            public boolean isCellEditable(int row, int column){
                return false;
            }
        };
        try {  
            String sql = ("	(\n" +
                            "		SELECT\n" +
                            "			CONCAT(\n" +
                            "				alumno_docente_piloto.Nombre,\n" +
                            "				\" \",\n" +
                            "				alumno_docente_piloto.ApellidoPaterno,\n" +
                            "				\" \",\n" +
                            "				alumno_docente_piloto.ApellidoMaterno\n" +
                            "			),\n" +
                            "			alertapersona.Descripcion,\n" +
                            "			tipoalerta.Nombre\n" +
                            "		FROM\n" +
                            "			alertapersona,\n" +
                            "			alumno_docente_piloto,\n" +
                            "			tipoalerta\n" +
                            "		WHERE\n" +
                            "			alertapersona.IDPersona = alumno_docente_piloto.IDPersona\n" +
                            "		AND alertapersona.IDTipoAlerta = tipoalerta.IDTipoAlerta\n" +
                            "	) UNION\n" +
                            "	(\n" +
                            "		SELECT\n" +
                            "			CONCAT(\n" +
                            "				avion.Marca,\n" +
                            "				\"-\",\n" +
                            "				avion.Modelo,\n" +
                            "				\"-\",\n" +
                            "				avion.NumeroDeSerie\n" +
                            "			),\n" +
                            "			alertaavion.Descripcion,\n" +
                            "			tipoalerta.Nombre\n" +
                            "		FROM\n" +
                            "			alertaavion,\n" +
                            "			avion,\n" +
                            "			tipoalerta\n" +
                            "		WHERE\n" +
                            "			alertaavion.IDAvion = avion.IDAvion\n" +
                            "		AND alertaavion.IDTipoAlerta = tipoalerta.IDTipoAlerta\n" +
                            "	)");
            Connection con = conexion.conectar(conection);
            Statement st = con.createStatement();
            variablex = st.executeQuery(sql);
            modelo.setColumnIdentifiers(new Object[]{"Nombre","Descripcion","Tipo"});
            while(variablex.next()){
                modelo.addRow(new Object[]{variablex.getString(1),variablex.getString(2),variablex.getString(3)});
            }
            con.close();     
        } catch (SQLException ex) {
            Logger.getLogger(conexion.class.getName()).log(Level.SEVERE, null, ex);
        }
        return modelo;
    }
    
    public void controlAlertasADP(){
        Calendar weeks = Calendar.getInstance();
        ResultSet variablex = null;
        try {  
            String sql = ("SELECT\n" +
                            "	alumno_docente_piloto.IDPersona,\n" +
                            "	alumno_docente_piloto.FechaVencimientoAptoMedico,\n" +
                            "	alumno_docente_piloto.FechaVencimientoProficciencyCheck,\n" +
                            "	alumno_docente_piloto.FechaVigenciaCorporativo,\n" +
                            "	alumno_docente_piloto.FechaVigenciaSabsa\n" +
                            "FROM\n" +
                            "	alumno_docente_piloto");
            Connection con = conexion.conectar(conection);
            Statement st = con.createStatement();
            variablex = st.executeQuery(sql);
            while(variablex.next()){
                if(variablex.getDate(2) != null){
                    compareDate(weeks, variablex.getDate(2), "Debe renovar Certificado Medico", variablex.getInt(1), "persona2");
                }
                if(variablex.getDate(3) != null){
                    compareDate(weeks, variablex.getDate(3), "Debe renovar Proficiency Check", variablex.getInt(1), "persona2");
                }
                if(variablex.getDate(4) != null){
                    compareDate(weeks, variablex.getDate(4), "Debe renovar Credencial Corporativa", variablex.getInt(1), "persona2");
                }
                if(variablex.getDate(5) != null){
                    compareDate(weeks, variablex.getDate(5), "Debe renovar Credencial SABSA", variablex.getInt(1), "persona2");
                }
            }
            con.close();     
        } catch (SQLException ex) {
            Logger.getLogger(conexion.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public void controlAlertasDeudas(){
        try {  
            String sql = ("SELECT\n" +
                            "	SUM( pago.CantidadPagada ),\n" +
                            "	alumnos.IDPersona \n" +
                            "FROM\n" +
                            "	alumnos\n" +
                            "LEFT JOIN pago\n" +
                            "	ON alumnos.IDPersona = pago.IDAlumno\n" +
                            "GROUP BY\n" +
                            "	alumnos.IDPersona");
            Connection con = conexion.conectar(conection);
            Statement st = con.createStatement();
            ResultSet rs = st.executeQuery(sql);
            while(rs.next()){
                String sql2 = ("SELECT\n" +
                                "	SUM( materia.CostoCreditos ) AS 'Costo Total' \n" +
                                "FROM\n" +
                                "	grupodisponibles,\n" +
                                "	alumno_grupoinscrito,\n" +
                                "	materia \n" +
                                "WHERE\n" +
                                "	materia.IDMateria = grupodisponibles.IDMateria \n" +
                                "	AND grupodisponibles.IDGrupo = alumno_grupoinscrito.IDGrupoDisponible \n" +
                                "	AND alumno_grupoinscrito.IDAlumno = "+rs.getInt(2));
                Statement st2 = con.createStatement();
                ResultSet rs2 = st2.executeQuery(sql2);
                while(rs2.next())
                {
                    int pagado = 0;
                    if(rs.getInt(1)!=0)
                    {
                        pagado = rs.getInt(1);
                    }
                    if(rs.getInt(1) < rs2.getInt(1)){
                        int deuda = rs2.getInt(1) - rs.getInt(1);
                        insertarAlertasPersona(rs.getInt(2),"persona1", "Debe: "+deuda+"Bs.");
                    }
                }
            }
            con.close();
        } catch (SQLException ex) {
            Logger.getLogger(conexion.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public void controlAlertasAvion(){
        Calendar weeks = Calendar.getInstance();
        ResultSet variablex = null;
        try {  
            String sql = ("SELECT\n" +
                            "	avion.FechaVencimientoCertificadoAeronavegabilidad,\n" +
                            "	avion.FechaVencimientoEquipoEmergencia,\n" +
                            "	avion.FechaVencimientoExtintor,\n" +
                            "	avion.FechaVencimientoRegistroMatricula,\n" +
                            "	avion.FechaVencimientoSeguro,\n" +
                            "	avion.IDAvion\n" +
                            "FROM\n" +
                            "	avion");
            Connection con = conexion.conectar(conection);
            Statement st = con.createStatement();
            variablex = st.executeQuery(sql);
            while(variablex.next()){
                if(variablex.getDate(1) != null){
                    compareDate(weeks, variablex.getDate(1), "Debe renovar Certificado de Aereonavegabilidad", variablex.getInt(6), "avion");
                }
                if(variablex.getDate(2) != null){
                    compareDate(weeks, variablex.getDate(2), "Debe renovar Equipo de Emergencia", variablex.getInt(6), "avion");
                }
                if(variablex.getDate(3) != null){
                    compareDate(weeks, variablex.getDate(3), "Debe renovar el Extintor", variablex.getInt(6), "avion");
                }
                if(variablex.getDate(4) != null){
                    compareDate(weeks, variablex.getDate(4), "Debe renovar Registro de Matricula", variablex.getInt(6), "avion");
                }
                if(variablex.getDate(5) != null){
                    compareDate(weeks, variablex.getDate(5), "Debe renovar Seguro", variablex.getInt(6), "avion");
                }
            }
            con.close();     
        } catch (SQLException ex) {
            Logger.getLogger(conexion.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    private void buscarMateriaDependencia(int id, Hashtable<Integer,Materia> dependencias) throws SQLException {
        String sql = "SELECT dm.IDMateriaAnt\n"
                + "FROM dependenciasmateria dm\n"
                + "WHERE dm.IDMateria = " + id;
        Connection con = conexion.conectar(conection);
        Statement st = con.createStatement();
        ResultSet rs = st.executeQuery(sql);
        while (rs.next()) {
            int idMateriaDependencia = rs.getInt(1);
            Materia aux = buscarMateria(idMateriaDependencia);
            if (aux != null) {
                dependencias.put(intaux,aux);
            }
            intaux++;
        }

    }

    
    public void compareDate(Calendar date1, Date aux, String caso, int id, String tabla){
        Date date = new Date();
        date1.setTime(aux);
        date1.add(Calendar.WEEK_OF_YEAR, -3);
        if(date.after(date1.getTime())){   
            if(tabla.equals("persona1") || tabla.equals("persona2")){
                try {
                    insertarAlertasPersona(id, tabla, caso);
                } catch (SQLException ex) {
                    Logger.getLogger(conexion.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
            else{
                try {
                    insertarAlertasAvion(id, tabla, caso);
                } catch (SQLException ex) {
                    Logger.getLogger(conexion.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
    }
    
    public void insertarAlertasPersona(int id, String tabla, String caso) throws SQLException {
        String query = ("INSERT INTO alertapersona (\n" +
                        "	IDAlerta,\n" +
                        "	IDPersona,\n" +
                        "	Descripcion,\n" +
                        "	IDTipoAlerta,\n" +
                        "	CreatedBy\n" +
                        ")\n" +
                        "VALUES\n" +
                        "	(?,?,?,?,?)");

        Connection con = conexion.conectar(conection);
        PreparedStatement pst;
        pst = con.prepareStatement(query);
        pst.setObject(1, null);
        
        pst.setInt(2, id);
        
        pst.setString(3, caso);
        
        if(tabla.equals("persona1")){
            pst.setInt(4, 1);
        }
        if(tabla.equals("persona2")){
            pst.setInt(4, 2);
        }
        
        pst.setInt(5, 1);
        
        pst.executeUpdate();

        pst.close();

        close(con);
        int idAlertas = getIDGenerated("alertapersona", "IDAlerta");
        try {
            loger("logalertapersona", "IDAlertaPersona",idAlertas ,1 , "created");
        } catch (SQLException sq) {
            throw new SQLException("Error al insertar el Log" + sq);
        }
    }
    
    public void insertarAlertasAvion(int id, String tabla, String caso) throws SQLException {
        String query = ("INSERT INTO alertaavion (\n" +
                        "	IDAlerta,\n" +
                        "	IDAvion,\n" +
                        "	Descripcion,\n" +
                        "	IDTipoAlerta,\n" +
                        "	CreatedBy\n" +
                        ")\n" +
                        "VALUES\n" +
                        "	(?,?,?,?,?)");

        Connection con = conexion.conectar(conection);
        PreparedStatement pst;
        pst = con.prepareStatement(query);
        pst.setObject(1, null);
        
        pst.setInt(2, id);
        
        pst.setString(3, caso);
        
        if(tabla.equals("avion")){
            pst.setInt(4, 3);
        }
        
        pst.setInt(5, 1);
        
        pst.executeUpdate();

        pst.close();

        close(con);

        int idAlertas = getIDGenerated("alertaavion", "IDAlerta");
        try {
            loger("logalertaavion", "IDAlertaAvion",idAlertas ,1 , "created");
        } catch (SQLException sq) {
            throw new SQLException("Error al insertar el Log");
        }
    }
    
    public DefaultTableModel buscarAlertasEconomicas(){
        ResultSet variablex = null;
        DefaultTableModel modelo = new DefaultTableModel(){
            public boolean isCellEditable(int row, int column){
                return false;
            }
        };
        try {  
            String sql = ("SELECT CONCAT(alumno_docente_piloto.Nombre,\" \",alumno_docente_piloto.ApellidoPaterno,\" \",alumno_docente_piloto.ApellidoMaterno), alertapersona.Descripcion, tipoalerta.Nombre\n" +
                            "FROM alertapersona, alumno_docente_piloto, tipoalerta\n" +
                            "WHERE alertapersona.IDPersona = alumno_docente_piloto.IDPersona AND alertapersona.IDTipoAlerta = tipoalerta.IDTipoAlerta AND tipoalerta.Nombre = \"Economicas\"");
            Connection con = conexion.conectar(conection);
            Statement st = con.createStatement();
            variablex = st.executeQuery(sql);
            modelo.setColumnIdentifiers(new Object[]{"Nombre de Alumno","Descripcion","Tipo"});
            while(variablex.next()){
                modelo.addRow(new Object[]{variablex.getString(1),variablex.getString(2),variablex.getString(3)});
            }
            con.close();     
        } catch (SQLException ex) {
            Logger.getLogger(conexion.class.getName()).log(Level.SEVERE, null, ex);
        }
        return modelo;
    }
    
    public DefaultTableModel buscarAlertasAvion(){
        ResultSet variablex = null;
        DefaultTableModel modelo = new DefaultTableModel(){
            public boolean isCellEditable(int row, int column){
                return false;
            }
        };
        try {  
            String sql = ("SELECT CONCAT(avion.Marca, \"-\", avion.Modelo, \"-\", avion.NumeroDeSerie ), alertaavion.Descripcion, tipoalerta.Nombre\n" +
                            "FROM alertaavion, avion, tipoalerta\n" +
                            "WHERE alertaavion.IDAvion = avion.IDAvion AND alertaavion.IDTipoAlerta = tipoalerta.IDTipoAlerta AND tipoalerta.Nombre = \"Avion\"");
            Connection con = conexion.conectar(conection);
            Statement st = con.createStatement();
            variablex = st.executeQuery(sql);
            modelo.setColumnIdentifiers(new Object[]{"Nombre de Avion","Descripcion","Tipo"});
            while(variablex.next()){
                modelo.addRow(new Object[]{variablex.getString(1),variablex.getString(2),variablex.getString(3)});
            }
            con.close();     
        } catch (SQLException ex) {
            Logger.getLogger(conexion.class.getName()).log(Level.SEVERE, null, ex);
        }
        return modelo;
    }
    
    public DefaultTableModel buscarAlertasVencimiento(){
        ResultSet variablex = null;
        DefaultTableModel modelo = new DefaultTableModel(){
            public boolean isCellEditable(int row, int column){
                return false;
            }
        };
        try {  
            String sql = ("SELECT CONCAT(alumno_docente_piloto.Nombre,\" \",alumno_docente_piloto.ApellidoPaterno,\" \",alumno_docente_piloto.ApellidoMaterno), alertapersona.Descripcion, tipoalerta.Nombre\n" +
                            "FROM alertapersona, alumno_docente_piloto, tipoalerta\n" +
                            "WHERE alertapersona.IDPersona = alumno_docente_piloto.IDPersona AND alertapersona.IDTipoAlerta = tipoalerta.IDTipoAlerta AND tipoalerta.Nombre = \"Vencimiento\"");
            Connection con = conexion.conectar(conection);
            Statement st = con.createStatement();
            variablex = st.executeQuery(sql);
            modelo.setColumnIdentifiers(new Object[]{"Nombre de Alumno","Descripcion","Tipo"});
            while(variablex.next()){
                modelo.addRow(new Object[]{variablex.getString(1),variablex.getString(2),variablex.getString(3)});
            }
            con.close();     
        } catch (SQLException ex) {
            Logger.getLogger(conexion.class.getName()).log(Level.SEVERE, null, ex);
        }
        return modelo;
    }

    /*private void buscarMateriaDependencia(int id, LinkedList<Materia> dependencias) throws SQLException {
        String sql = "SELECT dm.IDMateriaAnt\n"
                + "FROM dependenciasmateria dm\n"
                + "WHERE dm.IDMateria = " + id;
        Connection con = conexion.conectar(conection);
        Statement st = con.createStatement();
        ResultSet rs = st.executeQuery(sql);
        while (rs.next()) {
            int idMateriaDependencia = rs.getInt(1);
            Materia aux = buscarMateria(idMateriaDependencia);
            if (aux != null) {
                dependencias.put(i,aux);
            }
            i++;
        }
    }*/
    
    public DefaultTableModel cargarAvion(){
        ResultSet variablex = null;
        DefaultTableModel modelo = new DefaultTableModel(){
            public boolean isCellEditable(int row, int column){
                return true;
            }
        };
        try {  
            String sql = ("SELECT avion.Marca, avion.Modelo, avion.NumeroDeSerie\n" +
                            "FROM avion\n" +
                            "WHERE avion.isBorrado = 0");
            Connection con = conexion.conectar(conection);
            Statement st = con.createStatement();
            variablex = st.executeQuery(sql);
            modelo.setColumnIdentifiers(new Object[]{"Marca","Modelo","Numero de Serie","Detalles","Modificar", "Eliminar"});
            while(variablex.next()){
                modelo.addRow(new Object[]{variablex.getString(1),variablex.getString(2),variablex.getString(3), "", "", ""});
            }
            con.close();     
        } catch (SQLException ex) {
            Logger.getLogger(conexion.class.getName()).log(Level.SEVERE, null, ex);
        }
        return modelo;
    }
    
    public DefaultTableModel cargarGrupo() {
        ResultSet variablex = null;
        DefaultTableModel modelo = new DefaultTableModel() {
            public boolean isCellEditable(int row, int column) {
                return true;
            }
        };
        try {
            String sql = ("SELECT NombreGrupo\n"
                    + "FROM grupodisponibles\n"
                    + "WHERE grupodisponibles.isBorrado = 0");
            Connection con = conexion.conectar(conection);
            Statement st = con.createStatement();
            variablex = st.executeQuery(sql);
            modelo.setColumnIdentifiers(new Object[]{"Nombre", "Detalles", "Modificar", "Eliminar"});
            while (variablex.next()) {
                modelo.addRow(new Object[]{variablex.getString(1),"", "", ""});
            }
            con.close();
        } catch (SQLException ex) {
            Logger.getLogger(conexion.class.getName()).log(Level.SEVERE, null, ex);
        }
        return modelo;
    }
    
    public void IngresarHojaABordo(int HorasDeVuelo,int IDPiloto,int IDCopiloto,int IDAvion,String Detalles,int IDUser) throws SQLException{
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
	Date date = new Date();
        String sql = "INSERT INTO hojaabordo(Fecha,HorasDeVuelo,IDPiloto,IDCopiloto,IDAvion,Detalles,CreatedBy)\n"
                + "VALUES ('" + dateFormat.format(date) + "'," + HorasDeVuelo + "," + IDPiloto + ","+IDCopiloto+"," + IDAvion + ",'" + Detalles + "',"+IDUser+")";
        Connection con = conexion.conectar(conection);
        PreparedStatement pstm = con.prepareStatement(sql);
        pstm.executeUpdate();
        pstm.close();
    }
    
    public DefaultTableModel cargarReservas(){
        String NombreP,NombreE,NombreA;
        ResultSet variablex = null;
        DefaultTableModel modelo = new DefaultTableModel(){
            public boolean isCellEditable(int row, int column){
                return true;
            }
        };
        try {  
            String sql = ("SELECT vuelosregistrados.TipoDeVuelo, vuelosregistrados.IDPiloto, vuelosregistrados.IDEstudiante,vuelosregistrados.IDAvion,vuelosregistrados.HorasDeVuelo\n" +
                            "FROM vuelosregistrados\n" +
                            "WHERE vuelosregistrados.isBorrado = 0");
            Connection con = conexion.conectar(conection);
            Statement st = con.createStatement();
            variablex = st.executeQuery(sql);
            modelo.setColumnIdentifiers(new Object[]{"Tipo de Vuelo","Piloto","Estudiante","Avion","Horas","Confirmar","Cancelar"});
            while(variablex.next()){
                NombreP = ConseguirDocente(variablex.getInt(2));
                NombreE = ConseguirDocente(variablex.getInt(3));
                NombreA = ConseguirAvion(variablex.getInt(4));
                modelo.addRow(new Object[]{variablex.getString(1),NombreP,NombreE,NombreA,variablex.getInt(5),"",""});
            }
            con.close();     
        } catch (SQLException ex) {
            Logger.getLogger(conexion.class.getName()).log(Level.SEVERE, null, ex);
        }
        return modelo;
    }
    
    public DefaultTableModel cargarMotor(){
        ResultSet variablex = null;
        DefaultTableModel modelo = new DefaultTableModel(){
            public boolean isCellEditable(int row, int column){
                return true;
            }
        };
        try {  
            String sql = ("SELECT motor.Marca, motor.Modelo, motor.NumeroSerie\n" +
                            "FROM motor\n" +
                            "WHERE motor.isBorrado = 0");
            Connection con = conexion.conectar(conection);
            Statement st = con.createStatement();
            variablex = st.executeQuery(sql);
            modelo.setColumnIdentifiers(new Object[]{"Marca","Modelo","Numero de Serie","Detalles","Modificar", "Eliminar"});
            while(variablex.next()){
                modelo.addRow(new Object[]{variablex.getString(1),variablex.getString(2),variablex.getString(3), "", "", ""});
            }
            con.close();     
        } catch (SQLException ex) {
            Logger.getLogger(conexion.class.getName()).log(Level.SEVERE, null, ex);
        }
        return modelo;
    }
    
    public DefaultTableModel cargarMalla(){
        ResultSet variablex = null;
        DefaultTableModel modelo = new DefaultTableModel(){
            public boolean isCellEditable(int row, int column){
                return true;
            }
        };
        try {  
            String sql = ("SELECT nombreMalla\n" +
                            "FROM malla\n" +
                            "WHERE malla.isBorrado = 0");
            Connection con = conexion.conectar(conection);
            Statement st = con.createStatement();
            variablex = st.executeQuery(sql);
            modelo.setColumnIdentifiers(new Object[]{"Malla","Detalles","Modificar", "Eliminar"});
            while(variablex.next()){
                modelo.addRow(new Object[]{variablex.getString(1),"", "", ""});
            }
            con.close();     
        } catch (SQLException ex) {
            Logger.getLogger(conexion.class.getName()).log(Level.SEVERE, null, ex);
        }
        return modelo;
    }
    
    public DefaultTableModel cargarHelice(){
        ResultSet variablex = null;
        DefaultTableModel modelo = new DefaultTableModel(){
            public boolean isCellEditable(int row, int column){
                return true;
            }
        };
        try {  
            String sql = ("SELECT helice.Marca, helice.Modelo, helice.NroSerie\n" +
                            "FROM helice\n" +
                            "WHERE helice.isBorrado = 0");
            Connection con = conexion.conectar(conection);
            Statement st = con.createStatement();
            variablex = st.executeQuery(sql);
            modelo.setColumnIdentifiers(new Object[]{"Marca","Modelo","Numero de Serie","Detalles","Modificar", "Eliminar"});
            while(variablex.next()){
                modelo.addRow(new Object[]{variablex.getString(1),variablex.getString(2),variablex.getString(3), "", "", ""});
            }
            con.close();     
        } catch (SQLException ex) {
            Logger.getLogger(conexion.class.getName()).log(Level.SEVERE, null, ex);
        }
        return modelo;
    }
    
    public Vector<String> detallesAvion(int id){
        ResultSet variablex = null;
        Vector<String> modelo = new Vector<>(); 
        
        try {  
            String sql = ("SELECT\n" +
                            "	*\n" +
                            "FROM\n" +
                            "	avion\n" +
                            "WHERE\n" +
                            "	avion.IDAvion = " + id + ";");
            Connection con = conexion.conectar(conection);
            Statement st = con.createStatement();
            variablex = st.executeQuery(sql);
            while(variablex.next()){
                modelo.add(variablex.getString(1));
                modelo.add(variablex.getString(2));
                modelo.add(variablex.getString(4));
                modelo.add(variablex.getString(5));
                modelo.add(variablex.getString(3));
                modelo.add(variablex.getString(6));
                modelo.add(variablex.getString(7));
                modelo.add(variablex.getString(8));
                modelo.add(variablex.getString(9));
                modelo.add(variablex.getString(10));
            }
            sql = ("SELECT\n" +
                    "motor.Marca,\n" +
                    "motor.Modelo,\n" +
                    "motor.NumeroSerie\n" +
                    "FROM\n" +
                    "motor,\n" +
                    "avionmotor,\n" +
                    "avion\n" +
                    "WHERE\n" +
                    "avionmotor.IDAvion = " + id + "\n" +
                    "AND motor.IDMotor = avionmotor.IDMotor\n" +
                    "AND avionmotor.IDAvion = avion.IDAvion");
            con = conexion.conectar(conection);
            st = con.createStatement();
            variablex = st.executeQuery(sql);
            while(variablex.next()){
                modelo.add(variablex.getString(1));
                modelo.add(variablex.getString(2));
                modelo.add(variablex.getString(3));
            }
            sql = ("SELECT\n" +
                    "helice.Marca,\n" +
                    "helice.Modelo,\n" +
                    "helice.NroSerie\n" +
                    "FROM\n" +
                    "motor,\n" +
                    "avionmotor,\n" +
                    "avion,\n" +
                    "helice\n" +
                    "WHERE\n" +
                    "avionmotor.IDAvion = " + id + "\n" +
                    "AND motor.IDMotor = avionmotor.IDMotor\n" +
                    "AND avion.IDAvion = avionmotor.IDAvion\n" +
                    "AND	motor.IDHelice = helice.IDHelice");
            con = conexion.conectar(conection);
            st = con.createStatement();
            variablex = st.executeQuery(sql);
            while(variablex.next()){
                modelo.add(variablex.getString(1));
                modelo.add(variablex.getString(2));
                modelo.add(variablex.getString(3));
            }
            con.close();  
        } catch (SQLException ex) {
            Logger.getLogger(conexion.class.getName()).log(Level.SEVERE, null, ex);
        }
        return modelo;
    }
    
    public Vector<String> detallesMotor(int id){
        ResultSet variablex = null;
        Vector<String> modelo = new Vector<>(); 
        try {  
            String sql = ("SELECT\n" +
                            "	motor.Marca,\n" +
                            "	motor.Modelo,\n" +
                            "	motor.NumeroSerie,\n" +
                            "	motor.TBO,\n" +
                            "	motor.TT,\n" +
                            "	motor.TTO,\n" +
                            "	CONCAT(\n" +
                            "		helice.Marca,\n" +
                            "		\"-\",\n" +
                            "		helice.Modelo,\n" +
                            "		\"-\",\n" +
                            "		helice.NroSerie\n" +
                            "	)\n" +
                            "FROM\n" +
                            "	motor,\n" +
                            "	helice\n" +
                            "WHERE\n" +
                            "	motor.IDMotor = " + id +"\n" +
                            "AND motor.IDHelice = helice.IDHelice");
            Connection con = conexion.conectar(conection);
            Statement st = con.createStatement();
            variablex = st.executeQuery(sql);
            while(variablex.next()){
                modelo.add(variablex.getString(1));
                modelo.add(variablex.getString(2));
                modelo.add(variablex.getString(4));
                modelo.add(variablex.getString(5));
                modelo.add(variablex.getString(3));
                modelo.add(variablex.getString(6));
                modelo.add(variablex.getString(7));
            }
            con.close();  
        } catch (SQLException ex) {
            Logger.getLogger(conexion.class.getName()).log(Level.SEVERE, null, ex);
        }
        return modelo;
    }
    
    public Helices detallesHelice(int id){
        ResultSet variablex = null;
        Helices modelo = null; 
        try {  
            String sql = ("SELECT DISTINCT\n" +
                            "helice.Marca,\n" +
                            "helice.Modelo,\n" +
                            "helice.NroSerie,\n" +
                            "helice.TBO,\n" +
                            "helice.TT,\n" +
                            "helice.TTO\n" +
                            "FROM\n" +
                            "motor,\n" +
                            "helice\n" +
                            " WHERE\n" +
                            "	helice.IDHelice = "+id);
            Connection con = conexion.conectar(conection);
            Statement st = con.createStatement();
            variablex = st.executeQuery(sql);
            while(variablex.next()){
                String marca = (variablex.getString(1));
                String modelo2 = (variablex.getString(2));
                String NroSerie = (variablex.getString(4));
                int TBO = (variablex.getInt(5));
                int TT = (variablex.getInt(3));
                int TTO = (variablex.getInt(6));
                
                modelo = new Helices(0,marca,modelo2,NroSerie,TT,TTO,TBO);
            }
            con.close();  
        } catch (SQLException ex) {
            Logger.getLogger(conexion.class.getName()).log(Level.SEVERE, null, ex);
        }
        return modelo;
    }
    
    
    public Malla detallesMalla(int IDMalla) throws SQLException {
        Malla aux = null;
        
        String sql = "SELECT *\n" +
                    "FROM malla u\n" +
                    "WHERE u.isBorrado = 0 AND u.IDMalla = "+ IDMalla;
        Connection con = conexion.conectar(conection);
        Statement st = con.createStatement();
        ResultSet rs = st.executeQuery(sql);
        
        if (rs.next()) {
            //int idMalla = rs.getInt(1);
            String nombre = rs.getString(2);
            int requiereLic = rs.getInt(3);
            int costo = rs.getInt(4);
            int HV = rs.getInt(5);
            int HS = rs.getInt(6);
            
            System.out.println(nombre);
            System.out.println(requiereLic);
            System.out.println(costo);
            System.out.println(HV);
            System.out.println(HS);
            
            
            aux = new Malla(nombre,requiereLic,costo,HV,HS,0);
            
        }
        close(con);
        return aux;
    }
    
    public Vector<Integer> detallesMallaMaterias(int IDMalla) throws SQLException {
        Vector<Integer> aux = new Vector<Integer>();
        
        String sql = "SELECT IDMateria\n" +
                     "FROM malla_materia\n" +
                     "WHERE IDMalla = "+ IDMalla;
        Connection con = conexion.conectar(conection);
        Statement st = con.createStatement();
        ResultSet rs = st.executeQuery(sql);
        
        while (rs.next()) {
            aux.add(rs.getInt(1));
        }
        close(con);
        return aux;
    }
    
    public void vertablaAvion(JTable tabla, controlador control){
        tabla.setModel(cargarAvion());
        tabla.setRowHeight(30);
        
        if(deltacharlie.DeltaCharlie.IDRol == 1 || deltacharlie.DeltaCharlie.IDRol == 2){
            JButtonRenderer botonDetalles = new JButtonRenderer("Detalles", "Avion", control);
            tabla.getColumnModel().getColumn(3).setCellEditor(botonDetalles);
            tabla.getColumnModel().getColumn(3).setCellRenderer(botonDetalles);

            JButtonRenderer botonModificar = new JButtonRenderer("Modificar", "Avion", control);
            tabla.getColumnModel().getColumn(4).setCellEditor(botonModificar);
            tabla.getColumnModel().getColumn(4).setCellRenderer(botonModificar);

            JButtonRenderer botonEliminar = new JButtonRenderer("Eliminar", "Avion", control);
            tabla.getColumnModel().getColumn(5).setCellEditor(botonEliminar);
            tabla.getColumnModel().getColumn(5).setCellRenderer(botonEliminar);
        }
        if( deltacharlie.DeltaCharlie.IDRol == 3 ){
            JButtonRenderer botonDetalles = new JButtonRenderer("Detalles", "Avion", control);
            tabla.getColumnModel().getColumn(3).setCellEditor(botonDetalles);
            tabla.getColumnModel().getColumn(3).setCellRenderer(botonDetalles);

            JButtonRenderer botonModificar = new JButtonRenderer("Modificar", "Avion", control);
            tabla.getColumnModel().getColumn(4).setCellEditor(botonModificar);
            tabla.getColumnModel().getColumn(4).setCellRenderer(botonModificar);
            
            tabla.removeColumn(tabla.getColumnModel().getColumn(5));
        }
        if(deltacharlie.DeltaCharlie.IDRol == 4){
            JButtonRenderer botonDetalles = new JButtonRenderer("Detalles", "Avion", control);
            tabla.getColumnModel().getColumn(3).setCellEditor(botonDetalles);
            tabla.getColumnModel().getColumn(3).setCellRenderer(botonDetalles);
            
            tabla.removeColumn(tabla.getColumnModel().getColumn(5));
            tabla.removeColumn(tabla.getColumnModel().getColumn(4));
        }
        
    }
    
    public void vertablaGrupo(JTable tabla, controlador control){
        tabla.setModel(cargarGrupo());
        tabla.setRowHeight(30);
        
        if(deltacharlie.DeltaCharlie.IDRol == 1 || deltacharlie.DeltaCharlie.IDRol == 2){
            JButtonRenderer botonDetalles = new JButtonRenderer("Detalles", "Grupo", control);
            tabla.getColumnModel().getColumn(1).setCellEditor(botonDetalles);
            tabla.getColumnModel().getColumn(1).setCellRenderer(botonDetalles);

            JButtonRenderer botonModificar = new JButtonRenderer("Modificar", "Grupo", control);
            tabla.getColumnModel().getColumn(2).setCellEditor(botonModificar);
            tabla.getColumnModel().getColumn(2).setCellRenderer(botonModificar);

            JButtonRenderer botonEliminar = new JButtonRenderer("Eliminar", "Grupo", control);
            tabla.getColumnModel().getColumn(3).setCellEditor(botonEliminar);
            tabla.getColumnModel().getColumn(3).setCellRenderer(botonEliminar);
        }
        if( deltacharlie.DeltaCharlie.IDRol == 3 ){
            JButtonRenderer botonDetalles = new JButtonRenderer("Detalles", "Grupo", control);
            tabla.getColumnModel().getColumn(1).setCellEditor(botonDetalles);
            tabla.getColumnModel().getColumn(1).setCellRenderer(botonDetalles);

            JButtonRenderer botonModificar = new JButtonRenderer("Modificar", "Grupo", control);
            tabla.getColumnModel().getColumn(2).setCellEditor(botonModificar);
            tabla.getColumnModel().getColumn(2).setCellRenderer(botonModificar);
            
            tabla.removeColumn(tabla.getColumnModel().getColumn(3));
        }
        if(deltacharlie.DeltaCharlie.IDRol == 4){
            JButtonRenderer botonDetalles = new JButtonRenderer("Detalles", "Grupo", control);
            tabla.getColumnModel().getColumn(1).setCellEditor(botonDetalles);
            tabla.getColumnModel().getColumn(1).setCellRenderer(botonDetalles);
            
            tabla.removeColumn(tabla.getColumnModel().getColumn(3));
            tabla.removeColumn(tabla.getColumnModel().getColumn(2));
        }
    }
    
    public void vertablaReservas(JTable tabla, controlador control){
        tabla.setModel(cargarReservas());
        tabla.setRowHeight(30);
        
        JButtonRenderer botonRealizar = new JButtonRenderer("Confirmar", "Reservas", control);
        tabla.getColumnModel().getColumn(5).setCellEditor(botonRealizar);
        tabla.getColumnModel().getColumn(5).setCellRenderer(botonRealizar);
        
        JButtonRenderer botonCancelarReserva = new JButtonRenderer("Cancelar", "Reservas", control);
        tabla.getColumnModel().getColumn(6).setCellEditor(botonCancelarReserva);
        tabla.getColumnModel().getColumn(6).setCellRenderer(botonCancelarReserva);
    }
    
    public void vertablaMotor(JTable tabla, controlador control){
        tabla.setModel(cargarMotor());
        tabla.setRowHeight(30);
        
        if(deltacharlie.DeltaCharlie.IDRol == 1 || deltacharlie.DeltaCharlie.IDRol == 2){
            JButtonRenderer botonDetalles = new JButtonRenderer("Detalles", "Motor", control);
            tabla.getColumnModel().getColumn(3).setCellEditor(botonDetalles);
            tabla.getColumnModel().getColumn(3).setCellRenderer(botonDetalles);

            JButtonRenderer botonModificar = new JButtonRenderer("Modificar", "Motor", control);
            tabla.getColumnModel().getColumn(4).setCellEditor(botonModificar);
            tabla.getColumnModel().getColumn(4).setCellRenderer(botonModificar);

            JButtonRenderer botonEliminar = new JButtonRenderer("Eliminar", "Motor", control);
            tabla.getColumnModel().getColumn(5).setCellEditor(botonEliminar);
            tabla.getColumnModel().getColumn(5).setCellRenderer(botonEliminar);
        }
        if( deltacharlie.DeltaCharlie.IDRol == 3 ){
            JButtonRenderer botonDetalles = new JButtonRenderer("Detalles", "Motor", control);
            tabla.getColumnModel().getColumn(3).setCellEditor(botonDetalles);
            tabla.getColumnModel().getColumn(3).setCellRenderer(botonDetalles);

            JButtonRenderer botonModificar = new JButtonRenderer("Modificar", "Motor", control);
            tabla.getColumnModel().getColumn(4).setCellEditor(botonModificar);
            tabla.getColumnModel().getColumn(4).setCellRenderer(botonModificar);
            
            tabla.removeColumn(tabla.getColumnModel().getColumn(5));
        }
        if(deltacharlie.DeltaCharlie.IDRol == 4){
            JButtonRenderer botonDetalles = new JButtonRenderer("Detalles", "Motor", control);
            tabla.getColumnModel().getColumn(3).setCellEditor(botonDetalles);
            tabla.getColumnModel().getColumn(3).setCellRenderer(botonDetalles);
            
            tabla.removeColumn(tabla.getColumnModel().getColumn(5));
            tabla.removeColumn(tabla.getColumnModel().getColumn(4));
        }
    }
    
    public void vertablaMalla(JTable tabla, controlador control){
        tabla.setModel(cargarMalla());
        tabla.setRowHeight(30);
        
        if(deltacharlie.DeltaCharlie.IDRol == 1 || deltacharlie.DeltaCharlie.IDRol == 2){
            JButtonRenderer botonDetalles = new JButtonRenderer("Detalles", "Malla", control);
            tabla.getColumnModel().getColumn(1).setCellEditor(botonDetalles);
            tabla.getColumnModel().getColumn(1).setCellRenderer(botonDetalles);

            JButtonRenderer botonModificar = new JButtonRenderer("Modificar", "Malla", control);
            tabla.getColumnModel().getColumn(2).setCellEditor(botonModificar);
            tabla.getColumnModel().getColumn(2).setCellRenderer(botonModificar);

            JButtonRenderer botonEliminar = new JButtonRenderer("Eliminar", "Malla", control);
            tabla.getColumnModel().getColumn(3).setCellEditor(botonEliminar);
            tabla.getColumnModel().getColumn(3).setCellRenderer(botonEliminar);
        }
        if( deltacharlie.DeltaCharlie.IDRol == 3 ){
            JButtonRenderer botonDetalles = new JButtonRenderer("Detalles", "Malla", control);
            tabla.getColumnModel().getColumn(1).setCellEditor(botonDetalles);
            tabla.getColumnModel().getColumn(1).setCellRenderer(botonDetalles);

            JButtonRenderer botonModificar = new JButtonRenderer("Modificar", "Malla", control);
            tabla.getColumnModel().getColumn(2).setCellEditor(botonModificar);
            tabla.getColumnModel().getColumn(2).setCellRenderer(botonModificar);
            
            tabla.removeColumn(tabla.getColumnModel().getColumn(3));
        }
        if(deltacharlie.DeltaCharlie.IDRol == 4){
            JButtonRenderer botonDetalles = new JButtonRenderer("Detalles", "Malla", control);
            tabla.getColumnModel().getColumn(1).setCellEditor(botonDetalles);
            tabla.getColumnModel().getColumn(1).setCellRenderer(botonDetalles);
            
            tabla.removeColumn(tabla.getColumnModel().getColumn(3));
            tabla.removeColumn(tabla.getColumnModel().getColumn(2));
        }
    }
    
    public void vertablaHelice(JTable tabla, controlador control){
        tabla.setModel(cargarHelice());
        tabla.setRowHeight(30);
        
        if(deltacharlie.DeltaCharlie.IDRol == 1 || deltacharlie.DeltaCharlie.IDRol == 2){
            JButtonRenderer botonDetalles = new JButtonRenderer("Detalles", "Helice", control);
            tabla.getColumnModel().getColumn(3).setCellEditor(botonDetalles);
            tabla.getColumnModel().getColumn(3).setCellRenderer(botonDetalles);

            JButtonRenderer botonModificar = new JButtonRenderer("Modificar", "Helice", control);
            tabla.getColumnModel().getColumn(4).setCellEditor(botonModificar);
            tabla.getColumnModel().getColumn(4).setCellRenderer(botonModificar);

            JButtonRenderer botonEliminar = new JButtonRenderer("Eliminar", "Helice", control);
            tabla.getColumnModel().getColumn(5).setCellEditor(botonEliminar);
            tabla.getColumnModel().getColumn(5).setCellRenderer(botonEliminar);
        }
        if( deltacharlie.DeltaCharlie.IDRol == 3 ){
            JButtonRenderer botonDetalles = new JButtonRenderer("Detalles", "Helice", control);
            tabla.getColumnModel().getColumn(3).setCellEditor(botonDetalles);
            tabla.getColumnModel().getColumn(3).setCellRenderer(botonDetalles);

            JButtonRenderer botonModificar = new JButtonRenderer("Modificar", "Helice", control);
            tabla.getColumnModel().getColumn(4).setCellEditor(botonModificar);
            tabla.getColumnModel().getColumn(4).setCellRenderer(botonModificar);
            
            tabla.removeColumn(tabla.getColumnModel().getColumn(5));
        }
        if(deltacharlie.DeltaCharlie.IDRol == 4){
            JButtonRenderer botonDetalles = new JButtonRenderer("Detalles", "Helice", control);
            tabla.getColumnModel().getColumn(3).setCellEditor(botonDetalles);
            tabla.getColumnModel().getColumn(3).setCellRenderer(botonDetalles);
            
            tabla.removeColumn(tabla.getColumnModel().getColumn(5));
            tabla.removeColumn(tabla.getColumnModel().getColumn(4));
        }
    }

    public DefaultTableModel cargarAlumno(){
        ResultSet variablex = null;
        DefaultTableModel modelo = new DefaultTableModel(){
            public boolean isCellEditable(int row, int column){
                return true;
            }
        };
        try {  
            String sql = "SELECT a.Nombre, a.ApellidoPaterno , a.ApellidoMaterno\n" +
                         "FROM alumnos a \n" +
                         "WHERE a.isBorrado = 0";
            Connection con = conexion.conectar(conection);
            Statement st = con.createStatement();
            variablex = st.executeQuery(sql);
            modelo.setColumnIdentifiers(new Object[]{"Nombre","Apellido Paterno","Apellido Materno","Detalles","Modificar", "Eliminar"});
            while(variablex.next()){
                modelo.addRow(new Object[]{variablex.getString(1),variablex.getString(2),variablex.getString(3), "", "", ""});
            }
            con.close();     
        } catch (SQLException ex) {
            Logger.getLogger(conexion.class.getName()).log(Level.SEVERE, null, ex);
        }
        return modelo;
    }
    
    public DefaultTableModel cargarMateria(){
        ResultSet variablex = null;
        DefaultTableModel modelo = new DefaultTableModel(){
            public boolean isCellEditable(int row, int column){
                return true;
            }
        };
        try {  
            String sql = "SELECT a.Materia\n" +
                            "FROM materia a\n" +
                            "WHERE a.isBorrado = 0";
            Connection con = conexion.conectar(conection);
            Statement st = con.createStatement();
            variablex = st.executeQuery(sql);
            modelo.setColumnIdentifiers(new Object[]{"Nombre","Detalles","Modificar", "Eliminar"});
            while(variablex.next()){
                modelo.addRow(new Object[]{variablex.getString(1),"", "", ""});
            }
            con.close();     
        } catch (SQLException ex) {
            Logger.getLogger(conexion.class.getName()).log(Level.SEVERE, null, ex);
        }
        return modelo;
    }
    
    public Persona detallesAlumno(int IDAlumno) throws SQLException {
        Persona aux = new Persona();
        
        String sql = "SELECT *, malla.nombreMalla\n" +
                    "FROM alumnos, malla WHERE alumnos.IDMalla = malla.IDMalla AND alumnos.IDPersona = "+ IDAlumno;
        Connection con = conexion.conectar(conection);
        Statement st = con.createStatement();
        ResultSet rs = st.executeQuery(sql);
        
        while (rs.next()) {
            
            int iDPersona = rs.getInt(1);
            int iDMalla = rs.getInt(2);
            String nombre = rs.getString(3);
            String apellidoPaterno = rs.getString(4);
            if (rs.wasNull()) {
                apellidoPaterno = "";
            }
            String apellidoMaterno = rs.getString(5);
            if (rs.wasNull()) {
                apellidoMaterno = "";
            }
            int tipoDocumento = rs.getInt(6);
            String ciRucPas = rs.getString(7);
            int nroCredencialCorporativo = rs.getInt(8);
            if (rs.wasNull()) {
                nroCredencialCorporativo = 0;
            }
            int nroCredencialSabsa = rs.getInt(9);
            if (rs.wasNull()) {
                nroCredencialSabsa = 0;
            }
            int numFolio = rs.getInt(10);
            if (rs.wasNull()) {
                numFolio = 0;
            }
            int numLicencia = rs.getInt(11);
            if (rs.wasNull()) {
                numLicencia = 0;
            }
            int isPiloto = rs.getInt(12);

            int usaLentes = rs.getInt(14);

            String direccion = rs.getString(15);
            String tipoDeSangre = rs.getString(16);
            String fechaDeNacimiento = rs.getString(17);
            int celular = rs.getInt(18);//idReferencia
            
            ImageIcon image = null;
            if(rs.getBytes(19)!=null){
                byte[] img = rs.getBytes(19);
                image = new ImageIcon(img);
            }
            
            
                
            int creditoFalso = rs.getInt(23);

            String fechaVigenciaSabsa = rs.getString(24);
            if (rs.wasNull()) {
                fechaVigenciaSabsa = "";
            }
            System.out.println(fechaVigenciaSabsa);
            
            String fechaVigenciaCorporativo = rs.getString(25);
            if (rs.wasNull()) {
                fechaVigenciaCorporativo = "";
            }
            String fechaVencimientoAptoMedico = rs.getString(26);
            if (rs.wasNull()) {
                fechaVencimientoAptoMedico = "";
            }
            String fechaVencimientoProficciencyCheck = rs.getString(27);
            if (rs.wasNull()) {
                fechaVencimientoProficciencyCheck = "";
            }
            String nombreMalla = rs.getString(30);
            if (rs.wasNull()) {
                nombreMalla = "";
            }
            
            
            aux = new Persona(iDPersona, iDMalla, nombre, apellidoPaterno, apellidoMaterno, tipoDocumento,
                        ciRucPas, nroCredencialCorporativo, nroCredencialSabsa, numFolio, numLicencia,
                        usaLentes, direccion, tipoDeSangre, fechaDeNacimiento, celular, image,
                        creditoFalso, fechaVigenciaSabsa, fechaVigenciaCorporativo, fechaVencimientoAptoMedico, nombreMalla);
            
        }
        close(con);
        return aux;
    }
    
    public void vertablaAlumno(JTable tabla, controlador control){
        tabla.setModel(cargarAlumno());
        tabla.setRowHeight(30);
        
        if(deltacharlie.DeltaCharlie.IDRol == 1 || deltacharlie.DeltaCharlie.IDRol == 2){
            JButtonRenderer botonDetalles = new JButtonRenderer("Detalles", "Alumno", control);
            tabla.getColumnModel().getColumn(3).setCellEditor(botonDetalles);
            tabla.getColumnModel().getColumn(3).setCellRenderer(botonDetalles);

            JButtonRenderer botonModificar = new JButtonRenderer("Modificar", "Alumno", control);
            tabla.getColumnModel().getColumn(4).setCellEditor(botonModificar);
            tabla.getColumnModel().getColumn(4).setCellRenderer(botonModificar);

            JButtonRenderer botonEliminar = new JButtonRenderer("Eliminar", "Alumno", control);
            tabla.getColumnModel().getColumn(5).setCellEditor(botonEliminar);
            tabla.getColumnModel().getColumn(5).setCellRenderer(botonEliminar);
        }
        if( deltacharlie.DeltaCharlie.IDRol == 3 ){
            JButtonRenderer botonDetalles = new JButtonRenderer("Detalles", "Alumno", control);
            tabla.getColumnModel().getColumn(3).setCellEditor(botonDetalles);
            tabla.getColumnModel().getColumn(3).setCellRenderer(botonDetalles);

            JButtonRenderer botonModificar = new JButtonRenderer("Modificar", "Alumno", control);
            tabla.getColumnModel().getColumn(4).setCellEditor(botonModificar);
            tabla.getColumnModel().getColumn(4).setCellRenderer(botonModificar);
            
            tabla.removeColumn(tabla.getColumnModel().getColumn(5));
        }
        if(deltacharlie.DeltaCharlie.IDRol == 4){
            JButtonRenderer botonDetalles = new JButtonRenderer("Detalles", "Alumno", control);
            tabla.getColumnModel().getColumn(3).setCellEditor(botonDetalles);
            tabla.getColumnModel().getColumn(3).setCellRenderer(botonDetalles);
            
            tabla.removeColumn(tabla.getColumnModel().getColumn(5));
            tabla.removeColumn(tabla.getColumnModel().getColumn(4));
        }
    }
    
    public void vertablaMaterias(JTable tabla, controlador control){
        tabla.setModel(cargarMateria());
        tabla.setRowHeight(30);
        
        if(deltacharlie.DeltaCharlie.IDRol == 1 || deltacharlie.DeltaCharlie.IDRol == 2){
            JButtonRenderer botonDetalles = new JButtonRenderer("Detalles", "Materia", control);
            tabla.getColumnModel().getColumn(1).setCellEditor(botonDetalles);
            tabla.getColumnModel().getColumn(1).setCellRenderer(botonDetalles);

            JButtonRenderer botonModificar = new JButtonRenderer("Modificar", "Materia", control);
            tabla.getColumnModel().getColumn(2).setCellEditor(botonModificar);
            tabla.getColumnModel().getColumn(2).setCellRenderer(botonModificar);

            JButtonRenderer botonEliminar = new JButtonRenderer("Eliminar", "Materia", control);
            tabla.getColumnModel().getColumn(3).setCellEditor(botonEliminar);
            tabla.getColumnModel().getColumn(3).setCellRenderer(botonEliminar);
        }
        if( deltacharlie.DeltaCharlie.IDRol == 3 ){
            JButtonRenderer botonDetalles = new JButtonRenderer("Detalles", "Materia", control);
            tabla.getColumnModel().getColumn(1).setCellEditor(botonDetalles);
            tabla.getColumnModel().getColumn(1).setCellRenderer(botonDetalles);

            JButtonRenderer botonModificar = new JButtonRenderer("Modificar", "Materia", control);
            tabla.getColumnModel().getColumn(2).setCellEditor(botonModificar);
            tabla.getColumnModel().getColumn(2).setCellRenderer(botonModificar);
            
            tabla.removeColumn(tabla.getColumnModel().getColumn(3));
        }
        if(deltacharlie.DeltaCharlie.IDRol == 4){
            JButtonRenderer botonDetalles = new JButtonRenderer("Detalles", "Materia", control);
            tabla.getColumnModel().getColumn(1).setCellEditor(botonDetalles);
            tabla.getColumnModel().getColumn(1).setCellRenderer(botonDetalles);
            
            tabla.removeColumn(tabla.getColumnModel().getColumn(3));
            tabla.removeColumn(tabla.getColumnModel().getColumn(2));
        }
    }
    
    public DefaultTableModel cargarDocentePiloto(){
        ResultSet variablex = null;
        DefaultTableModel modelo = new DefaultTableModel(){
            public boolean isCellEditable(int row, int column){
                return true;
            }
        };
        try {  
            String sql = "SELECT a.Nombre, a.ApellidoPaterno , a.ApellidoMaterno\n" +
                        "FROM alumno_docente_piloto a\n" +
                        "WHERE a.isBorrado = 0 AND (a.IsDocente = 1 OR a.isPiloto = 1)";
            Connection con = conexion.conectar(conection);
            Statement st = con.createStatement();
            variablex = st.executeQuery(sql);
            modelo.setColumnIdentifiers(new Object[]{"Nombre","Apellido Paterno","Apellido Materno","Detalles","Modificar", "Eliminar"});
            while(variablex.next()){
                modelo.addRow(new Object[]{variablex.getString(1),variablex.getString(2),variablex.getString(3), "", "", ""});
            }
            con.close();     
        } catch (SQLException ex) {
            Logger.getLogger(conexion.class.getName()).log(Level.SEVERE, null, ex);
        }
        return modelo;
    }
    
    public Persona detallesDocentePiloto(int IDDocentePiloto) throws SQLException {
        Persona aux = new Persona();
        
        String sql = "SELECT *\n" +
                    "FROM alumno_docente_piloto\n" +
                    "WHERE alumno_docente_piloto.IDPersona = "+ IDDocentePiloto;
        Connection con = conexion.conectar(conection);
        Statement st = con.createStatement();
        ResultSet rs = st.executeQuery(sql);
        
        while (rs.next()) {
            
            int iDPersona = rs.getInt(1);
            int iDMalla = rs.getInt(2);
            String nombre = rs.getString(3);
            String apellidoPaterno = rs.getString(4);
            if (rs.wasNull()) {
                apellidoPaterno = "";
            }
            String apellidoMaterno = rs.getString(5);
            if (rs.wasNull()) {
                apellidoMaterno = "";
            }
            int tipoDocumento = rs.getInt(6);
            String ciRucPas = rs.getString(7);
            int nroCredencialCorporativo = rs.getInt(8);
            if (rs.wasNull()) {
                nroCredencialCorporativo = 0;
            }
            int nroCredencialSabsa = rs.getInt(9);
            if (rs.wasNull()) {
                nroCredencialSabsa = 0;
            }
            int numFolio = rs.getInt(10);
            if (rs.wasNull()) {
                numFolio = 0;
            }
            int numLicencia = rs.getInt(11);
            if (rs.wasNull()) {
                numLicencia = 0;
            }
            int isPiloto = rs.getInt(12);

            int usaLentes = rs.getInt(14);

            String direccion = rs.getString(15);
            String tipoDeSangre = rs.getString(16);
            String fechaDeNacimiento = rs.getString(17);
            int celular = rs.getInt(18);//idReferencia
            
            ImageIcon image = null;
            if(rs.getBytes(19)!=null){
                byte[] img = rs.getBytes(19);
                image = new ImageIcon(img);
            }
            
            
                
            int creditoFalso = rs.getInt(23);

            String fechaVigenciaSabsa = rs.getString(24);
            if (rs.wasNull()) {
                fechaVigenciaSabsa = "";
            }
            System.out.println(fechaVigenciaSabsa);
            
            String fechaVigenciaCorporativo = rs.getString(25);
            if (rs.wasNull()) {
                fechaVigenciaCorporativo = "";
            }
            String fechaVencimientoAptoMedico = rs.getString(26);
            if (rs.wasNull()) {
                fechaVencimientoAptoMedico = "";
            }
            String fechaVencimientoProficciencyCheck = rs.getString(27);
            if (rs.wasNull()) {
                fechaVencimientoProficciencyCheck = "";
            }
            
            
            aux = new Persona(iDPersona, nombre, apellidoPaterno, apellidoMaterno, tipoDocumento,
                        ciRucPas, nroCredencialCorporativo, nroCredencialSabsa,
                        numLicencia,isPiloto,usaLentes, direccion, 
                        tipoDeSangre, fechaDeNacimiento, celular, 
                         fechaVigenciaSabsa, fechaVigenciaCorporativo, fechaVencimientoAptoMedico,fechaVencimientoProficciencyCheck);
            
        }
        close(con);
        return aux;
    }
    
    public void vertablaDocentePiloto(JTable tabla, controlador control){
        tabla.setModel(cargarDocentePiloto());
        tabla.setRowHeight(30);
        
        if(deltacharlie.DeltaCharlie.IDRol == 1 || deltacharlie.DeltaCharlie.IDRol == 2){
            JButtonRenderer botonDetalles = new JButtonRenderer("Detalles", "Docente/Piloto", control);
            tabla.getColumnModel().getColumn(3).setCellEditor(botonDetalles);
            tabla.getColumnModel().getColumn(3).setCellRenderer(botonDetalles);

            JButtonRenderer botonModificar = new JButtonRenderer("Modificar", "Docente/Piloto", control);
            tabla.getColumnModel().getColumn(4).setCellEditor(botonModificar);
            tabla.getColumnModel().getColumn(4).setCellRenderer(botonModificar);

            JButtonRenderer botonEliminar = new JButtonRenderer("Eliminar", "Docente/Piloto", control);
            tabla.getColumnModel().getColumn(5).setCellEditor(botonEliminar);
            tabla.getColumnModel().getColumn(5).setCellRenderer(botonEliminar);
        }
        if( deltacharlie.DeltaCharlie.IDRol == 3 ){
            JButtonRenderer botonDetalles = new JButtonRenderer("Detalles", "Docente/Piloto", control);
            tabla.getColumnModel().getColumn(3).setCellEditor(botonDetalles);
            tabla.getColumnModel().getColumn(3).setCellRenderer(botonDetalles);

            JButtonRenderer botonModificar = new JButtonRenderer("Modificar", "Docente/Piloto", control);
            tabla.getColumnModel().getColumn(4).setCellEditor(botonModificar);
            tabla.getColumnModel().getColumn(4).setCellRenderer(botonModificar);
            
            tabla.removeColumn(tabla.getColumnModel().getColumn(5));
        }
        if(deltacharlie.DeltaCharlie.IDRol == 4){
            JButtonRenderer botonDetalles = new JButtonRenderer("Detalles", "Docente/Piloto", control);
            tabla.getColumnModel().getColumn(3).setCellEditor(botonDetalles);
            tabla.getColumnModel().getColumn(3).setCellRenderer(botonDetalles);
            
            tabla.removeColumn(tabla.getColumnModel().getColumn(5));
            tabla.removeColumn(tabla.getColumnModel().getColumn(4));
        }
    }
    
    public DefaultTableModel cargarUsuario(){
        ResultSet variablex = null;
        DefaultTableModel modelo = new DefaultTableModel(){
            public boolean isCellEditable(int row, int column){
                return true;
            }
        };
        try {  
            String sql = "SELECT u.UserName , u.NombreOwner\n" +
                        "FROM usuarios u\n" +
                        "WHERE u.isDeleted = 0 AND u.IDRol != 1";
            Connection con = conexion.conectar(conection);
            Statement st = con.createStatement();
            variablex = st.executeQuery(sql);
            modelo.setColumnIdentifiers(new Object[]{"Nombre de Usuario","Nombre","Detalles","Modificar", "Eliminar"});
            while(variablex.next()){
                modelo.addRow(new Object[]{variablex.getString(1),variablex.getString(2), "", "", ""});
            }
            con.close();     
        } catch (SQLException ex) {
            Logger.getLogger(conexion.class.getName()).log(Level.SEVERE, null, ex);
        }
        return modelo;
    }
    
    public Usuario detallesUsuario(int IDUsuario) throws SQLException {
        Usuario aux = new Usuario();
        
        String sql = "SELECT *\n" +
                    "FROM usuarios u\n" +
                    "WHERE u.isDeleted = 0 AND u.IDUser = "+ IDUsuario;
        Connection con = conexion.conectar(conection);
        Statement st = con.createStatement();
        ResultSet rs = st.executeQuery(sql);
        
        while (rs.next()) {
            int idUser = rs.getInt(1);
            String userName = rs.getString(2);
            String password = rs.getString(3);
            String nombre = rs.getString(4);
            int rol = rs.getInt(5);
            
            aux = new Usuario(idUser,nombre,userName,password,rol);
            
        }
        close(con);
        return aux;
    }
    
    public Materia detallesMateria(int IDMateria) throws SQLException {
        Materia aux = new Materia();
        
        String sql = "SELECT *\n" +
                    "FROM materia u\n" +
                    "WHERE u.isBorrado = 0 AND u.IDMateria = "+ IDMateria;
        Connection con = conexion.conectar(conection);
        Statement st = con.createStatement();
        ResultSet rs = st.executeQuery(sql);
        
        while (rs.next()) {
            int idMateria = rs.getInt(1);
            String nombre = rs.getString(2);
            int costo = rs.getInt(3);
            int minutos = rs.getInt(4);
            
            aux = new Materia(idMateria,nombre,costo,minutos);
            
        }
        close(con);
        return aux;
    }
    
    public Grupo detallesGrupo(int IDGrupo) throws SQLException {
        Grupo aux = new Grupo();
        
        String sql = "SELECT *\n" +
                    "FROM grupodisponibles u\n" +
                    "WHERE u.isBorrado = 0 AND u.IDGrupo = "+ IDGrupo;
        Connection con = conexion.conectar(conection);
        Statement st = con.createStatement();
        ResultSet rs = st.executeQuery(sql);
        
        while (rs.next()) {
            int idGrupo = rs.getInt(1);
            int idDocente = rs.getInt(2);
            int idMateria = rs.getInt(3);
            String FechaInicio = rs.getString(4);
            String FechaFin = rs.getString(5);
            String Horario = rs.getString(6);
            String nombre = rs.getString(7);
            String nombreM = ConseguirNombreMateria(idMateria);
            String nombreD = ConseguirDocente(idDocente);
            String nombreG = getNombreGrupo(idGrupo);
            aux = new Grupo(idGrupo, nombreM, nombreG, nombreD, FechaInicio, FechaFin, Horario);
        }
        close(con);
        return aux;
    }
    
    public void vertablaUsuario(JTable tabla, controlador control){
        tabla.setModel(cargarUsuario());
        tabla.setRowHeight(30);
        
        JButtonRenderer botonDetalles = new JButtonRenderer("Detalles", "Usuario", control);
        tabla.getColumnModel().getColumn(2).setCellEditor(botonDetalles);
        tabla.getColumnModel().getColumn(2).setCellRenderer(botonDetalles);
        
        JButtonRenderer botonModificar = new JButtonRenderer("Modificar", "Usuario", control);
        tabla.getColumnModel().getColumn(3).setCellEditor(botonModificar);
        tabla.getColumnModel().getColumn(3).setCellRenderer(botonModificar);
        
        JButtonRenderer botonEliminar = new JButtonRenderer("Eliminar", "Usuario", control);
        tabla.getColumnModel().getColumn(4).setCellEditor(botonEliminar);
        tabla.getColumnModel().getColumn(4).setCellRenderer(botonEliminar);
    }
    
    private Materia buscarMateria(int IDMateria) throws SQLException {
        Materia aux = null;
        DefaultComboBoxModel modelo = new DefaultComboBoxModel();
        String sql = "SELECT m.Materia,m.CostoCreditos,m.TotalMinutos\n"
                + "FROM materia m\n"
                + "WHERE m.IDMateria = " + IDMateria + " AND isBorrado = 0";
        Connection con = conexion.conectar(conection);
        Statement st = con.createStatement();
        ResultSet rs = st.executeQuery(sql);
        modelo.removeAllElements();
        if (rs.next()) {
            String materia = rs.getString(1);
            int costoCreditos = rs.getInt(2);
            int totalMinutos = rs.getInt(3);

            aux = new Materia(IDMateria, materia, costoCreditos, totalMinutos);
        }
        close(con);
        return aux;
    }

    public void eliminarMateria(int id, int IDUser) throws SQLException {
            Connection con = conexion.conectar(conection);

            PreparedStatement pstm = con.prepareStatement("UPDATE materia\n"
                    + "SET isBorrado = ?\n"
                    + "WHERE IDMateria = ?");
            pstm.setInt(1, 1);
            pstm.setInt(2, id);
            pstm.executeUpdate();
            close(con);
            loger("logmateria", "IDMateria", id, IDUser, "eliminado");

    }
    
    public void eliminarReserva(String TV,int idP,int idE,int idA,int HDV) throws SQLException {
            Connection con = conexion.conectar(conection);

            PreparedStatement pstm = con.prepareStatement("UPDATE vuelosregistrados\n"
                    + "SET isBorrado = ?\n"
                    + "WHERE TipoDeVuelo = ? AND IDPiloto = ? AND IDEstudiante = ? AND IDAvion = ? AND HorasDeVuelo = ? AND isBorrado=0");
            pstm.setInt(1, 1);
            pstm.setString(2, TV);
            pstm.setInt(3, idP);
            pstm.setInt(4, idE);
            pstm.setInt(5, idA);
            pstm.setInt(6, HDV);
            System.out.println(pstm);
            pstm.executeUpdate();
            close(con);
    }

    /*private boolean existeADepender(int id) throws SQLException {
        String sql = "SELECT dm.*\n"
                + "FROM dependenciasmateria dm\n"
                + "WHERE dm.IDMateriaAnt = " + id;
        Connection con = conexion.conectar(conection);
        Statement st = con.createStatement();
        ResultSet rs = st.executeQuery(sql);
        return rs.next();
    }

    private boolean existeEnMalla(int id) throws SQLException {
        String sql = "SELECT *\n"
                + "FROM malla_materia mm\n"
                + "WHERE mm.IDMateria =  " + id;
        Connection con = conexion.conectar(conection);
        Statement st = con.createStatement();
        ResultSet rs = st.executeQuery(sql);
        return rs.next();
    }*/

    public DefaultComboBoxModel ObtenerListaMallas(String palabra, LinkedList<CRTVuelo> mallas) throws SQLException {
        mallas = new LinkedList<CRTVuelo>();
        DefaultComboBoxModel modelo = new DefaultComboBoxModel();
        String sql = "SELECT\n" +
                    "	m.IDMalla,\n" +
                    "	m.nombreMalla,\n" +
                    "	m.costo \n" +
                    "FROM\n" +
                    "	malla m \n" +
                    "WHERE\n" +
                    "	m.isBorrado = 0 \n" +
                    "	AND m.nombreMalla LIKE '%"+palabra+"%'";
        Connection con = conexion.conectar(conection);
        Statement st = con.createStatement();
        ResultSet rs = st.executeQuery(sql);
        while (rs.next()) {
            int IDMalla = rs.getInt(1);
            String nombre = rs.getString(2);
            int costo = rs.getInt(3);

            modelo.addElement(nombre);
            CRTVuelo aux = new CRTVuelo(IDMalla, nombre, costo);
            mallas.add(aux);
        }
        close(con);
        return modelo;
    }
    
    public ComboBoxModel<String> ObtenerListaGrupo(String palabra, LinkedList<Grupo> grupos) throws SQLException {
        DefaultComboBoxModel modelo = new DefaultComboBoxModel();
        String sql = "SELECT g.IDGrupo,g.IDDocente,g.IDMateria,g.FechaIni,g.FechaFin,g.Horario,g.NombreGrupo\n"
                + "FROM grupodisponibles g\n"
                + "WHERE g.isBorrado = 0 AND\n"
                + "CONCAT(g.NombreGrupo,'') like '%" + palabra + "%'";
        Connection con = conexion.conectar(conection);
        Statement st = con.createStatement();
        ResultSet rs = st.executeQuery(sql);
        modelo.removeAllElements();
        while (rs.next()) {
            int IDGrupo = rs.getInt(1);
            int IDDocente = rs.getInt(2);
            int IDMateria = rs.getInt(3);
            String FechaIni = rs.getString(4);
            String FechaFin = rs.getString(5);
            String Horario = rs.getString(6);
            String NombreGrupo = rs.getString(7);
            String NombreMateria = ConseguirNombreMateria(IDMateria);
            String NombreDocente = ConseguirDocente(IDDocente);

            modelo.addElement(NombreGrupo);
            Grupo aux = new Grupo(IDGrupo, NombreMateria, NombreGrupo, NombreDocente, FechaIni, FechaFin, Horario);
            grupos.add(aux);
        }
        close(con);
        return modelo;
    }

    public void ObtenerMateriaMalla(int id, LinkedList<Materia> materias) throws SQLException {
        String sql = "SELECT mm.IDMateria\n"
                + "FROM malla_materia mm\n"
                + "WHERE mm.IDMalla = " + id;
        Connection con = conexion.conectar(conection);
        Statement st = con.createStatement();
        ResultSet rs = st.executeQuery(sql);

        while (rs.next()) {
            int Materia = rs.getInt(1);
            Materia aux = buscarMateria(Materia);
            materias.add(aux);
        }
        close(con);

    }

    public String nombreMateria(int id) throws SQLException {
        String sql = "SELECT m.Materia\n"
                + "FROM materia m\n"
                + "WHERE m.IDMateria = " + id;
        Connection con = conexion.conectar(conection);
        Statement st = con.createStatement();
        ResultSet rs = st.executeQuery(sql);
        String res = "";
        if (rs.next()) {
            res = rs.getString(1);
            res += " ";
        }
        close(con);
        return res;
    }

    public void modificarMalla(int id, String nombremalla, int requiere, LinkedList<Materia> materiasMalla, int costo) throws SQLException {
        String query = ("UPDATE  malla\n"
                + " SET nombreMalla = ? \n"
                + " ,RequiereLicencia = ?\n"
                + " ,costo = ?\n"
                + " WHERE IDMalla = ?\n");
        Connection con = conexion.conectar(conection);
        PreparedStatement pst;
        pst = con.prepareStatement(query);
        pst.setString(1, nombremalla);
        pst.setInt(2, requiere);
        pst.setInt(3, costo);
        pst.setInt(4, id);
        pst.executeUpdate();
        pst.close();
        close(con);
        try {
            eliminarMateriasMalla(id);
        } catch (SQLException e) {
            throw new SQLException("Error al eliminar Materias" + e.getMessage());
        }

        try {
            for (Materia materia : materiasMalla) {
                insertMateriasMalla(id, materia.getIDMateria());
            }
        } catch (SQLException sq) {
            throw new SQLException("Error al inserar las materias en malla");
        }
    }

    private void eliminarMateriasMalla(int idMalla) throws SQLException {
        String query = ("DELETE  malla_materia\n"
                + " WHERE IDMalla = ?\n");
        Connection con = conexion.conectar(conection);
        PreparedStatement pst;

        pst = con.prepareStatement(query);
        pst.setInt(1, idMalla);
        pst.executeUpdate();
        pst.close();
        close(con);
    }

    public void eliminarMalla(int idMalla) throws SQLException {
        String query = ("UPDATE  malla\n"
                + " SET isBorrado = ? \n"
                + " WHERE malla.IDMalla = ?\n");
        Connection con = conexion.conectar(conection);
        PreparedStatement pst;

        pst = con.prepareStatement(query);
        pst.setInt(1, 1);
        pst.setInt(2, idMalla);
        pst.executeUpdate();
        pst.close();
        close(con);
    }

    public void eliminarPersona(int id, int idUsuario) throws SQLException {
        String query = ("UPDATE  alumno_docente_piloto\n"
                + " SET isBorrado = ? \n"
                + " WHERE IDPersona = ?\n");
        Connection con = conexion.conectar(conection);
        PreparedStatement pst;

        pst = con.prepareStatement(query);
        pst.setInt(1, 1);
        pst.setInt(2, id);
        pst.executeUpdate();
        pst.close();
        close(con);
        try {
            loger("logalumnopilotodocente", "IDAlumnoPilotoDocente", id, idUsuario, "eliminado");
        } catch (SQLException sq) {
            throw new SQLException("Error al insertar el log" + sq.getMessage() );
        }
    }

    public void modificarHelice(Helices aux, int IDUser) throws SQLException {
        String query = ("UPDATE  helice\n"
                + "SET"
                + " Marca = ?\n"
                + " ,Modelo = ?\n"
                + " ,NroSerie = ?\n"
                + " ,TT = ?\n"
                + " ,TTO = ? \n"
                + " ,TBO = ?\n"
                + " WHERE IDHelice = ?");

        Connection con = conexion.conectar(conection);
        PreparedStatement pst;
        pst = con.prepareStatement(query);

        pst.setString(1, aux.getMarca());
        pst.setString(2, aux.getModelo());
        pst.setString(3, aux.getNumeroDeSerie());

        if (aux.getTTO() == -1) {
            pst.setObject(4, null);
        } else {
            pst.setInt(4, aux.getTTO());
        }
        if (aux.getTBO() == -1) {
            pst.setObject(5, null);
        } else {
            pst.setInt(5, aux.getTBO());
        }

        pst.setInt(6, aux.getTT());
        pst.setInt(7, aux.getIDHelice());

        pst.executeUpdate();

        pst.close();

        close(con);

        try {

            loger("loghelice", "IDHelice", aux.getIDHelice(), IDUser, "modificated");
        } catch (SQLException sq) {
            throw new SQLException("Error al insertar el Log");
        }
    }

    public void eliminarHelice(int idHelice, int IDUser) throws SQLException {
        String query = ("UPDATE  helice\n"
                + " SET isBorrado = ? \n"
                + " WHERE IDHelice = ?\n");
        Connection con = conexion.conectar(conection);
        PreparedStatement pst;

        pst = con.prepareStatement(query);
        pst.setInt(1, 1);
        pst.setInt(2, idHelice);
        pst.executeUpdate();
        pst.close();
        close(con);
        try {

            loger("loghelice", "IDHelice", idHelice, IDUser, "eliminado");
        } catch (SQLException sq) {
            throw new SQLException("Error al insertar el Log");
        }
    }
    
    public void eliminarHelice(Helices helice, int IDUser) throws SQLException {
        String query = ("UPDATE  helice\n"
                + " SET isBorrado = ? \n"
                + " WHERE IDHelice = ?\n");
        Connection con = conexion.conectar(conection);
        PreparedStatement pst;

        pst = con.prepareStatement(query);
        pst.setInt(1, 1);
        pst.setInt(2, helice.getIDHelice());
        pst.executeUpdate();
        pst.close();
        close(con);
        try {

            loger("loghelice", "IDHelice", helice.getIDHelice(), IDUser, "eliminado");
        } catch (SQLException sq) {
            throw new SQLException("Error al insertar el Log");
        }
    }

    public ComboBoxModel<String> obtenerHMA(String palabra, LinkedList<Helices> heliceBuscada,
        LinkedList<Motor> motorBuscado, LinkedList<Avion> avionBuscado, int HMA) throws SQLException {
        heliceBuscada.clear();
        motorBuscado.clear();
        avionBuscado.clear();
        DefaultComboBoxModel modelo = new DefaultComboBoxModel();
        String sql;
        Connection con = conexion.conectar(conection);
        Statement st = con.createStatement();
        ResultSet rs;
        switch (HMA) {
            case 1:

                sql = "SELECT CONCAT(h.Marca,\"-\",h.Modelo,\"-\",h.NroSerie) ,h.IDHelice,h.Marca,h.Modelo,h.NroSerie,h.TT,h.TBO,h.TTO\n"
                        + "FROM helice h\n"
                        + "WHERE h.isBorrado = 0 AND CONCAT(h.Marca,\"-\",h.Modelo,\"-\",h.NroSerie) LIKE '%" + palabra + "%'";
                rs = st.executeQuery(sql);
                while (rs.next()) {
                    String nombre = rs.getString(1);
                    int idHelice = rs.getInt(2);
                    String marca = rs.getString(3);
                    String modelo2 = rs.getString(4);
                    String numSerie = rs.getString(5);
                    int tt = rs.getInt(6);
                    int tbo = rs.getInt(7);
                    if (rs.wasNull()) {
                        tbo = 0;
                    }
                    int tto = rs.getInt(8);
                    if (rs.wasNull()) {
                        tto = 0;
                    }
                    modelo.addElement(nombre);
                    Helices aux = new Helices(idHelice, marca, modelo2, numSerie, tt, tto, tbo);
                    heliceBuscada.addLast(aux);
                }

                break;
            case 2:
                sql = "SELECT CONCAT(m.Marca,\"-\",m.Modelo,\"-\",m.NumeroSerie) ,m.IDMotor,m.Marca,m.Modelo,m.NumeroSerie,m.TT,m.TBO,m.TTO, m.IDHelice\n"
                        + "FROM motor m\n"
                        + "WHERE m.isBorrado = 0 AND CONCAT(m.Marca,\"-\",m.Modelo,\"-\",m.NumeroSerie) LIKE  '%" + palabra + "%'";
                rs = st.executeQuery(sql);
                while (rs.next()) {
                    String nombre = rs.getString(1);
                    int idmotor = rs.getInt(2);
                    String marca = rs.getString(3);
                    String modelo2 = rs.getString(4);
                    String numSerie = rs.getString(5);
                    int tt = rs.getInt(6);
                    int tbo = rs.getInt(7);
                    if (rs.wasNull()) {
                        tbo = 0;
                    }
                    int tto = rs.getInt(8);
                    if (rs.wasNull()) {
                        tto = 0;
                    }
                    int idHelice = rs.getInt(9);
                    if (rs.wasNull()) {
                        idHelice = -1;
                    }
                    modelo.addElement(nombre);
                    Motor aux = new Motor(idmotor, idHelice, marca, modelo2, numSerie, tt, tto, tbo);
                    motorBuscado.addLast(aux);
                }

                break;
            case 3:
                sql = "SELECT CONCAT(a.Marca,\"-\",a.Modelo,\"-\",a.NumeroDeSerie) ,a.IDAvion, a.minutosVuelo ,a.Marca,a.Modelo,a.NumeroDeSerie,a.FechaVencimientoRegistroMatricula,a.FechaVencimientoCertificadoAeronavegabilidad,a.FechaVencimientoExtintor,a.FechaVencimientoEquipoEmergencia,a.FechaVencimientoSeguro\n"
                        + "FROM avion a\n"
                        + "WHERE a.isBorrado = 0 AND CONCAT(a.Marca,\"-\",a.Modelo,\"-\",a.NumeroDeSerie) LIKE  '%" + palabra + "%'";
                rs = st.executeQuery(sql);
                while (rs.next()) {
                    String nombre = rs.getString(1);
                    int idAvion = rs.getInt(2);
                    int minutosVuelo = rs.getInt(3);
                    String marca = rs.getString(4);
                    String modelo2 = rs.getString(5);
                    String numSerie = rs.getString(6);
                    String fechaVencimientoMatricula = rs.getString(7);
                    String fechaVencimientoAeronavegabilidad = rs.getString(8);
                    String fechaVencimientoExtintor = rs.getString(9);
                    String fechaVencimientoEquipoEmergencia = rs.getString(10);
                    String fechaVencimientoSeguro = rs.getString(11);

                    modelo.addElement(nombre);
                    Avion aux = new Avion(idAvion, marca, minutosVuelo, modelo2, numSerie, fechaVencimientoExtintor, fechaVencimientoSeguro, fechaVencimientoMatricula, fechaVencimientoAeronavegabilidad, fechaVencimientoEquipoEmergencia);
                    avionBuscado.addLast(aux);
                }
                break;
        }
        return modelo;
    }
    
    public Avion obtenerAvion(int id) throws SQLException {
        String sql;
        Connection con = conexion.conectar(conection);
        Statement st = con.createStatement();
        ResultSet rs;
        Avion aux = new Avion();
        sql = "SELECT a.IDAvion, a.minutosVuelo ,a.Marca,a.Modelo,a.NumeroDeSerie,a.FechaVencimientoRegistroMatricula,a.FechaVencimientoCertificadoAeronavegabilidad,a.FechaVencimientoExtintor,a.FechaVencimientoEquipoEmergencia,a.FechaVencimientoSeguro\n"
                        + "FROM avion a\n"
                        + "WHERE\n" +
                        "	a.IDAvion = " + id + ";";
                rs = st.executeQuery(sql);
                while (rs.next()) {
                    int idAvion = rs.getInt(1);
                    int minutosVuelo = rs.getInt(2);
                    String marca = rs.getString(3);
                    String modelo2 = rs.getString(4);
                    String numSerie = rs.getString(5);
                    String fechaVencimientoMatricula = rs.getString(6);
                    String fechaVencimientoAeronavegabilidad = rs.getString(7);
                    String fechaVencimientoExtintor = rs.getString(8);
                    String fechaVencimientoEquipoEmergencia = rs.getString(9);
                    String fechaVencimientoSeguro = rs.getString(10);

                    aux = new Avion(idAvion, marca, minutosVuelo, modelo2, numSerie, fechaVencimientoExtintor, fechaVencimientoSeguro, fechaVencimientoMatricula, fechaVencimientoAeronavegabilidad, fechaVencimientoEquipoEmergencia);
                }
        return aux;
    }

    public ComboBoxModel<String> obtenerMelicesObtenibles(LinkedList<Helices> heliceBuscada) throws SQLException {
        heliceBuscada.clear();

        DefaultComboBoxModel modelo = new DefaultComboBoxModel();
        String sql;
        Connection con = conexion.conectar(conection);
        Statement st = con.createStatement();
        ResultSet rs;
        modelo.addElement("----------");
        sql = "SELECT h.IDHelice, h.Marca, h.Modelo, h.NroSerie, h.TT, h.TBO, h.TTO\n"
                + "FROM helice h\n"
                + "WHERE h.isBorrado = 0 AND h.IDHelice not in(\n"
                + "			SELECT m.IDHelice\n"
                + "			FROM motor m\n"
                + ")";
        rs = st.executeQuery(sql);
        while (rs.next()) {

            int idHelice = rs.getInt(1);
            String marca = rs.getString(2);
            String modelo2 = rs.getString(3);
            String numSerie = rs.getString(4);
            int tt = rs.getInt(5);
            int tbo = rs.getInt(6);
            if (rs.wasNull()) {
                tbo = 0;
            }
            int tto = rs.getInt(7);
            if (rs.wasNull()) {
                tto = 0;
            }
            Helices aux = new Helices(idHelice, marca, modelo2, numSerie, tt, tto, tbo);
            modelo.addElement(aux.toString());

            heliceBuscada.addLast(aux);
        }

        return modelo;
    }

    public Motor ObtenerMotor(int IDMotor) {
        Motor aux = null;
        try {
            Connection con = conexion.conectar(conection);
            Statement st = con.createStatement();
            ResultSet rs;

            String sql = "SELECT m.Marca,m.Modelo,m.NumeroSerie,m.TT,m.TBO,m.TTO, m.IDHelice\n" +
                            "FROM motor m\n" +
                            "WHERE m.isBorrado = 0 AND m.IDMotor = " + IDMotor;
            rs = st.executeQuery(sql);
            if (rs.next()) {

                String marca = rs.getString(1);
                String modelo2 = rs.getString(2);
                String numSerie = rs.getString(3);
                int tt = rs.getInt(4);
                int tbo = rs.getInt(5);
                if (rs.wasNull()) {
                    tbo = 0;
                }
                int tto = rs.getInt(6);
                if (rs.wasNull()) {
                    tto = 0;
                }
                int idHelice = rs.getInt(7);
                if (rs.wasNull()) {
                    idHelice = -1;
                }
                aux = new Motor(IDMotor, idHelice, marca, modelo2, numSerie, tt, tto, tbo);

            }
        } catch (SQLException ex) {
        }
        return aux;
    }
    
    public Helices ObtenerHelice(int IDHelice) {
        Helices aux = null;
        try {
            Connection con = conexion.conectar(conection);
            Statement st = con.createStatement();
            ResultSet rs;

            String sql = "SELECT m.Marca,m.Modelo,m.NroSerie,m.TT,m.TBO,m.TTO\n" +
                            "FROM helice m\n" +
                            "WHERE m.isBorrado = 0 AND m.IDHelice = " + IDHelice;
            rs = st.executeQuery(sql);
            if (rs.next()) {

                String marca = rs.getString(1);
                String modelo2 = rs.getString(2);
                String numSerie = rs.getString(3);
                int tt = rs.getInt(4);
                int tbo = rs.getInt(5);
                if (rs.wasNull()) {
                    tbo = 0;
                }
                int tto = rs.getInt(6);
                if (rs.wasNull()) {
                    tto = 0;
                }
                aux = new Helices(IDHelice, marca, modelo2, numSerie, tt, tto, tbo);

            }
        } catch (SQLException ex) {
        }
        return aux;
    }

    public void obtenerListaMotores(LinkedList<Motor> motores, int IdAvion) {

        try {

            String sql;
            Connection con = conexion.conectar(conection);
            Statement st = con.createStatement();
            ResultSet rs;
            sql = "SELECT am.IDMotor\n"
                    + "FROM avionmotor am\n"
                    + "WHERE am.IDAvion = " + IdAvion;
            rs = st.executeQuery(sql);
            while (rs.next()) {
                int idMotor = rs.getInt(1);
                Motor aux = ObtenerMotor(idMotor);
                if (aux != null) {
                    motores.addLast(aux);
                }

            }
        } catch (SQLException ex) {
            Logger.getLogger(conexion.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    public Helices getHeliceBuscada(int IDHelice) {
        Helices aux = null;
        try {
            Connection con = conexion.conectar(conection);
            Statement st = con.createStatement();
            ResultSet rs;

            String sql = "SELECT h.Marca, h.Modelo, h.NroSerie, h.TT, h.TBO, h.TTO\n"
                    + "FROM helice h\n"
                    + "WHERE h.IDHelice = " + IDHelice;
            rs = st.executeQuery(sql);
            if (rs.next()) {

                String marca = rs.getString(1);
                String modelo2 = rs.getString(2);
                String numSerie = rs.getString(3);
                int tt = rs.getInt(4);
                int tbo = rs.getInt(5);
                if (rs.wasNull()) {
                    tbo = 0;
                }
                int tto = rs.getInt(6);
                if (rs.wasNull()) {
                    tto = 0;
                }
                aux = new Helices(IDHelice, marca, modelo2, numSerie, tt, tto, tbo);

            }

        } catch (SQLException ex) {
        }
        return aux;
    }

    public void insertarMotor(Motor principal, int idUser) throws SQLException {
        String query = ("INSERT INTO motor\n"
                + "(\n"
                + "  IDMotor\n"
                + " ,IDHelice\n"
                + " ,Marca\n"
                + " ,Modelo\n"
                + " ,NumeroSerie\n"
                + " ,TT\n"
                + " ,TTO\n"
                + " ,TBO\n"
                + " ,CreatedBy\n"
                + " ,isBorrado\n"
                + ")\n"
                + "VALUES\n"
                + "(?,?,?,?,?,?,?,?,?,?)");

        Connection con = conexion.conectar(conection);
        PreparedStatement pst;
        pst = con.prepareStatement(query);

        pst.setObject(1, null);
        if (principal.getIDHelice() != -1) {
            pst.setInt(2, principal.getIDHelice());
        } else {
            pst.setObject(2, null);
        }
        pst.setString(3, principal.getMarca());
        pst.setString(4, principal.getModelo());
        pst.setString(5, principal.getNumeroDeSerie());
        pst.setInt(6, principal.getTT());
        if (principal.getTTO() == -1) {
            pst.setObject(7, null);
        } else {
            pst.setInt(7, principal.getTTO());
        }
        if (principal.getTBO() == -1) {
            pst.setObject(8, null);
        } else {
            pst.setInt(8, principal.getTBO());
        }

        pst.setInt(9, idUser);
        pst.setInt(10, 0);

        pst.executeUpdate();

        pst.close();

        close(con);

        int idMotor = getIDGenerated("motor", "IDMotor");
        try {

            loger("logmotor", "IDMotor", idMotor, idUser, "created");
        } catch (SQLException sq) {
            throw new SQLException("Error al insertar el Log");
        }
    }
    
    
    public int ObtenerMinutosAvionXID(int id) throws SQLException
    {
        int minutos=0;
        String sql = "SELECT avion.minutosVuelo\n"
                + "FROM avion\n"
                + "WHERE avion.IDAvion = "+Integer.toString(id);
        Connection con = conexion.conectar(conection);
        Statement st = con.createStatement();
        ResultSet rs = st.executeQuery(sql);
        while (rs.next()) {
            minutos = rs.getInt(1);
        }
        close(con);
        return minutos;
    }
    

    public void modificarMotor(Motor aux, int IDUser) throws SQLException {
        String query = ("UPDATE  motor\n"
                + "SET"
                + " IDHelice = ?\n"
                + " ,Marca = ?\n"
                + " ,Modelo = ?\n"
                + " ,NumeroSerie = ?\n"
                + " ,TT = ?\n"
                + " ,TTO = ? \n"
                + " ,TBO = ?\n"
                + " WHERE IDMotor = ?");

        Connection con = conexion.conectar(conection);
        PreparedStatement pst;
        pst = con.prepareStatement(query);

        if (aux.getIDHelice() > -1) {
            pst.setInt(1, aux.getIDHelice());
        } else {
            pst.setObject(1, null);
        }
        pst.setString(2, aux.getMarca());
        pst.setString(3, aux.getModelo());
        pst.setString(4, aux.getNumeroDeSerie());
        pst.setInt(5, aux.getTT());
        if (aux.getTTO() == -1) {
            pst.setObject(6, null);
        } else {
            pst.setInt(6, aux.getTTO());
        }
        if (aux.getTBO() == -1) {
            pst.setObject(7, null);
        } else {
            pst.setInt(7, aux.getTBO());
        }

        pst.setInt(8, aux.getIDMotor());

        pst.executeUpdate();

        pst.close();

        close(con);

        try {

            loger("logmotor", "IDMotor", aux.getIDMotor(), IDUser, "modificated");
        } catch (SQLException sq) {
            throw new SQLException("Error al insertar el Log");
        }
    }

    public void eliminarMotor(Motor motor, int IDUser) throws SQLException {

        String query = ("UPDATE  motor\n"
                + " SET isBorrado = ? \n"
                + " WHERE IDMotor = ?\n");
        Connection con = conexion.conectar(conection);
        PreparedStatement pst;

        pst = con.prepareStatement(query);
        pst.setInt(1, 1);
        pst.setInt(2, motor.getIDMotor());
        pst.executeUpdate();
        pst.close();
        close(con);
        if (motor.getIDHelice() != -1) {
            eliminarHelice(motor.getIDHelice(), IDUser);
        }
        try {

            loger("logmotor", "IDMotor", motor.getIDMotor(), IDUser, "eliminado");
        } catch (SQLException sq) {
            throw new SQLException("Error al insertar el Log");
        }
    }
    
    public void ModificarMinutos(int minutesNew,int idavion) throws SQLException {

        String query = ("UPDATE avion\n"
                + "SET minutosVuelo = ?\n"
                + "WHERE IDAvion = ?\n");
        Connection con = conexion.conectar(conection);
        PreparedStatement pst;

        pst = con.prepareStatement(query);
        pst.setInt(1, minutesNew);
        pst.setInt(2, idavion);
        pst.executeUpdate();
        pst.close();
        close(con);
    }

    public void ObtenerMotores(int idAvion) {
        try {
            Connection con = conexion.conectar(conection);
            Statement st = con.createStatement();
            ResultSet rs;

            String sql = "SELECT am.IDMotor\n"
                    + "FROM avionmotor am\n"
                    + "WHERE am.IDAvion = " + idAvion;
            rs = st.executeQuery(sql);
            while (rs.next()) {

                int idMotor = rs.getInt(1);
                Motor aux = ObtenerMotor(idMotor);
            }
            st.close();
            close(con);

        } catch (SQLException ex) {
        }
    }
    
    public String[] selectMotor(int id){
        String[] motor = new String[2];
        int i = 0;
        try {
            Connection con = conexion.conectar(conection);
            Statement st = con.createStatement();
            ResultSet rs;

            String sql = "SELECT CONCAT(motor.Marca,\"-\",motor.Modelo,\"-\",motor.NumeroSerie)\n" +
                            "FROM motor, avionmotor\n" +
                            "WHERE avionmotor.IDMotor = motor.IDMotor AND avionmotor.IDAvion  = "+ id;
            rs = st.executeQuery(sql);
            while (rs.next()) {
                motor[i]= rs.getString(1);
                i++;
            }
            st.close();
            close(con);

        } catch (SQLException ex) {
        }
        return motor;
    }
    
    public String selectHelice(String motor){
        String helice = null;
        try {
            Connection con = conexion.conectar(conection);
            Statement st = con.createStatement();
            ResultSet rs;

            String sql = "SELECT CONCAT(helice.Marca,\"-\",helice.Modelo,\"-\",helice.NroSerie)\n" +
                         "FROM helice,motor\n" +
                         "WHERE motor.IDHelice = helice.IDHelice AND CONCAT(motor.Marca,\"-\",motor.Modelo,\"-\",motor.NumeroSerie) = " + "'" + motor + "'";
            rs = st.executeQuery(sql);
            while (rs.next()) {
                helice= rs.getString(1);
            }
            st.close();
            close(con);

        } catch (SQLException ex) {
        }
        return helice;
    }

    public ComboBoxModel<String> ObtenerMotorDisponible() throws SQLException {
        DefaultComboBoxModel modelo = new DefaultComboBoxModel();
        String sql;
        Connection con = conexion.conectar(conection);
        Statement st = con.createStatement();
        ResultSet rs;
        modelo.addElement("----------");
        sql = "SELECT m.IDMotor,m.Marca,m.Modelo,m.NumeroSerie,m.TT,m.TBO,m.TTO, m.IDHelice\n"
                + "FROM motor m\n"
                + "WHERE m.isBorrado = 0 AND m.IDMotor not in(\n"
                + "			SELECT am.IDMotor\n"
                + "			FROM avionmotor am\n"
                + ")";
        rs = st.executeQuery(sql);
        while (rs.next()) {
            int idMotor = rs.getInt(1);
            String marca = rs.getString(2);
            String modelo2 = rs.getString(3);
            String numSerie = rs.getString(4);
            int tt = rs.getInt(5);
            int tbo = rs.getInt(6);
            if (rs.wasNull()) {
                tbo = 0;
            }
            int tto = rs.getInt(7);
            if (rs.wasNull()) {
                tto = 0;
            }
            int idHelice = rs.getInt(8);
            if (rs.wasNull()) {
                idHelice = -1;
            }
            Motor aux = new Motor(idMotor, idHelice, marca, modelo2, numSerie, tt, tto, tbo);
            modelo.addElement(aux.toString());
        }

        return modelo;
    }

    public void insertarAvion(Avion principal, int IDUser, String motorD, String motorI) throws SQLException {
        String query = ("INSERT INTO avion\n"
                + "(\n"
                + "  IDAvion\n"
                + " ,Marca\n"
                + " ,minutosVuelo\n"
                + " ,Modelo\n"
                + " ,NumeroDeSerie\n"
                + " ,FechaVencimientoRegistroMatricula\n"
                + " ,FechaVencimientoCertificadoAeronavegabilidad\n"
                + " ,FechaVencimientoExtintor\n"
                + " ,FechaVencimientoEquipoEmergencia\n"
                + " ,FechaVencimientoSeguro\n"
                + " ,CreatedBy\n"
                + " ,isBorrado\n"
                + ")\n"
                + "VALUES\n"
                + "(?,?,?,?,?,?,?,?,?,?,?,?)");

        Connection con = conexion.conectar(conection);
        PreparedStatement pst;
        pst = con.prepareStatement(query);

        pst.setObject(1, null);
        pst.setString(2, principal.getMarca());
        pst.setInt(3, principal.getMinutos());
        pst.setString(4, principal.getModelo());
        pst.setString(5, principal.getNumSerie());
        pst.setString(6, principal.getFechaVencimientoRegistroMatricula());
        pst.setString(7, principal.getFechaVencimientoAeronavegabilidad());
        pst.setString(8, principal.getFechaVencimientoExtintor());
        pst.setString(9, principal.getFechaVencimientoEquipoEmergencia());
        pst.setString(10, principal.getFechaVencimientoSeguro());
        pst.setInt(11, IDUser);
        pst.setInt(12, 0);

        pst.executeUpdate();

        pst.close();

        close(con);

        int idAvion = getIDGenerated("avion", "IDAvion");
        
        if(!" ".equals(motorD)){
            int idMotorD = getIDMotor(motorD);
            insertarMotoresAvion(idAvion, idMotorD);
        }
        
        if(!" ".equals(motorI)){
            int idMotorI = getIDMotor(motorI);
            insertarMotoresAvion(idAvion, idMotorI);
        }
        
        try {

            loger("logavion", "IDAvion", idAvion, IDUser, "created");
        } catch (SQLException sq) {
            throw new SQLException("Error al insertar el Log");
        }
    }

    private void insertarMotoresAvion(int idAvion, int idMotor) throws SQLException {
        String query = ("INSERT INTO avionmotor\n"
                + "(\n"
                + "  IDAvion\n"
                + " ,IDMotor\n"
                + ")\n"
                + "VALUES\n"
                + "(?,?)");

        Connection con = conexion.conectar(conection);
        PreparedStatement pst;
        pst = con.prepareStatement(query);

        pst.setInt(1, idAvion);
        pst.setInt(2, idMotor);

        pst.executeUpdate();

        pst.close();

        close(con);
    }

    public void modificarAvion(Avion aux, int IDUser,  String motorD, String motorI) throws SQLException {
        try {
            eliminarMotores(aux.getIDAvion());
        } catch (SQLException e) {
            throw new SQLException("Error al eliminiar motores");
        }
        String query = ("UPDATE  avion\n"
                + "SET\n"
                + "  Marca = ?\n"
                + " ,minutosVuelo = ?\n"
                + " ,Modelo = ?\n"
                + " ,NumeroDeSerie = ?\n"
                + " ,FechaVencimientoRegistroMatricula = ?\n"
                + " ,FechaVencimientoCertificadoAeronavegabilidad = ?\n"
                + " ,FechaVencimientoExtintor = ?"
                + " ,FechaVencimientoEquipoEmergencia = ?"
                + " ,FechaVencimientoSeguro = ?"
                + "WHERE\n"
                + "  IDAvion = ?");

        Connection con = conexion.conectar(conection);
        PreparedStatement pst;
        pst = con.prepareStatement(query);

        pst.setString(1, aux.getMarca());
        pst.setInt(2, aux.getMinutos());
        pst.setString(3, aux.getModelo());
        pst.setString(4, aux.getNumSerie());
        pst.setString(5, aux.getFechaVencimientoRegistroMatricula());
        pst.setString(6, aux.getFechaVencimientoAeronavegabilidad());
        pst.setString(7, aux.getFechaVencimientoExtintor());
        pst.setString(8, aux.getFechaVencimientoEquipoEmergencia());
        pst.setString(9, aux.getFechaVencimientoSeguro());

        pst.setInt(10, aux.getIDAvion());

        pst.executeUpdate();

        pst.close();

        close(con);
        
        int idAvion = getIDAvion(aux.getMarca()+"-"+aux.getModelo()+"-"+aux.getNumSerie());
        
        int idMotorD = getIDMotor(motorD);
        
        insertarMotoresAvion(idAvion, idMotorD);
        
        int idMotorI = getIDMotor(motorI);
        
        insertarMotoresAvion(idAvion, idMotorI);

        try {

            loger("logavion", "IDAvion", aux.getIDAvion(), IDUser, "modificated");
        } catch (SQLException sq) {
            throw new SQLException("Error al insertar el Log");
        }
    }
    
     public void eliminarAvion(Avion avion, int borrador) throws SQLException {

        Connection con = conexion.conectar(conection);

        PreparedStatement pstm = con.prepareStatement("UPDATE avion\n"
                + "SET isBorrado = ?\n"
                + "WHERE IDAvion = ?");
        pstm.setInt(1, 1);
        pstm.setInt(2, avion.getIDAvion());
        pstm.executeUpdate();
        close(con);
        try {
            loger("logavion", "IDAvion", avion.getIDAvion(), borrador, "eliminado");
        } catch (SQLException sq) {
            throw new SQLException("Error al insertar el Log " + sq.getMessage());
        }
        
         eliminarMotores(avion.getIDAvion());
    }

    private void eliminarMotores(int idAvion) throws SQLException {
        String query = ("DELETE FROM avionmotor\n"
                + "WHERE IDAvion = ? ");
        Connection con = conexion.conectar(conection);
        PreparedStatement pst;
        pst = con.prepareStatement(query);

        pst.setInt(1, idAvion);

        pst.executeUpdate();
        pst.close();

        close(con);
    }

    public void eliminarMotor(int idAvion, int IDUser, LinkedList<Motor> motoresSeleccionados) throws SQLException {
        try {
            eliminarMotores(idAvion);
        } catch (SQLException e) {
            throw new SQLException("Error al eliminiar motores");
        }
        String query = ("UPDATE  avion\n"
                + " SET isBorrado = ? \n"
                + " WHERE IDAvion = ?\n");
        Connection con = conexion.conectar(conection);
        PreparedStatement pst;

        pst = con.prepareStatement(query);
        pst.setInt(1, 1);
        pst.setInt(2, idAvion);
        pst.executeUpdate();
        pst.close();
        close(con);
        for (Motor motoresSeleccionado : motoresSeleccionados) {
            eliminarMotor(motoresSeleccionado, IDUser);
        }
        try {

            loger("logavion", "IDAvion", idAvion, IDUser, "eliminado");
        } catch (SQLException sq) {
            throw new SQLException("Error al insertar el Log");
        }
    }

    public ComboBoxModel<String> getTipoDocumento() throws SQLException {
        DefaultComboBoxModel modelo = new DefaultComboBoxModel();
        String sql = "SELECT *\n"
                + "  FROM tipodocumento";
        Connection con = conexion.conectar(conection);
        Statement st = con.createStatement();
        ResultSet rs = st.executeQuery(sql);

        modelo.removeAllElements();
        modelo.addElement("--------");
        while (rs.next()) {
            modelo.addElement(rs.getString(2));
        }
        close(con);
        return modelo;
    }
    
    public ComboBoxModel<String> getAlumnos() throws SQLException {
        DefaultComboBoxModel modelo = new DefaultComboBoxModel();
        String sql = "SELECT *\n" +
                    "FROM alumnos";
        Connection con = conexion.conectar(conection);
        Statement st = con.createStatement();
        ResultSet rs = st.executeQuery(sql);
        
        modelo.removeAllElements();
        while(rs.next()){
            modelo.addElement(rs.getString(3)+" "+rs.getString(4)+" "+rs.getString(5));
        }
        close(con);
        return modelo;
    }

    public LinkedList<Persona> getAlumnosToLinkedList() throws SQLException {
        LinkedList<Persona> modelo = new LinkedList();
        
        String sql = "SELECT *, malla.nombreMalla\n" +
                    "FROM alumnos, malla WHERE alumnos.IDMalla = malla.IDMalla";
        Connection con = conexion.conectar(conection);
        Statement st = con.createStatement();
        ResultSet rs = st.executeQuery(sql);
        
        while (rs.next()) {
            
            int iDPersona = rs.getInt(1);
            int iDMalla = rs.getInt(2);
            String nombre = rs.getString(3);
            String apellidoPaterno = rs.getString(4);
            if (rs.wasNull()) {
                apellidoPaterno = "";
            }
            String apellidoMaterno = rs.getString(5);
            if (rs.wasNull()) {
                apellidoMaterno = "";
            }
            int tipoDocumento = rs.getInt(6);
            String ciRucPas = rs.getString(7);
            int nroCredencialCorporativo = rs.getInt(8);
            if (rs.wasNull()) {
                nroCredencialCorporativo = 0;
            }
            int nroCredencialSabsa = rs.getInt(9);
            if (rs.wasNull()) {
                nroCredencialSabsa = 0;
            }
            int numFolio = rs.getInt(10);
            if (rs.wasNull()) {
                numFolio = 0;
            }
            int numLicencia = rs.getInt(11);
            if (rs.wasNull()) {
                numLicencia = 0;
            }
            int isPiloto = rs.getInt(12);

            int usaLentes = rs.getInt(14);

            String direccion = rs.getString(15);
            String tipoDeSangre = rs.getString(16);
            String fechaDeNacimiento = rs.getString(17);
            int celular = rs.getInt(18);//idReferencia
            
            
            
            byte[] img = rs.getBytes(18);
            ImageIcon image = null;
            if(img != null){
                image = new ImageIcon(img);
            }
                
            int creditoFalso = rs.getInt(23);

            String fechaVigenciaSabsa = rs.getString(24);
            if (rs.wasNull()) {
                fechaVigenciaSabsa = "";
            }
            System.out.println(fechaVigenciaSabsa);
            
            String fechaVigenciaCorporativo = rs.getString(25);
            if (rs.wasNull()) {
                fechaVigenciaCorporativo = "";
            }
            String fechaVencimientoAptoMedico = rs.getString(26);
            if (rs.wasNull()) {
                fechaVencimientoAptoMedico = "";
            }
            String fechaVencimientoProficciencyCheck = rs.getString(27);
            if (rs.wasNull()) {
                fechaVencimientoProficciencyCheck = "";
            }
            String nombreMalla = rs.getString(30);
            if (rs.wasNull()) {
                nombreMalla = "";
            }
            Persona aux;
            
            aux = new Persona(iDPersona, iDMalla, nombre, apellidoPaterno, apellidoMaterno, tipoDocumento,
                        ciRucPas, nroCredencialCorporativo, nroCredencialSabsa, numFolio, numLicencia,
                        usaLentes, direccion, tipoDeSangre, fechaDeNacimiento, celular, image,
                        creditoFalso, fechaVigenciaSabsa, fechaVigenciaCorporativo, fechaVencimientoAptoMedico, nombreMalla);
            
            modelo.add(aux);
        }
        close(con);
        return modelo;
    }
    
    public int getIDMalla(int IDAlumno) throws SQLException {
        int IDMalla = -1;
        String sql = "SELECT alumno_docente_piloto.IDMalla\n" +
                     "FROM alumno_docente_piloto\n" +
                     "WHERE alumno_docente_piloto.IDPersona = "+IDAlumno;
        Connection con = conexion.conectar(conection);
        Statement st = con.createStatement();
        ResultSet rs = st.executeQuery(sql);
       
        while(rs.next()){
            IDMalla = rs.getInt(1);
        }
        close(con);
        return IDMalla;
    }
    
    public ComboBoxModel<String> getMateriasDeUnaMalla(int IDAlumno) throws SQLException {
        int IDMallaAlumno = getIDMalla(IDAlumno);
        DefaultComboBoxModel modelo = new DefaultComboBoxModel();
        String sql = "SELECT materia.Materia\n" +
                     "FROM materia,malla_materia\n" +
                     "WHERE materia.IDMateria = malla_materia.IDMateria AND malla_materia.IDMalla = "+IDMallaAlumno;
        Connection con = conexion.conectar(conection);
        Statement st = con.createStatement();
        ResultSet rs = st.executeQuery(sql);
        modelo.removeAllElements();
        while (rs.next()) {
            modelo.addElement(rs.getString(1));
        }
        close(con);
        return modelo;
    }
    
    public LinkedList<String> getMateriasYaAprobadas(int IDAlumno) throws SQLException {
        LinkedList<String> materias = new LinkedList();
        int IDMallaAlumno = getIDMalla(IDAlumno);
        String sql = "SELECT materia.Materia,notas.Nota\n" +
                    "FROM materia,malla_materia,notas,grupodisponibles\n" +
                    "WHERE materia.IDMateria = malla_materia.IDMateria AND malla_materia.IDMalla = "+IDMallaAlumno+" AND malla_materia.IDMateria = grupodisponibles.IDMateria AND grupodisponibles.IDGrupo = notas.IDGrupo AND notas.IDAlumno = "+IDAlumno+" AND (notas.Nota>70)";
        Connection con = conexion.conectar(conection);
        Statement st = con.createStatement();
        ResultSet rs = st.executeQuery(sql);
        while (rs.next()) {
            materias.add(rs.getString(1));
        }
        close(con);
        return materias;
    }
    
    public int getIDMateria(String NombreMateria) throws SQLException {
        int IDMateria = -1;
        String sql = "SELECT materia.IDMateria\n" +
                     "FROM materia\n" +
                     "WHERE materia.Materia = '"+NombreMateria+"'";
        Connection con = conexion.conectar(conection);
        Statement st = con.createStatement();
        ResultSet rs = st.executeQuery(sql);
        while (rs.next()) {
            IDMateria = rs.getInt(1);
        }
        close(con);
        return IDMateria;
    }
    
    public LinkedList<GrupoHora> getGruposDeUnaMateria(String NombreMateria) throws SQLException {
        LinkedList<GrupoHora> materias = new LinkedList<GrupoHora>();
        int IDMateria = getIDMateria(NombreMateria);
        String sql = "SELECT grupodisponibles.NombreGrupo,grupodisponibles.Horario\n" +
"                    FROM grupodisponibles\n" +
"                    WHERE grupodisponibles.isBorrado = 0 AND grupodisponibles.IDMateria = "+IDMateria;
        Connection con = conexion.conectar(conection);
        Statement st = con.createStatement();
        ResultSet rs = st.executeQuery(sql);
        while (rs.next()) {
            DateFormat dateFormat = new SimpleDateFormat("hh:mm:ss");
            String strTime = dateFormat.format(rs.getTime(2));
            System.out.println(strTime);
            GrupoHora aux = new GrupoHora(rs.getString(1),strTime);
            materias.add(aux);
        }
        close(con);
        return materias;
    }
    
    public void inscribirAlumnoAGrupo(int IDAlumno,int IDGrupo) throws SQLException {
        String query = "INSERT INTO alumno_grupoinscrito(IDGrupoDisponible,IDAlumno, NotasIngresadas, FechaDeInscripcion)\n" +
                        "VALUES ("+IDGrupo+","+IDAlumno+", 0, ?)";
        Connection con = conexion.conectar(conection);
        PreparedStatement pst;
        pst = con.prepareStatement(query);
        pst.setString(1, FechaActual2());
        pst.executeUpdate();
        pst.close();
        
        query = "INSERT INTO notas(notas.IDGrupo,notas.IDAlumno,notas.IDTipoEval,notas.Nota,notas.Fecha,notas.Estado)\n"
                + "VALUES\n"
                + "(?,?,?,?,?,?)";
        PreparedStatement pst1;
        pst1 = con.prepareStatement(query);
        pst1.setInt(1, IDGrupo);
        pst1.setInt(2, IDAlumno);
        pst1.setInt(3, 1);
        pst1.setInt(4, 0);
        pst1.setString(5, "0000-00-00");
        pst1.setString(6,"CURSANDO");
        
        pst1.executeUpdate();
        pst1.close();
        close(con);
    }
    
    public int getIDGrupo(String nomGrupo)throws SQLException {
        int IDGrupo = -1;
        String sql = "SELECT grupodisponibles.IDGrupo\n" +
                    "FROM grupodisponibles\n" +
                    "WHERE grupodisponibles.NombreGrupo = '"+nomGrupo+"'";
        Connection con = conexion.conectar(conection);
        Statement st = con.createStatement();
        ResultSet rs = st.executeQuery(sql);
        while (rs.next()) {
            IDGrupo = rs.getInt(1);
        }
        close(con);
        return IDGrupo;
    }
    
    public String getNombreGrupo(int id) throws SQLException
    {
        String nombre="";
        String sql = "SELECT NombreGrupo\n"
                + "FROM grupodisponibles\n"
                + "WHERE grupodisponibles.IDGrupo="+id;
        Connection con = conexion.conectar(conection);
        Statement st = con.createStatement();
        ResultSet rs = st.executeQuery(sql);
        while (rs.next()) {
            nombre = rs.getString(1);
        }
        close(con);
        return nombre;
    }
    
    public LinkedList<String> getGruposYaTomados(int IDAlumno) throws SQLException {
        LinkedList<String> materias = new LinkedList();
        String sql = "SELECT grupodisponibles.NombreGrupo\n" +
                    "FROM alumno_grupoinscrito,grupodisponibles\n" +
                    "WHERE alumno_grupoinscrito.IDAlumno = "+IDAlumno+" AND grupodisponibles.IDGrupo = alumno_grupoinscrito.IDGrupoDisponible";
        Connection con = conexion.conectar(conection);
        Statement st = con.createStatement();
        ResultSet rs = st.executeQuery(sql);
        while (rs.next()) {
            materias.add(rs.getString(1));
        }
        close(con);
        return materias;
    }
    
    public DefaultComboBoxModel getModeloMalla(LinkedList<CRTVuelo> mandar) throws SQLException {

        DefaultComboBoxModel modelo = new DefaultComboBoxModel();
        String sql = "SELECT *\n"
                + "  FROM malla" 
                + "  WHERE malla.isBorrado = 0";
        Connection con = conexion.conectar(conection);
        Statement st = con.createStatement();
        ResultSet rs = st.executeQuery(sql);

        modelo.removeAllElements();
        modelo.addElement("--------");
        while (rs.next()) {
            int ID = rs.getInt(1);
            String desc = rs.getString(2);
            int minutos = 0;
            minutos = rs.getInt(3);
  
            CRTVuelo aux = new CRTVuelo(ID, desc, minutos);
            mandar.add(aux);

            modelo.addElement(desc);

        }
        close(con);
        return modelo;
    }
    

}
                
