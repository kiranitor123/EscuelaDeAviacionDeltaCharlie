/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

import controller.controlador;
import deltacharlie.DeltaCharlie;
import deltacharlie.JFrameInternalAgregarDetalles;
import deltacharlie.principal;
import entity.Avion;
import entity.CrearModificarBorrar;
import entity.Helices;
import entity.Materia;
import entity.Motor;
import entity.Pagos;
import entity.Persona;
import entity.Grupo;
import entity.Malla;
import entity.Grupo;
import entity.Materia;
import entity.Usuario;
import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.EventObject;
import java.util.LinkedList;
import java.util.Vector;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.AbstractCellEditor;
import javax.swing.JButton;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.table.TableCellEditor;
import javax.swing.table.TableCellRenderer;

/**
 *
 * @author Torrax
 */
public class JButtonRenderer extends AbstractCellEditor implements TableCellEditor, TableCellRenderer {

        private JFrameInternalAgregarDetalles frmDetalles;
        public String nombre;
        public String objeto;
        public static String NombreE;
        public static String NombreP;
        public static String NombreA;
        public static String TV;
        public static String HorasVuelo;
        private controlador control;
        public static Avion avionBuscado;
        public static Motor motorBuscado;
        public static Pagos multa;
        public static Persona alumnoBuscado;
        public static Helices heliceBuscada;
        public static Persona docentepilotoBuscado;
        public static Usuario usuarioBuscado;
        public static Malla mallaBuscada;
        public static Grupo grupoBuscado;
        public static Materia materiaBuscado;
    
        public JButtonRenderer(String nombre, String objeto, controlador control) {
            this.nombre = nombre;
            this.objeto = objeto;
            this.control = control;
            avionBuscado = null;
            motorBuscado = null;
            alumnoBuscado = null;
            docentepilotoBuscado = null;
            usuarioBuscado = null;
            mallaBuscada = null;
        }

        @Override
        public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
            JButton b = new JButton(nombre);
            return b;
        }

        @Override
        public Component getTableCellEditorComponent(JTable table, Object value, boolean isSelected, final int row, int column) {
            JButton b = new JButton(nombre);
            conexion modelo = new conexion();
            b.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    if(nombre.equals("Detalles")){
                        switch(objeto){
                            case "Avion":
                            {
                                System.out.println("Detalles de Avion");
                                String aux = table.getValueAt(row, 0).toString()+"-"+table.getValueAt(row, 1).toString()+"-"+table.getValueAt(row, 2).toString();
                                int id = modelo.getIDAvion(aux);
                                try {
                                    avionBuscado = modelo.obtenerAvion(id);
                                } catch (SQLException ex) {
                                    Logger.getLogger(JButtonRenderer.class.getName()).log(Level.SEVERE, null, ex);
                                }
                                control.detallesAvion(avionBuscado);
                                break;
                            }
                            case "Motor":
                            {
                                System.out.println("Detalles de Motor");
                                String aux = table.getValueAt(row, 0).toString()+"-"+table.getValueAt(row, 1).toString()+"-"+table.getValueAt(row, 2).toString();
                                int id = modelo.getIDMotor(aux);
                                Vector<String> detalles = new Vector<>();
                                detalles = modelo.detallesMotor(id);
                                control.detallesMotor(detalles);
                                break;
                            }
                            case "Helice":
                            {
                                System.out.println("Detalles de Helice");
                                String aux = table.getValueAt(row, 0).toString()+"-"+table.getValueAt(row, 1).toString()+"-"+table.getValueAt(row, 2).toString();
                                int id = modelo.getIDHelice(aux);
                                Helices detalles = modelo.detallesHelice(id);
                                control.detallesHelice(detalles);
                                break;
                            }
                            case "Alumno":
                            {
                                System.out.println("Detalles de Alumno");
                                String aux = table.getValueAt(row, 0).toString()+" "+table.getValueAt(row, 1).toString()+" "+table.getValueAt(row, 2).toString();
                                int id = modelo.getIDAlumno_Docente_Piloto(aux);
                                Persona detalles = new Persona();
                                try {
                                    detalles = modelo.detallesAlumno(id);
                                } catch (SQLException ex) {
                                    Logger.getLogger(JButtonRenderer.class.getName()).log(Level.SEVERE, null, ex);
                                }
                                control.detallesAlumno(detalles);
                                break;
                            }
                            case "Docente/Piloto":
                            {
                                System.out.println("Detalles de Docente/Piloto");
                                String aux = table.getValueAt(row, 0).toString()+" "+table.getValueAt(row, 1).toString()+" "+table.getValueAt(row, 2).toString();
                                int id = modelo.getIDAlumno_Docente_Piloto(aux);
                                Persona detalles = new Persona();
                                try {
                                    detalles = modelo.detallesDocentePiloto(id);
                                } catch (SQLException ex) {
                                    Logger.getLogger(JButtonRenderer.class.getName()).log(Level.SEVERE, null, ex);
                                }
                                control.detallesDocentePiloto(detalles);
                                break;
                            }
                            case "Usuario":
                            {
                                System.out.println("Detalles de Usuario");
                                String aux = table.getValueAt(row, 0).toString();
                                int id = modelo.getIDUsuario(aux);
                                Usuario detalles = new Usuario();
                                try {
                                    detalles = modelo.detallesUsuario(id);
                                } catch (SQLException ex) {
                                    Logger.getLogger(JButtonRenderer.class.getName()).log(Level.SEVERE, null, ex);
                                }
                                control.detallesUsuario(detalles);
                                break;
                            }
                            case "Materia":
                            {
                                System.out.println("Detalles de Materia");
                                String aux = table.getValueAt(row, 0).toString();
                                int id = 0;
                                try {
                                    id = modelo.getIDMateria(aux);
                                } catch (SQLException ex) {
                                    Logger.getLogger(JButtonRenderer.class.getName()).log(Level.SEVERE, null, ex);
                                }
                                Materia detalles = new Materia();
                                try {
                                    detalles = modelo.detallesMateria(id);
                                } catch (SQLException ex) {
                                    Logger.getLogger(JButtonRenderer.class.getName()).log(Level.SEVERE, null, ex);
                                }
                                control.detallesMateria(detalles);
                                break;
                            }
                            case "Malla":
                            {
                                try {
                                    Vector<Integer> auxMaterias = new Vector<Integer>();
                                    System.out.println("Detalles de Malla");
                                    String aux = table.getValueAt(row, 0).toString();
                                    int id = modelo.ConseguirIDMalla(aux);
                                    Malla detalles = new Malla();
                                    try {
                                        detalles = modelo.detallesMalla(id);
                                        auxMaterias = modelo.detallesMallaMaterias(id);
                                    } catch (SQLException ex) {
                                        Logger.getLogger(JButtonRenderer.class.getName()).log(Level.SEVERE, null, ex);
                                    }
                                    control.detallesMalla(detalles,auxMaterias);
                                    break;
                                } catch (SQLException ex) {
                                    Logger.getLogger(JButtonRenderer.class.getName()).log(Level.SEVERE, null, ex);
                                }
                            }
                            case "Grupo":
                            {
                                System.out.println("Detalles de Grupo");
                                Grupo detalles = new Grupo();
                                try{
                                    String aux = table.getValueAt(row, 0).toString();
                                    int id = modelo.getIDGrupo(aux);
                                    detalles=modelo.detallesGrupo(id);
                                }catch (SQLException ex) {
                                    Logger.getLogger(JButtonRenderer.class.getName()).log(Level.SEVERE, null, ex);
                                }
                                control.detallesGrupo(detalles);
                                break;
                            }
                            default:
                            {
                                System.out.println("Intente de nuevo");
                                break;
                            }
                        }
                    }
                    if(nombre.equals("Confirmar")){
                        switch(objeto){
                            case "Reservas":
                                {
                                        control.Detalles();
                                        System.out.println("Entro");
                                        TV = table.getValueAt(row,0).toString();
                                        NombreE = table.getValueAt(row, 2).toString();
                                        NombreP = table.getValueAt(row, 1).toString();
                                        NombreA = table.getValueAt(row, 3).toString();
                                        HorasVuelo = table.getValueAt(row, 4).toString();
                                        System.out.println("Entro despues de boton");
                                        System.out.println("Llegue hasta aqui puto");
                                }
                        }
                    }
                    if(nombre.equals("Cancelar")){
                        switch(objeto){
                            case "Reservas":
                                {
                                    DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
                                    Date date = new Date();
                                    DateFormat dateFormat2 = new SimpleDateFormat("HH:mm");
                                    Date date2 = new Date();
                                    TV = table.getValueAt(row,0).toString();
                                    NombreP = table.getValueAt(row, 1).toString();
                                    NombreA = table.getValueAt(row, 3).toString();
                                    NombreE = table.getValueAt(row, 2).toString();
                                    HorasVuelo = table.getValueAt(row, 4).toString();
                                    int auxid=0;
                                    int idCopiloto=0;
                                    int idAvion=0;
                                    try {
                                        auxid = modelo.ConseguirIDDocente(NombreE);
                                        idCopiloto = modelo.ConseguirIDDocente(NombreP);
                                        idAvion = modelo.getIDAvion(NombreA);
                                    } catch (SQLException ex) {
                                        Logger.getLogger(JButtonRenderer.class.getName()).log(Level.SEVERE, null, ex);
                                    }
                                    Pagos multa = new Pagos(0, auxid, dateFormat.format(date), dateFormat2.format(date2), "Multa", 500, DeltaCharlie.IDUser, 1, 4, 0);
                                    try {
                                        modelo.insertarPago(multa, DeltaCharlie.IDUser);
                                        modelo.eliminarReserva(TV, idCopiloto, auxid, idAvion, Integer.parseInt(HorasVuelo));
                                        modelo.vertablaReservas(control.frmVuelo.jTableReservas, control);
                                        control.frmVuelo.jTableReservas.repaint();
                                    } catch (SQLException ex) {
                                        Logger.getLogger(JButtonRenderer.class.getName()).log(Level.SEVERE, null, ex);
                                    }
                                }
                        }
                    }
                    if(nombre.equals("Modificar")){
                        switch(objeto){
                            case "Avion":
                            {
                                System.out.println("Modificar Avion");
                                String aux = table.getValueAt(row, 0).toString()+"-"+table.getValueAt(row, 1).toString()+"-"+table.getValueAt(row, 2).toString();
                                int id = modelo.getIDAvion(aux);
                                try {
                                    avionBuscado = modelo.obtenerAvion(id);
                                } catch (SQLException ex) {
                                    Logger.getLogger(JButtonRenderer.class.getName()).log(Level.SEVERE, null, ex);
                                }
                                control.crearAvion(CrearModificarBorrar.MODIFICAR);
                                break;
                            }
                            case "Motor":
                            {
                                System.out.println("Modificar Motor");
                                String aux = table.getValueAt(row, 0).toString()+"-"+table.getValueAt(row, 1).toString()+"-"+table.getValueAt(row, 2).toString();
                                int id = modelo.getIDMotor(aux);
                                motorBuscado = modelo.ObtenerMotor(id);
                                control.crearMotor(CrearModificarBorrar.MODIFICAR);
                                break;
                            }
                            case "Helice":
                            {
                                System.out.println("Modificar de Helice");
                                String aux = table.getValueAt(row, 0).toString()+"-"+table.getValueAt(row, 1).toString()+"-"+table.getValueAt(row, 2).toString();
                                int id = modelo.getIDHelice(aux);
                                heliceBuscada = modelo.ObtenerHelice(id);
                                control.crearHelice(CrearModificarBorrar.MODIFICAR);
                                break;
                            }
                            case "Alumno":
                            {
                                System.out.println("Modificar Alumno");
                                String aux = table.getValueAt(row, 0).toString()+" "+table.getValueAt(row, 1).toString()+" "+table.getValueAt(row, 2).toString();
                                int id = modelo.getIDAlumno_Docente_Piloto(aux);
                                Persona detalles = new Persona();
                                try {
                                    alumnoBuscado = modelo.detallesAlumno(id);
                                } catch (SQLException ex) {
                                    Logger.getLogger(JButtonRenderer.class.getName()).log(Level.SEVERE, null, ex);
                                }
                                control.crearAlumno(CrearModificarBorrar.MODIFICAR);    
                                break;
                            }
                            case "Docente/Piloto":
                            {
                                System.out.println("Modificar Docente/Piloto");
                                String aux = table.getValueAt(row, 0).toString()+" "+table.getValueAt(row, 1).toString()+" "+table.getValueAt(row, 2).toString();
                                int id = modelo.getIDAlumno_Docente_Piloto(aux);
                                Persona detalles = new Persona();
                                try {
                                    docentepilotoBuscado = modelo.detallesDocentePiloto(id);
                                } catch (SQLException ex) {
                                    Logger.getLogger(JButtonRenderer.class.getName()).log(Level.SEVERE, null, ex);
                                }
                                control.crearDocentePiloto(CrearModificarBorrar.MODIFICAR);  
                                break;
                            }
                            case "Usuario":
                            {
                                System.out.println("Modificar Usuario");
                                String aux = table.getValueAt(row, 0).toString();
                                int id = modelo.getIDUsuario(aux);
                                Usuario detalles = new Usuario();
                                try {
                                    usuarioBuscado = modelo.detallesUsuario(id);
                                } catch (SQLException ex) {
                                    Logger.getLogger(JButtonRenderer.class.getName()).log(Level.SEVERE, null, ex);
                                }
                                control.crearUsuario(CrearModificarBorrar.MODIFICAR);
                                break;
                            }
                            case "Materia":
                            {
                                System.out.println("Modificar Materia");
                                String aux = table.getValueAt(row, 0).toString();
                                int id=0;
                                try {
                                    id = modelo.getIDMateria(aux);
                                } catch (SQLException ex) {
                                    Logger.getLogger(JButtonRenderer.class.getName()).log(Level.SEVERE, null, ex);
                                }
                                try {
                                    materiaBuscado = modelo.detallesMateria(id);
                                } catch (SQLException ex) {
                                    Logger.getLogger(JButtonRenderer.class.getName()).log(Level.SEVERE, null, ex);
                                }
                                control.crearMateria(CrearModificarBorrar.MODIFICAR);
                                break;
                            }
                            case "Malla":
                            {
                                System.out.println("Modificar Malla");
                                String aux = table.getValueAt(row, 0).toString();
                                int id = modelo.getIDMalla(aux);
                                Malla detalles = new Malla();
                                try {
                                    mallaBuscada = modelo.detallesMalla(id);
                                    System.out.println(mallaBuscada.getHorasVuelo());
                                    System.out.println(mallaBuscada.getHorasSimulador());
                                    System.out.println(mallaBuscada.getNombreMalla());
                                } catch (SQLException ex) {
                                    Logger.getLogger(JButtonRenderer.class.getName()).log(Level.SEVERE, null, ex);
                                }
                                control.crearMalla(CrearModificarBorrar.MODIFICAR);
                                break;
                            }
                            case "Grupo":
                            {
                                System.out.println("Modificar Grupo");
                                String aux = table.getValueAt(row, 0).toString();
                                int id=0;
                                Malla detalles = new Malla();
                                try {
                                    id = modelo.getIDGrupo(aux);
                                    grupoBuscado = modelo.detallesGrupo(id);
                                } catch (SQLException ex) {
                                    Logger.getLogger(JButtonRenderer.class.getName()).log(Level.SEVERE, null, ex);
                                }
                                control.crearGrupo(CrearModificarBorrar.MODIFICAR);
                                break;
                            }
                            default:
                            {
                                System.out.println("Intente de nuevo");
                                break;
                            }
                        }
                    }
                    if(nombre.equals("Eliminar")){
                        switch(objeto){
                            case "Avion":
                            {
                                System.out.println("Eliminar Avion");
                                String aux = table.getValueAt(row, 0).toString()+"-"+table.getValueAt(row, 1).toString()+"-"+table.getValueAt(row, 2).toString();
                                int id = modelo.getIDAvion(aux);
                                try {
                                    avionBuscado = modelo.obtenerAvion(id);
                                } catch (SQLException ex) {
                                    Logger.getLogger(JButtonRenderer.class.getName()).log(Level.SEVERE, null, ex);
                                }
                                control.crearAvion(CrearModificarBorrar.BORRAR);
                                break;
                            }
                            case "Motor":
                            {
                                System.out.println("Eliminar Motor");
                                String aux = table.getValueAt(row, 0).toString()+"-"+table.getValueAt(row, 1).toString()+"-"+table.getValueAt(row, 2).toString();
                                int id = modelo.getIDMotor(aux);
                                motorBuscado = modelo.ObtenerMotor(id);
                                control.crearMotor(CrearModificarBorrar.BORRAR);
                                break;
                            }
                            case "Helice":
                            {
                                System.out.println("Eliminar Helice");
                                String aux = table.getValueAt(row, 0).toString()+"-"+table.getValueAt(row, 1).toString()+"-"+table.getValueAt(row, 2).toString();
                                int id = modelo.getIDHelice(aux);
                                heliceBuscada = modelo.ObtenerHelice(id);
                                control.crearHelice(CrearModificarBorrar.BORRAR);
                                break;
                            }
                            case "Alumno":
                            {
                                System.out.println("Eliminar Alumno");
                                String aux = table.getValueAt(row, 0).toString()+" "+table.getValueAt(row, 1).toString()+" "+table.getValueAt(row, 2).toString();
                                int id = modelo.getIDAlumno_Docente_Piloto(aux);
                                try {
                                    alumnoBuscado = modelo.detallesAlumno(id);
                                } catch (SQLException ex) {
                                    Logger.getLogger(JButtonRenderer.class.getName()).log(Level.SEVERE, null, ex);
                                }
                                control.crearAlumno(CrearModificarBorrar.BORRAR);
                                break;
                            }
                            case "Docente/Piloto":
                            {
                                System.out.println("Eliminar Docente/Piloto");
                                String aux = table.getValueAt(row, 0).toString()+" "+table.getValueAt(row, 1).toString()+" "+table.getValueAt(row, 2).toString();
                                int id = modelo.getIDAlumno_Docente_Piloto(aux);
                                Persona detalles = new Persona();
                                try {
                                    docentepilotoBuscado = modelo.detallesDocentePiloto(id);
                                } catch (SQLException ex) {
                                    Logger.getLogger(JButtonRenderer.class.getName()).log(Level.SEVERE, null, ex);
                                }
                                control.crearDocentePiloto(CrearModificarBorrar.BORRAR);
                                break;
                            }
                            case "Usuario":
                            {
                                System.out.println("Eliminar Usuario");
                                String aux = table.getValueAt(row, 0).toString();
                                int id = modelo.getIDUsuario(aux);
                                Usuario detalles = new Usuario();
                                try {
                                    usuarioBuscado = modelo.detallesUsuario(id);
                                } catch (SQLException ex) {
                                    Logger.getLogger(JButtonRenderer.class.getName()).log(Level.SEVERE, null, ex);
                                }
                                control.crearUsuario(CrearModificarBorrar.BORRAR);
                                break;
                            }
                            case "Materia":
                            {
                                System.out.println("Eliminar Materia");
                                String aux = table.getValueAt(row, 0).toString();
                                int id = 0;
                                try {
                                    id = modelo.getIDMateria(aux);
                                    materiaBuscado = (modelo.detallesMateria(id));                                    
                                } catch (SQLException ex) {
                                    Logger.getLogger(JButtonRenderer.class.getName()).log(Level.SEVERE, null, ex);
                                }
                                control.crearMateria(CrearModificarBorrar.BORRAR);
                                break;
                            }
                            case "Malla":
                            {
                                System.out.println("Eliminar Malla");
                                String aux = table.getValueAt(row, 0).toString();
                                int id = 0;
                                Malla detalles = new Malla();
                                try {
                                    id = modelo.getIDMalla(aux);
                                    mallaBuscada = (modelo.detallesMalla(id));
                                    
                                    System.out.println(mallaBuscada.getNombreMalla());
                                    System.out.println(mallaBuscada.getRequiereLicencia());
                                    System.out.println(mallaBuscada.getCosto());
                                    System.out.println(mallaBuscada.getIsBorrado());
                                    
                                } catch (SQLException ex) {
                                    Logger.getLogger(JButtonRenderer.class.getName()).log(Level.SEVERE, null, ex);
                                }
                                control.crearMalla(CrearModificarBorrar.BORRAR);
                                break;
                            }
                            case "Grupo":
                            {
                                System.out.println("Eliminar Grupo");
                                String aux = table.getValueAt(row, 0).toString();
                                int id = 0;
                                Grupo detalles = new Grupo();
                                try {
                                    id = modelo.getIDGrupo(aux);
                                    grupoBuscado = modelo.detallesGrupo(id);
                                } catch (SQLException ex) {
                                    Logger.getLogger(JButtonRenderer.class.getName()).log(Level.SEVERE, null, ex);
                                }
                                control.crearGrupo(CrearModificarBorrar.BORRAR);
                                break;
                            }
                            default:
                            {
                                System.out.println("Intente de nuevo");
                                break;
                            }
                        }
                    }
                }
            });
            return b;
        }

        @Override
        public Object getCellEditorValue() {
            return null;
        }

        @Override
        public boolean isCellEditable(EventObject anEvent) {
            return true;
        }

        @Override
        public boolean shouldSelectCell(EventObject anEvent) {
            return true;
        }
    }