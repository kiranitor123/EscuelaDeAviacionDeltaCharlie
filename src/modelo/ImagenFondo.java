/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

import java.awt.Component;
import java.awt.Graphics;
import java.awt.Insets;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;
import javax.swing.border.Border;

/**
 *
 * @author Ritha Carolina
 */
public class ImagenFondo implements Border {
    public BufferedImage back;
    
    public ImagenFondo(){
        try
        {
            URL imagePath = new URL(getClass().getResource("/imagenes/logo.png").toString());
            back = ImageIO.read(imagePath);
        }catch(MalformedURLException e){}
         catch (IOException ex) {
            Logger.getLogger(ImagenFondo.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    @Override
    public void paintBorder(Component c, Graphics g, int x, int y, int width, int height) {
        
        g.drawImage(back, (x+(width-back.getWidth())/2), (y+(height-back.getHeight())/2), c);
    }

    @Override
    public Insets getBorderInsets(Component c) {
        return new Insets(0,0,0,0);
    }

    @Override
    public boolean isBorderOpaque() {
        return false;
    }
    
}
