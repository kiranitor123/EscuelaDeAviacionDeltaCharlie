package controller;

import java.awt.Dimension;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.beans.PropertyVetoException;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.swing.JInternalFrame;
import javax.swing.JOptionPane;
import deltacharlie.*;
import entity.Avion;
import entity.CrearModificarBorrar;
import entity.Grupo;
import entity.Helices;
import entity.Malla;
import entity.Malla_Materia;
import entity.Materia;
import entity.Motor;
import entity.Pagos;
import entity.Persona;
import entity.Referencia;
import entity.TelefonoReferencia;
import entity.Usuario;
import java.awt.Image;
import java.io.File;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.LinkedList;
import java.util.Map;
import java.util.Vector;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.ImageIcon;
import javax.swing.JFileChooser;
import javax.swing.JPanel;
import javax.swing.filechooser.FileNameExtensionFilter;
import modelo.*;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.export.JRPdfExporter;
import net.sf.jasperreports.export.ExporterInput;
import net.sf.jasperreports.export.OutputStreamExporterOutput;
import net.sf.jasperreports.export.SimpleExporterInput;
import net.sf.jasperreports.export.SimpleOutputStreamExporterOutput;
import net.sf.jasperreports.export.SimplePdfExporterConfiguration;
import deltacharlie.ThreadAlertas;
import entity.MateriasATomar;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.swing.Icon;
import javax.swing.table.DefaultTableModel;
import java.security.SecureRandom;
import java.util.Base64;
import java.util.Optional;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.util.Arrays;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;


public class controlador implements ActionListener {

    private principal view;
    private static conexion modelo;
    private static FrameLogin frmlogin;
    public static FrameAjustes frmAjustes;
    private static ConnectionUtils Connection;

    //frames hijos
    private JFrameInternalAvion frmCrearAvion;
    private JFrameInternalBuscarDocente frmBuscarDocente;
    private JFrameInternalAgregarDetalles frmDetalles;
    private JFrameInternalBuscarMateria frmBuscarMateria;
    private JFrameInternalBuscarAlumno frmBuscarAlumno;
    private JFrameInternalCrearGrupo frmCrearGrupo;
    private JFrameInternalCrearMateria frmCrearMateria;
    private JFrameInternalBuscarGrupo frmBuscarGrupo;
    private JFrameInternalBuscarHMA frmBuscarHMA;
    private JFrameInternalHelice frmHelice;
    private JFrameInternalMotor frmMotor;
    private JFrameInternalDocentePiloto frmDocentePiloto;
    private JFrameInternalTomaMateria frmTomaMateria;
    private JFrameInternalCrearAlumno frmCrearNuevoAlumno;
    public JFrameInternalVuelo frmVuelo;
    private JFrameInternalCrearMalla frmCrearMalla;
    private JFrameInternalBuscarMalla frmBuscarMalla;
    private JFrameInternalCrearUsuario frmCrearUsuario;
    private JFrameInternalBuscarUsuario frmBuscarUsuario;
    private JFrameInternalReporteFlexible frmCrearReporteFlexible;
    private JFrameInternalPagos frmCrearPagos;
    private JFrameInternalAcercaDe frmAcercaDe;
    private JFrameInternalAlertas frmRevisarAlertas;
    private JFrameInternalDetallesAvion frmDetallesAvion;
    private JFrameInternalPrincipalAvion frmPrincipalAvion;
    private JFrameInternalDetallesMotor frmDetallesMotor;
    private JFrameInternalDetallesGrupo frmDetallesGrupo;
    private JFrameInternalPrincipalMotor frmPrincipalMotor;
    private JFrameInternalDetallesAlumno frmDetallesAlumno;
    private JFrameInternalPrincipalAlumno frmPrincipalAlumno;
    private JFrameInternalPrincipalGrupo frmPrincipalGrupo;
    private JFrameInternalPrincipalNotas frmPrincipalNotas;
    private JFrameInternalDetallesHelice frmDetallesHelice;
    private JFrameInternalDetallesMateria frmDetallesMateria;
    private JFrameInternalPrincipalMalla frmPrincipalMalla;
    private JFrameInternalDetallesMalla frmDetallesMalla;
    private JFrameInternalPrincipalHelice frmPrincipalHelice;
    private JFrameInternalPrincipalMateria frmPrincipalMateria;
    private JFrameInternalDetallesDocentePiloto frmDetallesDocentePiloto;
    private JFrameInternalPrincipalDocentePiloto frmPrincipalDocentePiloto;
    private JFrameInternalDetallesUsuario frmDetallesUsuario;
    private JFrameInternalPrincipalUsuario frmPrincipalUsuario;
    Hashtable<String,String> contenedormateriaid = new Hashtable<String,String>();
    Hashtable<String,String> contenedormateria = new Hashtable<String,String>();
    Hashtable<String,String> contenedorCosto = new Hashtable<String,String>();
    Hashtable<String,String> contenedorMinutos = new Hashtable<String,String>();
    Vector<Integer> ids = new Vector<>();
    Vector<Integer> NotasParciales = new Vector<>();
    private JFrameInternalCreditos frmControlCreditos;
    ThreadAlertas hilo1 = new ThreadAlertas("Uno");

    //Variables Utilizadas para generar Reportes
    String reportSrcFile = null;
    JasperReport jasperReport = null;
    Map<String, Object> parameters = null;
    JasperPrint print = null;
    JFileChooser chooser;
    String ruta;
    File outDir = null;
    JRPdfExporter exporter = null;
    ExporterInput exporterInput = null;
    OutputStreamExporterOutput exporterOutput = null;
    SimplePdfExporterConfiguration configuration = null;
    InputStream file = null;
    public static int inicio = 0;
    int idReportes = 0;
    
    private String usuario;
    public int auxidmalla;
    public int auxhash = 0;
    public int i=0;
    public String AnotadorParciales1="";
    public String AnotadorParciales2="";
    public String AnotadorParciales3="";
    public int parciales=0;
    File fichero = null;
    JPanel contentPane = null;
//___________________________________________________________________________________ Soy una barra separadora :)
    //En el constructor inicializamos nuestros objetos
    public controlador(principal vista, conexion modelo) {
        this.view = vista;
        controlador.modelo = modelo;
        login();
    }
    //___________________________________________________________________________________ Soy una barra separadora :)

    public controlador() {
        
    }

    /*Encriptacion SHA 1*/
    private String getHash(String message) {
        try {
            MessageDigest md;
            byte[] buffer, digest;
            String hash = "";
            buffer = message.getBytes();
            md = MessageDigest.getInstance("SHA1");
            md.update(buffer);
            digest = md.digest();
            for (byte aux : digest) {
                int b = aux & 0xff;
                if (Integer.toHexString(b).length() == 1) {
                    hash += "0";
                }
                hash += Integer.toHexString(b);
            }
            return hash;
        } catch (NoSuchAlgorithmException ex) {
            Logger.getLogger(controlador.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    private String cript(String pass) {

        String basura = "rño781uhhd12731yhsad123";

        return getHash(getHash(pass + basura) + basura);
    }
    //___________________________________________________________________________________ Soy una barra separadora :)

    /*LOGIN*/
    private void login() {
        if (frmlogin == null) {
            frmlogin = new FrameLogin();

            frmlogin.cmdIngresar.addActionListener(this);
            frmlogin.cmdCancelar.addActionListener(this);
        }
        frmlogin.setLocationRelativeTo(null);
        frmlogin.setVisible(true);
    }
    //___________________________________________________________________________________ Soy una barra separadora :)
    
    /*AJUSTES*/
    private void ajustes() {
        if (frmAjustes == null) {
            frmAjustes = new FrameAjustes();

            frmAjustes.jButtonAceptar.addActionListener(this);
            frmAjustes.jButtonCancelar.addActionListener(this);
            
        }
        frmAjustes.setLocationRelativeTo(null);
        frmAjustes.setVisible(true);
    }
    //___________________________________________________________________________________ Soy una barra separadora :)

    /*Internal Frame Crear Grupo*/
    private void crearGrupo() {
        if (estacerrado(frmCrearGrupo)) {

            try {
                frmCrearGrupo = new JFrameInternalCrearGrupo(modelo, CrearModificarBorrar.CREAR);
                this.view.jDesktopPane.add(frmCrearGrupo);

                frmCrearGrupo.jComboBoxMaterias.setModel(modelo.ObtenerListaMaterias(frmCrearGrupo.materias));

                frmCrearGrupo.jButtonAceptar.setActionCommand("Aceptar Crear Grupo");
                frmCrearGrupo.jButtonCancelar.setActionCommand("Cancelar Crear Grupo");
                
                frmCrearGrupo.jComboBoxDocentes.setActionCommand("Combo Docentes");
                frmCrearGrupo.jComboBoxMaterias.setActionCommand("Combo Materias");

                frmCrearGrupo.jButtonAceptar.addActionListener(this);
                frmCrearGrupo.jButtonCancelar.addActionListener(this);
                frmCrearGrupo.jComboBoxDocentes.addActionListener(this);
                frmCrearGrupo.jComboBoxMaterias.addActionListener(this);

                frmCrearGrupo.setLocation(centradoXY(frmCrearGrupo));
                frmCrearGrupo.setVisible(true);
            } catch (SQLException ex) {
                JOptionPane.showMessageDialog(this.view, "Error Carga Datos");

            }
        } else {
            JOptionPane.showMessageDialog(this.view, "La ventana Crear Malla se encuentra abierta");

        }
    }
    //___________________________________________________________________________________ Soy una barra separadora :)

    /*Internal Frame Crear Malla*/
    public void crearMalla(CrearModificarBorrar cmb) {
        if (estacerrado(frmCrearMalla)) {
            frmCrearMalla = new JFrameInternalCrearMalla(modelo, cmb);
            if(cmb.equals(CrearModificarBorrar.MODIFICAR) || cmb.equals(CrearModificarBorrar.CREAR)){
                    this.view.jDesktopPane.add(frmCrearMalla);

                    frmCrearMalla.jButtonAgregarMateriaSeleccionada.addActionListener(this);
                    frmCrearMalla.jButtonEliminarMateriaSeleccionada.addActionListener(this);
                    frmCrearMalla.jButtonCrearMalla.addActionListener(this);
                    frmCrearMalla.jButtonCancelar.addActionListener(this);

                    frmCrearMalla.setLocation(centradoXY(frmCrearMalla));
                    frmCrearMalla.setVisible(true);

                    frmCrearMalla.estadoInicial();
                    if(cmb.equals(CrearModificarBorrar.MODIFICAR)){
                        frmCrearMalla.buscada = JButtonRenderer.mallaBuscada; 
                        auxidmalla = 0;
                        contenedormateriaid.clear();
                        contenedormateria.clear();
                        contenedorCosto.clear();
                        contenedorMinutos.clear();
                        frmCrearMalla.jTableMateriasCorrespondientesMalla.setModel(modelo.LlenarTabla(contenedormateriaid, contenedormateria, contenedorCosto, contenedorMinutos));
                        frmCrearMalla.jTextFieldNombreMalla.setText("");
                        frmCrearMalla.jTextFieldBuscarMateria.setText("");
                        frmCrearMalla.jCheckBoxRequiereLicencia.setSelected(false);
                        frmCrearMalla.jLabelCostoTotalMallaV.setText("0");
                        frmCrearMalla.jLabelNumeroTotalMateriasV.setText("0");
                        frmCrearMalla.jLabelTotalMinutosV.setText("0");
                        try {
                            auxidmalla = modelo.sacaridmalla(frmCrearMalla.buscada.getNombreMalla());
                            ids = modelo.ConseguirIDMaterias(auxidmalla);
                            modelo.ConseguirNombreMalla(auxidmalla, frmCrearMalla.jTextFieldNombreMalla, frmCrearMalla.jCheckBoxRequiereLicencia);
                            modelo.CrearTablaMateriasMallaModificar(ids, contenedormateriaid, contenedormateria, contenedorCosto, contenedorMinutos);
                            frmCrearMalla.jTableMateriasCorrespondientesMalla.setModel(modelo.LlenarTabla(contenedormateriaid, contenedormateria, contenedorCosto, contenedorMinutos));
                            modelo.DatosIngresados(frmCrearMalla.jLabelCostoTotalMallaV, frmCrearMalla.jLabelTotalMinutosV, frmCrearMalla.jLabelNumeroTotalMateriasV, contenedormateriaid, contenedormateria, contenedorCosto, contenedorMinutos);
                            frmCrearMalla.jTextFieldHorasVuelo.setText(Integer.toString(frmCrearMalla.buscada.getHorasVuelo()));
                            frmCrearMalla.jTextFieldHorasSimulador.setText(Integer.toString(frmCrearMalla.buscada.getHorasSimulador()));
                        } catch (SQLException ex) {
                            Logger.getLogger(controlador.class.getName()).log(Level.SEVERE, null, ex);
                        }
                        frmCrearMalla.setLocation(centradoXY(frmCrearMalla));
                        frmCrearMalla.setVisible(true);
                    }                   
                }
            if (cmb.equals(CrearModificarBorrar.BORRAR)) {
                int respuesta = JOptionPane.showConfirmDialog(this.view, "¿Desea eliminar esta Malla?", "Confirmar",
                        JOptionPane.OK_CANCEL_OPTION, JOptionPane.INFORMATION_MESSAGE);
                if (respuesta == JOptionPane.OK_OPTION) {
                    int id = 0;
                    System.out.println(JButtonRenderer.mallaBuscada.getNombreMalla());
                    frmCrearMalla.buscada = JButtonRenderer.mallaBuscada;
                    System.out.println(frmCrearMalla.buscada.getNombreMalla());
                    System.out.println(frmCrearMalla.buscada.getCosto());
                    System.out.println(frmCrearMalla.buscada.getRequiereLicencia());
                    System.out.println(frmCrearMalla.buscada.getIsBorrado());
                    try {
                        id = modelo.ConseguirIDMalla(frmCrearMalla.buscada.getNombreMalla());
                        modelo.eliminarMalla(id);
                    } catch (SQLException ex) {
                        Logger.getLogger(controlador.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
            }
            modelo.vertablaMalla(frmPrincipalMalla.jTableMallas, this);
            frmPrincipalMalla.jTableMallas.repaint();
        } else {
            JOptionPane.showMessageDialog(this.view, "La ventana Crear Malla se encuentra abierta");

        }
    }
//___________________________________________________________________________________ Soy una barra separadora :)

    /*Internal Frame Crear Usuario*/

    public void crearUsuario(CrearModificarBorrar action) {
        if (estacerrado(frmCrearUsuario)) {
            try{
                frmCrearUsuario = new JFrameInternalCrearUsuario(modelo, action);
                if(action.equals(CrearModificarBorrar.MODIFICAR) || action.equals(CrearModificarBorrar.CREAR)){
                    this.view.jDesktopPane.add(frmCrearUsuario);

                    frmCrearUsuario = new JFrameInternalCrearUsuario(modelo, action);
                    this.view.jDesktopPane.add(frmCrearUsuario);

                    frmCrearUsuario.jComboBoxRol.setModel(modelo.getModelo2("rol", frmCrearUsuario.roles));

                    frmCrearUsuario.jButtonAceptar.addActionListener(this);
                    frmCrearUsuario.jButtonCancelar.addActionListener(this);

                    frmCrearUsuario.setLocation(centradoXY(frmCrearUsuario));
                    frmCrearUsuario.setVisible(true);

                    frmCrearUsuario.estadosInciales();
                    if(action.equals(CrearModificarBorrar.MODIFICAR)){
                        frmCrearUsuario.elegidoUsuario = JButtonRenderer.usuarioBuscado; 
                        frmCrearUsuario.llenarUsuario();
                    }
                    frmCrearUsuario.setLocation(centradoXY(frmCrearUsuario));
                    frmCrearUsuario.setVisible(true);
                }
                if(action.equals(CrearModificarBorrar.BORRAR)){
                    int respuesta = JOptionPane.showConfirmDialog(this.view, "¿Desea eliminar este Usuario?", "Confirmar",
                                    JOptionPane.OK_CANCEL_OPTION, JOptionPane.INFORMATION_MESSAGE);
                    if (respuesta == JOptionPane.OK_OPTION) {
                        frmCrearUsuario.elegidoUsuario = JButtonRenderer.usuarioBuscado;
                        modelo.eliminarUsuario(frmCrearUsuario.elegidoUsuario, DeltaCharlie.IDUser);
                        modelo.vertablaUsuario(frmPrincipalUsuario.jTableUsuarios, this);
                        frmPrincipalUsuario.jTableUsuarios.repaint();
                    }
                }
            } catch (SQLException ex) {
                JOptionPane.showMessageDialog(this.view, "Error Carga Datos");
                cerrar(frmCrearUsuario);
            } 
           
                
            

        } else {
            JOptionPane.showMessageDialog(this.view, "La ventana Crear Usuarios se encuentra abierta");

        }
    }
//___________________________________________________________________________________ Soy una barra separadora :)

    /*Internal Frame Crear Helice*/
    public void crearHelice(CrearModificarBorrar cmb) {
        if (estacerrado(frmHelice)) {
            try {
                frmHelice = new JFrameInternalHelice(cmb);
                if(cmb.equals(CrearModificarBorrar.MODIFICAR) || cmb.equals(CrearModificarBorrar.CREAR)){
                    if(cmb.equals(CrearModificarBorrar.MODIFICAR)){
                        frmHelice.heliceBuscada = JButtonRenderer.heliceBuscada;
                        frmHelice.llenarHelice();
                    }
                    this.view.jDesktopPane.add(frmHelice);

                    frmHelice.jButtonAceptar.addActionListener(this);
                    frmHelice.jButtonCancelar.addActionListener(this);

                    frmHelice.setLocation(centradoXY(frmHelice));
                    frmHelice.setVisible(true);
                }
                if(cmb.equals(CrearModificarBorrar.BORRAR)){
                    int respuesta = JOptionPane.showConfirmDialog(this.view, "¿Desea eliminar esta helice?", "Confirmar",
                                    JOptionPane.OK_CANCEL_OPTION, JOptionPane.INFORMATION_MESSAGE);
                    if (respuesta == JOptionPane.OK_OPTION) {
                        frmHelice.heliceBuscada = JButtonRenderer.heliceBuscada;
                        modelo.eliminarHelice(frmHelice.heliceBuscada, DeltaCharlie.IDUser);
                        modelo.vertablaHelice(frmPrincipalHelice.jTableHelices, this);
                        frmPrincipalHelice.jTableHelices.repaint();
                    }
                }
            } catch (SQLException ex) {
                JOptionPane.showMessageDialog(this.view, "Error al conectar a la base de datos");
                cerrar(frmCrearAvion);
            }

        } else {
            JOptionPane.showMessageDialog(this.view, "La ventana crear motor abierta");
            cerrar(frmMotor);
        }
    }
//___________________________________________________________________________________ Soy una barra separadora :)  

    /*Internal Frame Buscar Alumno*/
    private void buscarAlumno() {
        if (estacerrado(frmBuscarAlumno)) {
            try {
                frmBuscarAlumno = new JFrameInternalBuscarAlumno(modelo);
                this.view.jDesktopPane.add(frmBuscarAlumno);
                
                frmBuscarAlumno.jButtonAgregar.addActionListener(this);
                frmBuscarAlumno.jButtonCancelar.addActionListener(this);
                frmBuscarAlumno.setLocation(centradoXY(frmBuscarAlumno));
                frmBuscarAlumno.setVisible(true);
            } catch (SQLException ex) {
                JOptionPane.showMessageDialog(this.view, "Error en la base de datos");
            }

        } else {
            
            JOptionPane.showMessageDialog(this.view, "La ventana Buscar Alumno se encuentra abierta");

        }
    }

    private void buscarDocente() {
        if (estacerrado(frmBuscarDocente)) {

            frmBuscarDocente = new JFrameInternalBuscarDocente(modelo);
            this.view.jDesktopPane.add(frmBuscarDocente);

            frmBuscarDocente.jButtonAgregar.setActionCommand("Agregar Alumno Buscado");
            frmBuscarDocente.jButtonCancelar.setActionCommand("Cancelar Busqueda Alumno");

            frmBuscarDocente.jButtonAgregar.addActionListener(this);
            frmBuscarDocente.jButtonCancelar.addActionListener(this);

            frmBuscarDocente.setLocation(centradoXY(frmBuscarDocente));
            frmBuscarDocente.setVisible(true);

        } else {
            JOptionPane.showMessageDialog(this.view, "La ventana Buscar Docente se encuentra abierta");

        }
        // Alo polisia
    }

    /*Internal Frame buscar docente*/
    private void buscarDocente(ActionEvent comando) {
        if (comando.getSource().equals(frmBuscarDocente.jButtonAgregar)) {

                if (frmBuscarDocente.personaBuscada != null) {

                frmDocentePiloto.elegidaPersona = frmBuscarDocente.personaBuscada;
                    try {
                        frmDocentePiloto.llenarPersona();
                    } catch (SQLException ex) {
                        Logger.getLogger(controlador.class.getName()).log(Level.SEVERE, null, ex);
                    }

                cerrar(frmBuscarDocente);
            } else {
                JOptionPane.showMessageDialog(this.view, "No hay Docente seleccionado", "Error", JOptionPane.ERROR_MESSAGE);
            }
        } else if (comando.getSource().equals(frmBuscarDocente.jButtonCancelar)) {
            cerrar(frmBuscarDocente);
        }
    }

    /*Internal Frame buscar Alumno*/
    private void buscarAlumno(ActionEvent comando) {
        if (comando.getSource().equals(frmBuscarAlumno.jButtonAgregar)) {

            if (frmBuscarAlumno.personaBuscada   != null) {

                try {
                    frmCrearNuevoAlumno.elegidaPersona = frmBuscarAlumno.personaBuscada;
                    frmCrearNuevoAlumno.llenarPersona();//completar el llenar persona con los nuevos campos
                    cerrar(frmBuscarAlumno);
                } catch (SQLException ex) {
                    Logger.getLogger(controlador.class.getName()).log(Level.SEVERE, null, ex);
                }
            } else {
                JOptionPane.showMessageDialog(this.view, "No hay alumno seleccionado", "Error", JOptionPane.ERROR_MESSAGE);
            }
        } else if (comando.getSource().equals(frmBuscarAlumno.jButtonCancelar)) {
            cerrar(frmBuscarAlumno);
        }
    }

    /*Internal Frame Crear Alumno*/
    public void crearAlumno(CrearModificarBorrar action) {
        if (estacerrado(frmCrearNuevoAlumno)) {
            try {
                frmCrearNuevoAlumno = new JFrameInternalCrearAlumno(modelo, action);
                if(action.equals(CrearModificarBorrar.MODIFICAR) || action.equals(CrearModificarBorrar.CREAR)){
                    this.view.jDesktopPane.add(frmCrearNuevoAlumno);

                    frmCrearNuevoAlumno.jComboBoxMallaAInscribirse.setModel(modelo.getModeloMalla(frmCrearNuevoAlumno.mallas));

                    frmCrearNuevoAlumno.botonAceptar.addActionListener(this);
                    frmCrearNuevoAlumno.botonCancelar.addActionListener(this);

                    frmCrearNuevoAlumno.jComboBoxTipoDocumento.setModel(modelo.getTipoDocumento());

                    frmCrearNuevoAlumno.jButtonBuscarFoto.addActionListener(this);

                    frmCrearNuevoAlumno.jButtonAgregarTelefonoReferencia.addActionListener(this);
                    frmCrearNuevoAlumno.jButtonEliminarTelefonoReferencia.addActionListener(this);

                    frmCrearNuevoAlumno.jButtonAgregarReferencia.addActionListener(this);
                    frmCrearNuevoAlumno.jButtonEliminarReferencia.addActionListener(this);

                    frmCrearNuevoAlumno.estadoInicial();
                    if(action.equals(CrearModificarBorrar.MODIFICAR)){
                        frmCrearNuevoAlumno.elegidaPersona = JButtonRenderer.alumnoBuscado;
                        frmCrearNuevoAlumno.llenarPersona();   
                    }
                    frmCrearNuevoAlumno.setLocation(centradoXY(frmCrearNuevoAlumno));
                    frmCrearNuevoAlumno.setVisible(true);
                }
                if(action.equals(CrearModificarBorrar.BORRAR)){
                    int respuesta = JOptionPane.showConfirmDialog(this.view, "¿Desea eliminar este alumno?", "Confirmar",
                                    JOptionPane.OK_CANCEL_OPTION, JOptionPane.INFORMATION_MESSAGE);
                    if (respuesta == JOptionPane.OK_OPTION) {
                        frmCrearNuevoAlumno.elegidaPersona = JButtonRenderer.alumnoBuscado;
                        modelo.eliminarPersona(frmCrearNuevoAlumno.elegidaPersona.getiD(), DeltaCharlie.IDUser);
                        modelo.vertablaAlumno(frmPrincipalAlumno.jTableAlumnos, this);
                        frmPrincipalAlumno.jTableAlumnos.repaint();
                    }
                }
            } catch (SQLException ex) {
                JOptionPane.showMessageDialog(this.view, "Error Carga Datos");
                cerrar(frmCrearNuevoAlumno);
            }

        } else {
            JOptionPane.showMessageDialog(this.view, "La ventana crear Alumno se encuentra abierta");
        }
    }
//___________________________________________________________________________________ Soy una barra separadora :)

    /*Internal Frame Crear Reporte Flexible*/
    private void crearReporteFlexible() {
        if (estacerrado(frmCrearReporteFlexible)) {
            try {
                frmCrearReporteFlexible = new JFrameInternalReporteFlexible(modelo);
                this.view.jDesktopPane.add(frmCrearReporteFlexible);
                //se añade las acciones a los controles del frame buscar materia
                frmCrearReporteFlexible.jButtonFolder.setActionCommand("Reporte Flexible Buscar");
                //Se pone a escuchar las acciones del usuario
                frmCrearReporteFlexible.jButtonFolder.addActionListener(this);
                frmCrearReporteFlexible.jButtonRepMateria.addActionListener(this);
                frmCrearReporteFlexible.jButtonNotasFecha.addActionListener(this);
                frmCrearReporteFlexible.jButtonGeneralAlumnos.addActionListener(this);
                frmCrearReporteFlexible.jButtonEconomicoGlobal.addActionListener(this);
                frmCrearReporteFlexible.jButtonHorasAlumno.addActionListener(this);
                frmCrearReporteFlexible.jButtonNotaDocente.addActionListener(this);
                frmCrearReporteFlexible.jButtonBalanceGeneral.addActionListener(this);
                frmCrearReporteFlexible.jButtonAvion.addActionListener(this);
                frmCrearReporteFlexible.jButtonMotor.addActionListener(this);
                frmCrearReporteFlexible.jButtonHelice.addActionListener(this);
                frmCrearReporteFlexible.jButtonMallas.addActionListener(this);
                frmCrearReporteFlexible.jButtonCancelar.addActionListener(this);
                
                frmCrearReporteFlexible.jButtonAlertaVencimiento.addActionListener(this);
                frmCrearReporteFlexible.jButtonAlertasEconomicas.addActionListener(this);
                frmCrearReporteFlexible.jButtonAlertasGlobales.addActionListener(this);
                frmCrearReporteFlexible.jButtonHorasDeVueloPorAvion.addActionListener(this);
                frmCrearReporteFlexible.jButtonNotasAlumno.addActionListener(this);
                frmCrearReporteFlexible.jButtonMateriasPorAlumno.addActionListener(this);
                frmCrearReporteFlexible.jButton10.addActionListener(this);
                
                String current = new java.io.File( "." ).getCanonicalPath();
                frmCrearReporteFlexible.jLabelRuta.setText(current);
                ruta = current;
                //Se centrea y muestra pantalla
                frmCrearReporteFlexible.setLocation(centradoXY(frmCrearReporteFlexible));
                frmCrearReporteFlexible.setVisible(true);
            } catch (IOException ex) {
                Logger.getLogger(controlador.class.getName()).log(Level.SEVERE, null, ex);
            } catch (SQLException ex) {
                Logger.getLogger(controlador.class.getName()).log(Level.SEVERE, null, ex);
            }

        } else {
            JOptionPane.showMessageDialog(this.view, "La ventana Buscar Materia se encuentra abierta");
        }
    }

    

    private void crearReporteFlexible(ActionEvent comando) throws Exception {
        Connection conn = null;
        try {
            conn = conexion.getMySQLConnection();
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(controlador.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SQLException ex) {
            Logger.getLogger(controlador.class.getName()).log(Level.SEVERE, null, ex);
        }
        if (comando.getSource().equals(frmCrearReporteFlexible.jButtonFolder)) {
            chooser = new JFileChooser();
            chooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
            chooser.showSaveDialog(null);
            ruta = String.valueOf(chooser.getSelectedFile());
            if(ruta!="null")
            {
                frmCrearReporteFlexible.jLabelRuta.setText(ruta);
            }
        }else if (comando.getSource().equals(frmCrearReporteFlexible.jButtonCancelar)) {
            cerrar(frmCrearReporteFlexible);
        }
        else{
            reportSrcFile = null;
            jasperReport = null;
            parameters = new HashMap<String, Object>();
            print = null;
            outDir = null;
            exporter = null;
            exporterInput = null;
            exporterOutput = null;
            configuration = null;
            if (comando.getSource().equals(frmCrearReporteFlexible.jButtonRepMateria)) {
                try {
                    if(frmCrearReporteFlexible.jLabelRuta.getText().isEmpty()){
                        JOptionPane.showMessageDialog(this.view, "No existe una ruta de guardado" , "Error", JOptionPane.ERROR_MESSAGE);
                        throw new Exception("No existe una ruta de guardado");
                    }
                    idReportes = modelo.getIDAlumno_Docente_Piloto(frmCrearReporteFlexible.jComboBoxAlumnoABuscar.getSelectedItem().toString());
                    //Ruta del Reporte
                    //reportSrcFile = "jreports\\reportAlumnosMateria.jrxml";
                    if(idReportes != -1){
                        parameters.put("ID", idReportes);
                        file = getClass().getResourceAsStream("/jreports/reportAlumnosMateria.jrxml");
                        jasperReport = JasperCompileManager.compileReport(file);
                        print = JasperFillManager.fillReport(jasperReport,parameters, conn);
                    }
                    else{
                        file = getClass().getResourceAsStream("/jreports/reportAlumnosMateriaGeneral.jrxml");
                        jasperReport = JasperCompileManager.compileReport(file);
                        print = JasperFillManager.fillReport(jasperReport,parameters, conn);
                    }
                    // Ruta del Reporte Otra Vez
                    outDir = new File(ruta);
                    ruta = ruta.replace("\\", "/");
                    outDir.mkdirs();
                    System.out.println(ruta);
                    // Exportador PDF.
                    exporter = new JRPdfExporter();
                    exporterInput = new SimpleExporterInput(print);
                    // Input de exportacion.
                    exporter.setExporterInput(exporterInput);
                    // Output de exportacion
                    exporterOutput = new SimpleOutputStreamExporterOutput(ruta + "/ReportedeAlumnosporMateriaGrupo.pdf");
                    // Output
                    exporter.setExporterOutput(exporterOutput);
                    //Listo
                    configuration = new SimplePdfExporterConfiguration();
                    exporter.setConfiguration(configuration);
                    exporter.exportReport();
                    System.out.print("Exportacion existosa!");
                    JOptionPane.showMessageDialog(this.view, "Reporte generado con éxito!");
                } catch (Exception e) {
                    Logger.getLogger(controlador.class.getName()).log(Level.SEVERE, null, e);
                }
            }
            if (comando.getSource().equals(frmCrearReporteFlexible.jButtonNotasFecha)) {
                try {
                    if(frmCrearReporteFlexible.jLabelRuta.getText().isEmpty()){
                        JOptionPane.showMessageDialog(this.view, "No existe una ruta de guardado" , "Error", JOptionPane.ERROR_MESSAGE);
                        throw new Exception("No existe una ruta de guardado");
                    }
                    idReportes = modelo.getIDAlumno_Docente_Piloto(frmCrearReporteFlexible.jComboBoxAlumnoABuscar2.getSelectedItem().toString());
                    if(idReportes != -1){
                        if(frmCrearReporteFlexible.jCheckBoxReporteGeneralNotasPorFecha.isSelected()){
                            //reportAlumnoFechaGeneral
                            parameters.put("ID", idReportes);
                            file = getClass().getResourceAsStream("/jreports/reportAlumnoFechaGeneral.jrxml");
                            jasperReport = JasperCompileManager.compileReport(file);
                            print = JasperFillManager.fillReport(jasperReport,parameters, conn);
                        }
                        else{
                            //reportAlumnoFechaEspecifico
                            parameters.put("ID", idReportes);
                            parameters.put("FechaInicial", FechaParseada(frmCrearReporteFlexible.dateChooserComboFechaInicio.getSelectedDate().getTime()));
                            parameters.put("FechaFinal", FechaParseada(frmCrearReporteFlexible.dateChooserComboFechaFinal.getSelectedDate().getTime()));
                            file = getClass().getResourceAsStream("/jreports/reportAlumnoFechaEspecifico.jrxml");
                            jasperReport = JasperCompileManager.compileReport(file);
                            print = JasperFillManager.fillReport(jasperReport,parameters, conn);
                        }
                    }
                    else{
                        if(frmCrearReporteFlexible.jCheckBoxReporteGeneralNotasPorFecha.isSelected()){
                            //reportAlumnosFechaGeneral
                            file = getClass().getResourceAsStream("/jreports/reportAlumnosFechaGeneral.jrxml");
                            jasperReport = JasperCompileManager.compileReport(file);
                            print = JasperFillManager.fillReport(jasperReport,parameters, conn);
                        }
                        else{
                            //reportAlumnosFechaEspecifica
                            parameters.put("FechaInicial", FechaParseada(frmCrearReporteFlexible.dateChooserComboFechaInicio.getSelectedDate().getTime()));
                            parameters.put("FechaFinal", FechaParseada(frmCrearReporteFlexible.dateChooserComboFechaFinal.getSelectedDate().getTime()));
                            file = getClass().getResourceAsStream("/jreports/reportAlumnosFechaEspecifica.jrxml");
                            jasperReport = JasperCompileManager.compileReport(file);
                            print = JasperFillManager.fillReport(jasperReport,parameters, conn);
                        }
                    }
                    
                    outDir = new File(ruta);
                    ruta = ruta.replace("\\", "/");
                    outDir.mkdirs();
                    System.out.println(ruta);
                    exporter = new JRPdfExporter();
                    exporterInput = new SimpleExporterInput(print);
                    exporter.setExporterInput(exporterInput);
                    exporterOutput = new SimpleOutputStreamExporterOutput(ruta + "/ReportedeAlumnosporFecha.pdf");
                    exporter.setExporterOutput(exporterOutput);
                    configuration = new SimplePdfExporterConfiguration();
                    exporter.setConfiguration(configuration);
                    exporter.exportReport();
                    System.out.print("Exportacion existosa!");
                    JOptionPane.showMessageDialog(this.view, "Reporte generado con éxito!");
                } catch (Exception e) {
                    Logger.getLogger(controlador.class.getName()).log(Level.SEVERE, null, e);
                }
            }
            if (comando.getSource().equals(frmCrearReporteFlexible.jButtonMateriasPorAlumno)) {
                try {
                    if(frmCrearReporteFlexible.jLabelRuta.getText().isEmpty()){
                        JOptionPane.showMessageDialog(this.view, "No existe una ruta de guardado" , "Error", JOptionPane.ERROR_MESSAGE);
                        throw new Exception("No existe una ruta de guardado");
                    }
                    idReportes = modelo.getIDAlumno_Docente_Piloto(frmCrearReporteFlexible.jComboBoxAlumnoABuscar.getSelectedItem().toString());
                    if(idReportes != -1){
                        parameters.put("ID", idReportes);
                        file = getClass().getResourceAsStream("/jreports/MateriasPorAlumno.jrxml");
                        jasperReport = JasperCompileManager.compileReport(file);
                        print = JasperFillManager.fillReport(jasperReport,parameters, conn);
                    }
                    else{
                        file = getClass().getResourceAsStream("/jreports/MateriasPorAlumnoGeneral.jrxml");
                        jasperReport = JasperCompileManager.compileReport(file);
                        print = JasperFillManager.fillReport(jasperReport,parameters, conn);
                    }
                    outDir = new File(ruta);
                    ruta = ruta.replace("\\", "/");
                    outDir.mkdirs();
                    System.out.println(ruta);
                    exporter = new JRPdfExporter();
                    exporterInput = new SimpleExporterInput(print);
                    exporter.setExporterInput(exporterInput);
                    exporterOutput = new SimpleOutputStreamExporterOutput(ruta + "/MateriasPorAlumno.pdf");
                    exporter.setExporterOutput(exporterOutput);
                    configuration = new SimplePdfExporterConfiguration();
                    exporter.setConfiguration(configuration);
                    exporter.exportReport();
                    System.out.print("Exportacion existosa!");
                    JOptionPane.showMessageDialog(this.view, "Reporte generado con éxito!");
                } catch (Exception e) {
                    Logger.getLogger(controlador.class.getName()).log(Level.SEVERE, null, e);
                }
            }
            if (comando.getSource().equals(frmCrearReporteFlexible.jButtonNotasAlumno)) {
                try {
                    if(frmCrearReporteFlexible.jLabelRuta.getText().isEmpty()){
                        JOptionPane.showMessageDialog(this.view, "No existe una ruta de guardado" , "Error", JOptionPane.ERROR_MESSAGE);
                        throw new Exception("No existe una ruta de guardado");
                    }
                    idReportes = modelo.getIDAlumno_Docente_Piloto(frmCrearReporteFlexible.jComboBoxAlumnoABuscar.getSelectedItem().toString());
                    if(idReportes != -1){
                        parameters.put("ID", idReportes);
                        file = getClass().getResourceAsStream("/jreports/NotasPorAlumno.jrxml");
                        jasperReport = JasperCompileManager.compileReport(file);
                        print = JasperFillManager.fillReport(jasperReport,parameters, conn);
                    }
                    else{
                        file = getClass().getResourceAsStream("/jreports/NotasPorAlumnoGeneral.jrxml");
                        jasperReport = JasperCompileManager.compileReport(file);
                        print = JasperFillManager.fillReport(jasperReport,parameters, conn);
                    }
                    outDir = new File(ruta);
                    ruta = ruta.replace("\\", "/");
                    outDir.mkdirs();
                    System.out.println(ruta);
                    exporter = new JRPdfExporter();
                    exporterInput = new SimpleExporterInput(print);
                    exporter.setExporterInput(exporterInput);
                    exporterOutput = new SimpleOutputStreamExporterOutput(ruta + "/NotasPorAlumno.pdf");
                    exporter.setExporterOutput(exporterOutput);
                    configuration = new SimplePdfExporterConfiguration();
                    exporter.setConfiguration(configuration);
                    exporter.exportReport();
                    System.out.print("Exportacion existosa!");
                    JOptionPane.showMessageDialog(this.view, "Reporte generado con éxito!");
                } catch (Exception e) {
                    Logger.getLogger(controlador.class.getName()).log(Level.SEVERE, null, e);
                }
            }
            if (comando.getSource().equals(frmCrearReporteFlexible.jButtonHorasDeVueloPorAvion)) {
                try {
                    if(frmCrearReporteFlexible.jLabelRuta.getText().isEmpty()){
                        JOptionPane.showMessageDialog(this.view, "No existe una ruta de guardado" , "Error", JOptionPane.ERROR_MESSAGE);
                        throw new Exception("No existe una ruta de guardado");
                    }
                    
                    idReportes = modelo.getIDAlumno_Docente_Piloto(frmCrearReporteFlexible.jComboBoxAvionABuscar1.getSelectedItem().toString());
                    
                    if(idReportes != -1){
                        parameters.put("ID", idReportes);
                        file = getClass().getResourceAsStream("/jreports/HorasDeVueloPorAvion.jrxml");
                        jasperReport = JasperCompileManager.compileReport(file);
                        print = JasperFillManager.fillReport(jasperReport,parameters, conn);
                    }
                    else{
                        file = getClass().getResourceAsStream("/jreports/HorasDeVueloPorAvionGeneral.jrxml");
                        jasperReport = JasperCompileManager.compileReport(file);
                        print = JasperFillManager.fillReport(jasperReport,parameters, conn);
                    }
                    outDir = new File(ruta);
                    ruta = ruta.replace("\\", "/");
                    outDir.mkdirs();
                    System.out.println(ruta);
                    exporter = new JRPdfExporter();
                    exporterInput = new SimpleExporterInput(print);
                    exporter.setExporterInput(exporterInput);
                    exporterOutput = new SimpleOutputStreamExporterOutput(ruta + "/HorasDeVueloPorAvion.pdf");
                    exporter.setExporterOutput(exporterOutput);
                    configuration = new SimplePdfExporterConfiguration();
                    exporter.setConfiguration(configuration);
                    exporter.exportReport();
                    System.out.print("Exportacion existosa!");
                    JOptionPane.showMessageDialog(this.view, "Reporte generado con éxito!");
                } catch (Exception e) {
                    Logger.getLogger(controlador.class.getName()).log(Level.SEVERE, null, e);
                }
            }
            if (comando.getSource().equals(frmCrearReporteFlexible.jButtonAlertasGlobales)) {
                try {
                    if(frmCrearReporteFlexible.jLabelRuta.getText().isEmpty()){
                        JOptionPane.showMessageDialog(this.view, "No existe una ruta de guardado" , "Error", JOptionPane.ERROR_MESSAGE);
                        throw new Exception("No existe una ruta de guardado");
                    }
                    file = getClass().getResourceAsStream("/jreports/AlertasGlobales.jrxml");
                    jasperReport = JasperCompileManager.compileReport(file);
                    print = JasperFillManager.fillReport(jasperReport,parameters, conn);
                    outDir = new File(ruta);
                    ruta = ruta.replace("\\", "/");
                    outDir.mkdirs();
                    System.out.println(ruta);
                    exporter = new JRPdfExporter();
                    exporterInput = new SimpleExporterInput(print);
                    exporter.setExporterInput(exporterInput);
                    exporterOutput = new SimpleOutputStreamExporterOutput(ruta + "/ReportedeAlertasGlobales.pdf");
                    exporter.setExporterOutput(exporterOutput);
                    configuration = new SimplePdfExporterConfiguration();
                    exporter.setConfiguration(configuration);
                    exporter.exportReport();
                    System.out.print("Exportacion existosa!");
                    JOptionPane.showMessageDialog(this.view, "Reporte generado con éxito!");
                } catch (Exception e) {
                    Logger.getLogger(controlador.class.getName()).log(Level.SEVERE, null, e);
                }
            }
            if (comando.getSource().equals(frmCrearReporteFlexible.jButtonAlertaVencimiento)) {
                try {
                    if(frmCrearReporteFlexible.jLabelRuta.getText().isEmpty()){
                        JOptionPane.showMessageDialog(this.view, "No existe una ruta de guardado" , "Error", JOptionPane.ERROR_MESSAGE);
                        throw new Exception("No existe una ruta de guardado");
                    }
                    file = getClass().getResourceAsStream("/jreports/AlertasDeVencimiento.jrxml");
                    jasperReport = JasperCompileManager.compileReport(file);
                    print = JasperFillManager.fillReport(jasperReport,parameters, conn);
                    outDir = new File(ruta);
                    ruta = ruta.replace("\\", "/");
                    outDir.mkdirs();
                    System.out.println(ruta);
                    exporter = new JRPdfExporter();
                    exporterInput = new SimpleExporterInput(print);
                    exporter.setExporterInput(exporterInput);
                    exporterOutput = new SimpleOutputStreamExporterOutput(ruta + "/ReportedeAlertasDeVencimiento.pdf");
                    exporter.setExporterOutput(exporterOutput);
                    configuration = new SimplePdfExporterConfiguration();
                    exporter.setConfiguration(configuration);
                    exporter.exportReport();
                    System.out.print("Exportacion existosa!");
                    JOptionPane.showMessageDialog(this.view, "Reporte generado con éxito!");
                } catch (Exception e) {
                    Logger.getLogger(controlador.class.getName()).log(Level.SEVERE, null, e);
                }
            }
            if (comando.getSource().equals(frmCrearReporteFlexible.jButtonAlertasEconomicas)) {
                try {
                    if(frmCrearReporteFlexible.jLabelRuta.getText().isEmpty()){
                        JOptionPane.showMessageDialog(this.view, "No existe una ruta de guardado" , "Error", JOptionPane.ERROR_MESSAGE);
                        throw new Exception("No existe una ruta de guardado");
                    }
                    file = getClass().getResourceAsStream("/jreports/AlertasEconomicas.jrxml");
                    jasperReport = JasperCompileManager.compileReport(file);
                    print = JasperFillManager.fillReport(jasperReport,parameters, conn);
                    outDir = new File(ruta);
                    ruta = ruta.replace("\\", "/");
                    outDir.mkdirs();
                    System.out.println(ruta);
                    exporter = new JRPdfExporter();
                    exporterInput = new SimpleExporterInput(print);
                    exporter.setExporterInput(exporterInput);                 
                    exporterOutput = new SimpleOutputStreamExporterOutput(ruta + "/ReporteDeAlertasEconomicas.pdf");
                    exporter.setExporterOutput(exporterOutput);
                    configuration = new SimplePdfExporterConfiguration();
                    exporter.setConfiguration(configuration);
                    exporter.exportReport();
                    System.out.print("Exportacion existosa!");
                    JOptionPane.showMessageDialog(this.view, "Reporte generado con éxito!");
                } catch (Exception e) {
                    Logger.getLogger(controlador.class.getName()).log(Level.SEVERE, null, e);
                }
            }
            if (comando.getSource().equals(frmCrearReporteFlexible.jButtonGeneralAlumnos)) {
                try {
                    if(frmCrearReporteFlexible.jLabelRuta.getText().isEmpty()){
                        JOptionPane.showMessageDialog(this.view, "No existe una ruta de guardado" , "Error", JOptionPane.ERROR_MESSAGE);
                        throw new Exception("No existe una ruta de guardado");
                    }
                    idReportes = modelo.getIDAlumno_Docente_Piloto(frmCrearReporteFlexible.jComboBoxAlumnoABuscar.getSelectedItem().toString());
                    if(idReportes != -1){
                        parameters.put("ID", idReportes);
                        file = getClass().getResourceAsStream("/jreports/GeneralPorAlumnos.jrxml");
                        jasperReport = JasperCompileManager.compileReport(file);
                        print = JasperFillManager.fillReport(jasperReport,parameters, conn);
                    }
                    else{
                        file = getClass().getResourceAsStream("/jreports/GeneralPorAlumnosGeneral.jrxml");
                        jasperReport = JasperCompileManager.compileReport(file);
                        print = JasperFillManager.fillReport(jasperReport,parameters, conn);
                    }
                    outDir = new File(ruta);
                    ruta = ruta.replace("\\", "/");
                    outDir.mkdirs();
                    System.out.println(ruta);
                    exporter = new JRPdfExporter();
                    exporterInput = new SimpleExporterInput(print);
                    exporter.setExporterInput(exporterInput);
                    exporterOutput = new SimpleOutputStreamExporterOutput(ruta + "/ReporteGeneralAlumnos.pdf");
                    exporter.setExporterOutput(exporterOutput);
                    configuration = new SimplePdfExporterConfiguration();
                    exporter.setConfiguration(configuration);
                    exporter.exportReport();
                    System.out.print("Exportacion existosa!");
                    JOptionPane.showMessageDialog(this.view, "Reporte generado con éxito!");
                } catch (Exception e) {
                    Logger.getLogger(controlador.class.getName()).log(Level.SEVERE, null, e);
                }
            }
            if (comando.getSource().equals(frmCrearReporteFlexible.jButtonEconomicoGlobal)) {
                try {
                    if(frmCrearReporteFlexible.jLabelRuta.getText().isEmpty()){
                        JOptionPane.showMessageDialog(this.view, "No existe una ruta de guardado" , "Error", JOptionPane.ERROR_MESSAGE);
                        throw new Exception("No existe una ruta de guardado");
                    }
                    file = getClass().getResourceAsStream("/jreports/EconomicoGlobal.jrxml");
                    jasperReport = JasperCompileManager.compileReport(file);
                    parameters = new HashMap<String, Object>();
                    print = JasperFillManager.fillReport(jasperReport,parameters, conn);
                    outDir = new File(ruta);
                    ruta = ruta.replace("\\", "/");
                    outDir.mkdirs();
                    System.out.println(ruta);
                    exporter = new JRPdfExporter();
                    exporterInput = new SimpleExporterInput(print);
                    exporter.setExporterInput(exporterInput);
                    exporterOutput = new SimpleOutputStreamExporterOutput(ruta + "/ReporteEconomicoGlobal.pdf");
                    exporter.setExporterOutput(exporterOutput);
                    configuration = new SimplePdfExporterConfiguration();
                    exporter.setConfiguration(configuration);
                    exporter.exportReport();
                    System.out.print("Exportacion existosa!");
                    JOptionPane.showMessageDialog(this.view, "Reporte generado con éxito!");
                } catch (Exception e) {
                    Logger.getLogger(controlador.class.getName()).log(Level.SEVERE, null, e);
                }
            }
            if (comando.getSource().equals(frmCrearReporteFlexible.jButtonHorasAlumno)) {
                try {
                    if(frmCrearReporteFlexible.jLabelRuta.getText().isEmpty()){
                        JOptionPane.showMessageDialog(this.view, "No existe una ruta de guardado" , "Error", JOptionPane.ERROR_MESSAGE);
                        throw new Exception("No existe una ruta de guardado");
                    }
                    idReportes = modelo.getIDAlumno_Docente_Piloto(frmCrearReporteFlexible.jComboBoxAlumnoABuscar1.getSelectedItem().toString());
                    
                    if(idReportes != -1){
                        parameters.put("ID", idReportes);
                        file = getClass().getResourceAsStream("/jreports/HorasPorAlumno.jrxml");
                    jasperReport = JasperCompileManager.compileReport(file);
                    print = JasperFillManager.fillReport(jasperReport,parameters, conn);
                    }
                    else{
                        file = getClass().getResourceAsStream("/jreports/HorasPorAlumnoGeneral.jrxml");
                    jasperReport = JasperCompileManager.compileReport(file);
                    print = JasperFillManager.fillReport(jasperReport,parameters, conn);
                    }
                    outDir = new File(ruta);
                    ruta = ruta.replace("\\", "/");
                    outDir.mkdirs();
                    System.out.println(ruta);
                    exporter = new JRPdfExporter();
                    exporterInput = new SimpleExporterInput(print);
                    exporter.setExporterInput(exporterInput);
                    exporterOutput = new SimpleOutputStreamExporterOutput(ruta + "/ReporteHorasVueloPorAlumno.pdf");
                    exporter.setExporterOutput(exporterOutput);
                    configuration = new SimplePdfExporterConfiguration();
                    exporter.setConfiguration(configuration);
                    exporter.exportReport();
                    System.out.print("Exportacion existosa!");
                    JOptionPane.showMessageDialog(this.view, "Reporte generado con éxito!");
                } catch (Exception e) {
                    Logger.getLogger(controlador.class.getName()).log(Level.SEVERE, null, e);
                }
            }
            if (comando.getSource().equals(frmCrearReporteFlexible.jButtonAvion)) {
                try {
                    if(frmCrearReporteFlexible.jLabelRuta.getText().isEmpty()){
                        JOptionPane.showMessageDialog(this.view, "No existe una ruta de guardado" , "Error", JOptionPane.ERROR_MESSAGE);
                        throw new Exception("No existe una ruta de guardado");
                    }
                    idReportes = modelo.getIDAvion(frmCrearReporteFlexible.jComboBoxAvionABuscar.getSelectedItem().toString());
                    
                    if(idReportes != -1){
                        parameters.put("ID", idReportes);
                        file = getClass().getResourceAsStream("/jreports/reportAvion.jrxml");
                        jasperReport = JasperCompileManager.compileReport(file);
                        print = JasperFillManager.fillReport(jasperReport,parameters, conn);
                    }
                    else{
                        file = getClass().getResourceAsStream("/jreports/reportAvionGeneral.jrxml");
                        jasperReport = JasperCompileManager.compileReport(file);
                        print = JasperFillManager.fillReport(jasperReport,parameters, conn);
                    }
                    outDir = new File(ruta);
                    ruta = ruta.replace("\\", "/");
                    outDir.mkdirs();
                    System.out.println(ruta);
                    exporter = new JRPdfExporter();
                    exporterInput = new SimpleExporterInput(print);
                    exporter.setExporterInput(exporterInput);
                    exporterOutput = new SimpleOutputStreamExporterOutput(ruta + "/ReporteDeAviones.pdf");
                    exporter.setExporterOutput(exporterOutput);
                    configuration = new SimplePdfExporterConfiguration();
                    exporter.setConfiguration(configuration);
                    exporter.exportReport();
                    System.out.print("Exportacion existosa!");
                    JOptionPane.showMessageDialog(this.view, "Reporte generado con éxito!");
                } catch (Exception e) {
                    Logger.getLogger(controlador.class.getName()).log(Level.SEVERE, null, e);
                }
            }
            if (comando.getSource().equals(frmCrearReporteFlexible.jButtonMotor)) {
                try {
                    if(frmCrearReporteFlexible.jLabelRuta.getText().isEmpty()){
                        JOptionPane.showMessageDialog(this.view, "No existe una ruta de guardado" , "Error", JOptionPane.ERROR_MESSAGE);
                        throw new Exception("No existe una ruta de guardado");
                    }
                    idReportes = modelo.getIDMotor(frmCrearReporteFlexible.jComboBoxMotorABuscar.getSelectedItem().toString());
                    
                    if(idReportes != -1){
                        parameters.put("ID", idReportes);
                        file = getClass().getResourceAsStream("/jreports/reportMotor.jrxml");
                        jasperReport = JasperCompileManager.compileReport(file);
                        print = JasperFillManager.fillReport(jasperReport,parameters, conn);
                    }
                    else{
                        file = getClass().getResourceAsStream("/jreports/reportMotorGeneral.jrxml");
                        jasperReport = JasperCompileManager.compileReport(file);
                        print = JasperFillManager.fillReport(jasperReport,parameters, conn);
                    }
                    outDir = new File(ruta);
                    ruta = ruta.replace("\\", "/");
                    outDir.mkdirs();
                    System.out.println(ruta);
                    exporter = new JRPdfExporter();
                    exporterInput = new SimpleExporterInput(print);
                    exporter.setExporterInput(exporterInput);
                    exporterOutput = new SimpleOutputStreamExporterOutput(ruta + "/ReporteDeMotores.pdf");
                    exporter.setExporterOutput(exporterOutput);
                    configuration = new SimplePdfExporterConfiguration();
                    exporter.setConfiguration(configuration);
                    exporter.exportReport();
                    System.out.print("Exportacion existosa!");
                    JOptionPane.showMessageDialog(this.view, "Reporte generado con éxito!");
                } catch (Exception e) {
                    Logger.getLogger(controlador.class.getName()).log(Level.SEVERE, null, e);
                }
            }
            
            if (comando.getSource().equals(frmCrearReporteFlexible.jButtonHelice)) {
                try {
                    if(frmCrearReporteFlexible.jLabelRuta.getText().isEmpty()){
                        JOptionPane.showMessageDialog(this.view, "No existe una ruta de guardado" , "Error", JOptionPane.ERROR_MESSAGE);
                        throw new Exception("No existe una ruta de guardado");
                    }
                    idReportes = modelo.getIDHelice(frmCrearReporteFlexible.jComboBoxHeliceABuscar.getSelectedItem().toString());
                    
                    if(idReportes != -1){
                        parameters.put("ID", idReportes);
                        file = getClass().getResourceAsStream("/jreports/reportHelice.jrxml");
                        jasperReport = JasperCompileManager.compileReport(file);
                        print = JasperFillManager.fillReport(jasperReport,parameters, conn);
                    }
                    else{
                        file = getClass().getResourceAsStream("/jreports/reportHeliceGeneral.jrxml");
                        jasperReport = JasperCompileManager.compileReport(file);
                        print = JasperFillManager.fillReport(jasperReport,parameters, conn);
                    }
                    outDir = new File(ruta);
                    ruta = ruta.replace("\\", "/");
                    outDir.mkdirs();
                    System.out.println(ruta);
                    exporter = new JRPdfExporter();
                    exporterInput = new SimpleExporterInput(print);
                    exporter.setExporterInput(exporterInput);
                    exporterOutput = new SimpleOutputStreamExporterOutput(ruta + "/ReporteDeHelices.pdf");
                    exporter.setExporterOutput(exporterOutput);
                    configuration = new SimplePdfExporterConfiguration();
                    exporter.setConfiguration(configuration);
                    exporter.exportReport();
                    System.out.print("Exportacion existosa!");
                    JOptionPane.showMessageDialog(this.view, "Reporte generado con éxito!");
                } catch (Exception e) {
                    Logger.getLogger(controlador.class.getName()).log(Level.SEVERE, null, e);
                }
            }
            
            if (comando.getSource().equals(frmCrearReporteFlexible.jButtonNotaDocente)) {
                try {
                    if(frmCrearReporteFlexible.jLabelRuta.getText().isEmpty()){
                        JOptionPane.showMessageDialog(this.view, "No existe una ruta de guardado" , "Error", JOptionPane.ERROR_MESSAGE);
                        throw new Exception("No existe una ruta de guardado");
                    }
                    idReportes = modelo.getIDAlumno_Docente_Piloto(frmCrearReporteFlexible.jComboBoxDocenteABuscar.getSelectedItem().toString());
                    if(idReportes != -1){
                        parameters.put("ID", idReportes);
                        file = getClass().getResourceAsStream("/jreports/NotasPorDocente.jrxml");
                        jasperReport = JasperCompileManager.compileReport(file);
                        print = JasperFillManager.fillReport(jasperReport,parameters, conn);
                    }
                    else{
                        file = getClass().getResourceAsStream("/jreports/NotasPorDocenteGeneral.jrxml");
                        jasperReport = JasperCompileManager.compileReport(file);
                        print = JasperFillManager.fillReport(jasperReport,parameters, conn);
                    }
                    outDir = new File(ruta);
                    ruta = ruta.replace("\\", "/");
                    outDir.mkdirs();
                    System.out.println(ruta);
                    exporter = new JRPdfExporter();
                    exporterInput = new SimpleExporterInput(print);
                    exporter.setExporterInput(exporterInput);
                    exporterOutput = new SimpleOutputStreamExporterOutput(ruta + "/ReporteNotasPorDocente.pdf");
                    exporter.setExporterOutput(exporterOutput);
                    configuration = new SimplePdfExporterConfiguration();
                    exporter.setConfiguration(configuration);
                    exporter.exportReport();
                    System.out.print("Exportacion existosa!");
                    JOptionPane.showMessageDialog(this.view, "Reporte generado con éxito!");
                } catch (Exception e) {
                    Logger.getLogger(controlador.class.getName()).log(Level.SEVERE, null, e);
                }
            }
            if (comando.getSource().equals(frmCrearReporteFlexible.jButtonBalanceGeneral)) {
                try {
                    if(frmCrearReporteFlexible.jLabelRuta.getText().isEmpty()){
                        JOptionPane.showMessageDialog(this.view, "No existe una ruta de guardado" , "Error", JOptionPane.ERROR_MESSAGE);
                        throw new Exception("No existe una ruta de guardado");
                    }
                    file = getClass().getResourceAsStream("/jreports/reportBalanceGeneral.jrxml");
                    jasperReport = JasperCompileManager.compileReport(file);
                    print = JasperFillManager.fillReport(jasperReport,parameters, conn);
                    outDir = new File(ruta);
                    ruta = ruta.replace("\\", "/");
                    outDir.mkdirs();
                    System.out.println(ruta);
                    exporter = new JRPdfExporter();
                    exporterInput = new SimpleExporterInput(print);
                    exporter.setExporterInput(exporterInput);
                    exporterOutput = new SimpleOutputStreamExporterOutput(ruta + "/ReporteBalanceGeneral.pdf");
                    exporter.setExporterOutput(exporterOutput);
                    configuration = new SimplePdfExporterConfiguration();
                    exporter.setConfiguration(configuration);
                    exporter.exportReport();
                    System.out.print("Exportacion existosa!");
                    JOptionPane.showMessageDialog(this.view, "Reporte generado con éxito!");
                } catch (Exception e) {
                    Logger.getLogger(controlador.class.getName()).log(Level.SEVERE, null, e);
                }
            }
            if (comando.getSource().equals(frmCrearReporteFlexible.jButtonMallas)) {
                try {
                    if(frmCrearReporteFlexible.jLabelRuta.getText().isEmpty()){
                        JOptionPane.showMessageDialog(this.view, "No existe una ruta de guardado" , "Error", JOptionPane.ERROR_MESSAGE);
                        throw new Exception("No existe una ruta de guardado");
                    }
                    idReportes = modelo.getIDMalla(frmCrearReporteFlexible.jComboBoxMallaABuscar.getSelectedItem().toString());
                    System.out.println("AQUI :"+idReportes);
                    if(idReportes != -1){
                        parameters.put("ID", idReportes);
                        file = getClass().getResourceAsStream("/jreports/reportMalla.jrxml");
                        jasperReport = JasperCompileManager.compileReport(file);
                        print = JasperFillManager.fillReport(jasperReport,parameters, conn);
                    }
                    else{
                        file = getClass().getResourceAsStream("/jreports/reportMallaGeneral.jrxml");
                        jasperReport = JasperCompileManager.compileReport(file);
                        print = JasperFillManager.fillReport(jasperReport,parameters, conn);
                    }
                    outDir = new File(ruta);
                    ruta = ruta.replace("\\", "/");
                    outDir.mkdirs();
                    exporter = new JRPdfExporter();
                    exporterInput = new SimpleExporterInput(print);
                    exporter.setExporterInput(exporterInput);
                    exporterOutput = new SimpleOutputStreamExporterOutput(ruta + "/ReporteDeMallas.pdf");
                    exporter.setExporterOutput(exporterOutput);
                    configuration = new SimplePdfExporterConfiguration();
                    exporter.setConfiguration(configuration);
                    exporter.exportReport();
                    System.out.print("Exportacion existosa!");
                    JOptionPane.showMessageDialog(this.view, "Reporte generado con éxito!");
                } catch (Exception e) {
                    Logger.getLogger(controlador.class.getName()).log(Level.SEVERE, null, e);
                }
            }
            if (comando.getSource().equals(frmCrearReporteFlexible.jButton10)) {
                try {
                    if(frmCrearReporteFlexible.jLabelRuta.getText().isEmpty()){
                       JOptionPane.showMessageDialog(this.view, "No existe una ruta de guardado" , "Error", JOptionPane.ERROR_MESSAGE);
                       throw new Exception("No existe una ruta de guardado");
                    }
                    file = getClass().getResourceAsStream("/jreports/reportAlertasAvion.jrxml");
                    jasperReport = JasperCompileManager.compileReport(file);
                    JasperPrint print = JasperFillManager.fillReport(jasperReport,parameters, conn);
                    outDir = new File(ruta);
                    ruta = ruta.replace("\\", "/");
                    outDir.mkdirs();
                    exporter = new JRPdfExporter();
                    exporterInput = new SimpleExporterInput(print);
                    exporter.setExporterInput(exporterInput);
                    exporterOutput = new SimpleOutputStreamExporterOutput(ruta + "/ReporteAlertasAviones.pdf");
                    exporter.setExporterOutput(exporterOutput);
                    configuration = new SimplePdfExporterConfiguration();
                    exporter.setConfiguration(configuration);
                    exporter.exportReport();
                    System.out.print("Exportacion existosa!");
                    JOptionPane.showMessageDialog(this.view, "Reporte generado con éxito!");
                } catch (Exception e) {
                    Logger.getLogger(controlador.class.getName()).log(Level.SEVERE, null, e);
                }
            }
        }
        
    }
    
    private void crearPago() {
        if (estacerrado(frmCrearPagos)) {

            try {
                frmCrearPagos = new JFrameInternalPagos(modelo);
                this.view.jDesktopPane.add(frmCrearPagos);
                
                frmCrearPagos.setLocation(centradoXY(frmCrearPagos));
                frmCrearPagos.setVisible(true);
                
                frmCrearPagos.jButtonCancelar.addActionListener(this);
                frmCrearPagos.jButtonRealizarPago.addActionListener(this);
                frmCrearPagos.jComboBoxAlumnoABuscar.setModel(modelo.getAlumnos());
            } catch (SQLException ex) {
                Logger.getLogger(controlador.class.getName()).log(Level.SEVERE, null, ex);
            }

        } else {
            JOptionPane.showMessageDialog(this.view, "La ventana Buscar Alumno se encuentra abierta");
        }
    }
    
    private void crearPago(ActionEvent comando) {
        if(comando.getSource().equals(frmCrearPagos.jButtonRealizarPago)){
            try {
                String nombreAlumno = frmCrearPagos.jLabelNombre.getText() + " " + frmCrearPagos.jLabelApellidoP.getText() + " " + frmCrearPagos.jLabelApellidoM.getText();
                String descripcion = frmCrearPagos.jComboBox1.getSelectedItem().toString();
                int cantidadPagada = (Integer.parseInt(frmCrearPagos.jTextFieldMontoAPagar.getText()) - Integer.parseInt(frmCrearPagos.jTextFieldDescuento.getText()));
                int descuento = Integer.parseInt(frmCrearPagos.jTextFieldDescuento.getText());
                if (nombreAlumno.isEmpty()) {
                    throw new Exception("No se puede Crear Alumno Sin Nombre");
                }
                int mallaElegida = modelo.getIDMalla(frmCrearPagos.jLabelMalla.getText());
                int alumno = modelo.getIDAlumno_Docente_Piloto(nombreAlumno);
                int tipopago = modelo.getTipoDePago("Efectivo");
                //todo modificado prestar mucha atencion
                System.out.println(alumno);
                Pagos pago = new Pagos(-1, alumno, java.time.LocalDate.now().toString(), java.time.LocalTime.now().toString(), descripcion, cantidadPagada, DeltaCharlie.IDUser, tipopago , mallaElegida, descuento);
                modelo.insertarPago(pago, DeltaCharlie.IDUser);
                hilo1.refresh();
                int respuesta = JOptionPane.showConfirmDialog(this.view, "¿Quiere realizar otro pago?", "Insertar de nuevo",
                        JOptionPane.OK_CANCEL_OPTION, JOptionPane.INFORMATION_MESSAGE);
                if (respuesta == JOptionPane.OK_OPTION) {
                    //arreglar bien que vacie todo para la nueva insersion
                    frmCrearPagos.estadoInicial();
                    
                } else {
                    cerrar(frmCrearPagos);
                }
                } catch (Exception e) {
                    JOptionPane.showMessageDialog(this.view, e.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
                    System.out.println("Error");
                }
        }
        if (comando.getSource().equals(frmCrearPagos.jButtonCancelar)) {
            cerrar(frmCrearPagos);
        }
    }
    
    private void controlCreditos() {
        if (estacerrado(frmControlCreditos)) {
            frmControlCreditos = new JFrameInternalCreditos();
            this.view.jDesktopPane.add(frmControlCreditos);
            
            frmControlCreditos.jTableCreditos.setModel(modelo.crearTablaCreditos());
            frmControlCreditos.jTableCreditos1.setModel(modelo.crearTablaCreditosMulta());
            frmControlCreditos.jTableCreditos2.setModel(modelo.crearTablaCreditosGeneral());

            frmControlCreditos.setLocation(centradoXY(frmControlCreditos));
            frmControlCreditos.setVisible(true);

            frmControlCreditos.jButtonCancelar.addActionListener(this);
        } else {
            JOptionPane.showMessageDialog(this.view, "La ventana de Creditos se encuentra abierta");
        }
    }
    
    private void controlCreditos(ActionEvent comando) {
        if (comando.getSource().equals(frmControlCreditos.jButtonCancelar)) {
            cerrar(frmControlCreditos);
        }
    }
//___________________________________________________________________________________ Soy una barra separadora :)

    /*Internal Frame Buscar Materia*/
    private void buscarMateria(CrearModificarBorrar cmb) {
        if (estacerrado(frmBuscarMateria)) {
            frmBuscarMateria = new JFrameInternalBuscarMateria(cmb, modelo);
            this.view.jDesktopPane.add(frmBuscarMateria);
            //se añade las acciones a los controles del frame buscar materia
            frmBuscarMateria.jButtonAgregar.setActionCommand("Agregar Materia Dependencia");//**Hecho
            frmBuscarMateria.jButtonCancelar.setActionCommand("Cancelar Buscqueda Materia");//**Hecho

            //Se pone a escuchar las acciones del usuario
            frmBuscarMateria.jButtonAgregar.addActionListener(this);
            frmBuscarMateria.jButtonCancelar.addActionListener(this);

            //Se centrea y muestra pantalla
            frmBuscarMateria.setLocation(centradoXY(frmBuscarMateria));
            frmBuscarMateria.setVisible(true);

        } else {
            JOptionPane.showMessageDialog(this.view, "La ventana Buscar Materia se encuentra abierta");
        }
    }
//___________________________________________________________________________________ Soy una barra separadora :)

    /*Internal Frame Buscar Materia*/
    private void buscarGrupo(CrearModificarBorrar cmb) {
        if (estacerrado(frmBuscarGrupo)) {
            frmBuscarGrupo = new JFrameInternalBuscarGrupo(cmb, modelo);
            this.view.jDesktopPane.add(frmBuscarGrupo);
            //se añade las acciones a los controles del frame buscar materia
            frmBuscarGrupo.jButtonAceptar.setActionCommand("Agregar Datos a Modificar");
            frmBuscarGrupo.Cancelar.setActionCommand("Cancelar Buscqueda Grupo");//**Hecho

            //Se pone a escuchar las acciones del usuario
            frmBuscarGrupo.jButtonAceptar.addActionListener(this);
            frmBuscarGrupo.Cancelar.addActionListener(this);

            //Se centrea y muestra pantalla
            frmBuscarGrupo.setLocation(centradoXY(frmBuscarGrupo));
            frmBuscarGrupo.setVisible(true);

        } else {
            JOptionPane.showMessageDialog(this.view, "La ventana Buscar Grupo se encuentra abierta");
        }
    }
//___________________________________________________________________________________ Soy una barra separadora :)

    /*Internal Frame Crear DocentePiloto*/
    public void crearDocentePiloto(CrearModificarBorrar cmb) {
        if (estacerrado(frmDocentePiloto)) {
            try{
                frmDocentePiloto = new JFrameInternalDocentePiloto(modelo, cmb);
                if(cmb.equals(CrearModificarBorrar.MODIFICAR) || cmb.equals(CrearModificarBorrar.CREAR)){
                    this.view.jDesktopPane.add(frmDocentePiloto);

                    frmDocentePiloto = new JFrameInternalDocentePiloto(modelo, cmb);
                    this.view.jDesktopPane.add(frmDocentePiloto);
                    frmDocentePiloto.estadoInicial();

                    //Se pone a escuchar las acciones del usuario
                    frmDocentePiloto.jButtonAgregarReferencia.addActionListener(this);
                    frmDocentePiloto.jButtonEliminarReferencia.addActionListener(this);

                    frmDocentePiloto.jButtonAgregarTelefonoReferencia.addActionListener(this);
                    frmDocentePiloto.jButtonEliminarTelefonoReferencia.addActionListener(this);

                    frmDocentePiloto.JbotonAceptar.addActionListener(this);
                    frmDocentePiloto.JbotonCancelar.addActionListener(this);

                    //Se centrea y muestra pantalla
                    frmDocentePiloto.setLocation(centradoXY(frmDocentePiloto));
                    frmDocentePiloto.setVisible(true);

                    frmDocentePiloto.estadoInicial();
                    if(cmb.equals(CrearModificarBorrar.MODIFICAR)){
                        frmDocentePiloto.elegidaPersona = JButtonRenderer.docentepilotoBuscado; 
                        frmDocentePiloto.llenarPersona();
                    }
                    frmDocentePiloto.setLocation(centradoXY(frmDocentePiloto));
                    frmDocentePiloto.setVisible(true);
                }
                if(cmb.equals(CrearModificarBorrar.BORRAR)){
                    int respuesta = JOptionPane.showConfirmDialog(this.view, "¿Desea eliminar este Docente?", "Confirmar",
                                    JOptionPane.OK_CANCEL_OPTION, JOptionPane.INFORMATION_MESSAGE);
                    if (respuesta == JOptionPane.OK_OPTION) {
                        frmDocentePiloto.elegidaPersona = JButtonRenderer.docentepilotoBuscado;
                        modelo.eliminarPersona(frmDocentePiloto.elegidaPersona.getiD(), DeltaCharlie.IDUser);
                        modelo.vertablaDocentePiloto(frmPrincipalDocentePiloto.jTableDocentesPilotos, this);
                        frmPrincipalDocentePiloto.jTableDocentesPilotos.repaint();
                    }
                }
            } catch (SQLException ex) {
                JOptionPane.showMessageDialog(this.view, "Error Carga Datos");
                cerrar(frmCrearNuevoAlumno);
            }  
        } else {
            JOptionPane.showMessageDialog(this.view, "La ventana Crear Docente Piloto se encuentra abierta");

        }
    }

    private void buscarUsuario() {
        if (estacerrado(frmBuscarUsuario)) {
            try {
                frmBuscarUsuario = new JFrameInternalBuscarUsuario(modelo);
                this.view.jDesktopPane.add(frmBuscarUsuario);

                frmBuscarUsuario.jButtonAgregar.addActionListener(this);
                frmBuscarUsuario.jButtonCancelar.addActionListener(this);

                frmBuscarUsuario.setLocation(centradoXY(frmBuscarUsuario));
                frmBuscarUsuario.setVisible(true);
            } catch (Exception e) {
                JOptionPane.showMessageDialog(this.view, "Error en conexion");
                cerrar(frmBuscarUsuario);
            }
        } else {
            JOptionPane.showMessageDialog(this.view, "La ventana Buscar Usuario se encuentra abierta");

        }

    }
    
    private void acercaDe() {
        if (estacerrado(frmAcercaDe)) {
            frmAcercaDe = new JFrameInternalAcercaDe();
            this.view.jDesktopPane.add(frmAcercaDe);
            frmAcercaDe.jButtonCancelar.addActionListener(this);

            frmAcercaDe.setLocation(centradoXY(frmAcercaDe));
            frmAcercaDe.setVisible(true);
            frmAcercaDe.icon = new ImageIcon(".\\src\\imagenes\\icon.png");
            frmAcercaDe.icono = new ImageIcon(frmAcercaDe.icon.getImage().getScaledInstance(frmAcercaDe.jLabel1.getWidth(), frmAcercaDe.jLabel1.getHeight(), Image.SCALE_DEFAULT));
            frmAcercaDe.jLabel1.setText("");
            frmAcercaDe.jLabel1.setIcon(frmAcercaDe.icono);
        } else {
            JOptionPane.showMessageDialog(this.view, "La ventana Acerca de se encuentra abierta");

        }
    }
    
    private void acercaDe(ActionEvent comando) {
        if (comando.getSource().equals(frmAcercaDe.jButtonCancelar)) {
            cerrar(frmAcercaDe);
        }
    }
    
    public void detallesAvion(Avion aux) {
        if (estacerrado(frmDetallesAvion)) {
            frmDetallesAvion = new JFrameInternalDetallesAvion();
            this.view.jDesktopPane.add(frmDetallesAvion);
            frmDetallesAvion.jButtonCancelar.addActionListener(this);
            frmDetallesAvion.jLabelMarca.setText(aux.getMarca());
            frmDetallesAvion.jLabelModelo.setText(aux.getModelo());
            frmDetallesAvion.jLabelNroSerie.setText(aux.getNumSerie());
            frmDetallesAvion.jLabelMinutos.setText(String.valueOf(aux.getMinutos()));
            frmDetallesAvion.jLabelMatricula.setText(aux.getFechaVencimientoRegistroMatricula());
            frmDetallesAvion.jLabelAereo.setText(aux.getFechaVencimientoAeronavegabilidad());
            frmDetallesAvion.jLabelExtintor.setText(aux.getFechaVencimientoExtintor());
            frmDetallesAvion.jLabelEmergencia.setText(aux.getFechaVencimientoEquipoEmergencia());
            frmDetallesAvion.jLabelSeguro.setText(aux.getFechaVencimientoSeguro());
            
            String[] aux1 = modelo.selectMotor(aux.getIDAvion());
            frmDetallesAvion.jLabelMotor1.setText(aux1[0]);
            frmDetallesAvion.jLabelMotor2.setText(aux1[1]);
            frmDetallesAvion.Helice1.setText(modelo.selectHelice(aux1[0]));
            frmDetallesAvion.jLabelHelice2.setText(modelo.selectHelice(aux1[1]));

            frmDetallesAvion.setLocation(centradoXY(frmDetallesAvion));
            frmDetallesAvion.setVisible(true);
        } else {
            JOptionPane.showMessageDialog(this.view, "La ventana Acerca de se encuentra abierta");
        }
    }
    
    public void detallesAvion(ActionEvent comando) {
        if (comando.getSource().equals(frmDetallesAvion.jButtonCancelar)) {
            cerrar(frmDetallesAvion);
        }
    }
    
    public void detallesGrupo(Grupo aux) {
        if (estacerrado(frmDetallesGrupo)) {
            frmDetallesGrupo = new JFrameInternalDetallesGrupo();
            this.view.jDesktopPane.add(frmDetallesGrupo);
            frmDetallesGrupo.jButtonCancelar.addActionListener(this);
            frmDetallesGrupo.jLabelNombreG.setText(aux.getNombreGrupo());
            frmDetallesGrupo.jLabelDocente.setText(aux.getNombreDocente());
            frmDetallesGrupo.jLabelCosto.setText(aux.getNombreMateria());
            frmDetallesGrupo.jLabelMaterias.setText(aux.getFechaInicio());
            frmDetallesGrupo.jLabelMaterias1.setText(aux.getFechaFin());
            frmDetallesGrupo.jLabelMaterias2.setText(aux.getHora());
            frmDetallesGrupo.setLocation(centradoXY(frmDetallesGrupo));
            frmDetallesGrupo.setVisible(true);
        } else {
            JOptionPane.showMessageDialog(this.view, "La ventana Acerca de se encuentra abierta");
        }
    }
    
    public void detallesMotor(Vector<String> aux) {
        if (estacerrado(frmDetallesMotor)) {
            frmDetallesMotor = new JFrameInternalDetallesMotor();
            this.view.jDesktopPane.add(frmDetallesMotor);
            frmDetallesMotor.jButtonCancelar.addActionListener(this);
            frmDetallesMotor.jLabelMarca.setText(aux.get(0));
            frmDetallesMotor.jLabelModelo.setText(aux.get(1));
            frmDetallesMotor.jLabelNroSerie.setText(aux.get(2));
            frmDetallesMotor.jLabelTT.setText(aux.get(3));
            frmDetallesMotor.jLabelTTO.setText(aux.get(4));
            frmDetallesMotor.jLabelTBO.setText(aux.get(5));
            if(aux.size()>6){
                frmDetallesMotor.Helice.setText(aux.get(6));
            }

            frmDetallesMotor.setLocation(centradoXY(frmDetallesMotor));
            frmDetallesMotor.setVisible(true);
        } else {
            JOptionPane.showMessageDialog(this.view, "La ventana Acerca de se encuentra abierta");
        }
    }
    
    public void detallesMotor(ActionEvent comando) {
        if (comando.getSource().equals(frmDetallesMotor.jButtonCancelar)) {
            cerrar(frmDetallesMotor);
        }
    }
    
    public void detallesHelice(Helices aux) {
        if (estacerrado(frmDetallesHelice)) {
            frmDetallesHelice = new JFrameInternalDetallesHelice();
            this.view.jDesktopPane.add(frmDetallesHelice);
            frmDetallesHelice.jButtonCancelar.addActionListener(this);
            frmDetallesHelice.jLabelMarca.setText(aux.getMarca());
            frmDetallesHelice.jLabelModelo.setText(aux.getModelo());
            frmDetallesHelice.jLabelNroSerie.setText(aux.getNumeroDeSerie());
            frmDetallesHelice.jLabelTT.setText(String.valueOf( aux.getTT()));
            frmDetallesHelice.jLabelTTO.setText(String.valueOf( aux.getTTO()));
            frmDetallesHelice.jLabelTBO.setText(String.valueOf( aux.getTBO()));
            frmDetallesHelice.setLocation(centradoXY(frmDetallesHelice));
            frmDetallesHelice.setVisible(true);
        } else {
            JOptionPane.showMessageDialog(this.view, "La ventana Acerca de se encuentra abierta");
        }
    }
    
    public void detallesMalla(Malla aux,Vector<Integer> aux2) {
        if (estacerrado(frmDetallesMalla)) {
            frmDetallesMalla = new JFrameInternalDetallesMalla();
            this.view.jDesktopPane.add(frmDetallesMalla);
            frmDetallesMalla.jLabelMalla.setText(aux.getNombreMalla());
            frmDetallesMalla.jLabelRequiere.setText(Integer.toString(aux.getRequiereLicencia()));
            frmDetallesMalla.jLabelCosto.setText(Integer.toString(aux.getCosto()));
            String materias = "";
            for(int i=0;i<aux2.size();i++){
                try {
                    String aux12 = modelo.ConseguirNombreMateria(aux2.get(i));
                    materias=materias+aux12+"-";
                } catch (Exception e) {
                } 
            }
            frmDetallesMalla.jLabelMaterias.setText(materias);
            frmDetallesMalla.setLocation(centradoXY(frmDetallesMalla));
            frmDetallesMalla.setVisible(true);
        } else {
            JOptionPane.showMessageDialog(this.view, "La ventana Acerca de se encuentra abierta");
        }
    }
    
    public void detallesHelice(ActionEvent comando) {
        if (comando.getSource().equals(frmDetallesHelice.jButtonCancelar)) {
            cerrar(frmDetallesHelice);
        }
    }
    
    public void detallesMateria(Materia aux) {
        if (estacerrado(frmDetallesMateria)) {
            frmDetallesMateria = new JFrameInternalDetallesMateria();
            this.view.jDesktopPane.add(frmDetallesMateria);
            frmDetallesMateria.jButtonCancelar.addActionListener(this);
            frmDetallesMateria.jLabelMateria.setText(aux.getNombre());
            frmDetallesMateria.jLabelCosto.setText(String.valueOf(aux.getCostoCreditos()));
            frmDetallesMateria.jLabelMinutos.setText(String.valueOf(aux.getTotalMinutos()));
            frmDetallesMateria.setLocation(centradoXY(frmDetallesMateria));
            frmDetallesMateria.setVisible(true);
        } else {
            JOptionPane.showMessageDialog(this.view, "La ventana Acerca de se encuentra abierta");
        }
    }
    
    public void detallesMateria(ActionEvent comando) {
        if (comando.getSource().equals(frmDetallesMateria.jButtonCancelar)) {
            
        }
    }
    
    public void detallesAlumno(Persona aux) {
        if (estacerrado(frmDetallesAlumno)) {
            frmDetallesAlumno = new JFrameInternalDetallesAlumno();
            this.view.jDesktopPane.add(frmDetallesAlumno);
            frmDetallesAlumno.jButtonAceptar.addActionListener(this);
            
            frmDetallesAlumno.jLabelNombreAlumnoHolder.setText(aux.getNombre());
            frmDetallesAlumno.jLabelApellidoPaternoHolder.setText(aux.getApellidoPaterno());
            frmDetallesAlumno.jLabelApellidoMaternoHolder.setText(aux.getApellidoMaterno());
            frmDetallesAlumno.jLabelTipoDeSangreHolder.setText(aux.getTipoSangra());
            if(aux.getUsaLentes() == 0)
            {
                frmDetallesAlumno.jLabelLentesHolder.setText("No");
            }
            else
            {
                frmDetallesAlumno.jLabelLentesHolder.setText("Si");
            }
            switch(aux.getTipoDocumento())
            {
                case 1:
                {
                    frmDetallesAlumno.jLabelTipoDeIdentificacionHolder.setText("CI");
                    break;
                }
                case 2:
                {
                    frmDetallesAlumno.jLabelTipoDeIdentificacionHolder.setText("RUC");
                    break;
                }
                case 3:
                {
                    frmDetallesAlumno.jLabelTipoDeIdentificacionHolder.setText("Libreta de Servicio Militar");
                    break;
                }
                case 4:
                {
                    frmDetallesAlumno.jLabelTipoDeIdentificacionHolder.setText("Pasaporte");
                    break;
                }
                default:
                {
                    frmDetallesAlumno.jLabelTipoDeIdentificacionHolder.setText("Desconocido");
                    break;
                }
            }
            
            frmDetallesAlumno.jLabelNumeroDeIdentificacionHolder.setText(aux.getCiRucPas());
            frmDetallesAlumno.jLabelFechaDeNacimientoHolder.setText(aux.getFechaNacimiento());
            frmDetallesAlumno.jLabelDireccionActualHolder.setText(aux.getDireccion());
            frmDetallesAlumno.jLabelTelefonoHolder.setText(String.valueOf(aux.getTelefono()));
            if(aux.getNumFolio() != 0)
            {
                frmDetallesAlumno.jLabelNumeroDeFolioHolder.setText(String.valueOf(aux.getNumFolio()));
            }
            else
            {
                frmDetallesAlumno.jLabelNumeroDeFolioHolder.setText("No Tiene");
            }
            if(aux.getNumLicencia()!= 0)
            {
                frmDetallesAlumno.jLabelNumeroDeLicenciaDePilotoHolder.setText(String.valueOf(aux.getNumLicencia()));
            }
            else
            {
                frmDetallesAlumno.jLabelNumeroDeLicenciaDePilotoHolder.setText("No Tiene");
            }
            if(aux.getNumCredencialSABSA()!= 0)
            {
                frmDetallesAlumno.jLabelNumeroCredencialSABSAHolder.setText(String.valueOf(aux.getNumCredencialSABSA()));
                frmDetallesAlumno.jLabelFechaDeVencimientoDeLaCredencialSABSAHolder.setText(aux.getFechaVigenciaSABSA());
            }
            else
            {
                frmDetallesAlumno.jLabelNumeroCredencialSABSAHolder.setText("No Tiene");
                frmDetallesAlumno.jLabelFechaDeVencimientoDeLaCredencialSABSAHolder.setText("No Tiene");
            }
            if(aux.getNumCredicialCoorporativo()!= 0)
            {
                frmDetallesAlumno.jLabelNumeroDeCredencialCorporativoHolder.setText(String.valueOf(aux.getNumCredicialCoorporativo()));
                frmDetallesAlumno.jLabelFechaDeVencimientoDeLaCredencialCorporativaHolder.setText(aux.getFechaVigenciaCoorporativo());
            }
            else
            {
                frmDetallesAlumno.jLabelNumeroDeCredencialCorporativoHolder.setText("No Tiene");
                frmDetallesAlumno.jLabelFechaDeVencimientoDeLaCredencialCorporativaHolder.setText("No Tiene");
            }
            if(!aux.getFechaVencimientoAptoMedico().isEmpty())
            {
                frmDetallesAlumno.jLabelFechaDeVencimientoDelCertificadoMedicoHolder.setText(aux.getFechaVencimientoAptoMedico());
            }
            else
            {
                frmDetallesAlumno.jLabelFechaDeVencimientoDelCertificadoMedicoHolder.setText("No Tiene");
            }
            frmDetallesAlumno.jLabelMallaInscritaHolder.setText(aux.getNombreMalla());
            
            if(aux.getFoto()!=null)
            {
                ImageIcon image = aux.getFoto();
                Image im = image.getImage();
                Image myImg = im.getScaledInstance(frmDetallesAlumno.jLabelFotoHolder.getWidth(), frmDetallesAlumno.jLabelFotoHolder.getHeight(),Image.SCALE_SMOOTH);
                ImageIcon newImage = new ImageIcon(myImg);
                frmDetallesAlumno.jLabelFotoHolder.setIcon(newImage);
            }
            else
            {
                ImageIcon icon = new ImageIcon(".\\src\\imagenes\\defaultUser.png");
                Icon icono = new ImageIcon(icon.getImage().getScaledInstance(frmDetallesAlumno.jLabelFotoHolder.getWidth(), frmDetallesAlumno.jLabelFotoHolder.getHeight(), Image.SCALE_DEFAULT));
                frmDetallesAlumno.jLabelFotoHolder.setText("");
                frmDetallesAlumno.jLabelFotoHolder.setIcon(icono);
            }
            
            LinkedList<Referencia> referencias = new LinkedList<Referencia>();
            try {
                modelo.recuperarReferencias(aux.getiD(), referencias);
            } catch (SQLException ex) {
                Logger.getLogger(controlador.class.getName()).log(Level.SEVERE, null, ex);
            }
            frmDetallesAlumno.crearTablaReferencias(referencias);
            frmDetallesAlumno.llenarTablaReferencias(referencias);
            
            frmDetallesAlumno.setLocation(centradoXY(frmDetallesAlumno));
            frmDetallesAlumno.setVisible(true);
        }
    }
    
    public void detallesAlumno(ActionEvent comando) {
        if (comando.getSource().equals(frmDetallesAlumno.jButtonAceptar)) {
            cerrar(frmDetallesAlumno);
        }
    }
    
    public void detallesDocentePiloto(Persona aux) {
        if (estacerrado(frmDetallesDocentePiloto)) {
            frmDetallesDocentePiloto = new JFrameInternalDetallesDocentePiloto();
            this.view.jDesktopPane.add(frmDetallesDocentePiloto);
            frmDetallesDocentePiloto.jButtonAceptar.addActionListener(this);
            
            frmDetallesDocentePiloto.jLabelNombreAlumnoHolder.setText(aux.getNombre());
            frmDetallesDocentePiloto.jLabelApellidoPaternoHolder.setText(aux.getApellidoPaterno());
            frmDetallesDocentePiloto.jLabelApellidoMaternoHolder.setText(aux.getApellidoMaterno());
            frmDetallesDocentePiloto.jLabelTipoDeSangreHolder.setText(aux.getTipoSangra());
            if(aux.getUsaLentes() == 0)
            {
                frmDetallesDocentePiloto.jLabelLentesHolder.setText("No");
            }
            else
            {
                frmDetallesDocentePiloto.jLabelLentesHolder.setText("Si");
            }
            switch(aux.getTipoDocumento())
            {
                case 1:
                {
                    frmDetallesDocentePiloto.jLabelTipoDeIdentificacionHolder.setText("CI");
                    break;
                }
                case 2:
                {
                    frmDetallesDocentePiloto.jLabelTipoDeIdentificacionHolder.setText("RUC");
                    break;
                }
                case 3:
                {
                    frmDetallesDocentePiloto.jLabelTipoDeIdentificacionHolder.setText("Libreta de Servicio Militar");
                    break;
                }
                case 4:
                {
                    frmDetallesDocentePiloto.jLabelTipoDeIdentificacionHolder.setText("Pasaporte");
                    break;
                }
                default:
                {
                    frmDetallesDocentePiloto.jLabelTipoDeIdentificacionHolder.setText("Desconocido");
                    break;
                }
            }
            
            frmDetallesDocentePiloto.jLabelNumeroDeIdentificacionHolder.setText(aux.getCiRucPas());
            frmDetallesDocentePiloto.jLabelFechaDeNacimientoHolder.setText(aux.getFechaNacimiento());
            frmDetallesDocentePiloto.jLabelDireccionActualHolder.setText(aux.getDireccion());
            frmDetallesDocentePiloto.jLabelTelefonoHolder.setText(String.valueOf(aux.getTelefono()));
            if(aux.getNumLicencia()!= 0)
            {
                frmDetallesDocentePiloto.jLabelNumeroDeLicenciaDePilotoHolder.setText(String.valueOf(aux.getNumLicencia()));
            }
            else
            {
                frmDetallesDocentePiloto.jLabelNumeroDeLicenciaDePilotoHolder.setText("No Tiene");
            }
            if(aux.getNumCredencialSABSA()!= 0)
            {
                frmDetallesDocentePiloto.jLabelNumeroCredencialSABSAHolder.setText(String.valueOf(aux.getNumCredencialSABSA()));
                frmDetallesDocentePiloto.jLabelFechaDeVencimientoDeLaCredencialSABSAHolder.setText(aux.getFechaVigenciaSABSA());
            }
            else
            {
                frmDetallesDocentePiloto.jLabelNumeroCredencialSABSAHolder.setText("No Tiene");
                frmDetallesDocentePiloto.jLabelFechaDeVencimientoDeLaCredencialSABSAHolder.setText("No Tiene");
            }
            if(aux.getNumCredicialCoorporativo()!= 0)
            {
                frmDetallesDocentePiloto.jLabelNumeroDeCredencialCorporativoHolder.setText(String.valueOf(aux.getNumCredicialCoorporativo()));
                frmDetallesDocentePiloto.jLabelFechaDeVencimientoDeLaCredencialCorporativaHolder.setText(aux.getFechaVigenciaCoorporativo());
            }
            else
            {
                frmDetallesDocentePiloto.jLabelNumeroDeCredencialCorporativoHolder.setText("No Tiene");
                frmDetallesDocentePiloto.jLabelFechaDeVencimientoDeLaCredencialCorporativaHolder.setText("No Tiene");
            }
            if(!aux.getFechaVencimientoAptoMedico().isEmpty())
            {
                frmDetallesDocentePiloto.jLabelFechaDeVencimientoDelCertificadoMedicoHolder.setText(aux.getFechaVencimientoAptoMedico());
            }
            else
            {
                frmDetallesDocentePiloto.jLabelFechaDeVencimientoDelCertificadoMedicoHolder.setText("No Tiene");
            }
            if(!aux.getFechaVencimientoProficienceCheck().isEmpty())
            {
                frmDetallesDocentePiloto.jLabelFechaDeVencimientoDelProficiencyCheckHolder.setText(aux.getFechaVencimientoProficienceCheck());
            }
            else
            {
                frmDetallesDocentePiloto.jLabelFechaDeVencimientoDelProficiencyCheckHolder.setText("No Tiene");
            }
            
            
            LinkedList<Referencia> referencias = new LinkedList<Referencia>();
            try {
                modelo.recuperarReferencias(aux.getiD(), referencias);
            } catch (SQLException ex) {
                Logger.getLogger(controlador.class.getName()).log(Level.SEVERE, null, ex);
            }
            frmDetallesDocentePiloto.crearTablaReferencias(referencias);
            frmDetallesDocentePiloto.llenarTablaReferencias(referencias);
            
            frmDetallesDocentePiloto.setLocation(centradoXY(frmDetallesDocentePiloto));
            frmDetallesDocentePiloto.setVisible(true);
        }
    }
    
    public void detallesDocentePiloto(ActionEvent comando) {
        if (comando.getSource().equals(frmDetallesDocentePiloto.jButtonAceptar)) {
            cerrar(frmDetallesDocentePiloto);
        }
    }
    
    public void detallesUsuario(Usuario aux) {
        if (estacerrado(frmDetallesUsuario)) {
            frmDetallesUsuario = new JFrameInternalDetallesUsuario();
            this.view.jDesktopPane.add(frmDetallesUsuario);
            frmDetallesUsuario.jButtonAceptar.addActionListener(this);
            
            switch(aux.getRol())
            {
                case 1:
                {
                    frmDetallesUsuario.jLabelRolHolder.setText("SysAdmin");
                    break;
                }
                case 2:
                {
                    frmDetallesUsuario.jLabelRolHolder.setText("Administrador");
                    break;
                }
                case 3:
                {
                    frmDetallesUsuario.jLabelRolHolder.setText("Primario");
                    break;
                }
                case 4:
                {
                    frmDetallesUsuario.jLabelRolHolder.setText("Secundario");
                    break;
                }
                default:
                {
                    frmDetallesUsuario.jLabelRolHolder.setText("Desconocido");
                    break;
                }
            }
            frmDetallesUsuario.jLabelNombreDeUsuarioHolder.setText(aux.getNombreUsuario());
            frmDetallesUsuario.jLabelNombreHolder.setText(aux.getNombre());
            
            
            frmDetallesUsuario.setLocation(centradoXY(frmDetallesUsuario));
            frmDetallesUsuario.setVisible(true);
        }
    }
    
    public void detallesUsuario(ActionEvent comando) {
        if (comando.getSource().equals(frmDetallesUsuario.jButtonAceptar)) {
            cerrar(frmDetallesUsuario);
        }
    }

//___________________________________________________________________________________ Soy una barra separadora :)
    /*Internal Frame Crear Materia*/
    public void crearMateria(CrearModificarBorrar cmb) {
        if (estacerrado(frmCrearMateria)) {
        frmCrearMateria = new JFrameInternalCrearMateria(cmb);
        if (cmb.equals(CrearModificarBorrar.MODIFICAR) || cmb.equals(CrearModificarBorrar.CREAR)) {
            this.view.jDesktopPane.add(frmCrearMateria);

            //Se pone a escuchar las acciones del usuario
            frmCrearMateria.jButtonBuscarMateriaDependencia.addActionListener(this);
            frmCrearMateria.jButtonAgregarMateriaDependencia.addActionListener(this);
            frmCrearMateria.jButtonEliminarMateriaDependiente.addActionListener(this);

            frmCrearMateria.jButtonAceptarAgregarMateria.addActionListener(this);
            frmCrearMateria.jButtonCancelar.addActionListener(this);
            if (cmb.equals(CrearModificarBorrar.MODIFICAR)) {
                frmCrearMateria.buscada = JButtonRenderer.materiaBuscado;
                try {
                    modelo.getListMateriasDependecias(frmCrearMateria.buscada.getIDMateria(), ids);
                } catch (SQLException ex) {
                    Logger.getLogger(controlador.class.getName()).log(Level.SEVERE, null, ex);
                }
                frmCrearMateria.llenarDatosMateria();
                modelo.CrearTablaMateriasMallaModificar(ids, contenedormateriaid, contenedormateria, contenedorCosto, contenedorMinutos);
                frmCrearMateria.jTableMateriasDependientes.setModel(modelo.LlenarTabla(contenedormateriaid,contenedormateria,contenedorCosto,contenedorMinutos));
            }
            frmCrearMateria.setLocation(centradoXY(frmCrearMateria));
            frmCrearMateria.setVisible(true);
        }
        if (cmb.equals(CrearModificarBorrar.BORRAR)) {
                int respuesta = JOptionPane.showConfirmDialog(this.view, "¿Desea eliminar esta Materia?", "Confirmar",
                        JOptionPane.OK_CANCEL_OPTION, JOptionPane.INFORMATION_MESSAGE);
                if (respuesta == JOptionPane.OK_OPTION) {
                    frmCrearMateria.buscada = JButtonRenderer.materiaBuscado;
                    try {
                        int idmateria = frmCrearMateria.buscada.getIDMateria();
                        modelo.eliminarMateria(idmateria, DeltaCharlie.IDUser);
                    } catch (SQLException ex) {
                        Logger.getLogger(controlador.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
            }
        modelo.vertablaMaterias(frmPrincipalMateria.jTableMaterias, this);
        frmPrincipalMateria.jTableMaterias.repaint();
        }
        else {
            JOptionPane.showMessageDialog(this.view, "La ventana Crear Materia se encuentra abierta");

        }
    }
    
    public void crearGrupo(CrearModificarBorrar cmb) {
        if (estacerrado(frmCrearGrupo)) {
            frmCrearGrupo = new JFrameInternalCrearGrupo(modelo, cmb);
            if (cmb.equals(CrearModificarBorrar.MODIFICAR) || cmb.equals(CrearModificarBorrar.CREAR)) {
                this.view.jDesktopPane.add(frmCrearGrupo);

                frmCrearGrupo.jButtonAceptar.addActionListener(this);
                frmCrearGrupo.jButtonCancelar.addActionListener(this);

                frmCrearGrupo.estadoInicial();
                if (cmb.equals(CrearModificarBorrar.MODIFICAR)) {
                    frmCrearGrupo.buscado = JButtonRenderer.grupoBuscado;
                    frmCrearGrupo.jTextFieldNombreGrupo.setText(frmCrearGrupo.buscado.getNombreGrupo());
                    frmCrearGrupo.jTextFieldNombreDocente.setText(frmCrearGrupo.buscado.getNombreDocente());
                    frmCrearGrupo.jTextFieldNombreMateria.setText(frmCrearGrupo.buscado.getNombreMateria());
                    String horainicial = frmCrearGrupo.buscado.getHora();
                    char primeraHora = horainicial.charAt(0);
                    char segundaHora = horainicial.charAt(1);
                    String primeraHoraS = Character.toString(primeraHora);
                    String segundaHoraS = Character.toString(segundaHora);
                    int horafinal = Integer.parseInt(primeraHoraS + segundaHoraS);
                    frmCrearGrupo.jSpinnerHora.setValue(horafinal);
                    char primerMinuto = horainicial.charAt(3);
                    char segundoMinuto = horainicial.charAt(4);
                    String primerMinutoS = Character.toString(primerMinuto);
                    String segundoMinutoS = Character.toString(segundoMinuto);
                    int Minutofinal = Integer.parseInt(primerMinutoS + segundoMinutoS);
                    frmCrearGrupo.jSpinnerMinuto.setValue(Minutofinal);
                    //dateChooserComboFechaNacimiento.setSelectedDate(elegidaPersona.getFechaCalendar(elegidaPersona.getFechaNacimiento()));
                    frmCrearGrupo.dateChooserComboFechaFin.setSelectedDate(frmCrearGrupo.buscado.getFechaCalendar(frmCrearGrupo.buscado.getFechaFin()));
                    frmCrearGrupo.dateChooserComboFechaInicio.setSelectedDate(frmCrearGrupo.buscado.getFechaCalendar(frmCrearGrupo.buscado.getFechaInicio()));
                }
                frmCrearGrupo.setLocation(centradoXY(frmCrearGrupo));
                frmCrearGrupo.setVisible(true);
            }
            if (cmb.equals(CrearModificarBorrar.BORRAR)) {
                    int respuesta = JOptionPane.showConfirmDialog(this.view, "¿Desea eliminar este Grupo?", "Confirmar",
                            JOptionPane.OK_CANCEL_OPTION, JOptionPane.INFORMATION_MESSAGE);
                    if (respuesta == JOptionPane.OK_OPTION) {
                        frmCrearGrupo.buscado = JButtonRenderer.grupoBuscado;
                        try {
                            int idgrupo = frmCrearGrupo.buscado.getIDGrupo();
                            modelo.eliminarGrupo(idgrupo);
                        } catch (SQLException ex) {
                            Logger.getLogger(controlador.class.getName()).log(Level.SEVERE, null, ex);
                        }
                    }
                }
            modelo.vertablaGrupo(frmPrincipalGrupo.jTableDocentesGrupos, this);
            frmPrincipalGrupo.jTableDocentesGrupos.repaint();
        } else {
            JOptionPane.showMessageDialog(this.view, "La ventana Crear Malla se encuentra abierta");
        }
    }
    
    private void registrarVuelos() {
        if (estacerrado(frmVuelo)) {
            frmVuelo = new JFrameInternalVuelo(modelo);
            this.view.jDesktopPane.add(frmVuelo);
            frmVuelo.jButtonReservar.addActionListener(this);
            frmVuelo.jButtonCancelar.addActionListener(this);
            
            modelo.vertablaReservas(frmVuelo.jTableReservas, this);
            
            frmVuelo.setLocation(centradoXY(frmVuelo));
            frmVuelo.setVisible(true);
        } else {
            JOptionPane.showMessageDialog(this.view, "La ventana Control Vuelos se encuentra abierta");

        }
    }
//___________________________________________________________________________________ Soy una barra separadora :)
/* INICIA */
    private void iniciar() {
        view.setLocationRelativeTo(null);//centrado en pantalla
        view.setExtendedState(principal.MAXIMIZED_BOTH);//estado maximizado

        //Se pone a escuchar las acciones del usuario
        
        view.j2Button2.addActionListener(this);
        view.j2Button2.setEnabled(true);
        view.j2Button1.addActionListener(this);
        view.j2Button1.setEnabled(true);
        view.j2Button6.addActionListener(this);
        view.j2Button6.setEnabled(true);

        view.reporteFlexible.addActionListener(this);
        view.jMenuAvion.addActionListener(this);
        view.jMenuMotor.addActionListener(this);
        view.jMenuAlumno.addActionListener(this);
        view.jMenuHelice.addActionListener(this);
        view.jMenuItemMalla.addActionListener(this);
        view.jMenuItemGrupo.addActionListener(this);
        view.jMenuItemNotas.addActionListener(this);
        view.jMenuDocentePiloto.addActionListener(this);
        
        if(DeltaCharlie.IDRol == 1 || DeltaCharlie.IDRol == 2){
            view.jMenuUsuario.setVisible(true);
            view.jMenuUsuario.addActionListener(this);
        }
        else{
            view.jMenuUsuario.setVisible(false);
        }
        
        
        
        view.jMenuMateria.addActionListener(this);
        view.j2Button4.addActionListener(this);
        view.j2Button2.setEnabled(true);
        view.j2Button5.addActionListener(this);
        view.j2Button5.setEnabled(true);
        view.j2Button3.addActionListener(this);
        view.j2Button3.setEnabled(true);
        view.j2Button8.addActionListener(this);
        view.j2Button8.setEnabled(true);
        view.j2Button9.addActionListener(this);
        view.j2Button9.setEnabled(true);
        
       if(inicio == 0){
            hilo1.start();
        }
        else{
            hilo1.resume();
        }
       
        view.Salir.addActionListener(this);
                
        view.setVisible(true);

    }
        
    private void revisarAlertas(){
        if (estacerrado(frmRevisarAlertas)) {
            frmRevisarAlertas = hilo1.frmAlertas;
            this.view.jDesktopPane.add(frmRevisarAlertas);
            frmRevisarAlertas.setLocation(centradoXY(frmRevisarAlertas));
            frmRevisarAlertas.setVisible(true);

            frmRevisarAlertas.jButtonCancelar.addActionListener(this);
        }
        else{
            if(!frmRevisarAlertas.isVisible()){
                frmRevisarAlertas.setVisible(true);
            }
            else {
                JOptionPane.showMessageDialog(this.view, "La ventana de Alertas se encuentra abierta");
            }
        }
    }
    
    private void revisarAlertas(ActionEvent comando) {
        if (comando.getSource().equals(frmRevisarAlertas.jButtonCancelar)) {
            frmRevisarAlertas.dispose();
        }
    }
    
    private void cargarAvion() {
        if (estacerrado(frmPrincipalAvion)) {
            frmPrincipalAvion = new JFrameInternalPrincipalAvion();
            this.view.jDesktopPane.add(frmPrincipalAvion);
            frmPrincipalAvion.jButtonCancelar.addActionListener(this);
            frmPrincipalAvion.jButtonAnadir.addActionListener(this);
            
            modelo.vertablaAvion(frmPrincipalAvion.jTableAviones, this);

            frmPrincipalAvion.setLocation(centradoXY(frmPrincipalAvion));
            frmPrincipalAvion.setVisible(true);
        } else {
            JOptionPane.showMessageDialog(this.view, "La ventana Acerca de se encuentra abierta");

        }
    }
    
    private void cargarAvion(ActionEvent comando) {
        if (comando.getSource().equals(frmPrincipalAvion.jButtonCancelar)) {
            cerrar(frmPrincipalAvion);
        }
        if (comando.getSource().equals(frmPrincipalAvion.jButtonAnadir)) {
            crearAvion(CrearModificarBorrar.CREAR);
        }
    }
    
    private void cargarMotor() {
        if (estacerrado(frmPrincipalMotor)) {
            frmPrincipalMotor = new JFrameInternalPrincipalMotor();
            this.view.jDesktopPane.add(frmPrincipalMotor);
            frmPrincipalMotor.jButtonCancelar.addActionListener(this);
            frmPrincipalMotor.jButtonAnadir.addActionListener(this);
            
            modelo.vertablaMotor(frmPrincipalMotor.jTableMotores, this);

            frmPrincipalMotor.setLocation(centradoXY(frmPrincipalMotor));
            frmPrincipalMotor.setVisible(true);
        } else {
            JOptionPane.showMessageDialog(this.view, "La ventana Acerca de se encuentra abierta");

        }
    }
    
    
    
    private void cargarMotor(ActionEvent comando) {
        if (comando.getSource().equals(frmPrincipalMotor.jButtonCancelar)) {
            cerrar(frmPrincipalMotor);
        }
        if (comando.getSource().equals(frmPrincipalMotor.jButtonAnadir)) {
            crearMotor(CrearModificarBorrar.CREAR);
        }
    }
    private void cargarHelice() {
        if (estacerrado(frmPrincipalHelice)) {
            frmPrincipalHelice = new JFrameInternalPrincipalHelice();
            this.view.jDesktopPane.add(frmPrincipalHelice);
            frmPrincipalHelice.jButtonCancelar.addActionListener(this);
            frmPrincipalHelice.jButtonAnadir.addActionListener(this);
            
            modelo.vertablaHelice(frmPrincipalHelice.jTableHelices, this);

            frmPrincipalHelice.setLocation(centradoXY(frmPrincipalHelice));
            frmPrincipalHelice.setVisible(true);
        } else {
            JOptionPane.showMessageDialog(this.view, "La ventana Acerca de se encuentra abierta");

        }
    }
    
    private void cargarMalla() {
        if (estacerrado(frmPrincipalMalla)) {
            frmPrincipalMalla = new JFrameInternalPrincipalMalla();
            this.view.jDesktopPane.add(frmPrincipalMalla);
            frmPrincipalMalla.jButtonCancelar.addActionListener(this);
            frmPrincipalMalla.jButtonAnadir.addActionListener(this);
            
            modelo.vertablaMalla(frmPrincipalMalla.jTableMallas, this);

            frmPrincipalMalla.setLocation(centradoXY(frmPrincipalMalla));
            frmPrincipalMalla.setVisible(true);
        } else {
            JOptionPane.showMessageDialog(this.view, "La ventana Acerca de se encuentra abierta");

        }
    }
    
    private void cargarMalla(ActionEvent comando) {
        if (comando.getSource().equals(frmPrincipalMalla.jButtonAnadir)) {
            crearMalla(CrearModificarBorrar.CREAR);
        }
    }
    
    private void cargarGrupo(ActionEvent comando) {
        if (comando.getSource().equals(frmPrincipalGrupo.jButtonAnadir)) {
            crearGrupo(CrearModificarBorrar.CREAR);
        }
    }
    
    private void cargarGrupo() {
        if (estacerrado(frmPrincipalGrupo)) {
            frmPrincipalGrupo = new JFrameInternalPrincipalGrupo();
            this.view.jDesktopPane.add(frmPrincipalGrupo);
            frmPrincipalGrupo.jButtonCancelar.addActionListener(this);
            frmPrincipalGrupo.jButtonAnadir.addActionListener(this);
            
            modelo.vertablaGrupo(frmPrincipalGrupo.jTableDocentesGrupos, this);

            frmPrincipalGrupo.setLocation(centradoXY(frmPrincipalGrupo));
            frmPrincipalGrupo.setVisible(true);
        } else {
            JOptionPane.showMessageDialog(this.view, "La ventana Acerca de se encuentra abierta");

        }
    }
    
    private void cargarNotas() {
        if (estacerrado(frmPrincipalNotas)) {
            frmPrincipalNotas = new JFrameInternalPrincipalNotas(modelo);
            this.view.jDesktopPane.add(frmPrincipalNotas);
            
            frmPrincipalNotas.jButtonEmpezar.addActionListener(this);
            frmPrincipalNotas.jButtonIngresarParcial.addActionListener(this);
            frmPrincipalNotas.jButtonFinalizar.addActionListener(this);

            frmPrincipalNotas.setLocation(centradoXY(frmPrincipalNotas));
            frmPrincipalNotas.setVisible(true);
        } else {
            JOptionPane.showMessageDialog(this.view, "La ventana Acerca de se encuentra abierta");

        }
    }
    
    private void cargarNotas(ActionEvent comando) {
        try {
            if (comando.getSource().equals(frmPrincipalNotas.jButtonEmpezar)) {
                if (frmPrincipalNotas.jTextFieldAlumnoS.getText().isEmpty()) {
                    JOptionPane.showMessageDialog(this.view, "No podemos empezar sin un Alumno");
                }
                else if (frmPrincipalNotas.jTextFieldGrupoS.getText().isEmpty()) {
                    JOptionPane.showMessageDialog(this.view, "No podemos empezar sin un Grupo");
                }
                else if (frmPrincipalNotas.jTextFieldNumeroParciales.getText().isEmpty()) {
                    JOptionPane.showMessageDialog(this.view, "No podemos empezar sin la cantidad de parciales");
                }
                else{
                    parciales = Integer.parseInt(frmPrincipalNotas.jTextFieldNumeroParciales.getText());
                    frmPrincipalNotas.jTextFieldNotaParcial.setVisible(true);
                    frmPrincipalNotas.jButtonIngresarParcial.setVisible(true);
                    frmPrincipalNotas.jLabelParcialOne.setVisible(true);
                    frmPrincipalNotas.jLabelParcialTwo.setVisible(true);
                    frmPrincipalNotas.jLabelParcialThree.setVisible(true);
                    frmPrincipalNotas.jTextFieldNumeroParciales.setEditable(false);
                }
            }
            if(comando.getSource().equals(frmPrincipalNotas.jButtonIngresarParcial)){
                i++;
                System.out.println(i);
                System.out.println(parciales);
                NotasParciales.add(Integer.parseInt(frmPrincipalNotas.jTextFieldNotaParcial.getText().toString()));
                if(i<=4){
                    AnotadorParciales1=AnotadorParciales1+"Parcial N°"+Integer.toString(i)+":"+frmPrincipalNotas.jTextFieldNotaParcial.getText().toString()+"    ";
                }else if(i>4 && i<=8){
                    AnotadorParciales2=AnotadorParciales2+"Parcial N°"+Integer.toString(i)+":"+frmPrincipalNotas.jTextFieldNotaParcial.getText().toString()+"    ";
                }
                else{
                    AnotadorParciales3=AnotadorParciales3+"Parcial N°"+Integer.toString(i)+":"+frmPrincipalNotas.jTextFieldNotaParcial.getText().toString()+"    ";
                }
                frmPrincipalNotas.jLabelParcialOne.setText(AnotadorParciales1);
                frmPrincipalNotas.jLabelParcialTwo.setText(AnotadorParciales2);
                frmPrincipalNotas.jLabelParcialThree.setText(AnotadorParciales3);
                frmPrincipalNotas.jButtonIngresarParcial.setText("Ingresar Parcial N° "+Integer.toString(i+1));
                if (i == parciales) {
                    frmPrincipalNotas.jButtonIngresarParcial.setEnabled(false);
                    frmPrincipalNotas.jButtonIngresarParcial.setText("Ingresar Parcial N° "+Integer.toString(i));
                }
            }
            if(comando.getSource().equals(frmPrincipalNotas.jButtonFinalizar)){
                if (frmPrincipalNotas.jTextFieldNotaFinal.getText().isEmpty()) {
                    JOptionPane.showMessageDialog(this.view, "No podemos finalizar sin la nota final");
                }
                else{
                    int idg=modelo.getIDGrupo(frmPrincipalNotas.jTextFieldGrupoS.getText());
                    int ida=modelo.ConseguirIDDocente(frmPrincipalNotas.jTextFieldAlumnoS.getText());
                    int nota=Integer.parseInt(frmPrincipalNotas.jTextFieldNotaFinal.getText());
                    modelo.insertarNota(idg,ida,nota);
                    modelo.ActualizarNotasIngresadas(ida, idg);
                    int idnotafinal = modelo.ConseguirIDNota();
                    for (int j = 0; j < NotasParciales.size(); j++) {
                        modelo.insertarparciales(NotasParciales.get(j), idnotafinal);
                    }
                    int respuesta = JOptionPane.showConfirmDialog(this.view, "¿Quiere ingresar otras notas?", "Insertar de nuevo",
                            JOptionPane.OK_CANCEL_OPTION, JOptionPane.INFORMATION_MESSAGE);
                    if (respuesta == JOptionPane.OK_OPTION) {
                        //arreglar bien que vacie todo para la nueva insersion
                        frmPrincipalNotas.jTextFieldAlumnoS.setText("");
                        frmPrincipalNotas.jTextFieldGrupoS.setText("");
                        frmPrincipalNotas.jTextFieldMateriaS.setText("");
                        frmPrincipalNotas.jTextFieldNotaFinal.setText("");
                        frmPrincipalNotas.jTextFieldNotaParcial.setText("");
                        frmPrincipalNotas.jTextFieldNumeroParciales.setText("");
                        frmPrincipalNotas.jTextFieldAlumnoBuscado.setText("");
                        frmPrincipalNotas.jTextFieldNotaParcial.setVisible(false);
                        frmPrincipalNotas.jButtonIngresarParcial.setVisible(false);
                        frmPrincipalNotas.jLabelParcialOne.setVisible(false);
                        frmPrincipalNotas.jLabelParcialTwo.setVisible(false);
                        frmPrincipalNotas.jLabelParcialThree.setVisible(false);
                        frmPrincipalNotas.jTextFieldNumeroParciales.setEditable(true);
                        AnotadorParciales1 = "";
                        AnotadorParciales2 = "";
                        AnotadorParciales3 = "";
                        NotasParciales.clear();

                    } else {
                        cerrar(frmPrincipalNotas);
                    }
                    
                }
            }
            if (comando.getSource().equals(frmPrincipalNotas.jButtonCancelar)) {
                frmPrincipalNotas.jTextFieldAlumnoS.setText("");
                frmPrincipalNotas.jTextFieldGrupoS.setText("");
                frmPrincipalNotas.jTextFieldMateriaS.setText("");
                frmPrincipalNotas.jTextFieldNotaFinal.setText("");
                frmPrincipalNotas.jTextFieldNotaParcial.setText("");
                frmPrincipalNotas.jTextFieldNumeroParciales.setText("");
                frmPrincipalNotas.jTextFieldAlumnoBuscado.setText("");
                frmPrincipalNotas.jTextFieldNotaParcial.setVisible(false);
                frmPrincipalNotas.jButtonIngresarParcial.setVisible(false);
                frmPrincipalNotas.jLabelParcialOne.setVisible(false);
                frmPrincipalNotas.jLabelParcialTwo.setVisible(false);
                frmPrincipalNotas.jLabelParcialThree.setVisible(false);
                frmPrincipalNotas.jTextFieldNumeroParciales.setEditable(true);
                AnotadorParciales1 = "";
                AnotadorParciales2 = "";
                AnotadorParciales3 = "";
                NotasParciales.clear();
            }
                    
        } catch (Exception ex) {
            System.err.println(ex);
        }
    }
    
    private void cargarHelice(ActionEvent comando) {
        if (comando.getSource().equals(frmPrincipalHelice.jButtonCancelar)) {
            cerrar(frmPrincipalHelice);
        }
        if (comando.getSource().equals(frmPrincipalHelice.jButtonAnadir)) {
            crearHelice(CrearModificarBorrar.CREAR);
        }
    }
    
    private void cargarMateria() {
        if (estacerrado(frmPrincipalMateria)) {
            frmPrincipalMateria = new JFrameInternalPrincipalMateria();
            this.view.jDesktopPane.add(frmPrincipalMateria);
            frmPrincipalMateria.jButtonCancelar.addActionListener(this);
            frmPrincipalMateria.jButtonAnadir.addActionListener(this);
            
            modelo.vertablaMaterias(frmPrincipalMateria.jTableMaterias, this);

            frmPrincipalMateria.setLocation(centradoXY(frmPrincipalMateria));
            frmPrincipalMateria.setVisible(true);
        } else {
            JOptionPane.showMessageDialog(this.view, "La ventana Acerca de se encuentra abierta");

        }
    }
    
    private void cargarMateria(ActionEvent comando) {
        if (comando.getSource().equals(frmPrincipalMateria.jButtonCancelar)) {
            cerrar(frmPrincipalMateria);
        }
        if (comando.getSource().equals(frmPrincipalMateria.jButtonAnadir)) {
            crearMateria(CrearModificarBorrar.CREAR);
        }
    }
    
    private void cargarAlumno() {
        if (estacerrado(frmPrincipalAlumno)) {
            frmPrincipalAlumno = new JFrameInternalPrincipalAlumno();
            this.view.jDesktopPane.add(frmPrincipalAlumno);
            frmPrincipalAlumno.jButtonCancelar.addActionListener(this);
            frmPrincipalAlumno.jButtonAnadir.addActionListener(this);
            
            modelo.vertablaAlumno(frmPrincipalAlumno.jTableAlumnos, this);

            frmPrincipalAlumno.setLocation(centradoXY(frmPrincipalAlumno));
            frmPrincipalAlumno.setVisible(true);
        } else {
            JOptionPane.showMessageDialog(this.view, "La ventana Alumno se encuentra abierta");
        }        
    }
    
    private void cargarAlumno(ActionEvent comando) {
        if (comando.getSource().equals(frmPrincipalAlumno.jButtonCancelar)) {
            cerrar(frmPrincipalAlumno);
        }
        if (comando.getSource().equals(frmPrincipalAlumno.jButtonAnadir)) {
            crearAlumno(CrearModificarBorrar.CREAR);
        }
    }
    
    private void cargarDocentePiloto() {
        if (estacerrado(frmPrincipalDocentePiloto)) {
            frmPrincipalDocentePiloto = new JFrameInternalPrincipalDocentePiloto();
            this.view.jDesktopPane.add(frmPrincipalDocentePiloto);
            frmPrincipalDocentePiloto.jButtonCancelar.addActionListener(this);
            frmPrincipalDocentePiloto.jButtonAnadir.addActionListener(this);
            
            modelo.vertablaDocentePiloto(frmPrincipalDocentePiloto.jTableDocentesPilotos, this);

            frmPrincipalDocentePiloto.setLocation(centradoXY(frmPrincipalDocentePiloto));
            frmPrincipalDocentePiloto.setVisible(true);
        } else {
            JOptionPane.showMessageDialog(this.view, "La ventana Docente/Piloto se encuentra abierta");

        }
    }
    
    private void cargarDocentePiloto(ActionEvent comando) {
        if (comando.getSource().equals(frmPrincipalDocentePiloto.jButtonCancelar)) {
            cerrar(frmPrincipalDocentePiloto);
        }
        if (comando.getSource().equals(frmPrincipalDocentePiloto.jButtonAnadir)) {
            crearDocentePiloto(CrearModificarBorrar.CREAR);
        }
    }
    
    private void cargarUsuario() {
        if (estacerrado(frmPrincipalUsuario)) {
            frmPrincipalUsuario = new JFrameInternalPrincipalUsuario();
            this.view.jDesktopPane.add(frmPrincipalUsuario);
            frmPrincipalUsuario.jButtonCancelar.addActionListener(this);
            frmPrincipalUsuario.jButtonAnadir.addActionListener(this);
            
            modelo.vertablaUsuario(frmPrincipalUsuario.jTableUsuarios, this);

            frmPrincipalUsuario.setLocation(centradoXY(frmPrincipalUsuario));
            frmPrincipalUsuario.setVisible(true);
        } else {
            JOptionPane.showMessageDialog(this.view, "La ventana Docente/Piloto se encuentra abierta");

        }
    }
    
    private void cargarUsuario(ActionEvent comando) {
        if (comando.getSource().equals(frmPrincipalUsuario.jButtonCancelar)) {
            cerrar(frmPrincipalUsuario);
        }
        if (comando.getSource().equals(frmPrincipalUsuario.jButtonAnadir)) {
            crearUsuario(CrearModificarBorrar.CREAR);
        }
    }
//___________________________________________________________________________________ Soy una barra separadora :)
/*Acciones de todos los frames*/
    
    private void login(ActionEvent comando) {
        if (comando.getSource().equals(frmlogin.cmdIngresar)) {
            String nombreUsuario = frmlogin.txtUsername.getText();
            try {
                Pattern p = Pattern.compile("[^A-Za-z0-9]");
                Matcher m = p.matcher(nombreUsuario);
                boolean b = m.find();
                if (b)
                {
                    JOptionPane.showMessageDialog(this.view, "El nombre de usuario no puede tener caracteres especiales", "Error", JOptionPane.ERROR_MESSAGE);
                    throw new Exception("El nombre de usuario no puede tener caracteres especiales");
                }
                this.usuario = nombreUsuario;
                String passwordSHA1 = String.valueOf(frmlogin.txtPassword.getPassword());
                 try {
                    DeltaCharlie.IDUser = modelo.getIDUSer(usuario, passwordSHA1);
                    DeltaCharlie.IDRol = modelo.getIDRol(DeltaCharlie.IDUser);
                    modelo.login(DeltaCharlie.IDUser);
                    iniciar();
                    frmlogin.dispose();
                    frmlogin = null;

                } catch (SQLException sqle) {
                    JOptionPane.showMessageDialog(this.view, "Verifique la IP", "Error", JOptionPane.ERROR_MESSAGE);
                }
            } catch (Exception ex) {
                    Logger.getLogger(controlador.class.getName()).log(Level.SEVERE, null, ex);
            }
           
            

           

        }else if (comando.getSource().equals(frmlogin.cmdCancelar)) {
            System.exit(0);
        }
    }
    
    private void ajustes(ActionEvent comando) throws FileNotFoundException, UnsupportedEncodingException {

        if (comando.getSource().equals(frmAjustes.jButtonAceptar)) {
            String str = frmAjustes.jTextFieldIP.getText();
            try (PrintWriter out = new PrintWriter("src//modelo//IP's.txt", "UTF-8")) {
                out.write(str);
            }
            JOptionPane.showMessageDialog(this.view, "La IP se cambio correctamente");     
            frmAjustes.dispose();
        } else if (comando.getSource().equals(frmAjustes.jButtonCancelar)) {
            frmAjustes.dispose();
        }
    }
    

    public void crearUsuario(ActionEvent comando) {
        if (comando.getSource().equals(frmCrearUsuario.jButtonAceptar)) {
            if (null != frmCrearUsuario.cmbActual) {
                switch (frmCrearUsuario.cmbActual) {
                    case CREAR:
                        try {
                            if (frmCrearUsuario.jTextFieldNombreUsuario.getText().isEmpty()) {
                                throw new Exception("No se puede Crear sin Nombre de Usuario");
                            }
                            String username = frmCrearUsuario.jTextFieldNombreUsuario.getText();
                            String pass1 = String.valueOf(frmCrearUsuario.jPasswordField.getPassword());
                            String pass2 = String.valueOf(frmCrearUsuario.jPasswordFieldConfirmar.getPassword());
                            if (!pass1.equals(pass2)) {
                                throw new Exception("Las contraseñas no coinciden");
                            }
                            if (frmCrearUsuario.jTextFieldNombre.getText().isEmpty()) {
                                throw new Exception("Tiene que tener un Dueño");
                            }
                            if (frmCrearUsuario.jComboBoxRol.getSelectedIndex() == 0) {
                                throw new Exception("Elija algun rol");
                            }
                            String owner = frmCrearUsuario.jTextFieldNombre.getText();
                            modelo.insertarUsuario(username, pass2, owner, frmCrearUsuario.roles.get(frmCrearUsuario.jComboBoxRol.getSelectedIndex() - 1).getIDFase(),
                                    DeltaCharlie.IDUser);
                            modelo.vertablaUsuario(frmPrincipalUsuario.jTableUsuarios, this);
                            frmPrincipalUsuario.jTableUsuarios.repaint();
                            JOptionPane.showMessageDialog(this.view, "Usuario Insertado Correctamente", "Correcto", JOptionPane.INFORMATION_MESSAGE);
                            cerrar(frmCrearUsuario);
                        } catch (Exception ex) {
                            JOptionPane.showMessageDialog(this.view, ex.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
                        }
                        break;
                    case MODIFICAR:
                        try {
                            if (frmCrearUsuario.jTextFieldNombreUsuario.getText().isEmpty()) {
                                throw new Exception("No se puede Modificar sin Nombre de Usuario");
                            }
                            String username = frmCrearUsuario.jTextFieldNombreUsuario.getText();
                            String nombre = frmCrearUsuario.jTextFieldNombre.getText();

                            String pass1 = String.valueOf(frmCrearUsuario.jPasswordField.getPassword());
                            String pass2 = String.valueOf(frmCrearUsuario.jPasswordFieldConfirmar.getPassword());
                            if (!pass1.equals(pass2)) {
                                throw new Exception("Las contraseñas no coinciden");
                            }
                            if (frmCrearUsuario.jTextFieldNombre.getText().isEmpty()) {
                                throw new Exception("Tiene que tener un Dueño");
                            }
                            if (frmCrearUsuario.jComboBoxRol.getSelectedIndex() == 0) {
                                throw new Exception("Elija algun rol");
                            }
                            int rol = frmCrearUsuario.roles.get(frmCrearUsuario.jComboBoxRol.getSelectedIndex() - 1).getIDFase();
                            String salt = pass2;
                            Usuario usuarioMod = new Usuario(frmCrearUsuario.elegidoUsuario.getIdUsuario(), nombre, username, salt, rol);
                            modelo.modificarUsuario(usuarioMod, DeltaCharlie.IDUser);
                            JOptionPane.showMessageDialog(this.view, "Usuario Modificado Correctamente", "Correcto", JOptionPane.INFORMATION_MESSAGE);
                            modelo.vertablaUsuario(frmPrincipalUsuario.jTableUsuarios, this);
                            frmPrincipalUsuario.jTableUsuarios.repaint();
                            cerrar(frmCrearUsuario);
                        } catch (Exception ex) {
                            JOptionPane.showMessageDialog(this.view, ex.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
                        }
                        break;
                    //fill
                    case BORRAR:
                        if (frmCrearUsuario.elegidoUsuario != null) {
                            try {
                                int respuesta = JOptionPane.showConfirmDialog(this.view, "¿Desea Borrar este usuario?", "",JOptionPane.OK_CANCEL_OPTION, JOptionPane.INFORMATION_MESSAGE);
                                if (respuesta == JOptionPane.OK_OPTION) {
                                    modelo.eliminarUsuario(frmCrearUsuario.elegidoUsuario, DeltaCharlie.IDUser);
                                    modelo.vertablaUsuario(frmPrincipalUsuario.jTableUsuarios, this);
                                    frmPrincipalUsuario.jTableUsuarios.repaint();
                                } else {
                                    cerrar(frmPrincipalUsuario);
                                }
                            } catch (SQLException e) {
                                JOptionPane.showMessageDialog(this.view, e.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);

                            }
                        }
                        break;
                    default:
                        break;
                }
            }
        } else if (comando.getSource().equals(frmCrearUsuario.jButtonCancelar)) {
            cerrar(frmCrearUsuario);
        }
    }

    private void buscarUsuario(ActionEvent comando) {
        if (comando.getSource().equals(frmBuscarUsuario.jButtonAgregar)) {
            if (frmBuscarUsuario.usuarioBuscado != null) {
                frmCrearUsuario.elegidoUsuario = frmBuscarUsuario.usuarioBuscado;
                frmCrearUsuario.llenarUsuario();
                cerrar(frmBuscarUsuario);
            } else {
                JOptionPane.showMessageDialog(this.view, "No se selecciono ningun Usuario");
            }

        } else if (comando.getSource().equals(frmBuscarUsuario.jButtonCancelar)) {
            cerrar(frmBuscarUsuario);
        }
    }

    private void crearMateria(ActionEvent comando) {

        if (comando.getSource().equals(frmCrearMateria.jButtonBuscarMateriaDependencia)) {
            buscarMateria(CrearModificarBorrar.CREAR);
        } else if (comando.getSource().equals(frmCrearMateria.jButtonAgregarMateriaDependencia)) {
            if (frmCrearMateria.aDepender != null) {
                Materia aux2 = new Materia(frmCrearMateria.aDepender.getIDMateria(), frmCrearMateria.aDepender.getNombre(), frmCrearMateria.aDepender.getCostoCreditos(), frmCrearMateria.aDepender.getTotalMinutos());
                System.out.println(frmCrearMateria.aDepender.getIDMateria());
                System.out.println(frmCrearMateria.aDepender.getNombre());
                System.out.println(frmCrearMateria.aDepender.getCostoCreditos());
                System.out.println(frmCrearMateria.aDepender.getTotalMinutos());
                frmCrearMateria.contenedormaterias.put(auxhash,aux2);
                frmCrearMateria.crearTabla();
                frmCrearMateria.aDepender = null;
                frmCrearMateria.jTextFieldMateriaDependencia.setText("");
                frmCrearMateria.llenarTabla();
                auxhash++;
            } else {
                JOptionPane.showMessageDialog(this.view, "No hay materia", "Sin Datos", JOptionPane.ERROR_MESSAGE);
            }
        } else if (comando.getSource().equals(frmCrearMateria.jButtonEliminarMateriaDependiente)) {
            int aEliminar = frmCrearMateria.jTableMateriasDependientes.getSelectedRow();
            if (aEliminar >= 0) {
                frmCrearMateria.contenedormaterias.remove(aEliminar);
                frmCrearMateria.crearTabla();
                frmCrearMateria.llenarTabla();
            } else {
                JOptionPane.showMessageDialog(this.view, "No hay materia seleccionada", "Error Eliminar", JOptionPane.WARNING_MESSAGE);
            }
        } else if (comando.getSource().equals(frmCrearMateria.jButtonAceptarAgregarMateria)) {
            if (frmCrearMateria.cmbActual != null) {
                switch (frmCrearMateria.cmbActual) {
                    case CREAR:
                        try {
                            String nombre = frmCrearMateria.jTextFieldNombreMateria.getText();
                            if (nombre.isEmpty()) {
                                throw new Exception("Nombre no puede estar Vacio");
                            }
                            int costoCreditos = Integer.parseInt(frmCrearMateria.jTextFieldCostoUSD.getText());
                            if (Integer.toString(costoCreditos).isEmpty()) {
                                throw new Exception("El costo no puede estar vacio, ingrese uno");
                            }
                            int totalMinutos = 0;
                            totalMinutos = Integer.parseInt(frmCrearMateria.jSpinnerMinutosMateria.getValue().toString());
                            if (totalMinutos==0) {
                                throw new Exception("Los minutos no pueden ser 0");
                            }
                            Materia aux = new Materia( 0, nombre, costoCreditos, totalMinutos);
                            modelo.insertarMateria(aux,frmCrearMateria.contenedormaterias,DeltaCharlie.IDUser);
                            int val = JOptionPane.showConfirmDialog(frmCrearMateria, "Quisiera agregar Otra Materia", "Salir",
                                    JOptionPane.INFORMATION_MESSAGE, JOptionPane.OK_CANCEL_OPTION);
                            if (val == JOptionPane.OK_OPTION) {
                                frmCrearMateria.estadoIncial();
                                ids.clear();
                                contenedormateriaid.clear();
                                contenedormateria.clear();
                                contenedorMinutos.clear();
                                contenedorCosto.clear();
                                modelo.CrearTablaMateriasMallaModificar(ids, contenedormateriaid, contenedormateria, contenedorCosto, contenedorMinutos);
                                frmCrearMateria.jTableMateriasDependientes.setModel(modelo.LlenarTabla(contenedormateriaid, contenedormateria, contenedorCosto, contenedorMinutos));
                            } else {
                                cerrar(frmCrearMateria);
                            }
                        } catch (Exception ex) {
                            JOptionPane.showMessageDialog(this.view, ex.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
                        }
                        break;
                    case MODIFICAR:
                        try {
                            String nombre = frmCrearMateria.jTextFieldNombreMateria.getText();
                            if (nombre.isEmpty()) {
                                throw new Exception("Nombre no puede estar Vacio");
                            }
                            int costoCreditos = Integer.parseInt(frmCrearMateria.jTextFieldCostoUSD.getText());
                            int totalMinutos = 0;
                            totalMinutos = Integer.parseInt(frmCrearMateria.jSpinnerMinutosMateria.getValue().toString());
                            int auxid = modelo.ConseguirIDMateria(frmCrearMateria.buscada.getNombre());
                            Materia aux = new Materia(auxid,nombre, costoCreditos, totalMinutos);
                            modelo.modificarMateria(aux, DeltaCharlie.IDUser);
                            cerrar(frmCrearMateria);

                        } catch (Exception ex) {
                            JOptionPane.showMessageDialog(this.view, ex.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
                        }

                        break;
                    case BORRAR:
                        try {
                            if (frmCrearMateria.buscada != null) {
                                int auxid = modelo.ConseguirIDMateria(frmCrearMateria.buscada.getNombre());
                                modelo.eliminarMateria(auxid, DeltaCharlie.IDUser);
                                frmCrearMateria.jTextFieldNombreMateria.setText("");
                                frmCrearMateria.jTextFieldCostoUSD.setText("");
                                frmCrearMateria.jSpinnerMinutosMateria.setValue(0);
                            } else {
                                JOptionPane.showMessageDialog(this.view, "No hay materia a Eliminar", "Error", JOptionPane.ERROR_MESSAGE);
                            }
                        } catch (SQLException ex) {
                            JOptionPane.showMessageDialog(this.view, ex.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
                        }
                        break;

                }
            }

        } else if (comando.getSource().equals(frmCrearMateria.jButtonCancelar)) {
            auxhash = 0;
            cerrar(frmCrearMateria);
        }
    }

    private void buscarMateria(ActionEvent comando) {
        if (comando.getSource().equals(frmBuscarMateria.jButtonAgregar)) {
            if (frmBuscarMateria.cmbActual != null) {
                switch (frmBuscarMateria.cmbActual) {
                    case CREAR:
                        if (frmBuscarMateria.materiaBuscada != null) {
                            frmCrearMateria.aDepender = null;
                                frmCrearMateria.aDepender = frmBuscarMateria.materiaBuscada;
                                frmCrearMateria.actualizarMateriaBuscada();
                                cerrar(frmBuscarMateria);
                        } else {
                            JOptionPane.showMessageDialog(this.view, "No se selecciono ninguna Materia ", "Error", JOptionPane.ERROR_MESSAGE);
                        }
                        break;
                    case MODIFICAR:
                        if (frmBuscarMateria.materiaBuscada != null) {
                            try {
                                modelo.datosMateria(frmBuscarMateria.materiaBuscada, frmCrearMateria.contenedormaterias,frmCrearMateria.minutos);
                                frmCrearMateria.buscada = frmBuscarMateria.materiaBuscada;
                                frmCrearMateria.llenarDatosMateria();
                            } catch (SQLException e) {
                                JOptionPane.showMessageDialog(this.view, e.getCause(), "Error", JOptionPane.ERROR_MESSAGE);

                            }

                        }
                        break;
                }
            }

        }
        if (comando.getSource().equals(frmBuscarMateria.jButtonCancelar)) {
            cerrar(frmBuscarMateria);
        }
    }
    
    private void buscarGrupo(ActionEvent comando) {
        if (comando.getSource().equals(frmBuscarGrupo.jButtonAceptar)) {
            if (frmBuscarGrupo.cmbActual != null) {
                        if (frmBuscarGrupo.GrupoBuscado != null) {
                            frmCrearGrupo.buscado = frmBuscarGrupo.GrupoBuscado;
                            frmCrearGrupo.jTextFieldNombreGrupo.setText(frmCrearGrupo.buscado.getNombreGrupo());
                            frmCrearGrupo.jTextFieldNombreDocente.setText(frmCrearGrupo.buscado.getNombreDocente());
                            frmCrearGrupo.jTextFieldNombreMateria.setText(frmCrearGrupo.buscado.getNombreMateria());
                            String horainicial = frmCrearGrupo.buscado.getHora();
                            char primeraHora = horainicial.charAt(0);
                            char segundaHora = horainicial.charAt(1);
                            String primeraHoraS = Character.toString(primeraHora);
                            String segundaHoraS = Character.toString(segundaHora);
                            int horafinal = Integer.parseInt(primeraHoraS+segundaHoraS);
                            frmCrearGrupo.jSpinnerHora.setValue(horafinal);
                            char primerMinuto = horainicial.charAt(3);
                            char segundoMinuto = horainicial.charAt(4);
                            String primerMinutoS = Character.toString(primerMinuto);
                            String segundoMinutoS = Character.toString(segundoMinuto);
                            int Minutofinal = Integer.parseInt(primerMinutoS+segundoMinutoS);
                            frmCrearGrupo.jSpinnerMinuto.setValue(Minutofinal);
                            //dateChooserComboFechaNacimiento.setSelectedDate(elegidaPersona.getFechaCalendar(elegidaPersona.getFechaNacimiento()));
                            frmCrearGrupo.dateChooserComboFechaFin.setSelectedDate(frmCrearGrupo.buscado.getFechaCalendar(frmCrearGrupo.buscado.getFechaFin()));
                            frmCrearGrupo.dateChooserComboFechaInicio.setSelectedDate(frmCrearGrupo.buscado.getFechaCalendar(frmCrearGrupo.buscado.getFechaInicio()));
                            
                            cerrar(frmBuscarGrupo);
            }

        }
        if (comando.getSource().equals(frmBuscarGrupo.Cancelar)) {
            cerrar(frmBuscarGrupo);
        }
    }
    }
    
    private void CrearRegistroVuelo(ActionEvent comando){
        if(comando.getSource().equals(frmVuelo.jButtonReservar)){
            int IDPiloto,IDAlumno,IDAvion,horas;
            String nombreC;
            if (frmVuelo.jRadioButton1.isSelected()) {
                nombreC = frmVuelo.jRadioButton1.getText();
            } else {
                nombreC = frmVuelo.jRadioButton3.getText();
            }
            if(frmVuelo.jRadioButton1.isSelected() != true && frmVuelo.jRadioButton3.isSelected() != true){
                JOptionPane.showMessageDialog(this.view, "No se selecciono ningun tipo de vuelo ", "Error", JOptionPane.ERROR_MESSAGE);
            }else if(frmVuelo.jTextFieldPilotoT.getText().isEmpty()){
                JOptionPane.showMessageDialog(this.view, "No se selecciono a ningun Piloto ", "Error", JOptionPane.ERROR_MESSAGE);
            }else if(frmVuelo.jTextFieldAlumnoT.getText().isEmpty()){
                JOptionPane.showMessageDialog(this.view, "No se selecciono a ningun Alumno ", "Error", JOptionPane.ERROR_MESSAGE);
            }else if(frmVuelo.jTextFieldAvionT.getText().isEmpty() && nombreC == "Vuelo acompañado"){
                JOptionPane.showMessageDialog(this.view, "No se selecciono ningun avion ", "Error", JOptionPane.ERROR_MESSAGE);
            }else if(frmVuelo.jTextFieldHorasT.getText().isEmpty()){
                JOptionPane.showMessageDialog(this.view, "No se colocaron las horas a registrar ", "Error", JOptionPane.ERROR_MESSAGE);
            }else if(Integer.parseInt(frmVuelo.jTextFieldHorasAT.getText())<=Integer.parseInt(frmVuelo.jTextFieldHorasT.getText())){
                JOptionPane.showMessageDialog(this.view, "Las horas disponibles son insuficientes,ingrese una cantidad valida o realice un pago para adquirir mas ", "Error", JOptionPane.ERROR_MESSAGE);
            }else{
                try {
                    if(nombreC == "Simulacion"){
                        IDPiloto = modelo.ConseguirIDDocente(frmVuelo.jTextFieldPilotoT.getText());
                        IDAlumno = modelo.ConseguirIDDocente(frmVuelo.jTextFieldAlumnoT.getText());
                        horas = Integer.parseInt(frmVuelo.jTextFieldHorasT.getText());
                        modelo.IngresarReserva(nombreC, IDPiloto, IDAlumno, 0, horas);
                    }
                    if(nombreC == "Vuelo acompañado"){
                        IDPiloto = modelo.ConseguirIDDocente(frmVuelo.jTextFieldPilotoT.getText());
                        IDAlumno = modelo.ConseguirIDDocente(frmVuelo.jTextFieldAlumnoT.getText());
                        IDAvion = modelo.getIDAvion(frmVuelo.jTextFieldAvionT.getText());
                        horas = Integer.parseInt(frmVuelo.jTextFieldHorasT.getText());
                        modelo.IngresarReserva(nombreC, IDPiloto, IDAlumno, IDAvion, horas);
                    }
                    frmVuelo.jTextFieldAlumnoT.setText("");
                    frmVuelo.jTextFieldAvionBuscar.setText("");
                    frmVuelo.jTextFieldAvionT.setText("");
                    frmVuelo.jTextFieldEstudianteBuscar.setText("");
                    frmVuelo.jTextFieldHorasAT.setText("");
                    frmVuelo.jTextFieldHorasT.setText("");
                    frmVuelo.jTextFieldPilotoBuscar.setText("");
                    frmVuelo.jTextFieldPilotoT.setText("");
                } catch (SQLException ex) {
                    Logger.getLogger(controlador.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
            modelo.vertablaReservas(frmVuelo.jTableReservas,this);
            frmVuelo.jTableReservas.repaint();
            System.out.println("Paso coño");
        }
        if(comando.getSource().equals(frmVuelo.jButtonCancelar)){
            cerrar(frmVuelo);
        }
    }
    
    private void crearMalla(ActionEvent comando) {
        if (comando.getSource().equals(frmCrearMalla.jButtonAgregarMateriaSeleccionada)) {
            if (frmCrearMalla.aBuscar != null) {
                String NombreElejido = frmCrearMalla.jComboBoxMateriaABuscar.getSelectedItem().toString();
                modelo.CrearTablaMateriasMalla(NombreElejido,contenedormateriaid,contenedormateria,contenedorCosto,contenedorMinutos);
                frmCrearMalla.jTableMateriasCorrespondientesMalla.setModel(modelo.LlenarTabla(contenedormateriaid,contenedormateria,contenedorCosto,contenedorMinutos));
                modelo.DatosIngresados(frmCrearMalla.jLabelCostoTotalMallaV, frmCrearMalla.jLabelTotalMinutosV, frmCrearMalla.jLabelNumeroTotalMateriasV, contenedormateriaid, contenedormateria, contenedorCosto, contenedorMinutos);
            } else {
                JOptionPane.showMessageDialog(this.view, "Debe seleccionar alguna Materia", "Error", JOptionPane.ERROR_MESSAGE);
            }
        } else if (comando.getSource().equals(frmCrearMalla.jButtonEliminarMateriaSeleccionada)) {
            modelo.EliminarFila(frmCrearMalla.jTableMateriasCorrespondientesMalla,contenedormateriaid,contenedormateria,contenedorCosto,contenedorMinutos);
            modelo.DatosIngresados(frmCrearMalla.jLabelCostoTotalMallaV, frmCrearMalla.jLabelTotalMinutosV, frmCrearMalla.jLabelNumeroTotalMateriasV, contenedormateriaid, contenedormateria, contenedorCosto, contenedorMinutos);
        } else if (comando.getSource().equals(frmCrearMalla.jButtonCrearMalla)) {
            if (frmCrearMalla.cmbActual != null) {
                switch (frmCrearMalla.cmbActual) {
                    case CREAR:
                        try {

                            String nombremalla = frmCrearMalla.jTextFieldNombreMalla.getText();
                            String HorasVuelo = frmCrearMalla.jTextFieldHorasVuelo.getText();
                            String HorasSimulador = frmCrearMalla.jTextFieldHorasSimulador.getText();
                            if (nombremalla.isEmpty()) {
                                throw new Exception("No se puede crear sin nombre");
                            }
                            if (HorasVuelo.isEmpty()) {
                                throw new Exception("No se puede crear sin Horas Requeridas de Vuelo");
                            }
                            if (HorasSimulador.isEmpty()) {
                                throw new Exception("No se puede crear sin Horas Requeridas de Simulador");
                            }
                            if (contenedormateriaid.isEmpty()) {
                                throw new Exception("No se puede crear sin elegir materias");
                            }
                            int requiere = 0;
                            if (frmCrearMalla.jCheckBoxRequiereLicencia.isSelected()) {
                                requiere = 1;
                            }
                            Malla v = new Malla(frmCrearMalla.jTextFieldNombreMalla.getText(),requiere,Integer.parseInt(frmCrearMalla.jLabelCostoTotalMallaV.getText()),0,0,0);
                            modelo.anadirMalla(v,HorasVuelo,HorasSimulador);
                            int IDMalla = modelo.sacaridmalla(frmCrearMalla.jTextFieldNombreMalla.getText());
                            Enumeration<String> llaves1 = contenedormateriaid.keys();
                            while (llaves1.hasMoreElements()) {
                                Malla_Materia n = new Malla_Materia(IDMalla,Integer.parseInt(contenedormateriaid.get(llaves1.nextElement())));
                                modelo.anadirMalla_Materia(n);
                            }
                            contenedormateriaid.clear();
                            contenedormateria.clear();
                            contenedorCosto.clear();
                            contenedorMinutos.clear();
                            frmCrearMalla.jTableMateriasCorrespondientesMalla.setModel(modelo.LlenarTabla(contenedormateriaid,contenedormateria,contenedorCosto,contenedorMinutos));
                            modelo.nivel = 0;
                            /*modelo.CostoMaterias = 0;
                            modelo.MateriasVuelo = 0;
                            modelo.NumeroNormales = 0;
                            modelo.TotalMaterias = 0;
                            modelo.TotalMinutos = 0;*/
                            frmCrearMalla.jTextFieldNombreMalla.setText("");
                            frmCrearMalla.jTextFieldBuscarMateria.setText("");
                            frmCrearMalla.jCheckBoxRequiereLicencia.setSelected(false);
                            frmCrearMalla.jLabelCostoTotalMallaV.setText("0");
                            frmCrearMalla.jLabelNumeroTotalMateriasV.setText("0");
                            frmCrearMalla.jLabelTotalMinutosV.setText("0");
                        } catch (Exception ex) {
                            JOptionPane.showMessageDialog(this.view, ex.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
                        }
                        break;
                    case MODIFICAR:
                        try {

                            String nombremalla = frmCrearMalla.jTextFieldNombreMalla.getText();
                            String HorasVuelo = frmCrearMalla.jTextFieldHorasVuelo.getText();
                            String HorasSimulador = frmCrearMalla.jTextFieldHorasSimulador.getText();
                            if (nombremalla.isEmpty()) {
                                throw new Exception("No se puede modificar sin nombre");
                            }
                            if (HorasVuelo.isEmpty()) {
                                throw new Exception("No se puede crear sin Horas Requeridas de Vuelo");
                            }
                            if (HorasSimulador.isEmpty()) {
                                throw new Exception("No se puede crear sin Horas Requeridas de Simulador");
                            }
                            /*if (!frmCrearMalla.jTextFieldCostoMalla.getText().isEmpty()) {
                                throw new Exception("No se puede crear sin costo");
                            }*/
                            if (!contenedormateriaid.isEmpty()) {
                                
                            } else {
                                throw new Exception("Error no hay materias en la malla");
                            }
                            int requiere = 0;
                            if (frmCrearMalla.jCheckBoxRequiereLicencia.isSelected()) {
                                requiere = 1;
                            }
                            Malla v = new Malla(frmCrearMalla.jTextFieldNombreMalla.getText(),requiere,Integer.parseInt(frmCrearMalla.jLabelCostoTotalMallaV.getText()),0,0,0);
                            modelo.EliminarMalla_Materia(auxidmalla);
                            Enumeration<String> llaves1 = contenedormateriaid.keys();
                            while (llaves1.hasMoreElements()) {
                                Malla_Materia n = new Malla_Materia(auxidmalla,Integer.parseInt(contenedormateriaid.get(llaves1.nextElement())));
                                modelo.anadirMalla_Materia(n);
                            }
                            modelo.ModificarMalla(v, auxidmalla,HorasVuelo,HorasSimulador);
                            contenedormateriaid.clear();
                            contenedormateria.clear();
                            contenedorCosto.clear();
                            contenedorMinutos.clear();
                            frmCrearMalla.jTableMateriasCorrespondientesMalla.setModel(modelo.LlenarTabla(contenedormateriaid,contenedormateria,contenedorCosto,contenedorMinutos));
                            modelo.nivel = 0;
                            /*modelo.CostoMaterias = 0;
                            modelo.MateriasVuelo = 0;
                            modelo.NumeroNormales = 0;
                            modelo.TotalMaterias = 0;
                            modelo.TotalMinutos = 0;*/
                            frmCrearMalla.jTextFieldNombreMalla.setText("");
                            frmCrearMalla.jTextFieldBuscarMateria.setText("");
                            frmCrearMalla.jCheckBoxRequiereLicencia.setSelected(false);
                            frmCrearMalla.jLabelCostoTotalMallaV.setText("0");
                            frmCrearMalla.jLabelNumeroTotalMateriasV.setText("0");
                            frmCrearMalla.jLabelTotalMinutosV.setText("0");
                            //int costo = Integer.parseInt(frmCrearMalla.jTextFieldCostoMalla.getText());
                            //modelo.modificarMalla(frmCrearMalla.malla.getID(), nombremalla, requiere, frmCrearMalla.materiasMalla, costo);
//                            JOptionPane.showConfirmDialog(this.view, "Quiere Crear Otra Malla", "Crear Malla",
//                                    JOptionPane.INFORMATION_MESSAGE, JOptionPane.OK_CANCEL_OPTION);
                            
                        } catch (Exception ex) {
                            JOptionPane.showMessageDialog(this.view, ex.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
                        }
                        break;
                    case BORRAR:
                            /*modelo.EliminarMalla_Materia(auxidmalla);
                            modelo.EliminarMalla(auxidmalla);
                            contenedormateriaid.clear();
                            contenedormateria.clear();
                            contenedorCosto.clear();
                            contenedorMinutos.clear();
                            frmCrearMalla.jTableMateriasCorrespondientesMalla.setModel(modelo.LlenarTabla(contenedormateriaid, contenedormateria, contenedorCosto, contenedorMinutos));
                            modelo.nivel = 0;
                            frmCrearMalla.jTextFieldNombreMalla.setText("");
                            frmCrearMalla.jTextFieldBuscarMateria.setText("");
                            frmCrearMalla.jCheckBoxRequiereLicencia.setSelected(false);
                            frmCrearMalla.jLabelCostoTotalMallaV.setText("0");
                            frmCrearMalla.jLabelNumeroTotalMateriasV.setText("0");
                            frmCrearMalla.jLabelTotalMinutosV.setText("0");*/
                        break;
                }
            }

        } else if (comando.getSource().equals(frmCrearMalla.jButtonCancelar)) {
            contenedormateriaid.clear();
            contenedormateria.clear();
            contenedorCosto.clear();
            contenedorMinutos.clear();
            modelo.nivel=0;
            /*modelo.CostoMaterias = 0;
            modelo.MateriasVuelo = 0;
            modelo.NumeroNormales = 0;
            modelo.TotalMaterias = 0;
            modelo.TotalMinutos = 0;*/
            frmCrearMalla.jTextFieldNombreMalla.setText("");
            frmCrearMalla.jTextFieldBuscarMateria.setText("");
            frmCrearMalla.jCheckBoxRequiereLicencia.setSelected(false);
            frmCrearMalla.jLabelCostoTotalMallaV.setText("0");
            frmCrearMalla.jLabelNumeroTotalMateriasV.setText("0");
            frmCrearMalla.jLabelTotalMinutosV.setText("0");
            cerrar(frmCrearMalla);
        }
    }
    public void crearAlumno(ActionEvent comando) {
        if (comando.getSource().equals(frmCrearNuevoAlumno.jButtonAgregarTelefonoReferencia)) {
            try {
                    if (frmCrearNuevoAlumno.jTextFieldTelefonoReferencia.getText().isEmpty()) {
                        throw new Exception("No se puede Cargar sin un numero");
                    }
                    if (frmCrearNuevoAlumno.jTextFieldTipoDeTelefonoReferencia.getText().isEmpty()) {
                        throw new Exception("No se puede cargar el numero sin ubicacion");
                    }
                    
                    
                    Boolean ctrl = false;
                    int rowCount = frmCrearNuevoAlumno.jTableTelefonos.getRowCount();
                    for (int i = 0; i < rowCount; i++) {
                        String rowEntry = frmCrearNuevoAlumno.jTableTelefonos.getValueAt(0, i).toString();
                        if (rowEntry.equals(frmCrearNuevoAlumno.jTextFieldTelefonoReferencia.getText())) {
                            ctrl = true;
                        }
                    }
                    if(ctrl==true)
                    {
                        throw new Exception("La referencia ya tiene ese numero");
                    }
                    else{
                        int telefono = Integer.parseInt(frmCrearNuevoAlumno.jTextFieldTelefonoReferencia.getText());
                        String ubicacion = frmCrearNuevoAlumno.jTextFieldTipoDeTelefonoReferencia.getText();
                        TelefonoReferencia aux = new TelefonoReferencia(telefono, ubicacion);
                        frmCrearNuevoAlumno.telefonoReferencia.addLast(aux);
                        frmCrearNuevoAlumno.crearTablaTelefonos(frmCrearNuevoAlumno.telefonoReferencia);
                        frmCrearNuevoAlumno.llenarTablaTelefonos(frmCrearNuevoAlumno.telefonoReferencia);
                    }
                    
                    frmCrearNuevoAlumno.jTextFieldTelefonoReferencia.setText("");
                    frmCrearNuevoAlumno.jTextFieldTipoDeTelefonoReferencia.setText("");
            } catch (Exception e) {
                JOptionPane.showMessageDialog(this.view, e.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
            }
        } else if (comando.getSource().equals(frmCrearNuevoAlumno.jButtonEliminarTelefonoReferencia)) {
                int aEliminar = frmCrearNuevoAlumno.jTableTelefonos.getSelectedRow();
                if (aEliminar >= 0) {
                    frmCrearNuevoAlumno.telefonoReferencia.remove(aEliminar);
                    frmCrearNuevoAlumno.crearTablaTelefonos(frmCrearNuevoAlumno.telefonoReferencia);
                    frmCrearNuevoAlumno.llenarTablaTelefonos(frmCrearNuevoAlumno.telefonoReferencia);
                } else {
                    JOptionPane.showMessageDialog(this.view, "No hay telefono seleccionado", "Error Eliminar", JOptionPane.WARNING_MESSAGE);
                }
            
        } else if (comando.getSource().equals(frmCrearNuevoAlumno.jButtonBuscarFoto)) {
            JFileChooser file = new JFileChooser();
            FileNameExtensionFilter filtro = new FileNameExtensionFilter("*.jpg", "jpg","*.png",".png","*.jpeg",".jpeg");
            file.setFileFilter(filtro);

            int seleccion = file.showOpenDialog(contentPane);
            //Si el usuario, pincha en aceptar
            if (seleccion == JFileChooser.APPROVE_OPTION) {
                //Seleccionamos el fichero
                fichero = file.getSelectedFile();
                //Ecribe la ruta del fichero seleccionado en el campo de texto
                
                frmCrearNuevoAlumno.jTextFieldURLFoto.setText(fichero.getAbsolutePath());
                frmCrearNuevoAlumno.tipoImageIcon = new ImageIcon(fichero.toString());
                
                frmCrearNuevoAlumno.tipoIcon = new ImageIcon(frmCrearNuevoAlumno.tipoImageIcon.getImage().getScaledInstance(frmCrearNuevoAlumno.jLabelFotoEstudiante6.getWidth(), frmCrearNuevoAlumno.jLabelFotoEstudiante6.getHeight(), Image.SCALE_DEFAULT));
                frmCrearNuevoAlumno.jLabelFotoEstudiante6.setText("");
                frmCrearNuevoAlumno.jLabelFotoEstudiante6.setIcon(frmCrearNuevoAlumno.tipoIcon);
            }
        } else if (comando.getSource().equals(frmCrearNuevoAlumno.jButtonAgregarReferencia)) {

            try {
                

                    if (frmCrearNuevoAlumno.jTextFieldNombreCompletoReferencia.getText().isEmpty()) {
                        throw new Exception("No se puede crear Referencia sin Nombre");
                    }
                    if (frmCrearNuevoAlumno.telefonoReferencia.isEmpty()) {
                        throw new Exception("No se puede crear Referencia sin Telefonos");
                    }
                    if (frmCrearNuevoAlumno.jTextFieldParentesco.getText().isEmpty()) {
                        throw new Exception("No se puede crear Referencia sin Parentesco con el alumno");
                    }
                    String nombreReferencia = frmCrearNuevoAlumno.jTextFieldNombreCompletoReferencia.getText();
                    int idReferencia = frmCrearNuevoAlumno.idReferenciaElegida;

                        Boolean ctrl = false;
                        int rowCount = frmCrearNuevoAlumno.jTableReferencias.getRowCount();
                        for (int i = 0; i < rowCount; i++) {
                            String rowEntry = frmCrearNuevoAlumno.jTableReferencias.getValueAt(0, i).toString();
                            if (rowEntry.equals(frmCrearNuevoAlumno.jTextFieldNombreCompletoReferencia.getText())) {
                                ctrl = true;
                            }
                        }
                        if(ctrl==true)
                        {
                            throw new Exception("La referencia ya esta en la tabla");
                        }
                        else{
                            String parentesco = frmCrearNuevoAlumno.jTextFieldParentesco.getText();
                            Referencia aux = new Referencia(idReferencia, nombreReferencia, frmCrearNuevoAlumno.telefonoReferencia);
                            aux.setParentesco(parentesco);
                            frmCrearNuevoAlumno.jTextFieldNombreCompletoReferencia.setText("");
                            frmCrearNuevoAlumno.jTextFieldParentesco.setText("");
                            frmCrearNuevoAlumno.referencias.addLast(aux);
                            frmCrearNuevoAlumno.crearTablaReferencias();
                            frmCrearNuevoAlumno.llenarTablaReferencias();
                            for (Referencia referencia : frmCrearNuevoAlumno.referencias)
                            {
                                for(TelefonoReferencia TR : referencia.getTelefono() )
                                {
                                    System.out.println(referencia.getNombre()+" = "+TR.getNumTelefono());                                  
                                }
                            }
                            frmCrearNuevoAlumno.jButtonAgregarTelefonoReferencia.setEnabled(true);
                            frmCrearNuevoAlumno.jButtonEliminarTelefonoReferencia.setEnabled(true);
                            frmCrearNuevoAlumno.telefonoReferencia = new LinkedList<TelefonoReferencia>();
                            DefaultTableModel dm = (DefaultTableModel)frmCrearNuevoAlumno.jTableTelefonos.getModel();
                            dm.getDataVector().removeAllElements();
                            dm.fireTableDataChanged();
                            
                        }


            } catch (Exception e) {
                JOptionPane.showMessageDialog(this.view, e.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
            }
        } else if (comando.getSource().equals(frmCrearNuevoAlumno.jButtonEliminarReferencia)) {
            int aEliminar = frmCrearNuevoAlumno.jTableReferencias.getSelectedRow();
            if (aEliminar >= 0) {
                frmCrearNuevoAlumno.referencias.remove(aEliminar);
                frmCrearNuevoAlumno.crearTablaReferencias();
                frmCrearNuevoAlumno.llenarTablaReferencias();
            } else {
                JOptionPane.showMessageDialog(this.view, "No hay Referencia seleccionada", "Error Eliminar", JOptionPane.WARNING_MESSAGE);
            }
        } else if (comando.getSource().equals(frmCrearNuevoAlumno.botonAceptar)) {
            if (null != frmCrearNuevoAlumno.cmbActual) {
                switch (frmCrearNuevoAlumno.cmbActual) {
                    case CREAR:
                        try {
                            String nombreAlumno = frmCrearNuevoAlumno.jTextFieldNombre.getText();
                            String apellidoPaterno = frmCrearNuevoAlumno.jTextFieldApellidoPaterno.getText();
                            String apellidoMaterno = frmCrearNuevoAlumno.jTextFieldApellidoMaterno.getText();
                            String direccion = frmCrearNuevoAlumno.jTextFieldDireccionActual.getText();
                            if (nombreAlumno.isEmpty()) {
                                throw new Exception("No se puede Crear Alumno Sin Nombre");
                            }
                            if (apellidoPaterno.isEmpty() && apellidoMaterno.isEmpty()) {
                                throw new Exception("No se puede crear Alumno Sin Ningun Apellido");
                            }
                            if (frmCrearNuevoAlumno.jTextFieldNumeroDocumentoID.getText().isEmpty()) {
                                throw new Exception("No se puede crear Alumno sin Documento de Identificacion");
                            }
                            if (direccion.isEmpty()) {
                                throw new Exception("No se puede crear Alumno sin Direccion");
                            }
                            String tipoSangre = frmCrearNuevoAlumno.jComboBoxTipoSangre.getSelectedItem().toString();

                            String documuentoIdentificador = frmCrearNuevoAlumno.jTextFieldNumeroDocumentoID.getText();
                            int tipoDocumento = frmCrearNuevoAlumno.jComboBoxTipoDocumento.getSelectedIndex();
                            if (tipoDocumento == 0) {
                                throw new Exception("Elija algun tipo de Documento");
                            }
                            int usaLentes = 0;
                            if (frmCrearNuevoAlumno.jCheckBoxUsaLentes.isSelected()) {
                                usaLentes = 1;
                            }
                            String fechaNacimiento = FechaParseada(frmCrearNuevoAlumno.dateChooserComboFechaNacimiento.getSelectedDate().getTime());
                            if(frmCrearNuevoAlumno.jComboBoxMallaAInscribirse.getSelectedIndex()==0)
                            {
                                throw new Exception("No se puede crear Alumno sin una Malla");
                            }
                            if (frmCrearNuevoAlumno.mallaElegida.getMinutos() == 1) {
                                if (!frmCrearNuevoAlumno.jCheckBoxLicenciaPiloto.isSelected()) {
                                    throw new Exception("No cumple el requerimiento de la malla " + frmCrearNuevoAlumno.mallaElegida.getNombre());
                                }
                            }
                            String numFolio = frmCrearNuevoAlumno.jTextFieldNumFolio.getText();
                            if (frmCrearNuevoAlumno.jCheckBoxNumFolio.isSelected() && numFolio.isEmpty()) {
                                throw new Exception("Error dato invalido en numero de folio");
                            }
                            int numFolio2 = 0;
                            if (frmCrearNuevoAlumno.jCheckBoxNumFolio.isSelected()) {
                                numFolio2 = Integer.parseInt(numFolio);
                            }
                            String numLicenciaPiloto = frmCrearNuevoAlumno.jTextFieldNumeroLicenciaPiloto.getText();
                            if (frmCrearNuevoAlumno.jCheckBoxLicenciaPiloto.isSelected() && numLicenciaPiloto.isEmpty()) {
                                throw new Exception("Error dato invalido en numero de licencia piloto");
                            }
                            int numLicenciaPiloto2 = 0;
                            if (frmCrearNuevoAlumno.jCheckBoxLicenciaPiloto.isSelected()) {
                                numLicenciaPiloto2 = Integer.parseInt(numLicenciaPiloto);
                            }
                            String numCredencialSABSA = frmCrearNuevoAlumno.jTextFieldNumeroCredencialSABSA.getText();
                            if (frmCrearNuevoAlumno.jCheckBoxCredencialSABSA.isSelected() && numCredencialSABSA.isEmpty()) {
                                throw new Exception("Error dato invalido en numero de credencial SABSA");
                            }
                            int numCredencialSABSA2 = 0;
                            String fechaVencimientoCredencialSABSA = "";
                            if (frmCrearNuevoAlumno.jCheckBoxCredencialSABSA.isSelected()) {
                                fechaVencimientoCredencialSABSA = FechaParseada(frmCrearNuevoAlumno.dateChooserComboVencimientoSABSA.getSelectedDate().getTime());
                                numCredencialSABSA2 = Integer.parseInt(numCredencialSABSA);
                            }

                            String numCredencialCoorporativo = frmCrearNuevoAlumno.jTextFieldNumeroCredencialCoorporativa.getText();
                            if (frmCrearNuevoAlumno.jCheckBoxCredemcialCoorporativa.isSelected() && numCredencialCoorporativo.isEmpty()) {
                                throw new Exception("Error dato invalido en numero de credencial Coorporativo");
                            }
                            int numCredencialCoorporativo2 = 0;
                            String fechaVencimientoCoorporativo = "";
                            if (frmCrearNuevoAlumno.jCheckBoxCredemcialCoorporativa.isSelected()) {
                                fechaVencimientoCoorporativo = FechaParseada(frmCrearNuevoAlumno.dateChooserComboVencimientoCoorporativa.getSelectedDate().getTime());
                                numCredencialCoorporativo2 = Integer.parseInt(numCredencialCoorporativo);
                            }
                            String fechaVencimientoAptoMedico = "";
                            if (frmCrearNuevoAlumno.jCheckBoxCertificadoMedico.isSelected()) {
                                fechaVencimientoAptoMedico = FechaParseada(frmCrearNuevoAlumno.dateChooserComboVencimientoAptoMedico.getSelectedDate().getTime());
                            }

                            if (frmCrearNuevoAlumno.referencias.isEmpty()) {
                                throw new Exception("Error se requieren Referencias");
                            }
                            
                            
                            if (frmCrearNuevoAlumno.jTextFieldURLFoto.getText().equals("") || frmCrearNuevoAlumno.jTextFieldURLFoto.getText().equals(null) ) {
                                throw new Exception("Error se requiere una foto");
                            }
                            
                            ImageIcon icon = new ImageIcon();
                            String FotoPath = null;
                            
                            icon = new ImageIcon(frmCrearNuevoAlumno.tipoImageIcon.getImage().getScaledInstance(frmCrearNuevoAlumno.jLabelFotoEstudiante6.getWidth(), frmCrearNuevoAlumno.jLabelFotoEstudiante6.getHeight(), Image.SCALE_DEFAULT));
                            FotoPath  = frmCrearNuevoAlumno.jTextFieldURLFoto.getText();                              
                            
                            int telefono = -1;
                            try {
                                telefono = Integer.parseInt(frmCrearNuevoAlumno.jTextFieldTelefono.getText());
                            } catch (Exception e) {
                                telefono = -1;
                            }
                            int mallaElegida = frmCrearNuevoAlumno.mallaElegida.getID();
                            Persona aux = new Persona(-1, mallaElegida, nombreAlumno, apellidoPaterno,
                                    apellidoMaterno, tipoDocumento, documuentoIdentificador, numCredencialCoorporativo2, numCredencialSABSA2,
                                    numFolio2, numLicenciaPiloto2, usaLentes, direccion, tipoSangre, fechaNacimiento,
                                    telefono,icon, 0, fechaVencimientoCredencialSABSA, fechaVencimientoCoorporativo,
                                    fechaVencimientoAptoMedico,"");
                            //todo modificado prestar mucha atencion
                            LinkedList<Referencia> referencias = frmCrearNuevoAlumno.referencias;
                            modelo.insertarAlumno_docente_piloto(aux, referencias, DeltaCharlie.IDUser,FotoPath);                               
                            modelo.vertablaAlumno(frmPrincipalAlumno.jTableAlumnos, this);
                            frmPrincipalAlumno.jTableAlumnos.repaint();
                            hilo1.refresh();
                            int respuesta = JOptionPane.showConfirmDialog(this.view, "El Alumno ha sido creado, ¿Desea crear otro Alumno?", "",
                                    JOptionPane.OK_CANCEL_OPTION, JOptionPane.INFORMATION_MESSAGE);
                            if (respuesta == JOptionPane.OK_OPTION) {
                                //arreglar bien que vacie todo para la nueva insersion
                                frmCrearNuevoAlumno.estadoInicial();
                            } else {
                                cerrar(frmCrearNuevoAlumno);
                            }
                        } catch (Exception e) {
                            JOptionPane.showMessageDialog(this.view, e.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
                        }
                        break;
                    case MODIFICAR:
                        try {
                            if (frmCrearNuevoAlumno.elegidaPersona == null) {
                                throw new Exception("No se puede Modificar sin elegir");
                            }
                            String nombreAlumno = frmCrearNuevoAlumno.jTextFieldNombre.getText();
                            String apellidoPaterno = frmCrearNuevoAlumno.jTextFieldApellidoPaterno.getText();
                            String apellidoMaterno = frmCrearNuevoAlumno.jTextFieldApellidoMaterno.getText();
                            String direccion = frmCrearNuevoAlumno.jTextFieldDireccionActual.getText();
                            if (nombreAlumno.isEmpty()) {
                                throw new Exception("No se puede Crear Alumno Sin Nombre");
                            }
                            if (apellidoPaterno.isEmpty() && apellidoMaterno.isEmpty()) {
                                throw new Exception("No se puede crear Alumno Sin Ningun Apellido");
                            }
                            if (frmCrearNuevoAlumno.jTextFieldNumeroDocumentoID.getText().isEmpty()) {
                                throw new Exception("No se puede crear Alumno sin Documento de Identificacion");
                            }
                            if (direccion.isEmpty()) {
                                throw new Exception("No se puede crear Alumno sin Direccion");
                            }
                            String tipoSangre = frmCrearNuevoAlumno.jComboBoxTipoSangre.getSelectedItem().toString();

                            String documuentoIdentificador = frmCrearNuevoAlumno.jTextFieldNumeroDocumentoID.getText();
                            int tipoDocumento = frmCrearNuevoAlumno.jComboBoxTipoDocumento.getSelectedIndex();
                            int usaLentes = 0;
                            if (frmCrearNuevoAlumno.jCheckBoxUsaLentes.isSelected()) {
                                usaLentes = 1;
                            }
                            String fechaNacimiento = FechaParseada(frmCrearNuevoAlumno.dateChooserComboFechaNacimiento.getSelectedDate().getTime());
                            if (frmCrearNuevoAlumno.mallaElegida.getMinutos() == 1) {
                                if (!frmCrearNuevoAlumno.jCheckBoxLicenciaPiloto.isSelected()) {
                                    throw new Exception("No cumple el requerimiento de la malla " + frmCrearNuevoAlumno.mallaElegida.getNombre());
                                }
                            }
                            String numFolio = frmCrearNuevoAlumno.jTextFieldNumFolio.getText();
                            if (frmCrearNuevoAlumno.jCheckBoxNumFolio.isSelected() && numFolio.isEmpty()) {
                                throw new Exception("Error dato invalido en numero de folio");
                            }
                            int numFolio2 = 0;
                            if (frmCrearNuevoAlumno.jCheckBoxNumFolio.isSelected()) {
                                numFolio2 = Integer.parseInt(numFolio);
                            }
                            String numLicenciaPiloto = frmCrearNuevoAlumno.jTextFieldNumeroLicenciaPiloto.getText();
                            if (frmCrearNuevoAlumno.jCheckBoxLicenciaPiloto.isSelected() && numLicenciaPiloto.isEmpty()) {
                                throw new Exception("Error dato invalido en numero de licencia piloto");
                            }
                            int numLicenciaPiloto2 = 0;
                            if (frmCrearNuevoAlumno.jCheckBoxLicenciaPiloto.isSelected()) {
                                numLicenciaPiloto2 = Integer.parseInt(numLicenciaPiloto);
                            }
                            String numCredencialSABSA = frmCrearNuevoAlumno.jTextFieldNumeroCredencialSABSA.getText();
                            if (frmCrearNuevoAlumno.jCheckBoxCredencialSABSA.isSelected() && numCredencialSABSA.isEmpty()) {
                                throw new Exception("Error dato invalido en numero de credencial SABSA");
                            }
                            int numCredencialSABSA2 = 0;
                            String fechaVencimientoCredencialSABSA = "";
                            if (frmCrearNuevoAlumno.jCheckBoxCredencialSABSA.isSelected()) {
                                fechaVencimientoCredencialSABSA = FechaParseada(frmCrearNuevoAlumno.dateChooserComboVencimientoSABSA.getSelectedDate().getTime());
                                numCredencialSABSA2 = Integer.parseInt(numCredencialSABSA);
                            }

                            String numCredencialCoorporativo = frmCrearNuevoAlumno.jTextFieldNumeroCredencialCoorporativa.getText();
                            if (frmCrearNuevoAlumno.jCheckBoxCredemcialCoorporativa.isSelected() && numCredencialCoorporativo.isEmpty()) {
                                throw new Exception("Error dato invalido en numero de credencial Coorporativo");
                            }
                            int numCredencialCoorporativo2 = 0;
                            String fechaVencimientoCoorporativo = "";
                            if (frmCrearNuevoAlumno.jCheckBoxCredemcialCoorporativa.isSelected()) {
                                fechaVencimientoCoorporativo = FechaParseada(frmCrearNuevoAlumno.dateChooserComboVencimientoCoorporativa.getSelectedDate().getTime());
                                numCredencialCoorporativo2 = Integer.parseInt(numCredencialCoorporativo);
                            }
                            String fechaVencimientoAptoMedico = "";
                            if (frmCrearNuevoAlumno.jCheckBoxCertificadoMedico.isSelected()) {
                                fechaVencimientoAptoMedico = FechaParseada(frmCrearNuevoAlumno.dateChooserComboVencimientoAptoMedico.getSelectedDate().getTime());
                            }

                            ImageIcon icon = null;
                            String FotoPath = null;
                            System.out.println(frmCrearNuevoAlumno.jTextFieldURLFoto.getText());
                            if(!frmCrearNuevoAlumno.jTextFieldURLFoto.getText().equals("") || frmCrearNuevoAlumno.jTextFieldURLFoto.getText().equals(null))
                            {
                                icon = new ImageIcon(frmCrearNuevoAlumno.tipoImageIcon.getImage().getScaledInstance(frmCrearNuevoAlumno.jLabelFotoEstudiante6.getWidth(), frmCrearNuevoAlumno.jLabelFotoEstudiante6.getHeight(), Image.SCALE_DEFAULT));
                                FotoPath  = fichero.getAbsolutePath();
                            }
                            
                            if (frmCrearNuevoAlumno.referencias.isEmpty()) {
                                throw new Exception("Error se requieren Referencias");
                            }
                            int telefono;
                            try {
                                telefono = Integer.parseInt(frmCrearNuevoAlumno.jTextFieldTelefono.getText());
                            } catch (Exception e) {
                                telefono = -1;
                            }
                            int mallaElegida = frmCrearNuevoAlumno.mallaElegida.getID();

                            Persona aux = new Persona(frmCrearNuevoAlumno.elegidaPersona.getiD(), mallaElegida, nombreAlumno, apellidoPaterno,
                                    apellidoMaterno, tipoDocumento, documuentoIdentificador, numCredencialCoorporativo2, numCredencialSABSA2,
                                    numFolio2, numLicenciaPiloto2, usaLentes, direccion, tipoSangre, fechaNacimiento,
                                    telefono, icon, 0, fechaVencimientoCredencialSABSA, fechaVencimientoCoorporativo,
                                    fechaVencimientoAptoMedico,"");
                            //todo modificado prestar mucha atencion
                            LinkedList<Referencia> referencias = frmCrearNuevoAlumno.referencias;
                            if(FotoPath!=null)
                            {
                                modelo.modificarAlumno_docente_piloto(aux, referencias, DeltaCharlie.IDUser,FotoPath);
                            }
                            else
                            {
                                modelo.modificarAlumno_docente_piloto(aux, referencias, DeltaCharlie.IDUser);
                            }
                            modelo.vertablaAlumno(frmPrincipalAlumno.jTableAlumnos, this);
                            frmPrincipalAlumno.jTableAlumnos.repaint();
                            hilo1.refresh();
                            JOptionPane.showMessageDialog(this.view, "El alumno se ha modificado con exito");     
                            cerrar(frmCrearNuevoAlumno);
                        } catch (Exception e) {
                            JOptionPane.showMessageDialog(this.view, e.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
                        }
                        break;
                    case BORRAR:
                        try {
                            if (frmCrearNuevoAlumno.elegidaPersona != null) {
                                modelo.eliminarPersona(frmCrearNuevoAlumno.elegidaPersona.getiD(), DeltaCharlie.IDUser);
                                int respuesta = JOptionPane.showConfirmDialog(this.view, "El Alumno ha sido borrado, ¿Desea borrar otro Alumno?", "",JOptionPane.OK_CANCEL_OPTION, JOptionPane.INFORMATION_MESSAGE);
                                if (respuesta == JOptionPane.OK_OPTION) {
                                    modelo.vertablaAlumno(frmPrincipalAlumno.jTableAlumnos, this);
                                    frmPrincipalAlumno.jTableAlumnos.repaint();
                                    hilo1.refresh();
                                } else {
                                    cerrar(frmCrearNuevoAlumno);
                                }
                            }
                            else{
                                JOptionPane.showMessageDialog(this.view, "Elija a un alumno a Borrar", "Error", JOptionPane.ERROR_MESSAGE);
                            }
                        } catch (SQLException e) {
                            JOptionPane.showMessageDialog(this.view, e.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
                        }
                        break;

                }
            }
        } else if (comando.getSource().equals(frmCrearNuevoAlumno.botonCancelar)) {
            cerrar(frmCrearNuevoAlumno);
        }
    }

    public void crearDocentePiloto(ActionEvent comando) {
        //Lo mismo que alumno pero con su internal frame por Fas modificar
        if (comando.getSource().equals(frmDocentePiloto.jButtonAgregarTelefonoReferencia)) {
            try {
                if (frmDocentePiloto.jTextFieldTelefonoReferencia.getText().isEmpty()) {
                    throw new Exception("No se puede Cargar sin un numero");
                }
                if (frmDocentePiloto.jTextFieldTipoDeTelefonoReferencia.getText().isEmpty()) {
                    throw new Exception("No se puede cargar el numero sin ubicacion");
                }


                Boolean ctrl = false;
                int rowCount = frmDocentePiloto.jTableTelefonos.getRowCount();
                for (int i = 0; i < rowCount; i++) {
                    String rowEntry = frmDocentePiloto.jTableTelefonos.getValueAt(0, i).toString();
                    if (rowEntry.equals(frmDocentePiloto.jTextFieldTelefonoReferencia.getText())) {
                        ctrl = true;
                    }
                }
                if(ctrl==true)
                {
                    throw new Exception("La referencia ya tiene ese numero");
                }
                else{
                    int telefono = Integer.parseInt(frmDocentePiloto.jTextFieldTelefonoReferencia.getText());
                    String ubicacion = frmDocentePiloto.jTextFieldTipoDeTelefonoReferencia.getText();
                    TelefonoReferencia aux = new TelefonoReferencia(telefono, ubicacion);
                    frmDocentePiloto.telefonoReferencia.addLast(aux);
                    frmDocentePiloto.crearTablaTelefonos(frmDocentePiloto.telefonoReferencia);
                    frmDocentePiloto.llenarTablaTelefonos(frmDocentePiloto.telefonoReferencia);
                }

                frmDocentePiloto.jTextFieldTelefonoReferencia.setText("");
                frmDocentePiloto.jTextFieldTipoDeTelefonoReferencia.setText("");
            } catch (Exception e) {
                JOptionPane.showMessageDialog(this.view, e.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
            }
        }
        if (comando.getSource().equals(frmDocentePiloto.jButtonEliminarTelefonoReferencia)) {
            int aEliminar = frmDocentePiloto.jTableTelefonos.getSelectedRow();
            if (aEliminar >= 0) {
                frmDocentePiloto.telefonoReferencia.remove(aEliminar);
                frmDocentePiloto.crearTablaTelefonos(frmDocentePiloto.telefonoReferencia);
                frmDocentePiloto.llenarTablaTelefonos(frmDocentePiloto.telefonoReferencia);
            } else {
                JOptionPane.showMessageDialog(this.view, "No hay telefono seleccionado", "Error Eliminar", JOptionPane.WARNING_MESSAGE);
            }
        }
        if (comando.getSource().equals(frmDocentePiloto.jButtonAgregarReferencia)) {
            try {
                if (frmDocentePiloto.jTextFieldNombreCompletoReferencia.getText().isEmpty()) {
                    throw new Exception("No se puede crear Referencia sin Nombre");
                }
                if (frmDocentePiloto.telefonoReferencia.isEmpty()) {
                    throw new Exception("No se puede crear Referencia sin Telefonos");
                }
                if (frmDocentePiloto.jTextFieldParentesco.getText().isEmpty()) {
                    throw new Exception("No se puede crear Referencia sin Parentesco con el Docente");
                }
                String nombreReferencia = frmDocentePiloto.jTextFieldNombreCompletoReferencia.getText();
                int idReferencia = frmDocentePiloto.idReferenciaElegida;

                Boolean ctrl = false;
                int rowCount = frmDocentePiloto.jTableReferencias.getRowCount();
                for (int i = 0; i < rowCount; i++) {
                    String rowEntry = frmDocentePiloto.jTableReferencias.getValueAt(0, i).toString();
                    if (rowEntry.equals(frmDocentePiloto.jTextFieldNombreCompletoReferencia.getText())) {
                        ctrl = true;
                    }
                }
                if(ctrl==true)
                {
                    throw new Exception("La referencia ya esta en la tabla");
                }
                else{
                    String parentesco = frmDocentePiloto.jTextFieldParentesco.getText();
                    Referencia aux = new Referencia(idReferencia, nombreReferencia, frmDocentePiloto.telefonoReferencia);
                    aux.setParentesco(parentesco);
                    frmDocentePiloto.jTextFieldNombreCompletoReferencia.setText("");
                    frmDocentePiloto.jTextFieldParentesco.setText("");
                    frmDocentePiloto.referencias.addLast(aux);
                    frmDocentePiloto.crearTablaReferencias();
                    frmDocentePiloto.llenarTablaReferencias();
                    for (Referencia referencia : frmDocentePiloto.referencias)
                    {
                        for(TelefonoReferencia TR : referencia.getTelefono() )
                        {
                            System.out.println(referencia.getNombre()+" = "+TR.getNumTelefono());                                  
                        }
                    }
                    frmDocentePiloto.jButtonAgregarTelefonoReferencia.setEnabled(true);
                    frmDocentePiloto.jButtonEliminarTelefonoReferencia.setEnabled(true);
                    frmDocentePiloto.telefonoReferencia = new LinkedList<TelefonoReferencia>();
                    DefaultTableModel dm = (DefaultTableModel)frmDocentePiloto.jTableTelefonos.getModel();
                    dm.getDataVector().removeAllElements();
                    dm.fireTableDataChanged();
                }
            } catch (Exception ex) {
                JOptionPane.showMessageDialog(this.view, ex.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
            }
        }
        if (comando.getSource().equals(frmDocentePiloto.jButtonEliminarReferencia)) {
            int aEliminar = frmDocentePiloto.jTableReferencias.getSelectedRow();
            if (aEliminar >= 0) {
                frmDocentePiloto.referencias.remove(aEliminar);
                frmDocentePiloto.crearTablaReferencias();
                frmDocentePiloto.llenarTablaReferencias();
            } else {
                JOptionPane.showMessageDialog(this.view, "No hay telefono seleccionado", "Error Eliminar", JOptionPane.WARNING_MESSAGE);
            }
        } else if (comando.getSource().equals(frmDocentePiloto.JbotonAceptar)) {
            if (null != frmDocentePiloto.cmbActual) {
                switch (frmDocentePiloto.cmbActual) {
                    case CREAR:
                        try {
                            String nombreDocentePiloto = frmDocentePiloto.jTextFieldNombre.getText();
                            String apellidoPaterno = frmDocentePiloto.jTextFieldApellidoPaterno.getText();
                            String apellidoMaterno = frmDocentePiloto.jTextFieldApellidoMaterno.getText();
                            String direccion = frmDocentePiloto.jTextFieldDireccionActual.getText();
                            if (nombreDocentePiloto.isEmpty()) {
                                throw new Exception("No se puede Crear Alumno Sin Nombre");
                            }
                            if (apellidoPaterno.isEmpty() && apellidoMaterno.isEmpty()) {
                                throw new Exception("No se puede crear Alumno Sin Ningun Apellido");
                            }
                            if (frmDocentePiloto.jTextFieldDocumentoIdentificador.getText().isEmpty()) {
                                throw new Exception("No se puede crear Alumno sin Documento de Identificacion");
                            }
                            if (direccion.isEmpty()) {
                                throw new Exception("No se puede crear Alumno sin Direccion");
                            }
                            String tipoSangre = frmDocentePiloto.jComboBoxTipoSangre.getSelectedItem().toString();
                            String telefono = frmDocentePiloto.jTextFieldTelefono.getText();
                            int telefono2 = 0;
                            if (!telefono.isEmpty()) {
                                telefono2 = Integer.parseInt(telefono);
                            }

                            String documuentoIdentificador = frmDocentePiloto.jTextFieldDocumentoIdentificador.getText();
                            int tipoDocumento = frmDocentePiloto.jComboBoxTipodeDocumentoIdentificador.getSelectedIndex() + 1;
                            int usaLentes = 0;
                            if (frmDocentePiloto.jCheckBoxUsaLentes.isSelected()) {
                                usaLentes = 1;
                            }
                            String fechaNacimiento = FechaParseada(frmDocentePiloto.dateChooserComboFechaNacimiento.getSelectedDate().getTime());

                            String numLicenciaPiloto = frmDocentePiloto.jTextFieldNroLicienciaPiloto.getText();
                            if (frmDocentePiloto.jCheckBoxEsPiloto.isSelected() && numLicenciaPiloto.isEmpty()) {
                                throw new Exception("Error dato vacio o invalido en numero de licencia piloto");
                            }
                            int numLicenciaPiloto2 = 0;
                            if (frmDocentePiloto.jCheckBoxEsPiloto.isSelected()) {
                                numLicenciaPiloto2 = Integer.parseInt(numLicenciaPiloto);
                            }
                            String numCredencialSABSA = frmDocentePiloto.jTextFieldNroSABSA.getText();
                            if (frmDocentePiloto.jCheckBoxEsPiloto.isSelected() && numCredencialSABSA.isEmpty()) {
                                throw new Exception("Error dato vacio o invalido en numero de credencial SABSA");
                            }
                            int numCredencialSABSA2 = 0;
                            String fechaVencimientoCredencialSABSA = "";
                            if (frmDocentePiloto.jCheckBoxEsPiloto.isSelected()) {
                                fechaVencimientoCredencialSABSA = FechaParseada(frmDocentePiloto.dateChooserComboVencimientoSABSA.getSelectedDate().getTime());
                                numCredencialSABSA2 = Integer.parseInt(numCredencialSABSA);
                            }

                            String numCredencialCoorporativo = frmDocentePiloto.jTextFieldNroCoorporativo.getText();
                            if (frmDocentePiloto.jCheckBoxEsPiloto.isSelected() && numCredencialCoorporativo.isEmpty()) {
                                throw new Exception("Error dato vacio o invalido en numero de credencial Coorporativo");
                            }
                            int numCredencialCoorporativo2 = 0;
                            String fechaVencimientoCoorporativo = "";
                            if (frmDocentePiloto.jCheckBoxEsPiloto.isSelected()) {
                                fechaVencimientoCoorporativo = FechaParseada(frmDocentePiloto.dateChooserComboVencimientoCoorporativo.getSelectedDate().getTime());
                                numCredencialCoorporativo2 = Integer.parseInt(numCredencialCoorporativo);
                            }
                            String fechaVencimientoAptoMedico = "";
                            if (frmDocentePiloto.jCheckBoxCertificadoMedico.isSelected() && frmDocentePiloto.jCheckBoxEsPiloto.isSelected()) {
                                fechaVencimientoAptoMedico = FechaParseada(frmDocentePiloto.dateChooserComboVencimientoCertificadoMedico.getSelectedDate().getTime());
                            }
                            String fechaVencimientoProficienceCheck = "";
                            if (frmDocentePiloto.jCheckBoxProficiencyCheck.isSelected() && frmDocentePiloto.jCheckBoxEsPiloto.isSelected()) {
                                fechaVencimientoProficienceCheck = FechaParseada(frmDocentePiloto.dateChooserComboVencimientoProficiencyCheck.getSelectedDate().getTime());
                            }
                            int isPiloto2 = 0;
                            if (frmDocentePiloto.jCheckBoxEsPiloto.isSelected()) {
                                isPiloto2 = 1;
                            }

                          

                            Persona aux = new Persona(-1, nombreDocentePiloto, apellidoPaterno, apellidoMaterno, tipoDocumento,
                                    documuentoIdentificador, numCredencialCoorporativo2, numCredencialSABSA2,
                                    numLicenciaPiloto2, isPiloto2, usaLentes, direccion, tipoSangre,
                                    fechaNacimiento, telefono2, fechaVencimientoCredencialSABSA,
                                    fechaVencimientoCoorporativo, fechaVencimientoAptoMedico, fechaVencimientoProficienceCheck);
                            modelo.insertarAlumno_docente_piloto(aux, frmDocentePiloto.referencias, DeltaCharlie.IDUser,null);
                            modelo.vertablaDocentePiloto(frmPrincipalDocentePiloto.jTableDocentesPilotos, this);
                            frmPrincipalDocentePiloto.jTableDocentesPilotos.repaint();
                            hilo1.refresh();
                            int respuesta = JOptionPane.showConfirmDialog(this.view, "¿Quiere crear otro Docente/Piloto?", "Alerta",
                                    JOptionPane.OK_CANCEL_OPTION, JOptionPane.INFORMATION_MESSAGE);
                            if (respuesta == JOptionPane.OK_OPTION) {
                                frmDocentePiloto.estadoInicial();
                            } else {
                                cerrar(frmDocentePiloto);
                            }
                        } catch (Exception e) {
                            JOptionPane.showMessageDialog(this.view, e.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
                        }
                        break;
                    case MODIFICAR:
                        try {
                            String nombreDocentePiloto = frmDocentePiloto.jTextFieldNombre.getText();
                            String apellidoPaterno = frmDocentePiloto.jTextFieldApellidoPaterno.getText();
                            String apellidoMaterno = frmDocentePiloto.jTextFieldApellidoMaterno.getText();
                            String direccion = frmDocentePiloto.jTextFieldDireccionActual.getText();
                            if (nombreDocentePiloto.isEmpty()) {
                                throw new Exception("No se puede Crear Alumno Sin Nombre");
                            }
                            if (apellidoPaterno.isEmpty() && apellidoMaterno.isEmpty()) {
                                throw new Exception("No se puede crear Alumno Sin Ningun Apellido");
                            }
                            if (frmDocentePiloto.jTextFieldDocumentoIdentificador.getText().isEmpty()) {
                                throw new Exception("No se puede crear Alumno sin Documento de Identificacion");
                            }
                            if (direccion.isEmpty()) {
                                throw new Exception("No se puede crear Alumno sin Direccion");
                            }
                            String tipoSangre = frmDocentePiloto.jComboBoxTipoSangre.getSelectedItem().toString();
                            String telefono = frmDocentePiloto.jTextFieldTelefono.getText();
                            int telefono2 = 0;
                            if (!telefono.isEmpty()) {
                                telefono2 = Integer.parseInt(telefono);
                            }

                            String documuentoIdentificador = frmDocentePiloto.jTextFieldDocumentoIdentificador.getText();
                            int tipoDocumento = frmDocentePiloto.jComboBoxTipodeDocumentoIdentificador.getSelectedIndex() + 1;
                            int usaLentes = 0;
                            if (frmDocentePiloto.jCheckBoxUsaLentes.isSelected()) {
                                usaLentes = 1;
                            }
                            String fechaNacimiento = FechaParseada(frmDocentePiloto.dateChooserComboFechaNacimiento.getSelectedDate().getTime());

                            String numLicenciaPiloto = frmDocentePiloto.jTextFieldNroLicienciaPiloto.getText();
                            if (frmDocentePiloto.jCheckBoxEsPiloto.isSelected() && numLicenciaPiloto.isEmpty()) {
                                throw new Exception("Error dato vacio o invalido en numero de licencia piloto");
                            }
                            int numLicenciaPiloto2 = 0;
                            if (frmDocentePiloto.jCheckBoxEsPiloto.isSelected()) {
                                numLicenciaPiloto2 = Integer.parseInt(numLicenciaPiloto);
                            }
                            String numCredencialSABSA = frmDocentePiloto.jTextFieldNroSABSA.getText();
                            if (frmDocentePiloto.jCheckBoxEsPiloto.isSelected() && numCredencialSABSA.isEmpty()) {
                                throw new Exception("Error dato vacio o invalido en numero de credencial SABSA");
                            }
                            int numCredencialSABSA2 = 0;
                            String fechaVencimientoCredencialSABSA = "";
                            if (frmDocentePiloto.jCheckBoxEsPiloto.isSelected()) {
                                fechaVencimientoCredencialSABSA = FechaParseada(frmDocentePiloto.dateChooserComboVencimientoSABSA.getSelectedDate().getTime());
                                numCredencialSABSA2 = Integer.parseInt(numCredencialSABSA);
                            }

                            String numCredencialCoorporativo = frmDocentePiloto.jTextFieldNroCoorporativo.getText();
                            if (frmDocentePiloto.jCheckBoxEsPiloto.isSelected() && numCredencialCoorporativo.isEmpty()) {
                                throw new Exception("Error dato vacio o invalido en numero de credencial Coorporativo");
                            }
                            int numCredencialCoorporativo2 = 0;
                            String fechaVencimientoCoorporativo = "";
                            if (frmDocentePiloto.jCheckBoxEsPiloto.isSelected()) {
                                fechaVencimientoCoorporativo = FechaParseada(frmDocentePiloto.dateChooserComboVencimientoCoorporativo.getSelectedDate().getTime());
                                numCredencialCoorporativo2 = Integer.parseInt(numCredencialCoorporativo);
                            }
                            String fechaVencimientoAptoMedico = "";
                            if (frmDocentePiloto.jCheckBoxCertificadoMedico.isSelected() && frmDocentePiloto.jCheckBoxEsPiloto.isSelected()) {
                                fechaVencimientoAptoMedico = FechaParseada(frmDocentePiloto.dateChooserComboVencimientoCertificadoMedico.getSelectedDate().getTime());
                            }
                            String fechaVencimientoProficienceCheck = "";
                            if (frmDocentePiloto.jCheckBoxProficiencyCheck.isSelected() && frmDocentePiloto.jCheckBoxEsPiloto.isSelected()) {
                                fechaVencimientoProficienceCheck = FechaParseada(frmDocentePiloto.dateChooserComboVencimientoProficiencyCheck.getSelectedDate().getTime());
                            }
                            int isPiloto2 = 0;
                            if (frmDocentePiloto.jCheckBoxEsPiloto.isSelected()) {
                                isPiloto2 = 1;
                            }


                            Persona aux = new Persona(frmDocentePiloto.elegidaPersona.getiD(), nombreDocentePiloto, apellidoPaterno, apellidoMaterno, tipoDocumento,
                                    documuentoIdentificador, numCredencialCoorporativo2, numCredencialSABSA2,
                                    numLicenciaPiloto2, isPiloto2, usaLentes, direccion, tipoSangre,
                                    fechaNacimiento, telefono2, fechaVencimientoCredencialSABSA,
                                    fechaVencimientoCoorporativo, fechaVencimientoAptoMedico, fechaVencimientoProficienceCheck);
                            System.out.println(frmDocentePiloto.referencias);
                            modelo.modificarAlumno_docente_piloto(aux, frmDocentePiloto.referencias, DeltaCharlie.IDUser);
                            JOptionPane.showMessageDialog(this.view, "El docente/piloto se ha modificado con exito");
                            modelo.vertablaDocentePiloto(frmPrincipalDocentePiloto.jTableDocentesPilotos, this);
                            frmPrincipalDocentePiloto.jTableDocentesPilotos.repaint();
                            hilo1.refresh();
                            cerrar(frmDocentePiloto);
                        } catch (Exception e) {
                            JOptionPane.showMessageDialog(this.view, e.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
                        }
                        break;
                    case BORRAR:
                        try {
                            if (frmDocentePiloto.elegidaPersona != null) {
                                int respuesta = JOptionPane.showConfirmDialog(this.view, "El Docente/Piloto ha sido borrado, ¿Desea borrar otro Docente/Piloto?", "",JOptionPane.OK_CANCEL_OPTION, JOptionPane.INFORMATION_MESSAGE);
                                if (respuesta == JOptionPane.OK_OPTION) {
                                    modelo.eliminarPersona(frmDocentePiloto.elegidaPersona.getiD(), DeltaCharlie.IDUser);
                                    modelo.vertablaDocentePiloto(frmPrincipalDocentePiloto.jTableDocentesPilotos, this);
                                    frmPrincipalDocentePiloto.jTableDocentesPilotos.repaint();
                                    hilo1.refresh();
                                } else {
                                    cerrar(frmDocentePiloto);
                                }
                            }
                            else{
                                JOptionPane.showMessageDialog(this.view, "Elija a un Docente/Piloto a Borrar", "Error", JOptionPane.ERROR_MESSAGE);
                            }
                        } catch (SQLException e) {
                            JOptionPane.showMessageDialog(this.view, e.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
                        }
                        break;

                }
            }
        }
        if (comando.getSource().equals(frmDocentePiloto.JbotonCancelar)) {
            cerrar(frmDocentePiloto);
        }
    }

    private void crearGrupo(ActionEvent comando) {
        if (comando.getSource().equals(frmCrearGrupo.jComboBoxMaterias)){
            try {
                modelo.ConseguirMateria(frmCrearGrupo.jComboBoxMaterias);
            } catch (Exception e) {
                JOptionPane.showMessageDialog(this.view, e.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
            }
        } else if(comando.getSource().equals(frmCrearGrupo.jComboBoxDocentes)){
            try {
                modelo.ConseguirDocentes(frmCrearGrupo.jComboBoxDocentes);
            } catch (Exception e) {
                JOptionPane.showMessageDialog(this.view, e.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
            }
        } else if (comando.getSource().equals(frmCrearGrupo.jButtonAceptar)) {
            if (frmCrearGrupo.cmbActual != null) {
                switch (frmCrearGrupo.cmbActual) {
                    case CREAR:
                        try {
                            if (frmCrearGrupo.jTextFieldNombreMateria.getText().isEmpty()) {
                                throw new Exception("No se puede crear grupo sin Materia");
                            }
                            if (frmCrearGrupo.jTextFieldNombreDocente.getText().isEmpty()) {
                                throw new Exception("No se puede crear grupo sin Docente");
                            }
                            if (frmCrearGrupo.jTextFieldNombreGrupo.getText().isEmpty()) {
                                throw new Exception("No se puede crear grupo sin Nombre de Grupo");
                            }
                            if (frmCrearGrupo.jTextFieldNotaPase.getText().isEmpty()) {
                                throw new Exception("No se puede crear grupo sin Nota Pase");
                            }
                            if (frmCrearGrupo.jTextFieldNotaAprobacion.getText().isEmpty()) {
                                throw new Exception("No se puede crear grupo sin Nota de Aprovacion");
                            }
                            int NotaPase=Integer.parseInt(frmCrearGrupo.jTextFieldNotaPase.getText());
                            int NotaAprobacion=Integer.parseInt(frmCrearGrupo.jTextFieldNotaAprobacion.getText());
                            String nombreGrupo = frmCrearGrupo.jTextFieldNombreGrupo.getText();
                            String fechaIni = FechaParseada(frmCrearGrupo.dateChooserComboFechaInicio.getSelectedDate().getTime());
                            String fechaFin = FechaParseada(frmCrearGrupo.dateChooserComboFechaFin.getSelectedDate().getTime());
                            String horario = frmCrearGrupo.jSpinnerHora.getValue().toString() + ":" + frmCrearGrupo.jSpinnerMinuto.getValue().toString();
                            int auxdocente = modelo.ConseguirIDDocente(frmCrearGrupo.jComboBoxDocentes.getSelectedItem().toString());
                            int auxmateria = modelo.ConseguirIDMateria(frmCrearGrupo.jComboBoxMaterias.getSelectedItem().toString());
                            modelo.insertarGrupo(nombreGrupo, auxdocente, auxmateria, fechaIni, fechaFin, horario,NotaPase,NotaAprobacion);
                            frmCrearGrupo.jTextFieldNombreGrupo.setText("");
                            frmCrearGrupo.jTextFieldNombreDocente.setText("");
                            frmCrearGrupo.jTextFieldNombreMateria.setText("");
                            frmCrearGrupo.jTextFieldBuscarMateria.setText("");
                            frmCrearGrupo.jTextFieldBuscarDocente.setText("");
                            frmCrearGrupo.jTextFieldNotaAprobacion.setText("");
                            frmCrearGrupo.jTextFieldNotaPase.setText("");
                            frmCrearGrupo.jSpinnerHora.setValue(0);
                            frmCrearGrupo.jSpinnerMinuto.setValue(0);
                        } catch (Exception ex) {
                            JOptionPane.showMessageDialog(this.view, ex.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
                        }
                        break;
                    case MODIFICAR:
                        try {
                            if (frmCrearGrupo.jTextFieldNombreMateria.getText().isEmpty()) {
                                throw new Exception("No se puede modificar grupo sin Materia");
                            }
                            if (frmCrearGrupo.jTextFieldNombreDocente.getText().isEmpty()) {
                                throw new Exception("No se puede modificar grupo sin Docente");
                            }
                            if (frmCrearGrupo.jTextFieldNombreGrupo.getText().isEmpty()) {
                                throw new Exception("No se puede modificar grupo sin Nombre de Grupo");
                            }
                            int NotaPase=Integer.parseInt(frmCrearGrupo.jTextFieldNotaPase.getText());
                            int NotaAprobacion=Integer.parseInt(frmCrearGrupo.jTextFieldNotaAprobacion.getText());
                            String nombreGrupo = frmCrearGrupo.jTextFieldNombreGrupo.getText();
                            String fechaIni = FechaParseada(frmCrearGrupo.dateChooserComboFechaInicio.getSelectedDate().getTime());
                            String fechaFin = FechaParseada(frmCrearGrupo.dateChooserComboFechaFin.getSelectedDate().getTime());
                            String horario = frmCrearGrupo.jSpinnerHora.getValue().toString() + ":" + frmCrearGrupo.jSpinnerMinuto.getValue().toString();
                            int auxdocente = modelo.ConseguirIDDocente(frmCrearGrupo.jTextFieldNombreDocente.getText());
                            int auxmateria = modelo.ConseguirIDMateria(frmCrearGrupo.jTextFieldNombreMateria.getText());
                            Grupo v = new Grupo(0, frmCrearGrupo.jTextFieldNombreMateria.getText(), nombreGrupo, frmCrearGrupo.jTextFieldNombreDocente.getText(), fechaIni, fechaFin, horario);
                            modelo.ModificarGrupo(v,NotaPase,NotaAprobacion,frmBuscarGrupo.GrupoBuscado.getIDGrupo());
                            frmCrearGrupo.jTextFieldNombreGrupo.setText("");
                            frmCrearGrupo.jTextFieldNombreDocente.setText("");
                            frmCrearGrupo.jTextFieldNombreMateria.setText("");
                            frmCrearGrupo.jTextFieldBuscarMateria.setText("");
                            frmCrearGrupo.jTextFieldBuscarDocente.setText("");
                            frmCrearGrupo.jSpinnerHora.setValue(0);
                            frmCrearGrupo.jSpinnerMinuto.setValue(0);
                        } catch (Exception ex) {
                            JOptionPane.showMessageDialog(this.view, ex.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
                        }
                        break;
                    case BORRAR:
                {
                    /*try {
                        int idgrupo = frmBuscarGrupo.GrupoBuscado.getIDGrupo();
                        modelo.eliminarGrupo(idgrupo);
                        frmCrearGrupo.jTextFieldNombreGrupo.setText("");
                        frmCrearGrupo.jTextFieldNombreDocente.setText("");
                        frmCrearGrupo.jTextFieldNombreMateria.setText("");
                        frmCrearGrupo.jTextFieldBuscarMateria.setText("");
                        frmCrearGrupo.jTextFieldBuscarDocente.setText("");
                        frmCrearGrupo.jSpinnerHora.setValue(0);
                        frmCrearGrupo.jSpinnerMinuto.setValue(0);
                    } catch (SQLException ex) {
                        Logger.getLogger(controlador.class.getName()).log(Level.SEVERE, null, ex);
                    }*/
                }
                        break;
                }
            }
        } else if (comando.getSource().equals(frmCrearGrupo.jButtonCancelar)) {
            cerrar(frmCrearGrupo);
        }
    }

    public void crearHelice(ActionEvent comando) {

        if (comando.getSource().equals(frmHelice.jButtonAceptar)) {
            if (frmHelice.cmb != null) {
                switch (frmHelice.cmb) {
                    case CREAR:
                        try {
                            if (frmHelice.jTextFieldMarca.getText().isEmpty()) {
                                throw new Exception("No se puede Crear Sin Marca");
                            }
                            if (frmHelice.jTextFieldModelo.getText().isEmpty()) {
                                throw new Exception("No se puede Crear Sin Modelo");
                            }
                            if (frmHelice.jTextFieldSerie.getText().isEmpty()) {
                                throw new Exception("No se puede Crear Sin Serie");
                            }
                            if (frmHelice.jTextFieldTT.getText().isEmpty()) {
                                throw new Exception("No se puede Crear Sin Total Time");
                            }
                            String marca = frmHelice.jTextFieldMarca.getText();
                            String modeloHelice = frmHelice.jTextFieldModelo.getText();
                            String serie = frmHelice.jTextFieldSerie.getText();

                            String[] settt = frmHelice.jTextFieldTT.getText().split(":");
                            
                            System.out.println(settt.length);

                            String[] settbo;
                            String[] settto;
                            int tto = -1;
                            int tbo = -1;
                            if (settt.length != 2) {
                                throw new Exception("Formato incorrecto");
                            }
                            int tt = Integer.parseInt(settt[0]) * 60 + Integer.parseInt(settt[1]);
                            if (!frmHelice.jTextFieldTBO.getText().isEmpty()) {

                                settbo = frmHelice.jTextFieldTBO.getText().split(":");
                                if (settbo.length != 2) {
                                    throw new Exception("Formato incorrecto");
                                }
                                tbo = Integer.parseInt(settbo[0]) * 60 + Integer.parseInt(settbo[1]);
                            }
                            if (!frmHelice.jTextFieldTTO.getText().isEmpty()) {
                                settto = frmHelice.jTextFieldTTO.getText().split(":");
                                if (settto.length != 2) {
                                    throw new Exception("Formato incorrecto");
                                }
                                tto = Integer.parseInt(settto[0]) * 60 + Integer.parseInt(settto[1]);
                            }

                            Helices aux;
                            aux = new Helices(-1, marca, modeloHelice, serie, tt, tto, tbo);
                            modelo.insertarHelice(aux, DeltaCharlie.IDUser);
                            modelo.vertablaHelice(frmPrincipalHelice.jTableHelices, this);
                            frmPrincipalHelice.jTableHelices.repaint();
                            JOptionPane.showMessageDialog(this.view, "La Helice se ha creado con exito");     
                            cerrar(frmHelice);
                        } catch (Exception e) {
                            JOptionPane.showMessageDialog(this.view, e.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
                        }
                        break;
                    case MODIFICAR:
                        if (frmHelice.heliceBuscada != null) {
                            try {
                                if (frmHelice.jTextFieldMarca.getText().isEmpty()) {
                                    throw new Exception("No se puede Crear Sin Marca");
                                }
                                if (frmHelice.jTextFieldModelo.getText().isEmpty()) {
                                    throw new Exception("No se puede Crear Sin Modelo");
                                }
                                if (frmHelice.jTextFieldSerie.getText().isEmpty()) {
                                    throw new Exception("No se puede Crear Sin Serie");
                                }
                                if (frmHelice.jTextFieldTT.getText().isEmpty()) {
                                    throw new Exception("No se puede Crear Sin Total Time");
                                }
                                String marca = frmHelice.jTextFieldMarca.getText();
                                String modeloHelice = frmHelice.jTextFieldModelo.getText();
                                String serie = frmHelice.jTextFieldSerie.getText();

                                String[] settt = frmHelice.jTextFieldTT.getText().split(":");

                                String[] settbo;
                                String[] settto;
                                int tto = -1;
                                int tbo = -1;
                                if (settt.length != 2) {
                                    throw new Exception("Formato incorrecto");
                                }
                                int tt = Integer.parseInt(settt[0]) * 60 + Integer.parseInt(settt[1]);
                                if (!frmHelice.jTextFieldTBO.getText().isEmpty()) {

                                    settbo = frmHelice.jTextFieldTBO.getText().split(":");
                                    if (settbo.length != 2) {
                                        throw new Exception("Formato incorrecto");
                                    }
                                    tbo = Integer.parseInt(settbo[0]) * 60 + Integer.parseInt(settbo[1]);
                                }
                                if (!frmHelice.jTextFieldTTO.getText().isEmpty()) {
                                    settto = frmHelice.jTextFieldTTO.getText().split(":");
                                    if (settto.length != 2) {
                                        throw new Exception("Formato incorrecto");
                                    }
                                    tto = Integer.parseInt(settto[0]) * 60 + Integer.parseInt(settto[1]);
                                }

                                Helices aux;
                                aux = new Helices(frmHelice.heliceBuscada.getIDHelice(), marca, modeloHelice, serie, tt, tto, tbo);
                                modelo.modificarHelice(aux, DeltaCharlie.IDUser);
                                modelo.vertablaHelice(frmPrincipalHelice.jTableHelices, this);
                                frmPrincipalHelice.jTableHelices.repaint();
                                JOptionPane.showMessageDialog(this.view, "La Helice se ha modificado con exito");     
                                cerrar(frmHelice);
                            } catch (Exception e) {
                                JOptionPane.showMessageDialog(this.view, e.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
                            }
                        } else {
                            JOptionPane.showMessageDialog(this.view, "Elija alguna helice", "Error", JOptionPane.ERROR_MESSAGE);

                        }
                        break;
                    case BORRAR:
                        if (frmHelice.heliceBuscada != null) {
                            try {
                                modelo.eliminarHelice(frmHelice.heliceBuscada.getIDHelice(), DeltaCharlie.IDUser);
                            } catch (SQLException e) {
                                JOptionPane.showMessageDialog(this.view, e.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);

                            }
                        } else {
                            JOptionPane.showMessageDialog(this.view, "Elija alguna helice", "Error", JOptionPane.ERROR_MESSAGE);

                        }
                        break;
                }
            }

        } else if (comando.getSource().equals(frmHelice.jButtonCancelar)) {
            cerrar(frmHelice);
        }
    }
    
    private void tomaDeMateria()
    {
        if (estacerrado(frmTomaMateria)) {
            frmTomaMateria = new JFrameInternalTomaMateria(modelo);
            this.view.jDesktopPane.add(frmTomaMateria);

            frmTomaMateria.jButtonAceptar.addActionListener(this);
            frmTomaMateria.jButtonCancelar.addActionListener(this);
            frmTomaMateria.jButtonAgregarMateria.addActionListener(this);
            
            frmTomaMateria.setLocation(centradoXY(frmTomaMateria));
            frmTomaMateria.setVisible(true);
        } else {
            JOptionPane.showMessageDialog(this.view, "La ventana toma de materia se encuentra abierta");
        }
    }
    
    private void tomaDeMateria(ActionEvent comando)
    {
        if (comando.getSource().equals(frmTomaMateria.jButtonAgregarMateria)) {
            try {
                String MateriaTomada = frmTomaMateria.jComboBoxMateriaATomar.getSelectedItem().toString();
                String GrupoTomado = frmTomaMateria.jComboBoxGrupoATomar.getSelectedItem().toString();
                
                if(GrupoTomado!="" && GrupoTomado!="No hay grupos disponibles")
                {
                    Boolean ctrl = false;
                    int rowCount = frmTomaMateria.jTableMateriasATomar.getRowCount();
                    for (int i = 0; i < rowCount; i++) {
                        String rowEntry = frmTomaMateria.jTableMateriasATomar.getValueAt(0, i).toString();
                        if (rowEntry.equals(MateriaTomada)) {
                            ctrl = true;
                        }
                    }
                    if(ctrl==true)
                    {
                        throw new Exception("La materia ya esta en la tabla");
                    }

                    MateriasATomar aux = new MateriasATomar(MateriaTomada,GrupoTomado);
                    frmTomaMateria.materiasATomar.addLast(aux);
                    frmTomaMateria.crearTablaMateriasATomar(frmTomaMateria.materiasATomar);
                    frmTomaMateria.llenarTablaMateriasATomar(frmTomaMateria.materiasATomar);
                }
                else
                {
                    JOptionPane.showMessageDialog(this.view,"No hay un grupo seleccionado", "Error", JOptionPane.ERROR_MESSAGE);
                }
            } catch (Exception e) {
                JOptionPane.showMessageDialog(this.view, e.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
            }
        }else if(comando.getSource().equals(frmTomaMateria.jButtonAceptar)) {
            if(!frmTomaMateria.ifAlumnoIsSelected){
                JOptionPane.showMessageDialog(this.view,"No hay alumno Seleccionado", "Error", JOptionPane.ERROR_MESSAGE);
            }else{
                if(frmTomaMateria.jTableMateriasATomar.getSize().height == 0)
                {
                    JOptionPane.showMessageDialog(this.view,"No hay materias seleccionadas", "Error", JOptionPane.ERROR_MESSAGE);
                }
                else
                {
                    for(MateriasATomar materia :frmTomaMateria.materiasATomar)
                    {
                        int IDGrupo;
                        try {
                            IDGrupo = modelo.getIDGrupo(materia.getGrupo());
                            modelo.inscribirAlumnoAGrupo(frmTomaMateria.personaBuscada.getiD(),IDGrupo);
                            JOptionPane.showMessageDialog(this.view, "Se ha incrito satisfactoriamente", "Success", JOptionPane.INFORMATION_MESSAGE);
                        } catch (SQLException ex) {
                            Logger.getLogger(controlador.class.getName()).log(Level.SEVERE, null, ex);
                        }
                    }
                }
            }
        }
        else if(comando.getSource().equals(frmTomaMateria.jButtonCancelar)) {
            cerrar(frmTomaMateria);
        }
        
    }
    
    
//___________________________________________________________________________________ Soy una barra separadora :)

    /* ATENTO A LAS ACCIONES DEL USUARIO */
    @Override
    public void actionPerformed(ActionEvent e) {
        //Captura en String el comando accionado por el usuario
        String comando = e.getActionCommand();

        System.out.println(comando);
        /* Acciones del frame login */
        if (frmAjustes != null) {
            try {
                ajustes(e);
            } catch (FileNotFoundException ex) {
                Logger.getLogger(controlador.class.getName()).log(Level.SEVERE, null, ex);
            } catch (UnsupportedEncodingException ex) {
                Logger.getLogger(controlador.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        if (frmlogin != null) {
            login(e);
        } else {//..........................................................................................
        /* Acciones del formulario padre */ if (view != null) {
            if (e.getSource().equals(view.reporteFlexible)) {
                crearReporteFlexible();
            } else if (e.getSource().equals(view.Salir)) {
                System.exit(0);
            }
        }
        if (e.getSource().equals(view.j2Button2)) {
            crearAlumno(CrearModificarBorrar.CREAR);
        }
        if (e.getSource().equals(view.j2Button4)) {
            crearReporteFlexible();
        }
        if (e.getSource().equals(view.j2Button5)) {
            crearPago();
        }
        if (e.getSource().equals(view.j2Button1)) {
            crearMateria(CrearModificarBorrar.CREAR);
        }
        if (e.getSource().equals(view.j2Button6)) {
            registrarVuelos();
        }
        if (e.getSource().equals(view.j2Button3)) {
            revisarAlertas();
        }
        if (e.getSource().equals(view.j2Button8)) {
            controlCreditos();
        }
        if(e.getSource().equals(view.j2Button9)){
            tomaDeMateria();
        }
        if (e.getSource().equals(view.jMenuAlumno)) {
            cargarAlumno();
        }
        if (e.getSource().equals(view.jMenuAvion)) {
            cargarAvion();
        }
        if (e.getSource().equals(view.jMenuMotor)) {
            cargarMotor();
        }
        if (e.getSource().equals(view.jMenuMateria)) {
            cargarMateria();
        }
        if (e.getSource().equals(view.jMenuHelice)) {
            cargarHelice();
        }
        if (e.getSource().equals(view.jMenuItemMalla)) {
            cargarMalla();
        }
        if (e.getSource().equals(view.jMenuItemGrupo)) {
            cargarGrupo();
        }
        if (e.getSource().equals(view.jMenuItemNotas)) {
            cargarNotas();
        }
        if (e.getSource().equals(view.jMenuDocentePiloto)) {
            cargarDocentePiloto();
        }
        if (e.getSource().equals(view.jMenuUsuario)) {
            cargarUsuario();
        }
        //..........................................................................................
        /* Acciones del frame  crear Usuario */
        if (frmCrearUsuario != null) {
            crearUsuario(e);
        }
        if (frmBuscarUsuario != null) {
            buscarUsuario(e);
        }
        //..........................................................................................
        /* Acciones del frame  crear Materia */
        if (frmCrearMateria != null) {
            crearMateria(e);
        }
        if(frmDetalles != null){
            Detalle(e);
        }
        //..........................................................................................
        /* Acciones del frame  crear Helice */
        if (frmHelice != null) {
            crearHelice(e);
        }
        //..........................................................................................
        /* Acciones del frame  crear Alumno */
        if (frmCrearNuevoAlumno != null) {
            crearAlumno(e);
        }
        //..........................................................................................
        /* Acciones del frame  crear Docente Piloto */
        if (frmDocentePiloto != null) {
            crearDocentePiloto(e);
        }
        //..........................................................................................
        /* Acciones del frame  crear Malla */
        if (frmCrearMalla != null) {
            crearMalla(e);
        }
        if (frmVuelo != null){
            CrearRegistroVuelo(e);
        }
        if (frmBuscarMalla != null) {
            buscarMalla(e);
        }
        //..........................................................................................
        /* Acciones del frame  Buscar Materia */
        if (frmBuscarMateria != null) {
            buscarMateria(e);
        }
        //..........................................................................................
        /* Acciones del frame  Buscar Materia */
        if (frmBuscarDocente != null) {
            buscarDocente(e);
        }
        //..........................................................................................
        /* Acciones del frame  Crear Grupo */
        if (frmCrearGrupo != null) {
            crearGrupo(e);
        }
        
        if (frmBuscarGrupo != null) {
            buscarGrupo(e);
        }
        
        //..........................................................................................
        /* Acciones del frame  buscar alumno*/
        if (frmBuscarAlumno != null) {
            buscarAlumno(e);
        }
        //..........................................................................................
        /* Acciones del frame  crear Avion*/
        if (frmCrearAvion != null) {
            crearAvion(e);
        }
        //..........................................................................................
        /* Acciones del frame  crear Motor*/
        if (frmMotor != null) {
            crearMotor(e);
        }
        //..........................................................................................
        /* Acciones del frame  Reporte*/
        if (frmCrearReporteFlexible != null) {
            try {
                crearReporteFlexible(e);
            } catch (Exception ex) {
                Logger.getLogger(controlador.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        //..........................................................................................
        /* Acciones del frame crear pago*/
        if (frmCrearPagos != null) {
            crearPago(e);
        }
        if (frmPrincipalAvion != null) {
            cargarAvion(e);
        }
        if (frmPrincipalMotor != null) {
            cargarMotor(e);
        }
        if (frmPrincipalMalla != null) {
            cargarMalla(e);
        }
        if (frmPrincipalGrupo != null) {
            cargarGrupo(e);
        }
        if (frmPrincipalNotas != null) {
            cargarNotas(e);
        }
        if (frmPrincipalMateria != null) {
            cargarMateria(e);
        }
        if (frmPrincipalHelice != null) {
            cargarHelice(e);
        }
        if (frmPrincipalAlumno != null) {
            cargarAlumno(e);
        }
        if (frmPrincipalDocentePiloto != null) {
            cargarDocentePiloto(e);
        }
        if (frmPrincipalUsuario != null) {
            cargarUsuario(e);
        }
        if (frmAcercaDe != null) {
            acercaDe(e);
        }
        if (frmRevisarAlertas != null) {
            revisarAlertas(e);
        }
        if (frmControlCreditos != null) {
            controlCreditos(e);
        }
        if (frmDetallesAvion != null) {
            detallesAvion(e);
        }
        if (frmDetallesMotor != null) {
            detallesMotor(e);
        }
        if (frmDetallesHelice != null) {
            detallesHelice(e);
        }
        if (frmDetallesAlumno != null) {
            detallesAlumno(e);
        }
        if (frmDetallesDocentePiloto != null) {
            detallesDocentePiloto(e);
        }
        if (frmDetallesUsuario != null) {
            detallesUsuario(e);
        }
        /* Acciones del frame  buscar HMA*/
        if (frmBuscarHMA != null) {
            buscarHMA(e);
        }
        /* Acciones de toma de Materia*/
        if(frmTomaMateria != null){
            tomaDeMateria(e);
        }
        //..........................................................................................
    }
    }
//___________________________________________________________________________________ Soy una barra separadora :)
//METODO QUE DEVUELVE UN VALOR BOOLEAN PARA SABER SI UN JINTERNALFRAME ESTA ABIERTO O NO

    public boolean estacerrado(Object obj) {
        JInternalFrame[] activos = this.view.jDesktopPane.getAllFrames();
        boolean cerrado = true;
        int i = 0;
        while (i < activos.length && cerrado) {
            if (activos[i] == obj) {
                cerrado = false;
            }
            i++;
        }
        return cerrado;
    }
//___________________________________________________________________________________ Soy una barra separadora :)
// CIERRA TODOS LOS JInternalFrame QUE ESTEN ABIERTOS

    private void cerrar_todo() {
        JInternalFrame[] activos = this.view.jDesktopPane.getAllFrames();
        //boolean cerrado=true;
        int i = 0;
        while (i < activos.length) {
            cerrar(activos[i]);
            i++;
        }
    }
//___________________________________________________________________________________ Soy una barra separadora :)
// CIERRA UN JInternalFrame

    private void cerrar(JInternalFrame jif) {
        try {
            jif.setClosed(true);
        } catch (PropertyVetoException ex) {
        }
    }
//___________________________________________________________________________________ Soy una barra separadora :)
//funcion que dado un JInternalFrame calcula la posicion de centrado respecto a su contenedor, retorna las coordenadas en una variable de tipo POINT

    private Point centradoXY(JInternalFrame jif) {
        Point p = new Point(0, 0);
        //se obtiene dimension del contenedor
        Dimension pantalla = this.view.jDesktopPane.getSize();
        //se obtiene dimension del JInternalFrame
        Dimension ventana = jif.getSize();
        //se calcula posición para el centrado
        p.x = (pantalla.width - ventana.width) / 2;
        p.y = (pantalla.height - ventana.height) / 2;
        return p;
    }

//___________________________________________________________________________________ Soy una barra separadora :)
    private String FechaParseada(Date fecha) {
        SimpleDateFormat formato = new SimpleDateFormat("yyyy-MM-dd");
        return formato.format(fecha);
    }
    
    private String FechaParseada2(Date fecha) {
        SimpleDateFormat formato = new SimpleDateFormat("yy-MM-dd");
        return formato.format(fecha);
    }

    private void buscarMalla() {
        if (estacerrado(frmBuscarMalla)) {

            frmBuscarMalla = new JFrameInternalBuscarMalla(modelo);
            this.view.jDesktopPane.add(frmBuscarMalla);

            frmBuscarMalla.jButtonAceptar.addActionListener(this);
            frmBuscarMalla.Cancelar.addActionListener(this);

            frmBuscarMalla.setLocation(centradoXY(frmBuscarMalla));
            frmBuscarMalla.setVisible(true);

        } else {
            JOptionPane.showMessageDialog(this.view, "La ventana Buscar Malla se encuentra abierta");

        }

    }
    
    private void buscarMalla(ActionEvent comando) {
        if (comando.getSource().equals(frmBuscarMalla.jButtonAceptar)) {

            if (frmBuscarMalla.mallaSeleccionada != null) {

                try {
                    auxidmalla=0;
                    contenedormateriaid.clear();
                    contenedormateria.clear();
                    contenedorCosto.clear();
                    contenedorMinutos.clear();
                    frmCrearMalla.jTableMateriasCorrespondientesMalla.setModel(modelo.LlenarTabla(contenedormateriaid, contenedormateria, contenedorCosto, contenedorMinutos));
                    modelo.nivel = 0;
                    /*modelo.CostoMaterias = 0;
                    modelo.MateriasVuelo = 0;
                    modelo.NumeroNormales = 0;
                    modelo.TotalMaterias = 0;
                    modelo.TotalMinutos = 0;*/
                    frmCrearMalla.jTextFieldNombreMalla.setText("");
                    frmCrearMalla.jTextFieldBuscarMateria.setText("");
                    frmCrearMalla.jCheckBoxRequiereLicencia.setSelected(false);
                    frmCrearMalla.jLabelCostoTotalMallaV.setText("0");
                    frmCrearMalla.jLabelNumeroTotalMateriasV.setText("0");
                    frmCrearMalla.jLabelTotalMinutosV.setText("0");
                    auxidmalla=modelo.sacaridmalla(frmBuscarMalla.jComboBoxMallaBuscar.getSelectedItem().toString());
                    ids=modelo.ConseguirIDMaterias(auxidmalla);
                    modelo.ConseguirNombreMalla(auxidmalla, frmCrearMalla.jTextFieldNombreMalla, frmCrearMalla.jCheckBoxRequiereLicencia);
                    modelo.CrearTablaMateriasMallaModificar(ids, contenedormateriaid, contenedormateria, contenedorCosto, contenedorMinutos);
                    frmCrearMalla.jTableMateriasCorrespondientesMalla.setModel(modelo.LlenarTabla(contenedormateriaid,contenedormateria,contenedorCosto,contenedorMinutos));
                    modelo.DatosIngresados(frmCrearMalla.jLabelCostoTotalMallaV, frmCrearMalla.jLabelTotalMinutosV, frmCrearMalla.jLabelNumeroTotalMateriasV, contenedormateriaid, contenedormateria, contenedorCosto, contenedorMinutos);
                    //frmCrearMalla.malla = frmBuscarMalla.mallaSeleccionada;
                    //frmCrearMalla.llenarMalla();
                    //frmBuscarMalla.mallaSeleccionada = null;
                    cerrar(frmBuscarMalla);
                } catch (SQLException ex) {
                    Logger.getLogger(controlador.class.getName()).log(Level.SEVERE, null, ex);
                }
            } else {
                JOptionPane.showMessageDialog(this.view, "No hay alumno seleccionado", "Error", JOptionPane.ERROR_MESSAGE);
            }
        } else if (comando.getSource().equals(frmBuscarMalla.Cancelar)) {
            cerrar(frmBuscarMalla);
        }

    }

    private void buscarHMA(int HMA) {
        if (estacerrado(frmBuscarHMA)) {

            frmBuscarHMA = new JFrameInternalBuscarHMA(modelo, HMA);
            this.view.jDesktopPane.add(frmBuscarHMA);

            frmBuscarHMA.jButtonAceptar.addActionListener(this);
            frmBuscarHMA.jButtonCancelar.addActionListener(this);

            frmBuscarHMA.setLocation(centradoXY(frmBuscarHMA));
            frmBuscarHMA.setVisible(true);

        } else {
            JOptionPane.showMessageDialog(this.view, "La ventana Buscar Helice/Motor/Avion se encuentra abierta");
            cerrar(frmBuscarHMA);

        }
    }

    private void buscarHMA(ActionEvent comando) {
        if (comando.getSource().equals(frmBuscarHMA.jButtonAceptar)) {
            switch (frmBuscarHMA.HMA) {
                case 1:
                    if (frmBuscarHMA.heliceBuscada != null) {
                        frmHelice.heliceBuscada = frmBuscarHMA.heliceBuscada;
                        frmHelice.llenarHelice();
                        cerrar(frmBuscarHMA);
                    } else {
                        JOptionPane.showMessageDialog(this.view, "Seleccione una helice");
                    }
                    break;
                case 2:
                    if (frmBuscarHMA.motorBuscado != null) {
                        frmMotor.motorBuscado = frmBuscarHMA.motorBuscado;
                        frmMotor.llenarMotor();
                        cerrar(frmBuscarHMA);
                    } else {
                        JOptionPane.showMessageDialog(this.view, "Seleccione un motor");
                    }
                    break;
                case 3:
                    if (frmBuscarHMA.avionBuscado != null) {
                        frmCrearAvion.avionSeleccionado = frmBuscarHMA.avionBuscado;
                        frmCrearAvion.llenarAvion();
                        cerrar(frmBuscarHMA);
                    } else {
                        JOptionPane.showMessageDialog(this.view, "Seleccione un avion");
                    }
                    break;
            }
        } else if (comando.getSource().equals(frmBuscarHMA.jButtonCancelar)) {
            cerrar(frmBuscarHMA);
        }
    }

    public void crearMotor(CrearModificarBorrar cmb) {
        if (estacerrado(frmMotor)) {
            try {
                frmMotor = new JFrameInternalMotor(modelo, cmb);
                if(cmb.equals(CrearModificarBorrar.MODIFICAR) || cmb.equals(CrearModificarBorrar.CREAR)){
                    if(cmb.equals(CrearModificarBorrar.MODIFICAR)){
                        frmMotor.motorBuscado = JButtonRenderer.motorBuscado;
                        frmMotor.llenarMotor();
                    }
                    this.view.jDesktopPane.add(frmMotor);

                    frmMotor.jComboBoxHelices.setModel(modelo.obtenerMelicesObtenibles(frmMotor.helicesABuscar));
                    frmMotor.jButtonAceptar.addActionListener(this);
                    frmMotor.jButtonCancelar.addActionListener(this);

                    frmMotor.setLocation(centradoXY(frmMotor));
                    frmMotor.setVisible(true);
                }
                if(cmb.equals(CrearModificarBorrar.BORRAR)){
                    int respuesta = JOptionPane.showConfirmDialog(this.view, "¿Desea eliminar este motor?", "Confirmar",
                                    JOptionPane.OK_CANCEL_OPTION, JOptionPane.INFORMATION_MESSAGE);
                    if (respuesta == JOptionPane.OK_OPTION) {
                        frmMotor.motorBuscado = JButtonRenderer.motorBuscado;
                        modelo.eliminarMotor(frmMotor.motorBuscado, DeltaCharlie.IDUser);
                        modelo.vertablaMotor(frmPrincipalMotor.jTableMotores, this);
                        frmPrincipalMotor.jTableMotores.repaint();
                    }
                }
            } catch (SQLException ex) {
                JOptionPane.showMessageDialog(this.view, "Error al conectar a la base de datos");
                cerrar(frmCrearAvion);
            }

        } else {
            JOptionPane.showMessageDialog(this.view, "La ventana crear motor abierta");
            cerrar(frmMotor);
        }
    }

    public void crearMotor(ActionEvent comando) {
        if (comando.getSource().equals(frmMotor.jButtonAceptar)) {
            if (frmMotor.cmb != null) {
                switch (frmMotor.cmb) {
                    case CREAR:
                        try {
                            if (frmMotor.jTextFieldMarca.getText().isEmpty()) {
                                throw new Exception("No se puede Crear Sin Marca");
                            }
                            if (frmMotor.jTextFieldModelo.getText().isEmpty()) {
                                throw new Exception("No se puede Crear Sin Modelo");
                            }
                            if (frmMotor.jTextFieldSerie.getText().isEmpty()) {
                                throw new Exception("No se puede Crear Sin Serie");
                            }
                            if (frmMotor.jTextFieldTT.getText().isEmpty()) {
                                throw new Exception("No se puede Crear Sin Total Time");
                            }
                            String marca = frmMotor.jTextFieldMarca.getText();
                            String modeloHelice = frmMotor.jTextFieldModelo.getText();
                            String serie = frmMotor.jTextFieldSerie.getText();

                            String[] settt = frmMotor.jTextFieldTT.getText().split(":");

                            String[] settbo;
                            String[] settto;
                            int tto = -1;
                            int tbo = -1;
                            if (settt.length != 2) {
                                throw new Exception("Formato incorrecto");
                            }
                            int tt = Integer.parseInt(settt[0]) * 60 + Integer.parseInt(settt[1]);
                            if (!frmMotor.jTextFieldTBO.getText().isEmpty()) {

                                settbo = frmMotor.jTextFieldTBO.getText().split(":");
                                if (settbo.length != 2) {
                                    throw new Exception("Formato incorrecto");
                                }
                                tbo = Integer.parseInt(settbo[0]) * 60 + Integer.parseInt(settbo[1]);
                            }
                            if (!frmMotor.jTextFieldTTO.getText().isEmpty()) {
                                settto = frmMotor.jTextFieldTTO.getText().split(":");
                                if (settto.length != 2) {
                                    throw new Exception("Formato incorrecto");
                                }
                                tto = Integer.parseInt(settto[0]) * 60 + Integer.parseInt(settto[1]);
                            }
                            int posicion = frmMotor.jComboBoxHelices.getSelectedIndex();
                            Helices aUtilizar = null;
                            if (posicion > 0) {
                                aUtilizar = frmMotor.helicesABuscar.get(posicion - 1);
                            } else {
                                aUtilizar = new Helices(-1, "", "", "", 0, 0, 0);
                            }
                            Motor aux = new Motor(-1, aUtilizar.getIDHelice(), marca, marca, serie, tt, tto, tbo);
                            modelo.insertarMotor(aux, DeltaCharlie.IDUser);
                            modelo.vertablaMotor(frmPrincipalMotor.jTableMotores, this);
                            frmPrincipalMotor.jTableMotores.repaint();
                            JOptionPane.showMessageDialog(this.view, "El motor se ha creado con exito");     
                            cerrar(frmMotor);
                        } catch (Exception e) {
                            JOptionPane.showMessageDialog(this.view, e.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
                        }
                        break;
                    case MODIFICAR:
                        if (frmMotor.motorBuscado != null) {
                            try {
                                if (frmMotor.jTextFieldMarca.getText().isEmpty()) {
                                    throw new Exception("No se puede Crear Sin Marca");
                                }
                                if (frmMotor.jTextFieldModelo.getText().isEmpty()) {
                                    throw new Exception("No se puede Crear Sin Modelo");
                                }
                                if (frmMotor.jTextFieldSerie.getText().isEmpty()) {
                                    throw new Exception("No se puede Crear Sin Serie");
                                }
                                if (frmMotor.jTextFieldTT.getText().isEmpty()) {
                                    throw new Exception("No se puede Crear Sin Total Time");
                                }
                                String marca = frmMotor.jTextFieldMarca.getText();
                                String modelo2 = frmMotor.jTextFieldModelo.getText();
                                String serie = frmMotor.jTextFieldSerie.getText();

                                String[] settt = frmMotor.jTextFieldTT.getText().split(":");

                                String[] settbo;
                                String[] settto;
                                int tto = -1;
                                int tbo = -1;
                                if (settt.length != 2) {
                                    throw new Exception("Formato incorrecto");
                                }
                                int tt = Integer.parseInt(settt[0]) * 60 + Integer.parseInt(settt[1]);
                                if (!frmMotor.jTextFieldTBO.getText().isEmpty()) {

                                    settbo = frmMotor.jTextFieldTBO.getText().split(":");
                                    if (settbo.length != 2) {
                                        throw new Exception("Formato incorrecto");
                                    }
                                    tbo = Integer.parseInt(settbo[0]) * 60 + Integer.parseInt(settbo[1]);
                                }
                                if (!frmMotor.jTextFieldTTO.getText().isEmpty()) {
                                    settto = frmMotor.jTextFieldTTO.getText().split(":");
                                    if (settto.length != 2) {
                                        throw new Exception("Formato incorrecto");
                                    }
                                    tto = Integer.parseInt(settto[0]) * 60 + Integer.parseInt(settto[1]);
                                }
                                int posicion = frmMotor.jComboBoxHelices.getSelectedIndex();
                                Helices aUtilizar = null;
                                if (posicion > 0) {
                                    aUtilizar = frmMotor.helicesABuscar.get(posicion - 1);
                                } else {
                                    if (frmMotor.heliceAnexada != null) {
                                        aUtilizar = frmMotor.heliceAnexada;
                                    } else {
                                        aUtilizar = new Helices(-1, "", "", "", 0, 0, 0);
                                    }

                                }
                                Motor aux = new Motor(frmMotor.motorBuscado.getIDMotor(), aUtilizar.getIDHelice(), marca, modelo2, serie, tt, tto, tbo);
                                modelo.modificarMotor(aux, DeltaCharlie.IDUser);
                                modelo.vertablaMotor(frmPrincipalMotor.jTableMotores, this);
                                frmPrincipalMotor.jTableMotores.repaint();
                                JOptionPane.showMessageDialog(this.view, "El motor se ha modificado con exito");     
                                cerrar(frmMotor);
                            } catch (Exception e) {
                                JOptionPane.showMessageDialog(this.view, e.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
                            }
                        } else {
                            JOptionPane.showMessageDialog(this.view, "Elija algun motor", "Error", JOptionPane.ERROR_MESSAGE);

                        }
                        break;
                    case BORRAR:
                        
                        break;
                }
            }
        } else if (comando.getSource().equals(frmMotor.jButtonCancelar)) {
            cerrar(frmMotor);
        }
    }
    
    public void Detalles(){
        if(estacerrado(frmDetalles)){
            frmDetalles = new JFrameInternalAgregarDetalles();
            this.view.jDesktopPane.add(frmDetalles);
            frmDetalles.jButtonAgregar.addActionListener(this);
            frmDetalles.jButtonCancelar.addActionListener(this);
            frmDetalles.setLocation(centradoXY(frmDetalles));
            frmDetalles.setVisible(true);
        }
    }
    
    public void Detalle(ActionEvent comando){
        if(comando.getSource().equals(frmDetalles.jButtonAgregar)){
            int horasvueloint = Integer.parseInt(JButtonRenderer.HorasVuelo);
            try {
                String tipo = JButtonRenderer.TV;
                int idPiloto = modelo.ConseguirIDDocente(JButtonRenderer.NombreE);
                int idCopiloto = modelo.ConseguirIDDocente(JButtonRenderer.NombreP);
                int idAvion = modelo.getIDAvion(JButtonRenderer.NombreA);
                String detallesin = frmDetalles.jTextPaneDetalles.getText();
                modelo.IngresarHojaABordo(horasvueloint, idPiloto, idCopiloto, idAvion, detallesin, DeltaCharlie.IDUser);
                int minutosone = modelo.ObtenerMinutosAvionXID(idAvion);
                int minutosnew = minutosone + horasvueloint;
                modelo.ModificarMinutos(minutosnew, idAvion);
                modelo.eliminarReserva(tipo, idCopiloto, idPiloto, idAvion,horasvueloint);
                modelo.vertablaReservas(frmVuelo.jTableReservas, this);
                frmVuelo.jTableReservas.repaint();
            } catch (SQLException ex) {
                Logger.getLogger(controlador.class.getName()).log(Level.SEVERE, null, ex);
            }
            cerrar(frmDetalles);
        }
    }

    public void crearAvion(CrearModificarBorrar cmb) {
        if (estacerrado(frmCrearAvion)) {
            try {
                frmCrearAvion = new JFrameInternalAvion(modelo, cmb);
                if(cmb.equals(CrearModificarBorrar.MODIFICAR) || cmb.equals(CrearModificarBorrar.CREAR)){
                    if(cmb.equals(CrearModificarBorrar.MODIFICAR)){
                        frmCrearAvion.avionSeleccionado = JButtonRenderer.avionBuscado;
                        frmCrearAvion.llenarAvion();
                    }
                    this.view.jDesktopPane.add(frmCrearAvion);

                    frmCrearAvion.jComboBoxMotores.setModel(modelo.ObtenerMotorDisponible());
                    frmCrearAvion.jComboBoxMotores2.setModel(modelo.ObtenerMotorDisponible());

                    frmCrearAvion.jButtonAceptar.addActionListener(this);
                    frmCrearAvion.jButtonCancelar.addActionListener(this);

                    frmCrearAvion.jButtonAgregarMotor.addActionListener(this);

                    frmCrearAvion.jButtonAgregarMotor2.addActionListener(this);

                    frmCrearAvion.setLocation(centradoXY(frmCrearAvion));
                    frmCrearAvion.setVisible(true);
                }
                if(cmb.equals(CrearModificarBorrar.BORRAR)){
                    int respuesta = JOptionPane.showConfirmDialog(this.view, "¿Desea eliminar este avion?", "Confirmar",
                                    JOptionPane.OK_CANCEL_OPTION, JOptionPane.INFORMATION_MESSAGE);
                    if (respuesta == JOptionPane.OK_OPTION) {
                        frmCrearAvion.avionSeleccionado = JButtonRenderer.avionBuscado;
                        modelo.eliminarAvion(frmCrearAvion.avionSeleccionado, DeltaCharlie.IDUser);
                        modelo.vertablaAvion(frmPrincipalAvion.jTableAviones, this);
                        frmPrincipalAvion.jTableAviones.repaint();
                    }
                }
            } catch (SQLException ex) {
                JOptionPane.showMessageDialog(this.view, "Error al conectar a la base de datos");
                cerrar(frmCrearAvion);
            }
        } else {
            JOptionPane.showMessageDialog(this.view, "La ventana crear avion");
            cerrar(frmCrearAvion);

        }
    }

    public void crearAvion(ActionEvent comando) {
        String motorD = null, motorI = null;
        if (comando.getSource().equals(frmCrearAvion.jButtonAgregarMotor)) {
            if(frmCrearAvion.jComboBoxMotores.getSelectedIndex() != 0){
                frmCrearAvion.jLabelMotorD.setText(frmCrearAvion.jComboBoxMotores.getSelectedItem().toString());
                frmCrearAvion.jLabelHeliceD.setText(modelo.selectHelice(frmCrearAvion.jComboBoxMotores.getSelectedItem().toString()));
            }
        } else if (comando.getSource().equals(frmCrearAvion.jButtonAgregarMotor2)) {
            if(frmCrearAvion.jComboBoxMotores2.getSelectedIndex() != 0){
                frmCrearAvion.jLabelMotorI.setText(frmCrearAvion.jComboBoxMotores2.getSelectedItem().toString());
                frmCrearAvion.jLabelHeliceI.setText(modelo.selectHelice(frmCrearAvion.jComboBoxMotores2.getSelectedItem().toString()));
            }
        } else if (comando.getSource().equals(frmCrearAvion.jButtonAceptar)) {
            if (frmCrearAvion.cmb != null) {
                switch (frmCrearAvion.cmb) {
                    case CREAR:
                        try {
                            if (frmCrearAvion.jTextFieldMarcaAeronave.getText().isEmpty()) {
                                throw new Exception("No se puede Crear Sin Marca");
                            }
                            if (frmCrearAvion.jTextFieldModeloAeronave1.getText().isEmpty()) {
                                throw new Exception("No se puede Crear Sin Modelo");
                            }
                            if (frmCrearAvion.jTextFieldNumeroSerieAeronave.getText().isEmpty()) {
                                throw new Exception("No se puede Crear Sin Serie");
                            }
                            if (frmCrearAvion.jTextFieldHorasVuelo.getText().isEmpty()) {
                                throw new Exception("No se puede Crear Sin Horas Vuelo");
                            }
                            String marca = frmCrearAvion.jTextFieldMarcaAeronave.getText();
                            String modeloAvion = frmCrearAvion.jTextFieldModeloAeronave1.getText();
                            String serie = frmCrearAvion.jTextFieldNumeroSerieAeronave.getText();

                            String[] horasVuelo = frmCrearAvion.jTextFieldHorasVuelo.getText().split(":");

                            if (horasVuelo.length != 2) {
                                throw new Exception("Formato incorrecto");
                            }
                            int horasV = Integer.parseInt(horasVuelo[0]) * 60 + Integer.parseInt(horasVuelo[1]);
                            String vencimientoExtintor = FechaParseada(frmCrearAvion.dateChooserComboVencimientoExtintor.getSelectedDate().getTime());
                            String certificadoAeronavegabilidad = FechaParseada(frmCrearAvion.dateChooserComboCertificadoAeronavegabilidad.getSelectedDate().getTime());
                            String vencimientoMatricula = FechaParseada(frmCrearAvion.dateChooserComboRegistroMatricula.getSelectedDate().getTime());
                            String vencimientoEquipoEmergencia = FechaParseada(frmCrearAvion.dateChooserComboVencimientoEquipoEmergencia.getSelectedDate().getTime());
                            String vencimientoSeguro = FechaParseada(frmCrearAvion.dateChooserComboVencimientoSeguro.getSelectedDate().getTime());
                            motorD = frmCrearAvion.jLabelMotorD.getText();
                            motorI = frmCrearAvion.jLabelMotorI.getText();
                            if(motorD.equals(motorI)){
                                JOptionPane.showMessageDialog(this.view, "No se puede crear un avion con los motores iguales", "Error", JOptionPane.ERROR_MESSAGE);
                            }
                            else{
                                Avion aux = new Avion(-1, marca, horasV, modeloAvion, serie, vencimientoExtintor, vencimientoSeguro, vencimientoMatricula, certificadoAeronavegabilidad, vencimientoEquipoEmergencia);
                                modelo.insertarAvion(aux, DeltaCharlie.IDUser, motorD, motorI);
                                modelo.vertablaAvion(frmPrincipalAvion.jTableAviones, this);
                                frmPrincipalAvion.jTableAviones.repaint();
                                hilo1.refresh();
                                JOptionPane.showMessageDialog(this.view, "El avion se ha creado con exito");     
                                cerrar(frmCrearAvion);
                            }
                        } catch (Exception e) {
                            JOptionPane.showMessageDialog(this.view, e.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
                        }
                        break;
                    case MODIFICAR:
                            try {
                                if (frmCrearAvion.jTextFieldMarcaAeronave.getText().isEmpty()) {
                                    throw new Exception("No se puede Crear Sin Marca");
                                }
                                if (frmCrearAvion.jTextFieldModeloAeronave1.getText().isEmpty()) {
                                    throw new Exception("No se puede Crear Sin Modelo");
                                }
                                if (frmCrearAvion.jTextFieldNumeroSerieAeronave.getText().isEmpty()) {
                                    throw new Exception("No se puede Crear Sin Serie");
                                }
                                if (frmCrearAvion.jTextFieldHorasVuelo.getText().isEmpty()) {
                                    throw new Exception("No se puede Crear Sin Horas Vuelo");
                                }
                                String marca = frmCrearAvion.jTextFieldMarcaAeronave.getText();
                                String modeloAvion = frmCrearAvion.jTextFieldModeloAeronave1.getText();
                                String serie = frmCrearAvion.jTextFieldNumeroSerieAeronave.getText();

                                String[] horasVuelo = frmCrearAvion.jTextFieldHorasVuelo.getText().split(":");

                                if (horasVuelo.length != 2) {
                                    throw new Exception("Formato incorrecto");
                                }
                                int horasV = Integer.parseInt(horasVuelo[0]) * 60 + Integer.parseInt(horasVuelo[1]);
                                String vencimientoExtintor = FechaParseada(frmCrearAvion.dateChooserComboVencimientoExtintor.getSelectedDate().getTime());
                                String certificadoAeronavegabilidad = FechaParseada(frmCrearAvion.dateChooserComboCertificadoAeronavegabilidad.getSelectedDate().getTime());
                                String vencimientoMatricula = FechaParseada(frmCrearAvion.dateChooserComboRegistroMatricula.getSelectedDate().getTime());
                                String vencimientoEquipoEmergencia = FechaParseada(frmCrearAvion.dateChooserComboVencimientoEquipoEmergencia.getSelectedDate().getTime());
                                String vencimientoSeguro = FechaParseada(frmCrearAvion.dateChooserComboVencimientoSeguro.getSelectedDate().getTime());

                                Avion aux = new Avion(frmCrearAvion.avionSeleccionado.getIDAvion(), marca, horasV, modeloAvion, serie, vencimientoExtintor, vencimientoSeguro, vencimientoMatricula, certificadoAeronavegabilidad, vencimientoEquipoEmergencia);
                                motorD = frmCrearAvion.jLabelMotorD.getText();
                                motorI = frmCrearAvion.jLabelMotorI.getText();
                                modelo.modificarAvion(aux, DeltaCharlie.IDUser, motorD, motorI);
                                modelo.vertablaAvion(frmPrincipalAvion.jTableAviones, this);
                                frmPrincipalAvion.jTableAviones.repaint();
                                hilo1.refresh();
                                JOptionPane.showMessageDialog(this.view, "El avion se ha modificado con exito");     
                                cerrar(frmCrearAvion);
                            } catch (Exception e) {
                                JOptionPane.showMessageDialog(this.view, e.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
                            };
                            break;
                    case BORRAR:
                        
                        break;
                }
            }

        } else if (comando.getSource().equals(frmCrearAvion.jButtonCancelar)) {
            cerrar(frmCrearAvion);
        }
    }
}

