/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package deltacharlie;

/**
 *
 * @author Ritha Carolina
 */
import entity.CrearModificarBorrar;
import entity.Persona;
import entity.Referencia;
import entity.TelefonoReferencia;
import java.sql.SQLException;
import java.util.Calendar;
import java.util.LinkedList;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;
import modelo.conexion;
public class JFrameInternalDocentePiloto extends javax.swing.JInternalFrame {

    /**
     * Creates new form JFrameInternalDocentePiloto
     */
    public LinkedList<Referencia> referencias,buscadas;
    public Referencia elegida = null;
    public LinkedList<TelefonoReferencia> telefonoReferencia;
    private conexion con;
    public Persona elegidaPersona;
    public CrearModificarBorrar cmbActual;
    public int idReferenciaElegida;
    
    public JFrameInternalDocentePiloto(conexion con, CrearModificarBorrar _cmbActual) {
        initComponents();
        this.con = con;
        referencias = new LinkedList<>();
        buscadas = new LinkedList<>();
        telefonoReferencia = new LinkedList<>();
        elegidaPersona = null;
        this.cmbActual = _cmbActual;
        if(cmbActual == CrearModificarBorrar.CREAR)
        {
            
        }
        if(cmbActual == CrearModificarBorrar.MODIFICAR)
        {
            JbotonAceptar.setText("Modificar");
        }
        if(cmbActual == CrearModificarBorrar.BORRAR)
        {
            JbotonAceptar.setText("Eliminar");
            jButtonAgregarReferencia.setVisible(false);
            jButtonAgregarTelefonoReferencia.setVisible(false);
            jButtonEliminarReferencia.setVisible(false);
            jButtonEliminarTelefonoReferencia.setVisible(false);
        }
    }
    
    public void llenarPersona() throws SQLException
    {
        jTextFieldNombre.setText(elegidaPersona.getNombre());
        jTextFieldApellidoPaterno.setText(elegidaPersona.getApellidoPaterno());
        jTextFieldApellidoMaterno.setText(elegidaPersona.getApellidoMaterno());
        dateChooserComboFechaNacimiento.setCurrent(elegidaPersona.getFechaCalendar(elegidaPersona.getFechaNacimiento()));
        jTextFieldDireccionActual.setText(elegidaPersona.getDireccion());
        jTextFieldTelefono.setText(String.valueOf(elegidaPersona.getTelefono()));
        jTextFieldDocumentoIdentificador.setText(elegidaPersona.getCiRucPas());
        if(elegidaPersona.getTipoSangra().equals("O RH-"))
        {
            jComboBoxTipoSangre.setSelectedIndex(0);
             
        } else
        if(elegidaPersona.getTipoSangra().equals("O RH+"))
        {
            jComboBoxTipoSangre.setSelectedIndex(1);
             
        } else if(elegidaPersona.getTipoSangra().equals("A RH+"))
        {
            jComboBoxTipoSangre.setSelectedIndex(3);
             
        } else if(elegidaPersona.getTipoSangra().equals("A RH-"))
        {
            jComboBoxTipoSangre.setSelectedIndex(4);
             
        } else if(elegidaPersona.getTipoSangra().equals("B RH+"))
        {
            jComboBoxTipoSangre.setSelectedIndex(5);
             
        } else if(elegidaPersona.getTipoSangra().equals("B RH-"))
        {
            jComboBoxTipoSangre.setSelectedIndex(6);
             
        } else if(elegidaPersona.getTipoSangra().equals("AB RH+"))
        {
            jComboBoxTipoSangre.setSelectedIndex(7);
             
        } else if(elegidaPersona.getTipoSangra().equals("AB RH-"))
        {
            jComboBoxTipoSangre.setSelectedIndex(8);
             
        } else
        if(elegidaPersona.getUsaLentes()==0)
        {
           jCheckBoxUsaLentes.setSelected(false); 
        } else
        {
            jCheckBoxUsaLentes.setSelected(true);
        }
        
        if(elegidaPersona.getTipoDocumento() == 1)
        {
            jComboBoxTipodeDocumentoIdentificador.setSelectedIndex(0);
        } else if(elegidaPersona.getTipoDocumento() == 2)
        {
            jComboBoxTipodeDocumentoIdentificador.setSelectedIndex(1);
        } else if(elegidaPersona.getTipoDocumento() == 3)
        {
            jComboBoxTipodeDocumentoIdentificador.setSelectedIndex(2);
        } else if(elegidaPersona.getTipoDocumento() == 4)
        {
            jComboBoxTipodeDocumentoIdentificador.setSelectedIndex(3);
        } 
        
        if(elegidaPersona.getIsPiloto()==1)
        {
            jCheckBoxEsPiloto.setSelected(true);
            jTextFieldNroLicienciaPiloto.setEnabled(true);
            jTextFieldNroLicienciaPiloto.setEditable(true);
            jTextFieldNroSABSA.setEnabled(true);
            jTextFieldNroSABSA.setEditable(true);
            dateChooserComboVencimientoSABSA.setEnabled(true);
            jTextFieldNroCoorporativo.setEnabled(true);
            jTextFieldNroCoorporativo.setEditable(true);
            dateChooserComboVencimientoCoorporativo.setEnabled(true);
            jCheckBoxCertificadoMedico.setEnabled(true);
            jCheckBoxCertificadoMedico.setSelected(false);
            jCheckBoxProficiencyCheck.setEnabled(true);
            jCheckBoxProficiencyCheck.setSelected(false);
            jlabelNumLicenciaDePiloto.setEnabled(true);
            jlabelNumCredencialSABSA.setEnabled(true);
            jlabelNumCredencialCorporativa.setEnabled(true);
            jLabelVencimientoSABSA.setEnabled(true);
            jLabelVencimientoCoorporativa.setEnabled(true);
            jTextFieldNroLicienciaPiloto.setText(String.valueOf(elegidaPersona.getNumLicencia()));
            jTextFieldNroSABSA.setText(String.valueOf(elegidaPersona.getNumCredencialSABSA()));
            jTextFieldNroCoorporativo.setText(String.valueOf(elegidaPersona.getNumCredicialCoorporativo()));
            dateChooserComboVencimientoSABSA.setCurrent(elegidaPersona.getFechaCalendar(elegidaPersona.getFechaVigenciaSABSA()));
            dateChooserComboVencimientoCoorporativo.setCurrent(elegidaPersona.getFechaCalendar(elegidaPersona.getFechaVigenciaCoorporativo()));
            if(elegidaPersona.getFechaVencimientoAptoMedico() != null)
            {
                jCheckBoxCertificadoMedico.setSelected(true);
                jLabelFechaCertificadoMedico.setEnabled(true);
                dateChooserComboVencimientoCertificadoMedico.setEnabled(true);
                dateChooserComboVencimientoCertificadoMedico.setCurrent(elegidaPersona.getFechaCalendar(elegidaPersona.getFechaVencimientoAptoMedico()));
            }
            if(elegidaPersona.getFechaVencimientoProficienceCheck() != null)
            {
                jCheckBoxProficiencyCheck.setSelected(true);
                jLabelVencimientoProficiencyCheck.setEnabled(true);
                dateChooserComboVencimientoProficiencyCheck.setEnabled(true);
                dateChooserComboVencimientoProficiencyCheck.setCurrent(elegidaPersona.getFechaCalendar(elegidaPersona.getFechaVencimientoProficienceCheck()));
            }
        } else
        {
            jCheckBoxEsPiloto.setSelected(false);
        }
        
        con.recuperarReferencias(elegidaPersona.getiD(), referencias);
        crearTablaReferencias();
        llenarTablaReferencias();
    }
    
    public void estadoInicial()
    {
        jTextFieldNombre.setText("");
        jTextFieldApellidoPaterno.setText("");
        jTextFieldApellidoMaterno.setText("");
        dateChooserComboFechaNacimiento.setCurrent(Calendar.getInstance());
        jTextFieldDireccionActual.setText("");
        jTextFieldTelefono.setText("");

        jComboBoxTipoSangre.setSelectedIndex(0);
        jCheckBoxUsaLentes.setSelected(false);
        jComboBoxTipodeDocumentoIdentificador.setSelectedIndex(0);
        jCheckBoxEsPiloto.setSelected(false);
        jTextFieldNroLicienciaPiloto.setEnabled(false);
        jTextFieldNroLicienciaPiloto.setEditable(false);
        jTextFieldNroSABSA.setEnabled(false);
        jTextFieldNroSABSA.setEditable(false);
        dateChooserComboVencimientoSABSA.setEnabled(false);
        jTextFieldNroCoorporativo.setEnabled(false);
        jTextFieldNroCoorporativo.setEditable(false);
        dateChooserComboVencimientoCoorporativo.setEnabled(false);
        jCheckBoxCertificadoMedico.setEnabled(false);
        jCheckBoxCertificadoMedico.setSelected(false);
        dateChooserComboVencimientoCertificadoMedico.setEnabled(false);
        jCheckBoxProficiencyCheck.setEnabled(false);
        jCheckBoxProficiencyCheck.setSelected(false);
        dateChooserComboVencimientoProficiencyCheck.setEnabled(false);   
        jlabelNumLicenciaDePiloto.setEnabled(false);
        jlabelNumCredencialSABSA.setEnabled(false);
        jlabelNumCredencialCorporativa.setEnabled(false);
        jLabelVencimientoSABSA.setEnabled(false);
        jLabelVencimientoCoorporativa.setEnabled(false);
        jLabelFechaCertificadoMedico.setEnabled(false);
        jLabelVencimientoProficiencyCheck.setEnabled(false);
        jTextFieldDocumentoIdentificador.setText("");
        referencias.clear();
        
        crearTablaReferencias();
        llenarTablaReferencias();
        
        
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jComboBox1 = new javax.swing.JComboBox<>();
        JbotonAceptar = new javax.swing.JButton();
        JbotonCancelar = new javax.swing.JButton();
        jTabbedPane1 = new javax.swing.JTabbedPane();
        jPanel1 = new javax.swing.JPanel();
        labelApellido1 = new javax.swing.JLabel();
        jTextFieldApellidoMaterno = new javax.swing.JTextField();
        labelApellido2 = new javax.swing.JLabel();
        jComboBoxTipoSangre = new javax.swing.JComboBox<>();
        dateChooserComboFechaNacimiento = new datechooser.beans.DateChooserCombo();
        labelApellido3 = new javax.swing.JLabel();
        jTextFieldDocumentoIdentificador = new javax.swing.JTextField();
        labelNombre = new javax.swing.JLabel();
        jTextFieldApellidoPaterno = new javax.swing.JTextField();
        jTextFieldNombre = new javax.swing.JTextField();
        labelApellido = new javax.swing.JLabel();
        labelDocumentoDeIdentificacion = new javax.swing.JLabel();
        labelTipo = new javax.swing.JLabel();
        jComboBoxTipodeDocumentoIdentificador = new javax.swing.JComboBox<>();
        jCheckBoxUsaLentes = new javax.swing.JCheckBox();
        jLabel3 = new javax.swing.JLabel();
        jTextFieldDireccionActual = new javax.swing.JTextField();
        jLabel4 = new javax.swing.JLabel();
        jTextFieldTelefono = new javax.swing.JTextField();
        jPanel2 = new javax.swing.JPanel();
        jCheckBoxCertificadoMedico = new javax.swing.JCheckBox();
        jCheckBoxEsPiloto = new javax.swing.JCheckBox();
        dateChooserComboVencimientoCertificadoMedico = new datechooser.beans.DateChooserCombo();
        jTextFieldNroSABSA = new javax.swing.JTextField();
        jlabelNumLicenciaDePiloto = new javax.swing.JLabel();
        jlabelNumCredencialCorporativa = new javax.swing.JLabel();
        dateChooserComboVencimientoSABSA = new datechooser.beans.DateChooserCombo();
        jTextFieldNroCoorporativo = new javax.swing.JTextField();
        dateChooserComboVencimientoCoorporativo = new datechooser.beans.DateChooserCombo();
        jTextFieldNroLicienciaPiloto = new javax.swing.JTextField();
        jLabelVencimientoSABSA = new javax.swing.JLabel();
        jlabelNumCredencialSABSA = new javax.swing.JLabel();
        jCheckBoxProficiencyCheck = new javax.swing.JCheckBox();
        jLabelFechaCertificadoMedico = new javax.swing.JLabel();
        dateChooserComboVencimientoProficiencyCheck = new datechooser.beans.DateChooserCombo();
        jLabelVencimientoCoorporativa = new javax.swing.JLabel();
        jLabelVencimientoProficiencyCheck = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        jPanel3 = new javax.swing.JPanel();
        jButtonEliminarReferencia = new javax.swing.JButton();
        jLabel11 = new javax.swing.JLabel();
        jScrollPane3 = new javax.swing.JScrollPane();
        jTableTelefonos = new javax.swing.JTable();
        jTextFieldTelefonoReferencia = new javax.swing.JTextField();
        jLabel12 = new javax.swing.JLabel();
        jLabel13 = new javax.swing.JLabel();
        jTextFieldTipoDeTelefonoReferencia = new javax.swing.JTextField();
        jTextFieldParentesco = new javax.swing.JTextField();
        jButtonAgregarTelefonoReferencia = new javax.swing.JButton();
        jButtonEliminarTelefonoReferencia = new javax.swing.JButton();
        jButtonAgregarReferencia = new javax.swing.JButton();
        jLabel2 = new javax.swing.JLabel();
        jScrollPane2 = new javax.swing.JScrollPane();
        jTableReferencias = new javax.swing.JTable();
        jTextFieldNombreCompletoReferencia = new javax.swing.JTextField();
        jLabel6 = new javax.swing.JLabel();
        jSeparator1 = new javax.swing.JSeparator();

        jComboBox1.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));

        setIconifiable(true);
        setTitle("Crear Nuevo Docente Piloto");
        setToolTipText("Crear Nuevo Docente Piloto");

        JbotonAceptar.setText("Aceptar");
        JbotonAceptar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                JbotonAceptarActionPerformed(evt);
            }
        });

        JbotonCancelar.setText("Cancelar");
        JbotonCancelar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                JbotonCancelarActionPerformed(evt);
            }
        });

        labelApellido1.setText("Apellido Materno:");

        jTextFieldApellidoMaterno.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jTextFieldApellidoMaternoActionPerformed(evt);
            }
        });
        jTextFieldApellidoMaterno.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                jTextFieldApellidoMaternoKeyTyped(evt);
            }
        });

        labelApellido2.setText("Tipo de Sangre:");

        jComboBoxTipoSangre.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "O RH+", "O RH-", "A RH+", "A RH-", "B RH+", "B RH-", "AB RH+", "AB RH-" }));
        jComboBoxTipoSangre.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jComboBoxTipoSangreActionPerformed(evt);
            }
        });

        dateChooserComboFechaNacimiento.setNothingAllowed(false);
        try {
            dateChooserComboFechaNacimiento.setDefaultPeriods(new datechooser.model.multiple.PeriodSet(new datechooser.model.multiple.Period(new java.util.GregorianCalendar(2000, 0, 1),
                new java.util.GregorianCalendar(2000, 0, 1))));
    } catch (datechooser.model.exeptions.IncompatibleDataExeption e1) {
        e1.printStackTrace();
    }

    labelApellido3.setText("Fecha Nacimiento:");

    jTextFieldDocumentoIdentificador.addActionListener(new java.awt.event.ActionListener() {
        public void actionPerformed(java.awt.event.ActionEvent evt) {
            jTextFieldDocumentoIdentificadorActionPerformed(evt);
        }
    });
    jTextFieldDocumentoIdentificador.addKeyListener(new java.awt.event.KeyAdapter() {
        public void keyTyped(java.awt.event.KeyEvent evt) {
            jTextFieldDocumentoIdentificadorKeyTyped(evt);
        }
    });

    labelNombre.setText("Nombre:");

    jTextFieldApellidoPaterno.addActionListener(new java.awt.event.ActionListener() {
        public void actionPerformed(java.awt.event.ActionEvent evt) {
            jTextFieldApellidoPaternoActionPerformed(evt);
        }
    });
    jTextFieldApellidoPaterno.addKeyListener(new java.awt.event.KeyAdapter() {
        public void keyTyped(java.awt.event.KeyEvent evt) {
            jTextFieldApellidoPaternoKeyTyped(evt);
        }
    });

    jTextFieldNombre.addKeyListener(new java.awt.event.KeyAdapter() {
        public void keyTyped(java.awt.event.KeyEvent evt) {
            jTextFieldNombreKeyTyped(evt);
        }
    });

    labelApellido.setText("Apellido Paterno:");

    labelDocumentoDeIdentificacion.setText("Documento de Identificacion:");

    labelTipo.setText("Tipo:");

    jComboBoxTipodeDocumentoIdentificador.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Libreta Servicio", "CI", "RUC", "Pasaporte" }));
    jComboBoxTipodeDocumentoIdentificador.setSelectedIndex(1);

    jCheckBoxUsaLentes.setText("Usa Lentes");

    jLabel3.setText("Direccion Actual: ");

    jTextFieldDireccionActual.addKeyListener(new java.awt.event.KeyAdapter() {
        public void keyTyped(java.awt.event.KeyEvent evt) {
            jTextFieldDireccionActualKeyTyped(evt);
        }
    });

    jLabel4.setText("Telefono:");

    jTextFieldTelefono.addActionListener(new java.awt.event.ActionListener() {
        public void actionPerformed(java.awt.event.ActionEvent evt) {
            jTextFieldTelefonoActionPerformed(evt);
        }
    });
    jTextFieldTelefono.addKeyListener(new java.awt.event.KeyAdapter() {
        public void keyTyped(java.awt.event.KeyEvent evt) {
            jTextFieldTelefonoKeyTyped(evt);
        }
    });

    javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
    jPanel1.setLayout(jPanel1Layout);
    jPanel1Layout.setHorizontalGroup(
        jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
        .addGroup(jPanel1Layout.createSequentialGroup()
            .addGap(55, 55, 55)
            .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                .addGroup(jPanel1Layout.createSequentialGroup()
                    .addComponent(jLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, 140, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGap(18, 18, 18)
                    .addComponent(jTextFieldDireccionActual))
                .addGroup(jPanel1Layout.createSequentialGroup()
                    .addComponent(labelNombre, javax.swing.GroupLayout.PREFERRED_SIZE, 140, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGap(18, 18, 18)
                    .addComponent(jTextFieldNombre, javax.swing.GroupLayout.PREFERRED_SIZE, 290, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGroup(jPanel1Layout.createSequentialGroup()
                    .addComponent(labelApellido, javax.swing.GroupLayout.PREFERRED_SIZE, 140, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGap(18, 18, 18)
                    .addComponent(jTextFieldApellidoPaterno, javax.swing.GroupLayout.PREFERRED_SIZE, 290, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGroup(jPanel1Layout.createSequentialGroup()
                    .addComponent(labelApellido1, javax.swing.GroupLayout.PREFERRED_SIZE, 140, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGap(18, 18, 18)
                    .addComponent(jTextFieldApellidoMaterno, javax.swing.GroupLayout.PREFERRED_SIZE, 290, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGroup(jPanel1Layout.createSequentialGroup()
                    .addComponent(labelApellido3, javax.swing.GroupLayout.PREFERRED_SIZE, 140, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGap(18, 18, 18)
                    .addComponent(dateChooserComboFechaNacimiento, javax.swing.GroupLayout.PREFERRED_SIZE, 134, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGroup(jPanel1Layout.createSequentialGroup()
                    .addComponent(jLabel4, javax.swing.GroupLayout.PREFERRED_SIZE, 140, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGap(18, 18, 18)
                    .addComponent(jTextFieldTelefono, javax.swing.GroupLayout.PREFERRED_SIZE, 150, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGroup(jPanel1Layout.createSequentialGroup()
                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(jCheckBoxUsaLentes)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                            .addComponent(labelDocumentoDeIdentificacion, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(labelApellido2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                    .addGap(18, 18, 18)
                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(jPanel1Layout.createSequentialGroup()
                            .addComponent(jTextFieldDocumentoIdentificador, javax.swing.GroupLayout.PREFERRED_SIZE, 91, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                            .addComponent(labelTipo)
                            .addGap(18, 18, 18)
                            .addComponent(jComboBoxTipodeDocumentoIdentificador, javax.swing.GroupLayout.PREFERRED_SIZE, 94, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addComponent(jComboBoxTipoSangre, javax.swing.GroupLayout.PREFERRED_SIZE, 63, javax.swing.GroupLayout.PREFERRED_SIZE))))
            .addContainerGap(58, Short.MAX_VALUE))
    );
    jPanel1Layout.setVerticalGroup(
        jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
        .addGroup(jPanel1Layout.createSequentialGroup()
            .addContainerGap()
            .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                .addComponent(labelNombre)
                .addComponent(jTextFieldNombre, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
            .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                .addComponent(labelApellido)
                .addComponent(jTextFieldApellidoPaterno, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
            .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                .addComponent(labelApellido1)
                .addComponent(jTextFieldApellidoMaterno, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
            .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(jPanel1Layout.createSequentialGroup()
                    .addComponent(labelApellido2)
                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                    .addComponent(labelDocumentoDeIdentificacion, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                    .addComponent(jCheckBoxUsaLentes, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGroup(jPanel1Layout.createSequentialGroup()
                    .addComponent(jComboBoxTipoSangre, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jTextFieldDocumentoIdentificador)
                        .addComponent(labelTipo, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jComboBoxTipodeDocumentoIdentificador, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))))
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
            .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                .addComponent(labelApellido3, javax.swing.GroupLayout.PREFERRED_SIZE, 14, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addComponent(dateChooserComboFechaNacimiento, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
            .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                .addComponent(jLabel3)
                .addComponent(jTextFieldDireccionActual, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
            .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                .addComponent(jLabel4, javax.swing.GroupLayout.PREFERRED_SIZE, 17, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addComponent(jTextFieldTelefono, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
            .addGap(207, 207, 207))
    );

    jTabbedPane1.addTab("Datos Personales", jPanel1);

    jCheckBoxCertificadoMedico.setText("Certificado Medico");
    jCheckBoxCertificadoMedico.addChangeListener(new javax.swing.event.ChangeListener() {
        public void stateChanged(javax.swing.event.ChangeEvent evt) {
            jCheckBoxCertificadoMedicoStateChanged(evt);
        }
    });
    jCheckBoxCertificadoMedico.addActionListener(new java.awt.event.ActionListener() {
        public void actionPerformed(java.awt.event.ActionEvent evt) {
            jCheckBoxCertificadoMedicoActionPerformed(evt);
        }
    });

    jCheckBoxEsPiloto.setText("Es Piloto");
    jCheckBoxEsPiloto.addChangeListener(new javax.swing.event.ChangeListener() {
        public void stateChanged(javax.swing.event.ChangeEvent evt) {
            jCheckBoxEsPilotoStateChanged(evt);
        }
    });
    jCheckBoxEsPiloto.addActionListener(new java.awt.event.ActionListener() {
        public void actionPerformed(java.awt.event.ActionEvent evt) {
            jCheckBoxEsPilotoActionPerformed(evt);
        }
    });

    jTextFieldNroSABSA.addActionListener(new java.awt.event.ActionListener() {
        public void actionPerformed(java.awt.event.ActionEvent evt) {
            jTextFieldNroSABSAActionPerformed(evt);
        }
    });
    jTextFieldNroSABSA.addKeyListener(new java.awt.event.KeyAdapter() {
        public void keyTyped(java.awt.event.KeyEvent evt) {
            jTextFieldNroSABSAKeyTyped(evt);
        }
    });

    jlabelNumLicenciaDePiloto.setText("Nº Licencia de Piloto:");

    jlabelNumCredencialCorporativa.setText("Nº Credencial Coorporativa:");

    jTextFieldNroCoorporativo.addActionListener(new java.awt.event.ActionListener() {
        public void actionPerformed(java.awt.event.ActionEvent evt) {
            jTextFieldNroCoorporativoActionPerformed(evt);
        }
    });
    jTextFieldNroCoorporativo.addKeyListener(new java.awt.event.KeyAdapter() {
        public void keyTyped(java.awt.event.KeyEvent evt) {
            jTextFieldNroCoorporativoKeyTyped(evt);
        }
    });

    jTextFieldNroLicienciaPiloto.addActionListener(new java.awt.event.ActionListener() {
        public void actionPerformed(java.awt.event.ActionEvent evt) {
            jTextFieldNroLicienciaPilotoActionPerformed(evt);
        }
    });
    jTextFieldNroLicienciaPiloto.addKeyListener(new java.awt.event.KeyAdapter() {
        public void keyTyped(java.awt.event.KeyEvent evt) {
            jTextFieldNroLicienciaPilotoKeyTyped(evt);
        }
    });

    jLabelVencimientoSABSA.setText("Vence:");

    jlabelNumCredencialSABSA.setText("Nº Credencial SABSA:");

    jCheckBoxProficiencyCheck.setText("Proficiency Check");
    jCheckBoxProficiencyCheck.addChangeListener(new javax.swing.event.ChangeListener() {
        public void stateChanged(javax.swing.event.ChangeEvent evt) {
            jCheckBoxProficiencyCheckStateChanged(evt);
        }
    });
    jCheckBoxProficiencyCheck.addActionListener(new java.awt.event.ActionListener() {
        public void actionPerformed(java.awt.event.ActionEvent evt) {
            jCheckBoxProficiencyCheckActionPerformed(evt);
        }
    });

    jLabelFechaCertificadoMedico.setText("Fecha Vencimiento:");

    jLabelVencimientoCoorporativa.setText("Vence:");

    jLabelVencimientoProficiencyCheck.setText("Fecha Vencimiento:");

    javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
    jPanel2.setLayout(jPanel2Layout);
    jPanel2Layout.setHorizontalGroup(
        jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
        .addGroup(jPanel2Layout.createSequentialGroup()
            .addGap(70, 70, 70)
            .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addComponent(jCheckBoxEsPiloto, javax.swing.GroupLayout.PREFERRED_SIZE, 390, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGroup(jPanel2Layout.createSequentialGroup()
                    .addComponent(jCheckBoxCertificadoMedico, javax.swing.GroupLayout.PREFERRED_SIZE, 139, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                    .addComponent(jLabelFechaCertificadoMedico)
                    .addGap(18, 18, 18)
                    .addComponent(dateChooserComboVencimientoCertificadoMedico, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGroup(jPanel2Layout.createSequentialGroup()
                    .addComponent(jCheckBoxProficiencyCheck, javax.swing.GroupLayout.PREFERRED_SIZE, 139, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                    .addComponent(jLabelVencimientoProficiencyCheck)
                    .addGap(18, 18, 18)
                    .addComponent(dateChooserComboVencimientoProficiencyCheck, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGroup(jPanel2Layout.createSequentialGroup()
                    .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                        .addComponent(jlabelNumCredencialSABSA, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jlabelNumLicenciaDePiloto, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jlabelNumCredencialCorporativa, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                    .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(jPanel2Layout.createSequentialGroup()
                            .addComponent(jTextFieldNroSABSA, javax.swing.GroupLayout.PREFERRED_SIZE, 97, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGap(18, 18, 18)
                            .addComponent(jLabelVencimientoSABSA)
                            .addGap(18, 18, 18)
                            .addComponent(dateChooserComboVencimientoSABSA, javax.swing.GroupLayout.PREFERRED_SIZE, 99, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGroup(jPanel2Layout.createSequentialGroup()
                            .addComponent(jTextFieldNroCoorporativo, javax.swing.GroupLayout.PREFERRED_SIZE, 97, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGap(18, 18, 18)
                            .addComponent(jLabelVencimientoCoorporativa)
                            .addGap(18, 18, 18)
                            .addComponent(dateChooserComboVencimientoCoorporativo, javax.swing.GroupLayout.PREFERRED_SIZE, 99, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addComponent(jTextFieldNroLicienciaPiloto, javax.swing.GroupLayout.PREFERRED_SIZE, 97, javax.swing.GroupLayout.PREFERRED_SIZE))))
            .addContainerGap(87, Short.MAX_VALUE))
    );
    jPanel2Layout.setVerticalGroup(
        jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
        .addGroup(jPanel2Layout.createSequentialGroup()
            .addContainerGap()
            .addComponent(jCheckBoxEsPiloto)
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
            .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                .addComponent(jlabelNumLicenciaDePiloto)
                .addComponent(jTextFieldNroLicienciaPiloto, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
            .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jlabelNumCredencialCorporativa)
                    .addComponent(jTextFieldNroCoorporativo)
                    .addComponent(jLabelVencimientoCoorporativa))
                .addComponent(dateChooserComboVencimientoCoorporativo, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
            .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jlabelNumCredencialSABSA)
                    .addComponent(jTextFieldNroSABSA, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabelVencimientoSABSA))
                .addComponent(dateChooserComboVencimientoSABSA, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
            .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jCheckBoxCertificadoMedico)
                    .addComponent(jLabelFechaCertificadoMedico))
                .addComponent(dateChooserComboVencimientoCertificadoMedico, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
            .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jCheckBoxProficiencyCheck, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabelVencimientoProficiencyCheck))
                .addGroup(jPanel2Layout.createSequentialGroup()
                    .addGap(6, 6, 6)
                    .addComponent(dateChooserComboVencimientoProficiencyCheck, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
            .addContainerGap(256, Short.MAX_VALUE))
    );

    jTabbedPane1.addTab("Datos Pilotos", jPanel2);

    jButtonEliminarReferencia.setText("Eliminar Referencia");

    jLabel11.setText("Telefono : ");

    jTableTelefonos.setModel(new javax.swing.table.DefaultTableModel(
        new Object [][] {

        },
        new String [] {
            "Telefono", "Ubicacion"
        }
    ) {
        Class[] types = new Class [] {
            java.lang.Integer.class, java.lang.String.class
        };
        boolean[] canEdit = new boolean [] {
            false, false
        };

        public Class getColumnClass(int columnIndex) {
            return types [columnIndex];
        }

        public boolean isCellEditable(int rowIndex, int columnIndex) {
            return canEdit [columnIndex];
        }
    });
    jScrollPane3.setViewportView(jTableTelefonos);

    jTextFieldTelefonoReferencia.addKeyListener(new java.awt.event.KeyAdapter() {
        public void keyTyped(java.awt.event.KeyEvent evt) {
            jTextFieldTelefonoReferenciaKeyTyped(evt);
        }
    });

    jLabel12.setText("Tipo Telefono: ");

    jLabel13.setText("Parentesco: ");

    jTextFieldTipoDeTelefonoReferencia.addKeyListener(new java.awt.event.KeyAdapter() {
        public void keyTyped(java.awt.event.KeyEvent evt) {
            jTextFieldTipoDeTelefonoReferenciaKeyTyped(evt);
        }
    });

    jTextFieldParentesco.addKeyListener(new java.awt.event.KeyAdapter() {
        public void keyTyped(java.awt.event.KeyEvent evt) {
            jTextFieldParentescoKeyTyped(evt);
        }
    });

    jButtonAgregarTelefonoReferencia.setText("Agregar Telefono");

    jButtonEliminarTelefonoReferencia.setText("Eliminar Telefono");

    jButtonAgregarReferencia.setText("Agregar Referencia");
    jButtonAgregarReferencia.addActionListener(new java.awt.event.ActionListener() {
        public void actionPerformed(java.awt.event.ActionEvent evt) {
            jButtonAgregarReferenciaActionPerformed(evt);
        }
    });

    jLabel2.setText("Nombre completo : ");

    jTableReferencias.setModel(new javax.swing.table.DefaultTableModel(
        new Object [][] {

        },
        new String [] {
            "Nombre Referencia", "Telefonos"
        }
    ) {
        Class[] types = new Class [] {
            java.lang.String.class, java.lang.String.class
        };
        boolean[] canEdit = new boolean [] {
            false, false
        };

        public Class getColumnClass(int columnIndex) {
            return types [columnIndex];
        }

        public boolean isCellEditable(int rowIndex, int columnIndex) {
            return canEdit [columnIndex];
        }
    });
    jScrollPane2.setViewportView(jTableReferencias);

    jTextFieldNombreCompletoReferencia.addActionListener(new java.awt.event.ActionListener() {
        public void actionPerformed(java.awt.event.ActionEvent evt) {
            jTextFieldNombreCompletoReferenciaActionPerformed(evt);
        }
    });
    jTextFieldNombreCompletoReferencia.addKeyListener(new java.awt.event.KeyAdapter() {
        public void keyTyped(java.awt.event.KeyEvent evt) {
            jTextFieldNombreCompletoReferenciaKeyTyped(evt);
        }
    });

    jLabel6.setText("Referencias actuales del Docente/Piloto");

    javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
    jPanel3.setLayout(jPanel3Layout);
    jPanel3Layout.setHorizontalGroup(
        jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
        .addGroup(jPanel3Layout.createSequentialGroup()
            .addGap(67, 67, 67)
            .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jButtonEliminarReferencia, javax.swing.GroupLayout.PREFERRED_SIZE, 404, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                        .addGroup(jPanel3Layout.createSequentialGroup()
                            .addGap(99, 99, 99)
                            .addComponent(jLabel6))
                        .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
                        .addComponent(jSeparator1)
                        .addComponent(jScrollPane3, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
                        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel3Layout.createSequentialGroup()
                            .addComponent(jButtonAgregarTelefonoReferencia, javax.swing.GroupLayout.PREFERRED_SIZE, 179, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jButtonEliminarTelefonoReferencia, javax.swing.GroupLayout.PREFERRED_SIZE, 196, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGroup(jPanel3Layout.createSequentialGroup()
                            .addComponent(jLabel11)
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                            .addComponent(jTextFieldTelefonoReferencia, javax.swing.GroupLayout.PREFERRED_SIZE, 111, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGap(18, 18, 18)
                            .addComponent(jLabel12)
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                            .addComponent(jTextFieldTipoDeTelefonoReferencia))
                        .addComponent(jButtonAgregarReferencia, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 404, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel3Layout.createSequentialGroup()
                    .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(jLabel2)
                        .addComponent(jLabel13, javax.swing.GroupLayout.PREFERRED_SIZE, 93, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                    .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(jTextFieldParentesco)
                        .addComponent(jTextFieldNombreCompletoReferencia))))
            .addContainerGap(88, Short.MAX_VALUE))
    );
    jPanel3Layout.setVerticalGroup(
        jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
        .addGroup(jPanel3Layout.createSequentialGroup()
            .addContainerGap()
            .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                .addComponent(jLabel2)
                .addComponent(jTextFieldNombreCompletoReferencia, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
            .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                .addComponent(jTextFieldParentesco, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addComponent(jLabel13))
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
            .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                .addComponent(jLabel11)
                .addComponent(jTextFieldTelefonoReferencia, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addComponent(jLabel12)
                .addComponent(jTextFieldTipoDeTelefonoReferencia, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
            .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                .addComponent(jButtonAgregarTelefonoReferencia)
                .addComponent(jButtonEliminarTelefonoReferencia))
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
            .addComponent(jScrollPane3, javax.swing.GroupLayout.PREFERRED_SIZE, 92, javax.swing.GroupLayout.PREFERRED_SIZE)
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
            .addComponent(jButtonAgregarReferencia)
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
            .addComponent(jSeparator1, javax.swing.GroupLayout.PREFERRED_SIZE, 10, javax.swing.GroupLayout.PREFERRED_SIZE)
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
            .addComponent(jLabel6)
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
            .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 123, javax.swing.GroupLayout.PREFERRED_SIZE)
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
            .addComponent(jButtonEliminarReferencia)
            .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
    );

    jScrollPane1.setViewportView(jPanel3);

    jTabbedPane1.addTab("Datos Referencias", jScrollPane1);

    javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
    getContentPane().setLayout(layout);
    layout.setHorizontalGroup(
        layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
        .addGroup(layout.createSequentialGroup()
            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(layout.createSequentialGroup()
                    .addContainerGap()
                    .addComponent(JbotonAceptar)
                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(JbotonCancelar))
                .addComponent(jTabbedPane1))
            .addContainerGap())
    );
    layout.setVerticalGroup(
        layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
        .addGroup(layout.createSequentialGroup()
            .addComponent(jTabbedPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 448, javax.swing.GroupLayout.PREFERRED_SIZE)
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                .addComponent(JbotonAceptar)
                .addComponent(JbotonCancelar))
            .addContainerGap())
    );

    pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jCheckBoxCertificadoMedicoStateChanged(javax.swing.event.ChangeEvent evt) {//GEN-FIRST:event_jCheckBoxCertificadoMedicoStateChanged
    
    }//GEN-LAST:event_jCheckBoxCertificadoMedicoStateChanged

    private void jTextFieldNroSABSAActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jTextFieldNroSABSAActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jTextFieldNroSABSAActionPerformed

    private void jTextFieldNroSABSAKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTextFieldNroSABSAKeyTyped
        char c = evt.getKeyChar();
        if(Character.isDigit(c) && jTextFieldNroSABSA.getText().length()<9)
        {
            evt = evt;
        }
        else
        {
            evt.consume();
        }
    }//GEN-LAST:event_jTextFieldNroSABSAKeyTyped

    private void JbotonAceptarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_JbotonAceptarActionPerformed
 
    }//GEN-LAST:event_JbotonAceptarActionPerformed

    private void JbotonCancelarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_JbotonCancelarActionPerformed

    }//GEN-LAST:event_JbotonCancelarActionPerformed

    private void jTextFieldNroCoorporativoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jTextFieldNroCoorporativoActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jTextFieldNroCoorporativoActionPerformed

    private void jTextFieldNroCoorporativoKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTextFieldNroCoorporativoKeyTyped
        char c = evt.getKeyChar();
        if(Character.isDigit(c) && jTextFieldNroCoorporativo.getText().length()<9)
        {
            evt = evt;
        }
        else
        {
            evt.consume();
        }
    }//GEN-LAST:event_jTextFieldNroCoorporativoKeyTyped

    private void jTextFieldNroLicienciaPilotoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jTextFieldNroLicienciaPilotoActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jTextFieldNroLicienciaPilotoActionPerformed

    private void jTextFieldNroLicienciaPilotoKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTextFieldNroLicienciaPilotoKeyTyped
        char c = evt.getKeyChar();
        if(Character.isDigit(c)  && jTextFieldNroLicienciaPiloto.getText().length()<9)
        {
            evt = evt;
        }
        else
        {
            evt.consume();
        }
    }//GEN-LAST:event_jTextFieldNroLicienciaPilotoKeyTyped

    private void jCheckBoxProficiencyCheckStateChanged(javax.swing.event.ChangeEvent evt) {//GEN-FIRST:event_jCheckBoxProficiencyCheckStateChanged
 
    }//GEN-LAST:event_jCheckBoxProficiencyCheckStateChanged

    private void jCheckBoxEsPilotoStateChanged(javax.swing.event.ChangeEvent evt) {//GEN-FIRST:event_jCheckBoxEsPilotoStateChanged
        
    }//GEN-LAST:event_jCheckBoxEsPilotoStateChanged

    private void jCheckBoxEsPilotoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jCheckBoxEsPilotoActionPerformed
        if(jCheckBoxEsPiloto.isSelected())
        {
            jTextFieldNroLicienciaPiloto.setEnabled(true);
            jTextFieldNroLicienciaPiloto.setEditable(true);
            jTextFieldNroSABSA.setEnabled(true);
            jTextFieldNroSABSA.setEditable(true);
            dateChooserComboVencimientoSABSA.setEnabled(true);
            jTextFieldNroCoorporativo.setEnabled(true);
            jTextFieldNroCoorporativo.setEditable(true);
            dateChooserComboVencimientoCoorporativo.setEnabled(true);
            jCheckBoxCertificadoMedico.setEnabled(true);
            jCheckBoxCertificadoMedico.setSelected(false);
            jCheckBoxProficiencyCheck.setEnabled(true);
            jCheckBoxProficiencyCheck.setSelected(false);
            jlabelNumLicenciaDePiloto.setEnabled(true);
            jlabelNumCredencialSABSA.setEnabled(true);
            jlabelNumCredencialCorporativa.setEnabled(true);
            jLabelVencimientoSABSA.setEnabled(true);
            jLabelVencimientoCoorporativa.setEnabled(true);
        }
        else{
            jTextFieldNroLicienciaPiloto.setEnabled(false);
            jTextFieldNroLicienciaPiloto.setEditable(false);
            jTextFieldNroSABSA.setEnabled(false);
            jTextFieldNroSABSA.setEditable(false);
            dateChooserComboVencimientoSABSA.setEnabled(false);
            jTextFieldNroCoorporativo.setEnabled(false);
            jTextFieldNroCoorporativo.setEditable(false);
            dateChooserComboVencimientoCoorporativo.setEnabled(false);
            jCheckBoxCertificadoMedico.setEnabled(false);
            jCheckBoxCertificadoMedico.setSelected(false);
            dateChooserComboVencimientoCertificadoMedico.setEnabled(false);
            jCheckBoxProficiencyCheck.setEnabled(false);
            jCheckBoxProficiencyCheck.setSelected(false);
            dateChooserComboVencimientoProficiencyCheck.setEnabled(false);   
            jlabelNumLicenciaDePiloto.setEnabled(false);
            jlabelNumCredencialSABSA.setEnabled(false);
            jlabelNumCredencialCorporativa.setEnabled(false);
            jLabelVencimientoSABSA.setEnabled(false);
            jLabelVencimientoCoorporativa.setEnabled(false);
            jLabelFechaCertificadoMedico.setEnabled(false);
            jLabelVencimientoProficiencyCheck.setEnabled(false);
            
        }
    }//GEN-LAST:event_jCheckBoxEsPilotoActionPerformed

    private void jButtonAgregarReferenciaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonAgregarReferenciaActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jButtonAgregarReferenciaActionPerformed

    private void jCheckBoxCertificadoMedicoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jCheckBoxCertificadoMedicoActionPerformed
        if(jCheckBoxCertificadoMedico.isSelected())
        {
           jLabelFechaCertificadoMedico.setEnabled(true);
           dateChooserComboVencimientoCertificadoMedico.setEnabled(true);
        }
        else
        {
           jLabelFechaCertificadoMedico.setEnabled(false);
           dateChooserComboVencimientoCertificadoMedico.setEnabled(false);
        }
    }//GEN-LAST:event_jCheckBoxCertificadoMedicoActionPerformed

    private void jCheckBoxProficiencyCheckActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jCheckBoxProficiencyCheckActionPerformed
        if(jCheckBoxProficiencyCheck.isSelected())
        {
           jLabelVencimientoProficiencyCheck.setEnabled(true);
           dateChooserComboVencimientoProficiencyCheck.setEnabled(true);
        }
        else
        {
           jLabelVencimientoProficiencyCheck.setEnabled(false);
           dateChooserComboVencimientoProficiencyCheck.setEnabled(false);
        }
    }//GEN-LAST:event_jCheckBoxProficiencyCheckActionPerformed

    private void jTextFieldTelefonoKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTextFieldTelefonoKeyTyped
        char c = evt.getKeyChar();
        if(Character.isDigit(c) && jTextFieldTelefono.getText().length()<11)
        {
            evt = evt;
        }
        else
        {
            evt.consume();
        }
    }//GEN-LAST:event_jTextFieldTelefonoKeyTyped

    private void jTextFieldTelefonoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jTextFieldTelefonoActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jTextFieldTelefonoActionPerformed

    private void jTextFieldApellidoPaternoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jTextFieldApellidoPaternoActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jTextFieldApellidoPaternoActionPerformed

    private void jTextFieldDocumentoIdentificadorKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTextFieldDocumentoIdentificadorKeyTyped
        char c = evt.getKeyChar();
        if(Character.isDigit(c) && jTextFieldDocumentoIdentificador.getText().length()<11)
        {
            evt = evt;
        }
        else
        {
            evt.consume();
        }
    }//GEN-LAST:event_jTextFieldDocumentoIdentificadorKeyTyped

    private void jTextFieldDocumentoIdentificadorActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jTextFieldDocumentoIdentificadorActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jTextFieldDocumentoIdentificadorActionPerformed

    private void jComboBoxTipoSangreActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jComboBoxTipoSangreActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jComboBoxTipoSangreActionPerformed

    private void jTextFieldApellidoMaternoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jTextFieldApellidoMaternoActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jTextFieldApellidoMaternoActionPerformed

    private void jTextFieldNombreKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTextFieldNombreKeyTyped
        char c = evt.getKeyChar();
        if(Character.isLetter(c) || Character.isSpaceChar(c))
        {
            evt = evt;
        }
        else
        {
            evt.consume();
        }
    }//GEN-LAST:event_jTextFieldNombreKeyTyped

    private void jTextFieldApellidoPaternoKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTextFieldApellidoPaternoKeyTyped
        char c = evt.getKeyChar();
        if(Character.isLetter(c)  || Character.isSpaceChar(c))
        {
            evt = evt;
        }
        else
        {
            evt.consume();
        }
    }//GEN-LAST:event_jTextFieldApellidoPaternoKeyTyped

    private void jTextFieldApellidoMaternoKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTextFieldApellidoMaternoKeyTyped
        char c = evt.getKeyChar();
        if(Character.isLetter(c) || Character.isSpaceChar(c))
        {
            evt = evt;
        }
        else
        {
            evt.consume();
        }
    }//GEN-LAST:event_jTextFieldApellidoMaternoKeyTyped

    private void jTextFieldDireccionActualKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTextFieldDireccionActualKeyTyped
        // TODO add your handling code here:
    }//GEN-LAST:event_jTextFieldDireccionActualKeyTyped

    private void jTextFieldNombreCompletoReferenciaKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTextFieldNombreCompletoReferenciaKeyTyped
        char c = evt.getKeyChar();
        if(Character.isLetter(c) || Character.isSpaceChar(c))
        {
            evt = evt;
        }
        else
        {
            evt.consume();
        }
    }//GEN-LAST:event_jTextFieldNombreCompletoReferenciaKeyTyped

    private void jTextFieldParentescoKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTextFieldParentescoKeyTyped
        char c = evt.getKeyChar();
        if(Character.isLetter(c))
        {
            evt = evt;
        }
        else
        {
            evt.consume();
        }
    }//GEN-LAST:event_jTextFieldParentescoKeyTyped

    private void jTextFieldTelefonoReferenciaKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTextFieldTelefonoReferenciaKeyTyped
        char c = evt.getKeyChar();
        if(Character.isDigit(c) && jTextFieldTelefonoReferencia.getText().length()<8)
        {
            evt = evt;
        }
        else
        {
            evt.consume();
        }
    }//GEN-LAST:event_jTextFieldTelefonoReferenciaKeyTyped

    private void jTextFieldTipoDeTelefonoReferenciaKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTextFieldTipoDeTelefonoReferenciaKeyTyped
        char c = evt.getKeyChar();
        if(Character.isLetter(c))
        {
            evt = evt;
        }
        else
        {
            evt.consume();
        }
    }//GEN-LAST:event_jTextFieldTipoDeTelefonoReferenciaKeyTyped

    private void jTextFieldNombreCompletoReferenciaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jTextFieldNombreCompletoReferenciaActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jTextFieldNombreCompletoReferenciaActionPerformed
    public void actualizarReferencia()
    {
        if(elegida != null)
        {
            jTextFieldNombreCompletoReferencia.setText(elegida.getNombre());
            crearTablaTelefonos(elegida.getTelefono());
            llenarTablaTelefonos(elegida.getTelefono());
        }
        else{
            jTextFieldNombreCompletoReferencia.setText("");
            jTextFieldParentesco.setText("");
            crearTablaTelefonos(telefonoReferencia);
            llenarTablaTelefonos(telefonoReferencia);
        }
    }
    public void crearTablaTelefonos(LinkedList<TelefonoReferencia> tel)
    {
        int tam = tel.size();
        jTableTelefonos = new javax.swing.JTable();

        jTableTelefonos.setModel(new javax.swing.table.DefaultTableModel(
            new Object [tam][2],
            new String [] {
                "Telefono", "Ubicacion"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.Integer.class, java.lang.String.class
            };
            boolean[] canEdit = new boolean [] {
                false, false
            };

            @Override
            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            @Override
            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jScrollPane3.setViewportView(jTableTelefonos);

        if (jTableTelefonos.getColumnModel().getColumnCount() > 0) {
            jTableTelefonos.getColumnModel().getColumn(0).setResizable(false);
            jTableTelefonos.getColumnModel().getColumn(1).setResizable(false);
        }

    }
    public void llenarTablaTelefonos(LinkedList<TelefonoReferencia> tel)
    {
        DefaultTableModel modelo = (DefaultTableModel)jTableTelefonos.getModel();
        
        if(!tel.isEmpty())
        {
            for (int i = 0; i < tel.size(); i++) {
                TelefonoReferencia aux = tel.get(i);
                modelo.setValueAt(aux.getNumTelefono(), i, 0);
                modelo.setValueAt(aux.getLocalizacion(), i, 1);
            }
        }
    }
      public void llenarTablaReferencias()
    {
         DefaultTableModel modelo = (DefaultTableModel) jTableReferencias.getModel();
        
        if(!referencias.isEmpty())
        {
            for(int i=0; i<referencias.size(); i++)
            {
                Referencia aux = referencias.get(i);
                modelo.setValueAt(aux.getNombre(), i, 0);
                modelo.setValueAt(aux.getParentesco(), i, 1);
                String telefonos = "";
                LinkedList<TelefonoReferencia> telfAux = aux.getTelefono();
                for (int j = 0; j < telfAux.size(); j++) {
                    if(j ==  (telfAux.size()-1))
                    {
                        telefonos += telfAux.get(j).getNumTelefono()+"";
                    }
                    else{
                        telefonos += telfAux.get(j).getNumTelefono()+"-";
                    }              
                }
                modelo.setValueAt(telefonos, i, 2);       
            }
            
            
        }
    }
    public void crearTablaReferencias()
    {
        int tam = referencias.size();
        
        jTableReferencias = new javax.swing.JTable();

        jTableReferencias.setModel(new javax.swing.table.DefaultTableModel(
            new Object [tam][3],
            new String [] {
                "Nombre Referencia","Parentesco", "Telefonos"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.String.class,java.lang.String.class, java.lang.String.class
            };
            boolean[] canEdit = new boolean [] {
                false, false, false
            };

            @Override
            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            @Override
            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });



        jScrollPane2.setViewportView(jTableReferencias);

        if (jTableReferencias.getColumnModel().getColumnCount() > 0) {
            jTableReferencias.getColumnModel().getColumn(0).setResizable(false);
            jTableReferencias.getColumnModel().getColumn(1).setResizable(false);
            jTableReferencias.getColumnModel().getColumn(2).setResizable(false);
        }

    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    public javax.swing.JButton JbotonAceptar;
    public javax.swing.JButton JbotonCancelar;
    public datechooser.beans.DateChooserCombo dateChooserComboFechaNacimiento;
    public datechooser.beans.DateChooserCombo dateChooserComboVencimientoCertificadoMedico;
    public datechooser.beans.DateChooserCombo dateChooserComboVencimientoCoorporativo;
    public datechooser.beans.DateChooserCombo dateChooserComboVencimientoProficiencyCheck;
    public datechooser.beans.DateChooserCombo dateChooserComboVencimientoSABSA;
    public javax.swing.JButton jButtonAgregarReferencia;
    public javax.swing.JButton jButtonAgregarTelefonoReferencia;
    public javax.swing.JButton jButtonEliminarReferencia;
    public javax.swing.JButton jButtonEliminarTelefonoReferencia;
    public javax.swing.JCheckBox jCheckBoxCertificadoMedico;
    public javax.swing.JCheckBox jCheckBoxEsPiloto;
    public javax.swing.JCheckBox jCheckBoxProficiencyCheck;
    public javax.swing.JCheckBox jCheckBoxUsaLentes;
    private javax.swing.JComboBox<String> jComboBox1;
    public javax.swing.JComboBox<String> jComboBoxTipoSangre;
    public javax.swing.JComboBox<String> jComboBoxTipodeDocumentoIdentificador;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabelFechaCertificadoMedico;
    private javax.swing.JLabel jLabelVencimientoCoorporativa;
    private javax.swing.JLabel jLabelVencimientoProficiencyCheck;
    private javax.swing.JLabel jLabelVencimientoSABSA;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JTabbedPane jTabbedPane1;
    public javax.swing.JTable jTableReferencias;
    public javax.swing.JTable jTableTelefonos;
    public javax.swing.JTextField jTextFieldApellidoMaterno;
    public javax.swing.JTextField jTextFieldApellidoPaterno;
    public javax.swing.JTextField jTextFieldDireccionActual;
    public javax.swing.JTextField jTextFieldDocumentoIdentificador;
    public javax.swing.JTextField jTextFieldNombre;
    public javax.swing.JTextField jTextFieldNombreCompletoReferencia;
    public javax.swing.JTextField jTextFieldNroCoorporativo;
    public javax.swing.JTextField jTextFieldNroLicienciaPiloto;
    public javax.swing.JTextField jTextFieldNroSABSA;
    public javax.swing.JTextField jTextFieldParentesco;
    public javax.swing.JTextField jTextFieldTelefono;
    public javax.swing.JTextField jTextFieldTelefonoReferencia;
    public javax.swing.JTextField jTextFieldTipoDeTelefonoReferencia;
    private javax.swing.JLabel jlabelNumCredencialCorporativa;
    private javax.swing.JLabel jlabelNumCredencialSABSA;
    private javax.swing.JLabel jlabelNumLicenciaDePiloto;
    private javax.swing.JLabel labelApellido;
    private javax.swing.JLabel labelApellido1;
    private javax.swing.JLabel labelApellido2;
    private javax.swing.JLabel labelApellido3;
    private javax.swing.JLabel labelDocumentoDeIdentificacion;
    private javax.swing.JLabel labelNombre;
    private javax.swing.JLabel labelTipo;
    // End of variables declaration//GEN-END:variables
}
