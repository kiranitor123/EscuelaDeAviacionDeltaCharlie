/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package deltacharlie;

import entity.CRTVuelo;
import entity.CrearModificarBorrar;
import entity.Persona;
import entity.Referencia;
import entity.TelefonoReferencia;
import java.awt.Image;
import java.io.File;
import java.io.IOException;
import java.sql.SQLException;

import java.util.Calendar;

import java.util.LinkedList;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.swing.DefaultComboBoxModel;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;
import modelo.conexion;

/**
 *
 * @author Ritha Carolina
 */
public class JFrameInternalCrearAlumno extends javax.swing.JInternalFrame {

    /**
     * Creates new form JFrameInternalCrearAlumno
     */
    public LinkedList<CRTVuelo> mallas;
    public CRTVuelo mallaElegida = null;
    public LinkedList<Referencia> referencias,buscadas;
    public Referencia elegida = null;
    public LinkedList<TelefonoReferencia> telefonoReferencia;
    public Icon tipoIcon;
    public ImageIcon tipoImageIcon;
    
    public int idReferenciaElegida;
    public Persona elegidaPersona;
    public CrearModificarBorrar cmbActual;
    private conexion con;
    public String palabra;
    public DefaultComboBoxModel combomodel;
    
    public JFrameInternalCrearAlumno(conexion con, CrearModificarBorrar cmbActual) throws SQLException {
        initComponents();
        this.con = con;
        mallas = new LinkedList<>();
        referencias = new LinkedList<>();
        jTextFieldURLFoto.setText(null);
        
        buscadas = new LinkedList<>();
        telefonoReferencia = new LinkedList<>();
        this.cmbActual = cmbActual;
        elegidaPersona = null;

        if(cmbActual == CrearModificarBorrar.CREAR)
        {
            
        }
        if(cmbActual == CrearModificarBorrar.MODIFICAR)
        {
            botonAceptar.setText("Modificar");
        }
        if(cmbActual == CrearModificarBorrar.BORRAR)
        {
            botonAceptar.setText("Eliminar");
            jButtonAgregarReferencia.setVisible(false);
            jButtonAgregarTelefonoReferencia.setVisible(false);
            jButtonBuscarFoto.setVisible(false);
            jButtonEliminarReferencia.setVisible(false);
            jButtonEliminarTelefonoReferencia.setVisible(false);
            jTextFieldURLFoto.setVisible(false);
        }
    }
    
    public void llenarPersona() throws SQLException
    {
        if(elegidaPersona!=null)
        {
            jTextFieldNombre.setText(elegidaPersona.getNombre());
            jTextFieldApellidoPaterno.setText(elegidaPersona.getApellidoPaterno());
            jTextFieldApellidoMaterno.setText(elegidaPersona.getApellidoMaterno());
            jTextFieldNumeroDocumentoID.setText(elegidaPersona.getCiRucPas().split("-")[0]);
            jCheckBoxUsaLentes.setSelected(false);
            if(elegidaPersona.getUsaLentes() == 1)
            {
                jCheckBoxUsaLentes.setSelected(true);
            }
            switch(elegidaPersona.getTipoDocumento())
            {
                case 1:
                {
                    jComboBoxTipoDocumento.setSelectedItem("CI");
                    break;
                }
                case 2:
                {
                    jComboBoxTipoDocumento.setSelectedItem("RUC");
                    break;
                }
                case 3:
                {
                    jComboBoxTipoDocumento.setSelectedItem("Libreta Servicio Militar");
                    break;
                }
                case 4:
                {
                    jComboBoxTipoDocumento.setSelectedItem("Pasaporte");
                    break;
                }
                default:
                {
                    jComboBoxTipoDocumento.setSelectedItem("--------");
                    break;
                }
            }
            jTextFieldDireccionActual.setText(elegidaPersona.getDireccion());

            if(elegidaPersona.getFoto()!=null)
            {
                ImageIcon image = elegidaPersona.getFoto();
                Image im = image.getImage();
                Image myImg = im.getScaledInstance(jLabelFotoEstudiante6.getWidth(), jLabelFotoEstudiante6.getHeight(),Image.SCALE_SMOOTH);
                ImageIcon newImage = new ImageIcon(myImg);
                jLabelFotoEstudiante6.setIcon(newImage);
            }
            dateChooserComboFechaNacimiento.setSelectedDate(elegidaPersona.getFechaCalendar(elegidaPersona.getFechaNacimiento()));
            if(elegidaPersona.getTelefono() != 0)
                jTextFieldTelefono.setText(elegidaPersona.getTelefono()+"");

            if(elegidaPersona.getNumFolio()!=0)
            {
                jCheckBoxNumFolio.setSelected(true);
                jTextFieldNumFolio.setText(elegidaPersona.getNumFolio()+"");
            }
            if(elegidaPersona.getNumLicencia()!=0)
            {
                jCheckBoxLicenciaPiloto.setSelected(true);
                jTextFieldNumeroLicenciaPiloto.setText(elegidaPersona.getNumLicencia()+"");

            }
            if(elegidaPersona.getNumCredencialSABSA()!=0)
            {
                jCheckBoxCredencialSABSA.setSelected(true);
                jTextFieldNumeroCredencialSABSA.setText(elegidaPersona.getNumCredencialSABSA()+"");
                dateChooserComboVencimientoSABSA.setSelectedDate(elegidaPersona.getFechaCalendar(elegidaPersona.getFechaVigenciaSABSA()));
            }
            if(elegidaPersona.getNumCredicialCoorporativo()!=0)
            {
                jCheckBoxCredemcialCoorporativa.setSelected(true);
                jTextFieldNumeroCredencialCoorporativa.setText(elegidaPersona.getNumCredicialCoorporativo()+"");
                dateChooserComboVencimientoCoorporativa.setSelectedDate(elegidaPersona.getFechaCalendar(elegidaPersona.getFechaVigenciaCoorporativo()));
            }
            if(!elegidaPersona.getFechaVencimientoAptoMedico().isEmpty())
            {
                jCheckBoxCertificadoMedico.setSelected(true);
                dateChooserComboVencimientoAptoMedico.setSelectedDate(elegidaPersona.getFechaCalendar(elegidaPersona.getFechaVencimientoAptoMedico()));

            }
            int posMalla = -1;
            int idMallaElegida = elegidaPersona.getiDMalla();
            for (int i = 0; i < mallas.size(); i++) {
                if(mallas.get(i).getID() == idMallaElegida)
                {
                    posMalla = i;
                    mallaElegida = mallas.get(i);
                    break;
                }
            }
            jComboBoxMallaAInscribirse.setSelectedIndex(posMalla+1);

            con.recuperarReferencias(elegidaPersona.getiD(), referencias);
            crearTablaReferencias();
            llenarTablaReferencias();
        }

    }
    public void estadoInicial()
    {
            jTextFieldNombre.setText("");
            jTextFieldApellidoPaterno.setText("");
            jTextFieldApellidoMaterno.setText("");
            dateChooserComboFechaNacimiento.setCurrent(Calendar.getInstance());
            jTextFieldDireccionActual.setText("");
            jTextFieldTelefono.setText("");
            jTextFieldURLFoto.setText("");
            jLabelFotoEstudiante6.revalidate();
            jComboBoxTipoSangre.setSelectedIndex(0);
            jTextFieldNumeroDocumentoID.setText("");
            jTextFieldParentesco.setText("");
            jCheckBoxUsaLentes.setSelected(false);
            jComboBoxTipoDocumento.setSelectedIndex(0);
            jCheckBoxNumFolio.setSelected(true);
            jCheckBoxNumFolio.setSelected(false);
            jCheckBoxLicenciaPiloto.setSelected(true);
            jCheckBoxLicenciaPiloto.setSelected(false);
            jCheckBoxCredencialSABSA.setSelected(true);
            jCheckBoxCredencialSABSA.setSelected(false);
            jCheckBoxCredemcialCoorporativa.setSelected(true);
            jCheckBoxCredemcialCoorporativa.setSelected(false);
            jCheckBoxCertificadoMedico.setSelected(false);
            jComboBoxMallaAInscribirse.setSelectedIndex(0);
            jTextFieldTelefono.setText("");
            jTextFieldURLFoto.setText("");
            jTextFieldTipoDeTelefonoReferencia.setText("");
            jTextFieldNombreCompletoReferencia.setText("");
            actualizarDatos();
            referencias.clear();
            
            crearTablaReferencias();
            llenarTablaReferencias();


        
        
    }
    private void actualizarDatos()
    {
        if(mallaElegida!= null)
        {
            jLabelPrecioSugerido.setText("Precio Sugerido : "+mallaElegida.getMinutos());
        }
        else{
             jLabelPrecioSugerido.setText("Precio Sugerido : ");
        }
    }
    public void crearTablaTelefonos(LinkedList<TelefonoReferencia> tel)
    {
        int tam = tel.size();
        jTableTelefonos = new javax.swing.JTable();

        jTableTelefonos.setModel(new javax.swing.table.DefaultTableModel(
            new Object [tam][2],
            new String [] {
                "Telefono", "Ubicacion"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.Integer.class, java.lang.String.class
            };
            boolean[] canEdit = new boolean [] {
                false, false
            };

            @Override
            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            @Override
            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jScrollPane3.setViewportView(jTableTelefonos);

        if (jTableTelefonos.getColumnModel().getColumnCount() > 0) {
            jTableTelefonos.getColumnModel().getColumn(0).setResizable(false);
            jTableTelefonos.getColumnModel().getColumn(1).setResizable(false);
        }

    }
    public void llenarTablaTelefonos(LinkedList<TelefonoReferencia> tel)
    {
        DefaultTableModel modelo = (DefaultTableModel)jTableTelefonos.getModel();
        
        if(!tel.isEmpty())
        {
            for (int i = 0; i < tel.size(); i++) {
                TelefonoReferencia aux = tel.get(i);
                modelo.setValueAt(aux.getNumTelefono(), i, 0);
                modelo.setValueAt(aux.getLocalizacion(), i, 1);
            }
        }
    }
    public void llenarTablaReferencias()
    {
         DefaultTableModel modelo = (DefaultTableModel) jTableReferencias.getModel();
        
        if(!referencias.isEmpty())
        {
            for(int i=0; i<referencias.size(); i++)
            {
                Referencia aux = referencias.get(i);
                modelo.setValueAt(aux.getNombre(), i, 0);
                modelo.setValueAt(aux.getParentesco(), i, 1);
                String telefonos = "";
                LinkedList<TelefonoReferencia> telfAux = aux.getTelefono();
                for (int j = 0; j < telfAux.size(); j++) {
                    if(j ==  (telfAux.size()-1))
                    {
                        telefonos += telfAux.get(j).getNumTelefono()+"";
                    }
                    else{
                        telefonos += telfAux.get(j).getNumTelefono()+"-";
                    }              
                }
                modelo.setValueAt(telefonos, i, 2);       
            }
            
            
        }
    }
   
   
    public void crearTablaReferencias()
    {
        int tam = referencias.size();
        
        jTableReferencias = new javax.swing.JTable();

        jTableReferencias.setModel(new javax.swing.table.DefaultTableModel(
            new Object [tam][3],
            new String [] {
                "Nombre Referencia","Parentesco", "Telefonos"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.String.class,java.lang.String.class, java.lang.String.class
            };
            boolean[] canEdit = new boolean [] {
                false, false, false
            };

            @Override
            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            @Override
            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });



        jScrollPane2.setViewportView(jTableReferencias);

        if (jTableReferencias.getColumnModel().getColumnCount() > 0) {
            jTableReferencias.getColumnModel().getColumn(0).setResizable(false);
            jTableReferencias.getColumnModel().getColumn(1).setResizable(false);
            jTableReferencias.getColumnModel().getColumn(2).setResizable(false);
        }

    }
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollPane4 = new javax.swing.JScrollPane();
        jList1 = new javax.swing.JList<>();
        jTabbedPane1 = new javax.swing.JTabbedPane();
        jPanel1 = new javax.swing.JPanel();
        jTextFieldNumeroDocumentoID = new javax.swing.JTextField();
        jLabel10 = new javax.swing.JLabel();
        jComboBoxTipoDocumento = new javax.swing.JComboBox<>();
        jTextFieldApellidoPaterno = new javax.swing.JTextField();
        jTextFieldApellidoMaterno = new javax.swing.JTextField();
        jCheckBoxUsaLentes = new javax.swing.JCheckBox();
        labelApellido4 = new javax.swing.JLabel();
        labelApellido5 = new javax.swing.JLabel();
        jTextFieldTelefono = new javax.swing.JTextField();
        jLabel11 = new javax.swing.JLabel();
        labelNombre1 = new javax.swing.JLabel();
        jComboBoxTipoSangre = new javax.swing.JComboBox<>();
        dateChooserComboFechaNacimiento = new datechooser.beans.DateChooserCombo();
        labelApellido6 = new javax.swing.JLabel();
        labelTipo1 = new javax.swing.JLabel();
        labelApellido7 = new javax.swing.JLabel();
        jTextFieldNombre = new javax.swing.JTextField();
        jTextFieldDireccionActual = new javax.swing.JTextField();
        labelDocumentoDeIdentificacion1 = new javax.swing.JLabel();
        jButtonBuscarFoto = new javax.swing.JButton();
        jLabelFotoEstudiante6 = new javax.swing.JLabel();
        jTextFieldURLFoto = new javax.swing.JTextField();
        jPanel2 = new javax.swing.JPanel();
        jLabelPrecioSugerido = new javax.swing.JLabel();
        jLabel21 = new javax.swing.JLabel();
        jTextFieldNumeroCredencialSABSA = new javax.swing.JTextField();
        jComboBoxMallaAInscribirse = new javax.swing.JComboBox<>();
        jCheckBoxNumFolio = new javax.swing.JCheckBox();
        jCheckBoxCertificadoMedico = new javax.swing.JCheckBox();
        jCheckBoxLicenciaPiloto = new javax.swing.JCheckBox();
        dateChooserComboVencimientoCoorporativa = new datechooser.beans.DateChooserCombo();
        jCheckBoxCredencialSABSA = new javax.swing.JCheckBox();
        jCheckBoxCredemcialCoorporativa = new javax.swing.JCheckBox();
        jLabel22 = new javax.swing.JLabel();
        jLabel23 = new javax.swing.JLabel();
        jTextFieldNumeroLicenciaPiloto = new javax.swing.JTextField();
        jTextFieldNumFolio = new javax.swing.JTextField();
        jTextFieldNumeroCredencialCoorporativa = new javax.swing.JTextField();
        dateChooserComboVencimientoSABSA = new datechooser.beans.DateChooserCombo();
        jLabel24 = new javax.swing.JLabel();
        dateChooserComboVencimientoAptoMedico = new datechooser.beans.DateChooserCombo();
        jScrollPane1 = new javax.swing.JScrollPane();
        jPanel3 = new javax.swing.JPanel();
        jLabel2 = new javax.swing.JLabel();
        jTextFieldNombreCompletoReferencia = new javax.swing.JTextField();
        jLabel3 = new javax.swing.JLabel();
        jTextFieldTelefonoReferencia = new javax.swing.JTextField();
        jLabel4 = new javax.swing.JLabel();
        jTextFieldTipoDeTelefonoReferencia = new javax.swing.JTextField();
        jButtonAgregarTelefonoReferencia = new javax.swing.JButton();
        jButtonEliminarTelefonoReferencia = new javax.swing.JButton();
        jButtonAgregarReferencia = new javax.swing.JButton();
        jScrollPane3 = new javax.swing.JScrollPane();
        jTableTelefonos = new javax.swing.JTable();
        jLabel5 = new javax.swing.JLabel();
        jTextFieldParentesco = new javax.swing.JTextField();
        jLabel6 = new javax.swing.JLabel();
        jScrollPane2 = new javax.swing.JScrollPane();
        jTableReferencias = new javax.swing.JTable();
        jButtonEliminarReferencia = new javax.swing.JButton();
        jSeparator2 = new javax.swing.JSeparator();
        botonAceptar = new javax.swing.JButton();
        botonCancelar = new javax.swing.JButton();

        jList1.setModel(new javax.swing.AbstractListModel<String>() {
            String[] strings = { "Item 1", "Item 2", "Item 3", "Item 4", "Item 5" };
            public int getSize() { return strings.length; }
            public String getElementAt(int i) { return strings[i]; }
        });
        jScrollPane4.setViewportView(jList1);

        setIconifiable(true);
        setTitle("Crear Nuevo Alumno");
        setToolTipText("Crear Nuevo Alumno");

        jTextFieldNumeroDocumentoID.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                jTextFieldNumeroDocumentoIDKeyTyped(evt);
            }
        });

        jLabel10.setText("Teléfono:");

        jComboBoxTipoDocumento.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jComboBoxTipoDocumentoActionPerformed(evt);
            }
        });

        jTextFieldApellidoPaterno.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jTextFieldApellidoPaternoActionPerformed(evt);
            }
        });
        jTextFieldApellidoPaterno.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                jTextFieldApellidoPaternoKeyTyped(evt);
            }
        });

        jTextFieldApellidoMaterno.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jTextFieldApellidoMaternoActionPerformed(evt);
            }
        });
        jTextFieldApellidoMaterno.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                jTextFieldApellidoMaternoKeyTyped(evt);
            }
        });

        jCheckBoxUsaLentes.setText("Usa Lentes");

        labelApellido4.setText("Tipo de Sangre:");

        labelApellido5.setText("Fecha Nacimiento:");

        jTextFieldTelefono.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jTextFieldTelefonoActionPerformed(evt);
            }
        });
        jTextFieldTelefono.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                jTextFieldTelefonoKeyTyped(evt);
            }
        });

        jLabel11.setText("Direccion Actual: ");

        labelNombre1.setText("Nombre:");

        jComboBoxTipoSangre.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "O RH+", "O RH-", "A RH+", "A RH-", "B RH+", "B RH-", "AB RH+", "AB RH-" }));

        labelApellido6.setText("Apellido Materno:");

        labelTipo1.setText("Tipo");

        labelApellido7.setText("Apellido Paterno:");

        jTextFieldNombre.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                jTextFieldNombreKeyTyped(evt);
            }
        });

        jTextFieldDireccionActual.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jTextFieldDireccionActualActionPerformed(evt);
            }
        });

        labelDocumentoDeIdentificacion1.setText("Documento de Identificación:");

        jButtonBuscarFoto.setText("Buscar Foto");

        jLabelFotoEstudiante6.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/defaultUser.png"))); // NOI18N

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(46, 46, 46)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabelFotoEstudiante6, javax.swing.GroupLayout.PREFERRED_SIZE, 121, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jButtonBuscarFoto)
                            .addComponent(labelApellido5)
                            .addComponent(jLabel11)
                            .addComponent(jLabel10)
                            .addComponent(labelDocumentoDeIdentificacion1)
                            .addComponent(labelNombre1, javax.swing.GroupLayout.PREFERRED_SIZE, 58, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(labelApellido4)
                            .addComponent(labelApellido7, javax.swing.GroupLayout.PREFERRED_SIZE, 109, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(18, 18, 18)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addComponent(jComboBoxTipoSangre, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(22, 22, 22)
                                .addComponent(jCheckBoxUsaLentes))
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jTextFieldNombre, javax.swing.GroupLayout.PREFERRED_SIZE, 120, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(jTextFieldApellidoPaterno, javax.swing.GroupLayout.PREFERRED_SIZE, 120, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGap(18, 18, 18)
                                .addComponent(labelApellido6)
                                .addGap(18, 18, 18)
                                .addComponent(jTextFieldApellidoMaterno, javax.swing.GroupLayout.PREFERRED_SIZE, 120, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                .addComponent(jTextFieldURLFoto, javax.swing.GroupLayout.Alignment.LEADING)
                                .addComponent(jTextFieldTelefono, javax.swing.GroupLayout.Alignment.LEADING)
                                .addComponent(dateChooserComboFechaNacimiento, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                            .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                .addComponent(jTextFieldDireccionActual, javax.swing.GroupLayout.Alignment.LEADING)
                                .addGroup(jPanel1Layout.createSequentialGroup()
                                    .addComponent(jTextFieldNumeroDocumentoID, javax.swing.GroupLayout.PREFERRED_SIZE, 119, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addGap(14, 14, 14)
                                    .addComponent(labelTipo1)
                                    .addGap(18, 18, 18)
                                    .addComponent(jComboBoxTipoDocumento, javax.swing.GroupLayout.PREFERRED_SIZE, 185, javax.swing.GroupLayout.PREFERRED_SIZE))))))
                .addContainerGap(57, Short.MAX_VALUE))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(labelNombre1)
                    .addComponent(jTextFieldNombre, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(labelApellido7, javax.swing.GroupLayout.PREFERRED_SIZE, 14, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jTextFieldApellidoPaterno, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(labelApellido6, javax.swing.GroupLayout.PREFERRED_SIZE, 14, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jTextFieldApellidoMaterno, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(10, 10, 10)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(labelApellido4, javax.swing.GroupLayout.PREFERRED_SIZE, 15, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jComboBoxTipoSangre, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jCheckBoxUsaLentes))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(labelDocumentoDeIdentificacion1)
                    .addComponent(jTextFieldNumeroDocumentoID, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(labelTipo1, javax.swing.GroupLayout.PREFERRED_SIZE, 19, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jComboBoxTipoDocumento, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(labelApellido5, javax.swing.GroupLayout.PREFERRED_SIZE, 15, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(dateChooserComboFechaNacimiento, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel11, javax.swing.GroupLayout.PREFERRED_SIZE, 16, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jTextFieldDireccionActual, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(7, 7, 7)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel10)
                    .addComponent(jTextFieldTelefono, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jButtonBuscarFoto)
                    .addComponent(jTextFieldURLFoto, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabelFotoEstudiante6, javax.swing.GroupLayout.PREFERRED_SIZE, 120, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(81, Short.MAX_VALUE))
        );

        jTabbedPane1.addTab("Datos Personales", jPanel1);

        jLabelPrecioSugerido.setText("Precio Malla:");

        jLabel21.setText("Programa a Inscribirse");

        jTextFieldNumeroCredencialSABSA.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jTextFieldNumeroCredencialSABSAActionPerformed(evt);
            }
        });
        jTextFieldNumeroCredencialSABSA.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                jTextFieldNumeroCredencialSABSAKeyTyped(evt);
            }
        });

        jComboBoxMallaAInscribirse.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));
        jComboBoxMallaAInscribirse.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jComboBoxMallaAInscribirseActionPerformed(evt);
            }
        });

        jCheckBoxNumFolio.setText("Nº Folio");
        jCheckBoxNumFolio.addChangeListener(new javax.swing.event.ChangeListener() {
            public void stateChanged(javax.swing.event.ChangeEvent evt) {
                jCheckBoxNumFolioStateChanged(evt);
            }
        });
        jCheckBoxNumFolio.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jCheckBoxNumFolioActionPerformed(evt);
            }
        });

        jCheckBoxCertificadoMedico.setText("Certificado Médico");
        jCheckBoxCertificadoMedico.addChangeListener(new javax.swing.event.ChangeListener() {
            public void stateChanged(javax.swing.event.ChangeEvent evt) {
                jCheckBoxCertificadoMedicoStateChanged(evt);
            }
        });

        jCheckBoxLicenciaPiloto.setText("Nº Licencia de Piloto");
        jCheckBoxLicenciaPiloto.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                jCheckBoxLicenciaPilotoItemStateChanged(evt);
            }
        });
        jCheckBoxLicenciaPiloto.addChangeListener(new javax.swing.event.ChangeListener() {
            public void stateChanged(javax.swing.event.ChangeEvent evt) {
                jCheckBoxLicenciaPilotoStateChanged(evt);
            }
        });

        jCheckBoxCredencialSABSA.setText("Nº Credencial SABSA");
        jCheckBoxCredencialSABSA.addChangeListener(new javax.swing.event.ChangeListener() {
            public void stateChanged(javax.swing.event.ChangeEvent evt) {
                jCheckBoxCredencialSABSAStateChanged(evt);
            }
        });

        jCheckBoxCredemcialCoorporativa.setText("Nº Credencial Coorporativa");
        jCheckBoxCredemcialCoorporativa.addChangeListener(new javax.swing.event.ChangeListener() {
            public void stateChanged(javax.swing.event.ChangeEvent evt) {
                jCheckBoxCredemcialCoorporativaStateChanged(evt);
            }
        });

        jLabel22.setText("Fecha de Vencimiento:");

        jLabel23.setText("Fecha de Vencimiento:");

        jTextFieldNumeroLicenciaPiloto.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jTextFieldNumeroLicenciaPilotoActionPerformed(evt);
            }
        });
        jTextFieldNumeroLicenciaPiloto.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                jTextFieldNumeroLicenciaPilotoKeyReleased(evt);
            }
            public void keyTyped(java.awt.event.KeyEvent evt) {
                jTextFieldNumeroLicenciaPilotoKeyTyped(evt);
            }
        });

        jTextFieldNumFolio.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                jTextFieldNumFolioKeyTyped(evt);
            }
        });

        jTextFieldNumeroCredencialCoorporativa.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jTextFieldNumeroCredencialCoorporativaActionPerformed(evt);
            }
        });
        jTextFieldNumeroCredencialCoorporativa.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                jTextFieldNumeroCredencialCoorporativaKeyTyped(evt);
            }
        });

        jLabel24.setText("Fecha de Vencimiento:");

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabelPrecioSugerido, javax.swing.GroupLayout.PREFERRED_SIZE, 415, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jCheckBoxNumFolio)
                            .addComponent(jCheckBoxLicenciaPiloto)
                            .addComponent(jCheckBoxCredencialSABSA)
                            .addComponent(jCheckBoxCredemcialCoorporativa)
                            .addComponent(jCheckBoxCertificadoMedico)
                            .addComponent(jLabel21))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel2Layout.createSequentialGroup()
                                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                    .addComponent(jTextFieldNumeroCredencialSABSA, javax.swing.GroupLayout.PREFERRED_SIZE, 188, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(jTextFieldNumeroCredencialCoorporativa, javax.swing.GroupLayout.PREFERRED_SIZE, 188, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(jTextFieldNumeroLicenciaPiloto, javax.swing.GroupLayout.PREFERRED_SIZE, 188, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(jTextFieldNumFolio, javax.swing.GroupLayout.PREFERRED_SIZE, 188, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addGroup(jPanel2Layout.createSequentialGroup()
                                        .addComponent(jLabel24)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(dateChooserComboVencimientoAptoMedico, javax.swing.GroupLayout.PREFERRED_SIZE, 96, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addGroup(jPanel2Layout.createSequentialGroup()
                                        .addComponent(jLabel23)
                                        .addGap(18, 18, 18)
                                        .addComponent(dateChooserComboVencimientoCoorporativa, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE))
                                    .addGroup(jPanel2Layout.createSequentialGroup()
                                        .addComponent(jLabel22)
                                        .addGap(18, 18, 18)
                                        .addComponent(dateChooserComboVencimientoSABSA, javax.swing.GroupLayout.PREFERRED_SIZE, 90, javax.swing.GroupLayout.PREFERRED_SIZE))))
                            .addComponent(jComboBoxMallaAInscribirse, javax.swing.GroupLayout.PREFERRED_SIZE, 256, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addContainerGap(24, Short.MAX_VALUE))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGap(142, 142, 142)
                        .addComponent(jLabel21))
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jTextFieldNumFolio, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jCheckBoxNumFolio))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jTextFieldNumeroLicenciaPiloto, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jCheckBoxLicenciaPiloto))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addGroup(jPanel2Layout.createSequentialGroup()
                                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                    .addComponent(jTextFieldNumeroCredencialSABSA, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(jCheckBoxCredencialSABSA))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                    .addComponent(jTextFieldNumeroCredencialCoorporativa, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(jCheckBoxCredemcialCoorporativa)))
                            .addGroup(jPanel2Layout.createSequentialGroup()
                                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel22)
                                    .addComponent(dateChooserComboVencimientoSABSA, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(dateChooserComboVencimientoCoorporativa, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(jLabel23))))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(jLabel24)
                                .addComponent(jCheckBoxCertificadoMedico))
                            .addComponent(dateChooserComboVencimientoAptoMedico, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jComboBoxMallaAInscribirse, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(19, 19, 19)
                .addComponent(jLabelPrecioSugerido)
                .addContainerGap(228, Short.MAX_VALUE))
        );

        jTabbedPane1.addTab("Datos Academicos", jPanel2);

        jLabel2.setText("Nombre completo : ");

        jTextFieldNombreCompletoReferencia.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                jTextFieldNombreCompletoReferenciaKeyTyped(evt);
            }
        });

        jLabel3.setText("Telefono : ");

        jTextFieldTelefonoReferencia.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                jTextFieldTelefonoReferenciaKeyTyped(evt);
            }
        });

        jLabel4.setText("Tipo Telefono: ");

        jTextFieldTipoDeTelefonoReferencia.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                jTextFieldTipoDeTelefonoReferenciaKeyTyped(evt);
            }
        });

        jButtonAgregarTelefonoReferencia.setText("Agregar Telefono");

        jButtonEliminarTelefonoReferencia.setText("Eliminar Telefono");

        jButtonAgregarReferencia.setText("Agregar Referencia");

        jTableTelefonos.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Telefono", "Ubicacion"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.Integer.class, java.lang.String.class
            };
            boolean[] canEdit = new boolean [] {
                false, false
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jScrollPane3.setViewportView(jTableTelefonos);
        if (jTableTelefonos.getColumnModel().getColumnCount() > 0) {
            jTableTelefonos.getColumnModel().getColumn(0).setResizable(false);
            jTableTelefonos.getColumnModel().getColumn(1).setResizable(false);
        }

        jLabel5.setText("Parentesco: ");

        jTextFieldParentesco.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                jTextFieldParentescoKeyTyped(evt);
            }
        });

        jLabel6.setText("Referencias actuales del Alumno:");

        jTableReferencias.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Nombre Referencia", "Telefonos"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.String.class, java.lang.String.class
            };
            boolean[] canEdit = new boolean [] {
                false, false
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jScrollPane2.setViewportView(jTableReferencias);
        if (jTableReferencias.getColumnModel().getColumnCount() > 0) {
            jTableReferencias.getColumnModel().getColumn(0).setResizable(false);
            jTableReferencias.getColumnModel().getColumn(1).setResizable(false);
        }

        jButtonEliminarReferencia.setText("Eliminar Referencia");

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel3Layout.createSequentialGroup()
                .addContainerGap(101, Short.MAX_VALUE)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel2)
                            .addComponent(jLabel5, javax.swing.GroupLayout.PREFERRED_SIZE, 93, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(4, 4, 4)
                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jTextFieldNombreCompletoReferencia)
                            .addComponent(jTextFieldParentesco)))
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addComponent(jLabel3)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jTextFieldTelefonoReferencia, javax.swing.GroupLayout.PREFERRED_SIZE, 122, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jLabel4)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jTextFieldTipoDeTelefonoReferencia, javax.swing.GroupLayout.PREFERRED_SIZE, 133, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(jButtonAgregarReferencia, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addGap(113, 113, 113)
                        .addComponent(jLabel6))
                    .addComponent(jSeparator2)
                    .addComponent(jButtonEliminarReferencia, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addComponent(jButtonAgregarTelefonoReferencia, javax.swing.GroupLayout.PREFERRED_SIZE, 159, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jButtonEliminarTelefonoReferencia, javax.swing.GroupLayout.PREFERRED_SIZE, 180, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(jScrollPane3, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE))
                .addGap(116, 116, 116))
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jTextFieldNombreCompletoReferencia, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel2))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jTextFieldParentesco, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel5))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel3)
                    .addComponent(jTextFieldTipoDeTelefonoReferencia, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel4)
                    .addComponent(jTextFieldTelefonoReferencia, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jButtonAgregarTelefonoReferencia)
                    .addComponent(jButtonEliminarTelefonoReferencia))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jScrollPane3, javax.swing.GroupLayout.PREFERRED_SIZE, 98, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jButtonAgregarReferencia)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jSeparator2, javax.swing.GroupLayout.PREFERRED_SIZE, 10, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel6)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 140, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jButtonEliminarReferencia)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jScrollPane1.setViewportView(jPanel3);

        jTabbedPane1.addTab("Datos de Referencias", jScrollPane1);

        botonAceptar.setText("Aceptar");
        botonAceptar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                botonAceptarActionPerformed(evt);
            }
        });

        botonCancelar.setText("Cancelar");
        botonCancelar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                botonCancelarActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(botonAceptar, javax.swing.GroupLayout.PREFERRED_SIZE, 137, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(botonCancelar))
                    .addComponent(jTabbedPane1))
                .addGap(0, 0, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jTabbedPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 462, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(botonAceptar, javax.swing.GroupLayout.DEFAULT_SIZE, 31, Short.MAX_VALUE)
                    .addComponent(botonCancelar, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGap(18, 18, 18))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void botonCancelarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_botonCancelarActionPerformed
       
    }//GEN-LAST:event_botonCancelarActionPerformed

    private void botonAceptarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_botonAceptarActionPerformed
       
    }//GEN-LAST:event_botonAceptarActionPerformed

    private void jTextFieldNumeroCredencialCoorporativaKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTextFieldNumeroCredencialCoorporativaKeyTyped
        char c = evt.getKeyChar();
        if(Character.isDigit(c) && jTextFieldNumeroCredencialCoorporativa.getText().length()<9)
        {
            evt = evt;
        }
        else
        {
            evt.consume();
        }
    }//GEN-LAST:event_jTextFieldNumeroCredencialCoorporativaKeyTyped

    private void jTextFieldNumeroCredencialCoorporativaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jTextFieldNumeroCredencialCoorporativaActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jTextFieldNumeroCredencialCoorporativaActionPerformed

    private void jTextFieldNumFolioKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTextFieldNumFolioKeyTyped
        char c = evt.getKeyChar();
        if(Character.isDigit(c) && jTextFieldNumFolio.getText().length()<9)
        {
            evt = evt;
        }
        else
        {
            evt.consume();
        }
    }//GEN-LAST:event_jTextFieldNumFolioKeyTyped

    private void jTextFieldNumeroLicenciaPilotoKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTextFieldNumeroLicenciaPilotoKeyTyped
        char c = evt.getKeyChar();
        if(Character.isDigit(c) && jTextFieldNumeroLicenciaPiloto.getText().length()<9)
        {
            evt = evt;
        }
        else
        {
            evt.consume();
        }
    }//GEN-LAST:event_jTextFieldNumeroLicenciaPilotoKeyTyped

    private void jTextFieldNumeroLicenciaPilotoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jTextFieldNumeroLicenciaPilotoActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jTextFieldNumeroLicenciaPilotoActionPerformed

    private void jCheckBoxCredemcialCoorporativaStateChanged(javax.swing.event.ChangeEvent evt) {//GEN-FIRST:event_jCheckBoxCredemcialCoorporativaStateChanged
        if(jCheckBoxCredemcialCoorporativa.isSelected())
        {
            jTextFieldNumeroCredencialCoorporativa.setEnabled(true);
            dateChooserComboVencimientoCoorporativa.setEnabled(true);
        }
        else{
            jTextFieldNumeroCredencialCoorporativa.setText("");
            jTextFieldNumeroCredencialCoorporativa.setEnabled(false);
            dateChooserComboVencimientoCoorporativa.setEnabled(false);
        }
    }//GEN-LAST:event_jCheckBoxCredemcialCoorporativaStateChanged

    private void jCheckBoxCredencialSABSAStateChanged(javax.swing.event.ChangeEvent evt) {//GEN-FIRST:event_jCheckBoxCredencialSABSAStateChanged
        if(jCheckBoxCredencialSABSA.isSelected())
        {
            jTextFieldNumeroCredencialSABSA.setEnabled(true);
            dateChooserComboVencimientoSABSA.setEnabled(true);
        }
        else{
            jTextFieldNumeroCredencialSABSA.setText("");
            jTextFieldNumeroCredencialSABSA.setEnabled(false);
            dateChooserComboVencimientoSABSA.setEnabled(false);
        }
    }//GEN-LAST:event_jCheckBoxCredencialSABSAStateChanged

    private void jCheckBoxLicenciaPilotoStateChanged(javax.swing.event.ChangeEvent evt) {//GEN-FIRST:event_jCheckBoxLicenciaPilotoStateChanged
        if(jCheckBoxLicenciaPiloto.isSelected())
        {
            jTextFieldNumeroLicenciaPiloto.setEnabled(true);
        }
        else{
            jTextFieldNumeroLicenciaPiloto.setText("");
            jTextFieldNumeroLicenciaPiloto.setEnabled(false);

        }
    }//GEN-LAST:event_jCheckBoxLicenciaPilotoStateChanged

    private void jCheckBoxLicenciaPilotoItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_jCheckBoxLicenciaPilotoItemStateChanged
        // TODO add your handling code here:
    }//GEN-LAST:event_jCheckBoxLicenciaPilotoItemStateChanged

    private void jCheckBoxCertificadoMedicoStateChanged(javax.swing.event.ChangeEvent evt) {//GEN-FIRST:event_jCheckBoxCertificadoMedicoStateChanged
        if(jCheckBoxCertificadoMedico.isSelected())
        {
            dateChooserComboVencimientoAptoMedico.setEnabled(true);
        }
        else{
            dateChooserComboVencimientoAptoMedico.setEnabled(false);
        }
    }//GEN-LAST:event_jCheckBoxCertificadoMedicoStateChanged

    private void jCheckBoxNumFolioActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jCheckBoxNumFolioActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jCheckBoxNumFolioActionPerformed

    private void jCheckBoxNumFolioStateChanged(javax.swing.event.ChangeEvent evt) {//GEN-FIRST:event_jCheckBoxNumFolioStateChanged
        if(jCheckBoxNumFolio.isSelected())
        {
            jTextFieldNumFolio.setEnabled(true);
        }
        else{
            jTextFieldNumFolio.setText("");
            jTextFieldNumFolio.setEnabled(false);
        }
    }//GEN-LAST:event_jCheckBoxNumFolioStateChanged

    private void jComboBoxMallaAInscribirseActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jComboBoxMallaAInscribirseActionPerformed
        int pos = jComboBoxMallaAInscribirse.getSelectedIndex();
        if(pos>0)
        {
            mallaElegida = mallas.get(pos-1);
            actualizarDatos();
        }
    }//GEN-LAST:event_jComboBoxMallaAInscribirseActionPerformed

    private void jTextFieldNumeroCredencialSABSAKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTextFieldNumeroCredencialSABSAKeyTyped
        char c = evt.getKeyChar();
        if(Character.isDigit(c) && jTextFieldNumeroCredencialSABSA.getText().length()<9)
        {
            evt = evt;
        }
        else
        {
            evt.consume();
        }
    }//GEN-LAST:event_jTextFieldNumeroCredencialSABSAKeyTyped

    private void jTextFieldNumeroCredencialSABSAActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jTextFieldNumeroCredencialSABSAActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jTextFieldNumeroCredencialSABSAActionPerformed

    private void jTextFieldDireccionActualActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jTextFieldDireccionActualActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jTextFieldDireccionActualActionPerformed

    private void jTextFieldNombreKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTextFieldNombreKeyTyped
        char c = evt.getKeyChar();
        if(Character.isLetter(c) || Character.isSpaceChar(c))
        {
            evt = evt;
        }
        else
        {
            evt.consume();
        }
    }//GEN-LAST:event_jTextFieldNombreKeyTyped

    private void jTextFieldTelefonoKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTextFieldTelefonoKeyTyped
        char c = evt.getKeyChar();
        if(Character.isDigit(c) && jTextFieldTelefono.getText().length()<11)
        {
            evt = evt;
        }
        else
        {
            evt.consume();
        }
    }//GEN-LAST:event_jTextFieldTelefonoKeyTyped

    private void jTextFieldTelefonoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jTextFieldTelefonoActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jTextFieldTelefonoActionPerformed

    private void jTextFieldApellidoMaternoKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTextFieldApellidoMaternoKeyTyped
        char c = evt.getKeyChar();
        if(Character.isLetter(c)  || Character.isSpaceChar(c))
        {
            evt = evt;
        }
        else
        {
            evt.consume();
        }
    }//GEN-LAST:event_jTextFieldApellidoMaternoKeyTyped

    private void jTextFieldApellidoMaternoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jTextFieldApellidoMaternoActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jTextFieldApellidoMaternoActionPerformed

    private void jTextFieldApellidoPaternoKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTextFieldApellidoPaternoKeyTyped
        char c = evt.getKeyChar();
        if(Character.isLetter(c)  || Character.isSpaceChar(c))
        {
            evt = evt;
        }
        else
        {
            evt.consume();
        }
    }//GEN-LAST:event_jTextFieldApellidoPaternoKeyTyped

    private void jTextFieldApellidoPaternoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jTextFieldApellidoPaternoActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jTextFieldApellidoPaternoActionPerformed

    private void jComboBoxTipoDocumentoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jComboBoxTipoDocumentoActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jComboBoxTipoDocumentoActionPerformed

    private void jTextFieldNumeroDocumentoIDKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTextFieldNumeroDocumentoIDKeyTyped
        char c = evt.getKeyChar();
        if(Character.isDigit(c) && jTextFieldNumeroDocumentoID.getText().length()<11)
        {
            evt = evt;
        }
        else
        {
            evt.consume();
        }
    }//GEN-LAST:event_jTextFieldNumeroDocumentoIDKeyTyped

    private void jTextFieldParentescoKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTextFieldParentescoKeyTyped
        char c = evt.getKeyChar();
        if(Character.isLetter(c))
        {
            evt = evt;
        }
        else
        {
            evt.consume();
        }
    }//GEN-LAST:event_jTextFieldParentescoKeyTyped

    private void jTextFieldTipoDeTelefonoReferenciaKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTextFieldTipoDeTelefonoReferenciaKeyTyped
        char c = evt.getKeyChar();
        if(Character.isLetter(c))
        {
            evt = evt;
        }
        else
        {
            evt.consume();
        }
    }//GEN-LAST:event_jTextFieldTipoDeTelefonoReferenciaKeyTyped

    private void jTextFieldTelefonoReferenciaKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTextFieldTelefonoReferenciaKeyTyped
        char c = evt.getKeyChar();
        if(Character.isDigit(c) && jTextFieldTelefonoReferencia.getText().length()<8)
        {
            evt = evt;
        }
        else
        {
            evt.consume();
        }
    }//GEN-LAST:event_jTextFieldTelefonoReferenciaKeyTyped

    private void jTextFieldNombreCompletoReferenciaKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTextFieldNombreCompletoReferenciaKeyTyped
        char c = evt.getKeyChar();
        if(Character.isLetter(c) || Character.isSpaceChar(c))
        {
            evt = evt;
        }
        else
        {
            evt.consume();
        }
    }//GEN-LAST:event_jTextFieldNombreCompletoReferenciaKeyTyped

    private void jTextFieldNumeroLicenciaPilotoKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTextFieldNumeroLicenciaPilotoKeyReleased
        // TODO add your handling code here:
    }//GEN-LAST:event_jTextFieldNumeroLicenciaPilotoKeyReleased

    public void actualizarReferencia()
    {
        if(elegida != null)
        {
            jTextFieldNombreCompletoReferencia.setText(elegida.getNombre());
            crearTablaTelefonos(elegida.getTelefono());
            llenarTablaTelefonos(elegida.getTelefono());
        }
        else{
            jTextFieldNombreCompletoReferencia.setText("");
            jTextFieldParentesco.setText("");
            crearTablaTelefonos(telefonoReferencia);
            llenarTablaTelefonos(telefonoReferencia);
        }
    }
        

    // Variables declaration - do not modify//GEN-BEGIN:variables
    public javax.swing.JButton botonAceptar;
    public javax.swing.JButton botonCancelar;
    public datechooser.beans.DateChooserCombo dateChooserComboFechaNacimiento;
    public datechooser.beans.DateChooserCombo dateChooserComboVencimientoAptoMedico;
    public datechooser.beans.DateChooserCombo dateChooserComboVencimientoCoorporativa;
    public datechooser.beans.DateChooserCombo dateChooserComboVencimientoSABSA;
    public javax.swing.JButton jButtonAgregarReferencia;
    public javax.swing.JButton jButtonAgregarTelefonoReferencia;
    public javax.swing.JButton jButtonBuscarFoto;
    public javax.swing.JButton jButtonEliminarReferencia;
    public javax.swing.JButton jButtonEliminarTelefonoReferencia;
    public javax.swing.JCheckBox jCheckBoxCertificadoMedico;
    public javax.swing.JCheckBox jCheckBoxCredemcialCoorporativa;
    public javax.swing.JCheckBox jCheckBoxCredencialSABSA;
    public javax.swing.JCheckBox jCheckBoxLicenciaPiloto;
    public javax.swing.JCheckBox jCheckBoxNumFolio;
    public javax.swing.JCheckBox jCheckBoxUsaLentes;
    public javax.swing.JComboBox<String> jComboBoxMallaAInscribirse;
    public javax.swing.JComboBox<String> jComboBoxTipoDocumento;
    public javax.swing.JComboBox<String> jComboBoxTipoSangre;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel21;
    private javax.swing.JLabel jLabel22;
    private javax.swing.JLabel jLabel23;
    private javax.swing.JLabel jLabel24;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    public javax.swing.JLabel jLabelFotoEstudiante6;
    public javax.swing.JLabel jLabelPrecioSugerido;
    private javax.swing.JList<String> jList1;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JScrollPane jScrollPane4;
    private javax.swing.JSeparator jSeparator2;
    private javax.swing.JTabbedPane jTabbedPane1;
    public javax.swing.JTable jTableReferencias;
    public javax.swing.JTable jTableTelefonos;
    public javax.swing.JTextField jTextFieldApellidoMaterno;
    public javax.swing.JTextField jTextFieldApellidoPaterno;
    public javax.swing.JTextField jTextFieldDireccionActual;
    public javax.swing.JTextField jTextFieldNombre;
    public javax.swing.JTextField jTextFieldNombreCompletoReferencia;
    public javax.swing.JTextField jTextFieldNumFolio;
    public javax.swing.JTextField jTextFieldNumeroCredencialCoorporativa;
    public javax.swing.JTextField jTextFieldNumeroCredencialSABSA;
    public javax.swing.JTextField jTextFieldNumeroDocumentoID;
    public javax.swing.JTextField jTextFieldNumeroLicenciaPiloto;
    public javax.swing.JTextField jTextFieldParentesco;
    public javax.swing.JTextField jTextFieldTelefono;
    public javax.swing.JTextField jTextFieldTelefonoReferencia;
    public javax.swing.JTextField jTextFieldTipoDeTelefonoReferencia;
    public javax.swing.JTextField jTextFieldURLFoto;
    private javax.swing.JLabel labelApellido4;
    private javax.swing.JLabel labelApellido5;
    private javax.swing.JLabel labelApellido6;
    private javax.swing.JLabel labelApellido7;
    private javax.swing.JLabel labelDocumentoDeIdentificacion1;
    private javax.swing.JLabel labelNombre1;
    private javax.swing.JLabel labelTipo1;
    // End of variables declaration//GEN-END:variables
}
