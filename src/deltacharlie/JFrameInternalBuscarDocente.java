/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package deltacharlie;

import entity.Persona;
import java.io.IOException;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JOptionPane;
import modelo.conexion;

/**
 *
 * @author Deluxe
 */
public class JFrameInternalBuscarDocente extends javax.swing.JInternalFrame {

    /**
     * Creates new form JFrameInternalBuscarDocente
     */
    public LinkedList<Persona> personas = new LinkedList<>();
    
    public Persona personaBuscada;
    private final conexion modelo;
    public JFrameInternalBuscarDocente(conexion modelo) {
        initComponents();
        this.modelo = modelo;
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jButtonAgregar = new javax.swing.JButton();
        jButtonCancelar = new javax.swing.JButton();
        jComboBoxPilotoABuscar = new javax.swing.JComboBox<>();
        jLabelNombreAlumno = new javax.swing.JLabel();
        jLabel1 = new javax.swing.JLabel();
        jTextFieldPilotoAbuscar = new javax.swing.JTextField();
        jLabelApellidoPaterno = new javax.swing.JLabel();
        jLabelApellidoMaterno = new javax.swing.JLabel();

        jButtonAgregar.setText("Agregar");
        jButtonAgregar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonAgregarActionPerformed(evt);
            }
        });

        jButtonCancelar.setText("Cancelar");

        jComboBoxPilotoABuscar.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "------------------------------------------" }));
        jComboBoxPilotoABuscar.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                jComboBoxPilotoABuscarItemStateChanged(evt);
            }
        });
        jComboBoxPilotoABuscar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jComboBoxPilotoABuscarActionPerformed(evt);
            }
        });

        jLabelNombreAlumno.setText("Nombre:");

        jLabel1.setText("Docente/Piloto a Buscar:");

        jTextFieldPilotoAbuscar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jTextFieldPilotoAbuscarActionPerformed(evt);
            }
        });
        jTextFieldPilotoAbuscar.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                jTextFieldPilotoAbuscarKeyTyped(evt);
            }
        });

        jLabelApellidoPaterno.setText("Apellido Paterno:");

        jLabelApellidoMaterno.setText("Apellido Materno:");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(34, 34, 34)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(40, 40, 40)
                        .addComponent(jButtonAgregar)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jButtonCancelar)
                        .addGap(58, 58, 58))
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabelApellidoPaterno)
                            .addComponent(jLabelApellidoMaterno)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(jLabel1)
                                .addGap(18, 18, 18)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addComponent(jComboBoxPilotoABuscar, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(jTextFieldPilotoAbuscar)))
                            .addComponent(jLabelNombreAlumno))
                        .addGap(66, 66, 66))))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(jTextFieldPilotoAbuscar, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addComponent(jComboBoxPilotoABuscar, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 44, Short.MAX_VALUE)
                .addComponent(jLabelNombreAlumno)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jLabelApellidoPaterno)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jLabelApellidoMaterno)
                .addGap(40, 40, 40)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jButtonAgregar)
                    .addComponent(jButtonCancelar))
                .addContainerGap(38, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jButtonAgregarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonAgregarActionPerformed

    }//GEN-LAST:event_jButtonAgregarActionPerformed

    private void jComboBoxPilotoABuscarItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_jComboBoxPilotoABuscarItemStateChanged

    }//GEN-LAST:event_jComboBoxPilotoABuscarItemStateChanged

    private void jComboBoxPilotoABuscarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jComboBoxPilotoABuscarActionPerformed
        int pos = jComboBoxPilotoABuscar.getSelectedIndex();

        if(pos>-1){
            System.out.println("Antes");
            if(!jComboBoxPilotoABuscar.getSelectedItem().toString().equals("No hay elementos"))
            {
                System.out.println("Dentro");
                personaBuscada = personas.get(pos);
                jLabelNombreAlumno.setText("Nombre : "+personaBuscada.getNombre());
                jLabelApellidoPaterno.setText("Apellido Paterno : "+personaBuscada.getApellidoPaterno());
                jLabelApellidoMaterno.setText("Apellido Materno : " +personaBuscada.getApellidoMaterno());

            }
        }
    }//GEN-LAST:event_jComboBoxPilotoABuscarActionPerformed

    private void jTextFieldPilotoAbuscarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jTextFieldPilotoAbuscarActionPerformed

    }//GEN-LAST:event_jTextFieldPilotoAbuscarActionPerformed

    private void jTextFieldPilotoAbuscarKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTextFieldPilotoAbuscarKeyTyped
        String palabra = jTextFieldPilotoAbuscar.getText();

        DefaultComboBoxModel combomodel = new DefaultComboBoxModel();

        try {
            try {
                jComboBoxPilotoABuscar.setModel(modelo.ObtenerListaPersonas(palabra,personas,1));
            } catch (IOException ex) {
                Logger.getLogger(JFrameInternalBuscarDocente.class.getName()).log(Level.SEVERE, null, ex);
            }
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, ex.getMessage(), "Error DB  ", JOptionPane.ERROR_MESSAGE);
        }
        if(jComboBoxPilotoABuscar.getModel().getSize() == 0)
        {

            combomodel.addElement("No hay elementos");
            jComboBoxPilotoABuscar.setModel(combomodel);
        }
    }//GEN-LAST:event_jTextFieldPilotoAbuscarKeyTyped


    // Variables declaration - do not modify//GEN-BEGIN:variables
    public javax.swing.JButton jButtonAgregar;
    public javax.swing.JButton jButtonCancelar;
    private javax.swing.JComboBox<String> jComboBoxPilotoABuscar;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabelApellidoMaterno;
    private javax.swing.JLabel jLabelApellidoPaterno;
    private javax.swing.JLabel jLabelNombreAlumno;
    private javax.swing.JTextField jTextFieldPilotoAbuscar;
    // End of variables declaration//GEN-END:variables
}
