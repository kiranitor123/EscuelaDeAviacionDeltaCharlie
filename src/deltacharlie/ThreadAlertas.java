/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package deltacharlie;

import controller.controlador;
import java.awt.Color;
import java.awt.Component;
import java.awt.Font;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;
import modelo.conexion;

/**
 *
 * @author LENOVO
 */

public class ThreadAlertas implements Runnable{
    
    public Thread t;
    private String threadName;
    boolean suspended = false;
    conexion modelo = new conexion();
    public JFrameInternalAlertas frmAlertas;
    public DefaultTableModel tabla;
    principal view;
    public static final Color VERY_LIGHT_RED = new Color(255,102,102);
    public static final Color VERY_LIGHT_GREEN = new Color(102,255,102);
    public static final Color VERY_LIGHT_YELLOW = new Color(255,255,153);
    private volatile boolean running = true;
    
    public ThreadAlertas (String name){
    threadName = name;
      System.out.println("Creating " +  threadName );
    }
    
    public void refresh()
    {
        modelo.limpiarAlertas();
        modelo.controlAlertasADP();
        modelo.controlAlertasAvion();
        modelo.controlAlertasDeudas();
    }
     public void run() {
        frmAlertas = new JFrameInternalAlertas();
        refresh();
      try {
         while (true) {
            if(modelo.isBorrado(DeltaCharlie.IDUser) != 0){
                JOptionPane.showMessageDialog(this.view, "El usuario fue eliminado", "Advertencia", JOptionPane.WARNING_MESSAGE);
                System.exit(0);
            }
            tabla = (DefaultTableModel) frmAlertas.jTableGlobales.getModel();
                if(tabla.getRowCount() < modelo.buscarAlertas().getRowCount()){
                    JOptionPane.showMessageDialog(this.view, "Tiene nuevas alertas");
                }
                frmAlertas.jTableGlobales.setModel(modelo.buscarAlertas());
                frmAlertas.jTableGlobales.repaint();
                frmAlertas.jTableGlobales.setDefaultRenderer(Object.class, new DefaultTableCellRenderer(){
                    @Override
                    public Component getTableCellRendererComponent(JTable table,
                            Object value, boolean isSelected, boolean hasFocus, int row, int col) {
                        
                        

                        super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, col);

                        String status = (String)table.getModel().getValueAt(row, 2);
                        if ("Avion".equals(status)) {
                            setBackground(VERY_LIGHT_RED);
                            this.setFont(this.getFont().deriveFont(Font.BOLD));
                        }
                        if ("Economicas".equals(status)) {
                            setBackground(VERY_LIGHT_GREEN);
                            this.setFont(this.getFont().deriveFont(Font.BOLD));
                        }
                        if ("Vencimiento".equals(status)) {
                            setBackground(VERY_LIGHT_YELLOW);
                            this.setFont(this.getFont().deriveFont(Font.BOLD));
                        }
                        return this;
                    }   
                });
                frmAlertas.jTableEconomicas.setModel(modelo.buscarAlertasEconomicas());
                frmAlertas.jTableEconomicas.repaint();
                frmAlertas.jTableAvion.setModel(modelo.buscarAlertasAvion());
                frmAlertas.jTableAvion.repaint();
                frmAlertas.jTableVencimiento.setModel(modelo.buscarAlertasVencimiento());
                frmAlertas.jTableVencimiento.repaint();
                Thread.sleep(2000);
            synchronized(this) {
               while(suspended) {
                  wait();
               }
            }
         }
      } catch (InterruptedException e) {
         System.out.println("Thread " +  threadName + " interrupted.");
      }
      System.out.println("Thread " +  threadName + " exiting.");
   }
     
     public void start () {
      System.out.println("Starting " +  threadName );
      if (t == null) {
         t = new Thread (this, threadName);
         t.start ();
      }
   }
   
   public void suspend() {
      suspended = true;
   }
   
   public synchronized void resume() {
      suspended = false;
      notify();
   }
}
