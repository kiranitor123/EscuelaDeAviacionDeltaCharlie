/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package deltacharlie;

import entity.Referencia;
import entity.TelefonoReferencia;
import java.util.LinkedList;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author Andres
 */
public class JFrameInternalDetallesDocentePiloto extends javax.swing.JInternalFrame {

    /**
     * Creates new form JFrameInternalDetallesDocentePiloto
     */
    public JFrameInternalDetallesDocentePiloto() {
        initComponents();
    }

    public void llenarTablaReferencias(LinkedList<Referencia> referencias)
    {
         DefaultTableModel modelo = (DefaultTableModel) jTableReferencias.getModel();
        
        if(!referencias.isEmpty())
        {
            for(int i=0; i<referencias.size(); i++)
            {
                Referencia aux = referencias.get(i);
                modelo.setValueAt(aux.getNombre(), i, 0);
                modelo.setValueAt(aux.getParentesco(), i, 1);
                String telefonos = "";
                LinkedList<TelefonoReferencia> telfAux = aux.getTelefono();
                for (int j = 0; j < telfAux.size(); j++) {
                    if(j ==  (telfAux.size()-1))
                    {
                        telefonos += telfAux.get(j).getNumTelefono()+"";
                    }
                    else{
                        telefonos += telfAux.get(j).getNumTelefono()+"-";
                    }              
                }
                modelo.setValueAt(telefonos, i, 2);       
            }
            
            
        }
    }
   
   
    public void crearTablaReferencias(LinkedList<Referencia> referencias)
    {
        int tam = referencias.size();
        
        jTableReferencias = new javax.swing.JTable();

        jTableReferencias.setModel(new javax.swing.table.DefaultTableModel(
            new Object [tam][3],
            new String [] {
                "Nombre Referencia","Parentesco", "Telefonos"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.String.class,java.lang.String.class, java.lang.String.class
            };
            boolean[] canEdit = new boolean [] {
                false, false, false
            };

            @Override
            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            @Override
            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });



        jScrollPaneReferencias.setViewportView(jTableReferencias);

        if (jTableReferencias.getColumnModel().getColumnCount() > 0) {
            jTableReferencias.getColumnModel().getColumn(0).setResizable(false);
            jTableReferencias.getColumnModel().getColumn(1).setResizable(false);
            jTableReferencias.getColumnModel().getColumn(2).setResizable(false);
        }

    }
    
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollPaneReferencias = new javax.swing.JScrollPane();
        jTableReferencias = new javax.swing.JTable();
        jButtonAceptar = new javax.swing.JButton();
        jLabelFechaDeVencimientoDelProficiencyCheckHolder = new javax.swing.JLabel();
        jLabelFechaDeVencimientoDelCertificadoMedico = new javax.swing.JLabel();
        jLabelFechaDeVencimientoDelProficiencyCheck = new javax.swing.JLabel();
        jLabelFechaDeVencimientoDelCertificadoMedicoHolder = new javax.swing.JLabel();
        jLabelFechaDeVencimientoDeLaCredencialCorporativaHolder = new javax.swing.JLabel();
        jLabelFechaDeVencimientoDeLaCredencialCorporativa = new javax.swing.JLabel();
        jLabelNumeroDeCredencialCorporativo = new javax.swing.JLabel();
        jLabelNumeroDeCredencialCorporativoHolder = new javax.swing.JLabel();
        jLabelFechaDeVencimientoDeLaCredencialSABSA = new javax.swing.JLabel();
        jLabelFechaDeVencimientoDeLaCredencialSABSAHolder = new javax.swing.JLabel();
        jLabelNumeroCredencialSABSAHolder = new javax.swing.JLabel();
        jLabelNumeroCredencialSABSA = new javax.swing.JLabel();
        jLabelNumeroDeLicenciaDePiloto = new javax.swing.JLabel();
        jLabelNumeroDeLicenciaDePilotoHolder = new javax.swing.JLabel();
        jLabelTelefonoHolder = new javax.swing.JLabel();
        jLabelTelefono = new javax.swing.JLabel();
        jLabelDireccionActual = new javax.swing.JLabel();
        jLabelDireccionActualHolder = new javax.swing.JLabel();
        jLabelFechaDeNacimientoHolder = new javax.swing.JLabel();
        jLabelFechaDeNacimiento = new javax.swing.JLabel();
        jLabelNumeroDeIdentificacion = new javax.swing.JLabel();
        jLabelNumeroDeIdentificacionHolder = new javax.swing.JLabel();
        jLabelTipoDeIdentificacionHolder = new javax.swing.JLabel();
        jLabelTipoDeIdentificacion = new javax.swing.JLabel();
        jLabelLentesHolder = new javax.swing.JLabel();
        jLabelLentes = new javax.swing.JLabel();
        jLabelTipoDeSangre = new javax.swing.JLabel();
        jLabelTipoDeSangreHolder = new javax.swing.JLabel();
        jLabelApellidoMaternoHolder = new javax.swing.JLabel();
        jLabelApellidoMaterno = new javax.swing.JLabel();
        jLabelApellidoPaternoAlumno = new javax.swing.JLabel();
        jLabelApellidoPaternoHolder = new javax.swing.JLabel();
        jLabelNombreAlumnoHolder = new javax.swing.JLabel();
        jLabelNombreAlumno = new javax.swing.JLabel();
        jLabelEsPiloto = new javax.swing.JLabel();
        jLabelEsPilotoHolder = new javax.swing.JLabel();

        jTableReferencias.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Nombre Referencia", "Telefonos"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.String.class, java.lang.String.class
            };
            boolean[] canEdit = new boolean [] {
                false, false
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jScrollPaneReferencias.setViewportView(jTableReferencias);

        jButtonAceptar.setText("Aceptar");

        jLabelFechaDeVencimientoDelProficiencyCheckHolder.setText("Fecha de Vencimiento");

        jLabelFechaDeVencimientoDelCertificadoMedico.setText("Fecha de Vencimiento del Certificado Medico:");

        jLabelFechaDeVencimientoDelProficiencyCheck.setText("Fecha de Vencimiento del Proficiency Check:");

        jLabelFechaDeVencimientoDelCertificadoMedicoHolder.setText("Fecha de Vencimiento");

        jLabelFechaDeVencimientoDeLaCredencialCorporativaHolder.setText("Fecha de Vencimiento");

        jLabelFechaDeVencimientoDeLaCredencialCorporativa.setText("Fecha de Vencimiento de la Credencial Corporativa:");

        jLabelNumeroDeCredencialCorporativo.setText("Numero de la Credencial Corporativa:");

        jLabelNumeroDeCredencialCorporativoHolder.setText("Numero de Credencial Corporativo");

        jLabelFechaDeVencimientoDeLaCredencialSABSA.setText("Fecha de Vencimiento de la Credencial SABSA:");

        jLabelFechaDeVencimientoDeLaCredencialSABSAHolder.setText("Fecha de Vencimiento");

        jLabelNumeroCredencialSABSAHolder.setText("Numero de Credencial SABSA");

        jLabelNumeroCredencialSABSA.setText("Numero de Credencial SABSA:");

        jLabelNumeroDeLicenciaDePiloto.setText("Numero de Licencia de Piloto:");

        jLabelNumeroDeLicenciaDePilotoHolder.setText("Numero de Licencia de Piloto");

        jLabelTelefonoHolder.setText("Telefono");

        jLabelTelefono.setText("Telefono:");

        jLabelDireccionActual.setText("Direccion Actual:");

        jLabelDireccionActualHolder.setText("Direccion Actual");

        jLabelFechaDeNacimientoHolder.setText("Fecha de Nacimiento");

        jLabelFechaDeNacimiento.setText("Fecha de Nacimiento:");

        jLabelNumeroDeIdentificacion.setText("Numero de Identificacion:");

        jLabelNumeroDeIdentificacionHolder.setText("Numero de Identificacion");

        jLabelTipoDeIdentificacionHolder.setText("Tipo de Identificacion");

        jLabelTipoDeIdentificacion.setText("Tipo de Identificacion:");

        jLabelLentesHolder.setText("Lentes");

        jLabelLentes.setText("Lentes:");

        jLabelTipoDeSangre.setText("Tipo de Sangre:");

        jLabelTipoDeSangreHolder.setText("Tipo de Sangre");

        jLabelApellidoMaternoHolder.setText("Apellido Materno");

        jLabelApellidoMaterno.setText("Apellido Materno:");

        jLabelApellidoPaternoAlumno.setText("Apellido Paterno:");

        jLabelApellidoPaternoHolder.setText("Apellido Paterno");

        jLabelNombreAlumnoHolder.setText("Nombre");

        jLabelNombreAlumno.setText("Nombre:");

        jLabelEsPiloto.setText("Es Piloto:");

        jLabelEsPilotoHolder.setText("Es Piloto");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jButtonAceptar, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(jLabelFechaDeVencimientoDelProficiencyCheck)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jLabelFechaDeVencimientoDelProficiencyCheckHolder))
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(jLabelFechaDeVencimientoDeLaCredencialSABSA)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jLabelFechaDeVencimientoDeLaCredencialSABSAHolder))
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(jLabelNumeroCredencialSABSA)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jLabelNumeroCredencialSABSAHolder))
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(jLabelNumeroDeCredencialCorporativo)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jLabelNumeroDeCredencialCorporativoHolder))
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(jLabelFechaDeVencimientoDeLaCredencialCorporativa)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jLabelFechaDeVencimientoDeLaCredencialCorporativaHolder))
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(jLabelFechaDeVencimientoDelCertificadoMedico)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jLabelFechaDeVencimientoDelCertificadoMedicoHolder))
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(jLabelFechaDeNacimiento)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jLabelFechaDeNacimientoHolder))
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(jLabelDireccionActual)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jLabelDireccionActualHolder))
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(jLabelNumeroDeLicenciaDePiloto)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jLabelNumeroDeLicenciaDePilotoHolder))
                            .addGroup(layout.createSequentialGroup()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabelTelefono)
                                    .addComponent(jLabelEsPiloto))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabelEsPilotoHolder)
                                    .addComponent(jLabelTelefonoHolder)))
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(jLabelNombreAlumno)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jLabelNombreAlumnoHolder))
                            .addGroup(layout.createSequentialGroup()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabelApellidoPaternoAlumno)
                                    .addComponent(jLabelApellidoMaterno))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabelApellidoMaternoHolder)
                                    .addComponent(jLabelApellidoPaternoHolder)))
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(jLabelTipoDeSangre)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jLabelTipoDeSangreHolder))
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(jLabelLentes)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jLabelLentesHolder))
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(jLabelNumeroDeIdentificacion)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jLabelNumeroDeIdentificacionHolder))
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(jLabelTipoDeIdentificacion)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jLabelTipoDeIdentificacionHolder)))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jScrollPaneReferencias, javax.swing.GroupLayout.DEFAULT_SIZE, 382, Short.MAX_VALUE)))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addGap(103, 103, 103)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                    .addComponent(jLabelTipoDeIdentificacion)
                                    .addComponent(jLabelTipoDeIdentificacionHolder))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabelNumeroDeIdentificacionHolder)
                                    .addComponent(jLabelNumeroDeIdentificacion, javax.swing.GroupLayout.Alignment.TRAILING))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabelFechaDeNacimientoHolder)
                                    .addComponent(jLabelFechaDeNacimiento))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                    .addComponent(jLabelDireccionActual)
                                    .addComponent(jLabelDireccionActualHolder))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                    .addComponent(jLabelTelefono)
                                    .addComponent(jLabelTelefonoHolder)))
                            .addGroup(layout.createSequentialGroup()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                    .addComponent(jLabelNombreAlumno)
                                    .addComponent(jLabelNombreAlumnoHolder))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                    .addComponent(jLabelApellidoPaternoAlumno)
                                    .addComponent(jLabelApellidoPaternoHolder))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                    .addComponent(jLabelApellidoMaterno)
                                    .addComponent(jLabelApellidoMaternoHolder))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                    .addComponent(jLabelTipoDeSangre)
                                    .addComponent(jLabelTipoDeSangreHolder))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                    .addComponent(jLabelLentes)
                                    .addComponent(jLabelLentesHolder))))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabelEsPiloto)
                            .addComponent(jLabelEsPilotoHolder))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabelNumeroDeLicenciaDePiloto)
                            .addComponent(jLabelNumeroDeLicenciaDePilotoHolder))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabelNumeroCredencialSABSA)
                            .addComponent(jLabelNumeroCredencialSABSAHolder))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabelFechaDeVencimientoDeLaCredencialSABSA)
                            .addComponent(jLabelFechaDeVencimientoDeLaCredencialSABSAHolder))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabelNumeroDeCredencialCorporativo)
                            .addComponent(jLabelNumeroDeCredencialCorporativoHolder))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabelFechaDeVencimientoDeLaCredencialCorporativa)
                            .addComponent(jLabelFechaDeVencimientoDeLaCredencialCorporativaHolder))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabelFechaDeVencimientoDelCertificadoMedico)
                            .addComponent(jLabelFechaDeVencimientoDelCertificadoMedicoHolder))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabelFechaDeVencimientoDelProficiencyCheck)
                            .addComponent(jLabelFechaDeVencimientoDelProficiencyCheckHolder)))
                    .addComponent(jScrollPaneReferencias, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 27, Short.MAX_VALUE)
                .addComponent(jButtonAceptar)
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents


    // Variables declaration - do not modify//GEN-BEGIN:variables
    public javax.swing.JButton jButtonAceptar;
    private javax.swing.JLabel jLabelApellidoMaterno;
    public javax.swing.JLabel jLabelApellidoMaternoHolder;
    private javax.swing.JLabel jLabelApellidoPaternoAlumno;
    public javax.swing.JLabel jLabelApellidoPaternoHolder;
    private javax.swing.JLabel jLabelDireccionActual;
    public javax.swing.JLabel jLabelDireccionActualHolder;
    private javax.swing.JLabel jLabelEsPiloto;
    private javax.swing.JLabel jLabelEsPilotoHolder;
    private javax.swing.JLabel jLabelFechaDeNacimiento;
    public javax.swing.JLabel jLabelFechaDeNacimientoHolder;
    private javax.swing.JLabel jLabelFechaDeVencimientoDeLaCredencialCorporativa;
    public javax.swing.JLabel jLabelFechaDeVencimientoDeLaCredencialCorporativaHolder;
    private javax.swing.JLabel jLabelFechaDeVencimientoDeLaCredencialSABSA;
    public javax.swing.JLabel jLabelFechaDeVencimientoDeLaCredencialSABSAHolder;
    private javax.swing.JLabel jLabelFechaDeVencimientoDelCertificadoMedico;
    public javax.swing.JLabel jLabelFechaDeVencimientoDelCertificadoMedicoHolder;
    private javax.swing.JLabel jLabelFechaDeVencimientoDelProficiencyCheck;
    public javax.swing.JLabel jLabelFechaDeVencimientoDelProficiencyCheckHolder;
    private javax.swing.JLabel jLabelLentes;
    public javax.swing.JLabel jLabelLentesHolder;
    private javax.swing.JLabel jLabelNombreAlumno;
    public javax.swing.JLabel jLabelNombreAlumnoHolder;
    private javax.swing.JLabel jLabelNumeroCredencialSABSA;
    public javax.swing.JLabel jLabelNumeroCredencialSABSAHolder;
    private javax.swing.JLabel jLabelNumeroDeCredencialCorporativo;
    public javax.swing.JLabel jLabelNumeroDeCredencialCorporativoHolder;
    private javax.swing.JLabel jLabelNumeroDeIdentificacion;
    public javax.swing.JLabel jLabelNumeroDeIdentificacionHolder;
    private javax.swing.JLabel jLabelNumeroDeLicenciaDePiloto;
    public javax.swing.JLabel jLabelNumeroDeLicenciaDePilotoHolder;
    private javax.swing.JLabel jLabelTelefono;
    public javax.swing.JLabel jLabelTelefonoHolder;
    private javax.swing.JLabel jLabelTipoDeIdentificacion;
    public javax.swing.JLabel jLabelTipoDeIdentificacionHolder;
    private javax.swing.JLabel jLabelTipoDeSangre;
    public javax.swing.JLabel jLabelTipoDeSangreHolder;
    public javax.swing.JScrollPane jScrollPaneReferencias;
    public javax.swing.JTable jTableReferencias;
    // End of variables declaration//GEN-END:variables
}
