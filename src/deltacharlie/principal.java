package deltacharlie;

import java.awt.Dimension;
import modelo.ImagenFondo;
import java.awt.Image;
import java.awt.Toolkit;


public class principal extends javax.swing.JFrame {

   
    /** Creates new form principal */
    public principal() {
        initComponents();
        jDesktopPane.setBorder(new ImagenFondo());
        Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
        setMinimumSize(screenSize);
    }
    
   

    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jToolBar1 = new javax.swing.JToolBar();
        j2Button1 = new j2Button.j2Button();
        j2Button2 = new j2Button.j2Button();
        j2Button9 = new j2Button.j2Button();
        j2Button3 = new j2Button.j2Button();
        j2Button4 = new j2Button.j2Button();
        j2Button5 = new j2Button.j2Button();
        j2Button8 = new j2Button.j2Button();
        j2Button6 = new j2Button.j2Button();
        jDesktopPane = new javax.swing.JDesktopPane();
        jMenuBar1 = new javax.swing.JMenuBar();
        jMenu1 = new javax.swing.JMenu();
        jMenuAlumno = new javax.swing.JMenuItem();
        jMenuDocentePiloto = new javax.swing.JMenuItem();
        jMenuUsuario = new javax.swing.JMenuItem();
        Salir = new javax.swing.JMenuItem();
        jMenuEscuela = new javax.swing.JMenu();
        jMenuMateria = new javax.swing.JMenuItem();
        jMenuItemMalla = new javax.swing.JMenuItem();
        jMenuItemGrupo = new javax.swing.JMenuItem();
        jMenuItemNotas = new javax.swing.JMenuItem();
        jMenu9 = new javax.swing.JMenu();
        reporteFlexible = new javax.swing.JMenuItem();
        jMenu2 = new javax.swing.JMenu();
        jMenuHelice = new javax.swing.JMenuItem();
        jMenuMotor = new javax.swing.JMenuItem();
        jMenuAvion = new javax.swing.JMenuItem();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setMinimumSize(new java.awt.Dimension(2147483647, 2147483647));

        jToolBar1.setFloatable(false);
        jToolBar1.setRollover(true);

        j2Button1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/document.png"))); // NOI18N
        j2Button1.setText("<html>  <style type=\"text/css\">   .estilo2{font-family:Arial;font-weight:bold; font-size:8px;color:rgb(0, 0, 0);}  </style>   <span class=\"estilo2\">Inscribir<br>Materia</span>  </html>  ");
        j2Button1.setFocusable(false);
        j2Button1.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        j2Button1.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        j2Button1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                j2Button1ActionPerformed(evt);
            }
        });
        jToolBar1.add(j2Button1);

        j2Button2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/user-1.png"))); // NOI18N
        j2Button2.setText("<html>  <style type=\"text/css\">   .estilo2{font-family:Arial;font-weight:bold; font-size:8px;color:rgb(0, 0, 0);}  </style>   <span class=\"estilo2\">Inscribir<br>Alumno</span>  </html>  ");
        j2Button2.setFocusable(false);
        j2Button2.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        j2Button2.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        j2Button2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                j2Button2ActionPerformed(evt);
            }
        });
        jToolBar1.add(j2Button2);
        j2Button2.getAccessibleContext().setAccessibleName("");

        j2Button9.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/document-3.png"))); // NOI18N
        j2Button9.setText("<html>  <style type=\"text/css\">   .estilo2{font-family:Arial;font-weight:bold; font-size:8px;color:rgb(0, 0, 0);}  </style>   <span class=\"estilo2\">Toma De<br>Materias</span>  </html>  ");
        j2Button9.setFocusable(false);
        j2Button9.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        j2Button9.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        jToolBar1.add(j2Button9);

        j2Button3.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/notification.png"))); // NOI18N
        j2Button3.setText("<html>  <style type=\"text/css\">   .estilo2{font-family:Arial;font-weight:bold; font-size:8px;color:rgb(0, 0, 0);}  </style>   <span class=\"estilo2\">Revisar<br>Alertas</span>  </html>  ");
        j2Button3.setFocusable(false);
        j2Button3.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        j2Button3.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        jToolBar1.add(j2Button3);

        j2Button4.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/list.png"))); // NOI18N
        j2Button4.setText("<html>  <style type=\"text/css\">   .estilo2{font-family:Arial;font-weight:bold; font-size:8px;color:rgb(0, 0, 0);}  </style>   <span class=\"estilo2\">Reportes<br>Normales</span>  </html>   ");
        j2Button4.setFocusable(false);
        j2Button4.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        j2Button4.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        jToolBar1.add(j2Button4);

        j2Button5.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/push-pin.png"))); // NOI18N
        j2Button5.setText("<html>  <style type=\"text/css\">   .estilo2{font-family:Arial;font-weight:bold; font-size:8px;color:rgb(0, 0, 0);}  </style>   <span class=\"estilo2\">Crear <br>Pago</span>  </html>  ");
        j2Button5.setFocusable(false);
        j2Button5.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        j2Button5.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        jToolBar1.add(j2Button5);

        j2Button8.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/layers.png"))); // NOI18N
        j2Button8.setText("<html>   <style type=\"text/css\">   .estilo2{font-family:Arial;font-weight:bold; font-size:8px;color:rgb(0, 0, 0);}  </style>   <span class=\"estilo2\">Control<br>Pagos</span>  </html>  ");
        j2Button8.setFocusable(false);
        j2Button8.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        j2Button8.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        jToolBar1.add(j2Button8);

        j2Button6.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/airplane.png"))); // NOI18N
        j2Button6.setText("<html>  <style type=\"text/css\">   .estilo2{font-family:Arial;font-weight:bold; font-size:8px;color:rgb(0, 0, 0);}  </style>   <span class=\"estilo2\">Control<br>Vuelos</span>  </html>  ");
        j2Button6.setFocusable(false);
        j2Button6.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        j2Button6.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        jToolBar1.add(j2Button6);

        javax.swing.GroupLayout jDesktopPaneLayout = new javax.swing.GroupLayout(jDesktopPane);
        jDesktopPane.setLayout(jDesktopPaneLayout);
        jDesktopPaneLayout.setHorizontalGroup(
            jDesktopPaneLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 785, Short.MAX_VALUE)
        );
        jDesktopPaneLayout.setVerticalGroup(
            jDesktopPaneLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 390, Short.MAX_VALUE)
        );

        jMenu1.setText("Persona");
        jMenu1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jMenu1MouseClicked(evt);
            }
        });
        jMenu1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenu1ActionPerformed(evt);
            }
        });

        jMenuAlumno.setText("Alumno");
        jMenu1.add(jMenuAlumno);

        jMenuDocentePiloto.setText("Docente/Piloto");
        jMenu1.add(jMenuDocentePiloto);

        jMenuUsuario.setText("Usuario");
        jMenu1.add(jMenuUsuario);

        Salir.setText("Salir");
        Salir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                SalirActionPerformed(evt);
            }
        });
        jMenu1.add(Salir);

        jMenuBar1.add(jMenu1);

        jMenuEscuela.setText("Escuela");

        jMenuMateria.setText("Materia");
        jMenuEscuela.add(jMenuMateria);

        jMenuItemMalla.setText("Malla");
        jMenuEscuela.add(jMenuItemMalla);

        jMenuItemGrupo.setText("Grupo");
        jMenuEscuela.add(jMenuItemGrupo);

        jMenuItemNotas.setText("Ingresar Notas");
        jMenuEscuela.add(jMenuItemNotas);

        jMenu9.setText("Reportes");

        reporteFlexible.setText("Reporte Normal");
        jMenu9.add(reporteFlexible);

        jMenuEscuela.add(jMenu9);

        jMenuBar1.add(jMenuEscuela);

        jMenu2.setText("Avion");

        jMenuHelice.setText("Helice");
        jMenu2.add(jMenuHelice);

        jMenuMotor.setText("Motor");
        jMenu2.add(jMenuMotor);

        jMenuAvion.setText("Avion");
        jMenuAvion.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuAvionActionPerformed(evt);
            }
        });
        jMenu2.add(jMenuAvion);

        jMenuBar1.add(jMenu2);

        setJMenuBar(jMenuBar1);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jDesktopPane)
            .addComponent(jToolBar1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addComponent(jToolBar1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jDesktopPane))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void SalirActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_SalirActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_SalirActionPerformed

    private void j2Button2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_j2Button2ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_j2Button2ActionPerformed

    private void jMenu1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenu1ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jMenu1ActionPerformed

    private void jMenu1MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jMenu1MouseClicked
        // TODO add your handling code here:
    }//GEN-LAST:event_jMenu1MouseClicked

    private void j2Button1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_j2Button1ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_j2Button1ActionPerformed

    private void j2Button9ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_j2Button9ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_j2Button9ActionPerformed

    private void jMenuAvionActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuAvionActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jMenuAvionActionPerformed

    /**
    * @param args the command line arguments
    */
    public static void main(String args[]) {
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("WindowsXP".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException | InstantiationException | IllegalAccessException | javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(MainFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new principal().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    public javax.swing.JMenuItem Salir;
    public j2Button.j2Button j2Button1;
    public j2Button.j2Button j2Button2;
    public j2Button.j2Button j2Button3;
    public j2Button.j2Button j2Button4;
    public j2Button.j2Button j2Button5;
    public j2Button.j2Button j2Button6;
    public j2Button.j2Button j2Button8;
    public j2Button.j2Button j2Button9;
    public javax.swing.JDesktopPane jDesktopPane;
    private javax.swing.JMenu jMenu1;
    public javax.swing.JMenu jMenu2;
    private javax.swing.JMenu jMenu9;
    public javax.swing.JMenuItem jMenuAlumno;
    public javax.swing.JMenuItem jMenuAvion;
    private javax.swing.JMenuBar jMenuBar1;
    public javax.swing.JMenuItem jMenuDocentePiloto;
    public javax.swing.JMenu jMenuEscuela;
    public javax.swing.JMenuItem jMenuHelice;
    public javax.swing.JMenuItem jMenuItemGrupo;
    public javax.swing.JMenuItem jMenuItemMalla;
    public javax.swing.JMenuItem jMenuItemNotas;
    public javax.swing.JMenuItem jMenuMateria;
    public javax.swing.JMenuItem jMenuMotor;
    public javax.swing.JMenuItem jMenuUsuario;
    private javax.swing.JToolBar jToolBar1;
    public javax.swing.JMenuItem reporteFlexible;
    // End of variables declaration//GEN-END:variables

}
